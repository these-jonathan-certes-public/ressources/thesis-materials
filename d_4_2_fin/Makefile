
#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

PROOF = proof.v
DEPS  = axioms.v decide.v transducer.v
LIBPATH = ../4_5_2_psl2coq/lib/

#===============================================================================

BASENAME = $(basename ${PROOF})
DEPSNAME = $(basename ${DEPS})

all: $(addsuffix .vos, ${DEPSNAME}) $(addsuffix .vos, ${BASENAME})
	@echo "The proof has run correctly."


%.vos : %.v
	make -C "${LIBPATH}"
	coqc -verbose -Q "${LIBPATH}" Thesis -Q . Dependencies $^

%.spec.smv:
	echo "MODULE main"           > $@
	cat ../d_4_2_mc_moniteur/$@ >> $@

decide.v: decide.spec.smv
	cp header.v $@
	../4_5_2_psl2coq/psl2coq.py --filter "decide_wait_secure" --properties $< >> $@

transducer.v: transducer.spec.smv
	tclsh ../b_2_5_nusmvall/nusmvAll.tcl             \
	  --verbose                                      \
	  --vcd                                          \
	  --verilog "../c_1_1_transducteur/transducer.v" \
	  --spec "$<"
	cp header.v $@
	../4_5_2_psl2coq/psl2coq.py --properties $< >> $@


clean:
	$(RM) $(addsuffix .glob, ${DEPSNAME})
	$(RM) $(addsuffix .vo  , ${DEPSNAME})
	$(RM) $(addsuffix .vok , ${DEPSNAME})
	$(RM) $(addsuffix .vos , ${DEPSNAME})
	$(RM) $(addsuffix .aux , $(addprefix ., ${DEPSNAME}))
	#
	$(RM) $(addsuffix .glob, ${BASENAME})
	$(RM) $(addsuffix .vo  , ${BASENAME})
	$(RM) $(addsuffix .vok , ${BASENAME})
	$(RM) $(addsuffix .vos , ${BASENAME})
	$(RM) $(addsuffix .aux , $(addprefix ., ${BASENAME}))
	#
	$(RM) .lia.cache
	#
	$(RM) decide.v decide.spec.smv transducer.v
	#
	$(RM) -r ltl_tmp
	$(RM) soundness.smv soundness.v

clean_lib:
	make -C "${LIBPATH}" clean

