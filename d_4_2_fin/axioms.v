(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

(******************************************************************************)
(******************************************************************************)

(**
 * Definition of all the predicates:
 *)

Axiom clk : CV0 Bit (eq O).
Axiom deduced_PC_eq_CRmin : CV0 Bit (eq O).
Axiom deduced_PC_eq_CRmax : CV0 Bit (eq O).

Axiom i_eq_end_addr : CV0 Bit (eq O).
Axiom done          : CV0 Bit (eq O).

Definition END_ADDR := uwconst(0x3ff, 10).

(******************************************************************************)

(**
 * Model of the environment:
 *)

(**
 * The clock in the FPGA is always active.
 *)
Axiom clk_is_on:
  ∀ π : Path, π ⊨ G('clk).

(**
 * We selected the end address so that it is just before we branch to CR_min.
 *)
Axiom end_addr_PC_CRmin:
  ∀ π : Path, π ⊨ G( X('i_eq_end_addr) --> ('deduced_PC_eq_CRmin) ).

