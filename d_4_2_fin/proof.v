(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.decide.
Require Import Dependencies.transducer.

(******************************************************************************)
(******************************************************************************)

Theorem decide_wait_secure_proof:
∀ π : Path, π ⊨ G(
  (
    ('done) ∧ ('deduced_PC_eq_CRmax)
  )
  -->
  (
    X(negP ('done))
  )
).
Proof.
  intros π.
  pose proof (_decide_wait_secure_         π) as H0.
  pose proof (clk_is_on                    π) as H1.
  pose proof (end_addr_PC_CRmin            π) as H2.
  pose proof (_transducer_CRmin_neq_CRmax_ π) as H3.
  rewrite Thesis.toolbox.simpl_globally    in H0, H2.
  rewrite Thesis.toolbox.globally_to_state in H1, H3.
  rewrite Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (H0 s) as H0.
  pose proof (H1 s) as H1.
  pose proof (H2 s) as H2.
  pose proof (H3 s) as H3.

  (* we use H0 and discriminate all impossible cases: *)
  simpl.
  simpl in H0.
  destruct H0 as [H0 | H0].
  - destruct H0 as [H0 | H0].
    + destruct H0 as [H0 | H0].
      * destruct H0 as [H0 | H0].

        (* discriminate !clk *)
        -- simpl in H1.
          rewrite H1 in H0.
          discriminate.

        (* X(!done) *)
        -- left; left.
          exact H0.

      (* !deduced_PC_eq_CRmax *)
      * left; right.
        exact H0.

    + destruct H2 as [H2 | H2].

      (* discriminate X(i = END_ADDR) *)
      * simpl in H2.
        rewrite H2 in H0.
        discriminate.

      * simpl in H3.
        destruct H3 as [H3 | H3].

        (* discriminate deduced_PC_eq_CRmin *)
        -- simpl in H2.
          rewrite H2 in H3.
          discriminate.

        (* !deduced_PC_eq_CRmax *)
        -- left; right.
          exact H3.

  (* X(!done) *)
  - right.
    exact H0.
Qed.
