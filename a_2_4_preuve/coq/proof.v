(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import LTL.

(**
 * \brief
 *  Definition of an operator to call function \ref impP, the implication
 *  between two \ref PathF.
 *)
Infix "-->" := impP (at level 20).

(**
 * \brief
 *  Proof that applying negP twice on a formula is like not applying at all.
 *)
Lemma negP_invol :
  forall p : PathF,
    negP (negP p) = p.
Proof.
  intros p.

  induction p;
  (* we apply these two tactics on each branch of the induction: *)
  (simpl; intuition).
  (* this application removes the two first branches. Then: *)
  - rewrite IHp1.
    rewrite IHp2.
    reflexivity.
  - rewrite IHp1; rewrite IHp2; reflexivity.
  - rewrite IHp1; rewrite IHp2; reflexivity.
  - rewrite IHp1; rewrite IHp2; reflexivity.
  - rewrite IHp; reflexivity.
Qed.

(**
 * \brief
 *  At a given State, if a formula is verified, then its negation is falsified.
 *)
Lemma negP_forms_state:
  forall π: Path,
    forall s: State,
      forall p: PathF,
        (
           (path_up π s ⊨ p)
        ->
          ~(path_up π s ⊨ negP p)
        ).
Proof.
  intros π.
  intros s.
  intros p.
  intros H1.

  unfold not.
  intros H2.
  apply   negP_forms in H2.
  rewrite negP_invol in H2.
  unfold not in H2.
  apply H2 in H1.
  case H1.
Qed.

(*****************************************************************************)

Section proof_example.

Variable (π: Path).
Variable (a b: PathF).

Hypothesis ф₁:
  π ⊨ F(a).

Hypothesis ф₂:
  π ⊨ G(a --> F(b)).

Theorem ф:
  π ⊨ F(a ∧ F(b)).
Proof.
  simpl in ф₁.
  simpl.

  (* according to ф₁, I know there exists a State where "a" is top, so I pose
     "s: State" and I remove ∃i from ф₁: *)
  destruct ф₁ as (s & H).
  (* I think that "i0", state in ф where "a" is top, is my hypothesis "s"; so I
     instantiate ∃i0 = s: *)
  exists s.

  split.    (* F() in ф: "a" is top at one state AND bot at all states before *)

  (* state where "a" is top *)
  - split.  (* proving "a" and "F(b)" in F() in ф *)

    (* proving "a" in F() in ф *)
    * destruct H as (H1 & H2); exact H1.  (* "F(a)" is in ф₁ *)

    (* proving "F(b)" in F() in ф *)
    * simpl in ф₂.               (* we need ф₂ to prove "F(b)" *)
      destruct ф₂ as [H1 | H2].  (* F() in ф₂, "b" is top at one state AND bot
                                    all states before *)

      + pose (H2 := H1 s).        (* get to state "s", where "a" is top in ф₁ *)
        destruct H2 as [H2 | H3]. (* globally either "¬a" or "F(b)" *)

        (* case of "¬a" in ф₂: "a" cannot be both top in ф₁ and bot here *)
        --  destruct H as (H3 & H4).
            apply negP_forms_state in H3.
            apply H3 in H2.
            case H2.

        (* case of "F(b)" in ф₂ *)
        -- exact H3.

      (* every state before having "b" in ф₂, an hypothesis is false *)
      + destruct H2 as (H2 & H3).
        destruct H3 as (H3 & H4).
        apply state_top in H3.
        case H3.

  (* every state before having "a", I have bot *)
  - intros.
    apply state_bot.
Qed.

Compute ф.

End proof_example.
