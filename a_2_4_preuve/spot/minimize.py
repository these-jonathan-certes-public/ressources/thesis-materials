#!/usr/bin/python3

#
# Copyright (C) 2022 Jonathan Certes
# jonathan.certes@online.fr
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys
sys.path.append("/usr/local/lib/python3.7/site-packages/")
import spot

# examples of specifications to imply:
theSpecList = [
  "F(a & F(b))", # implied
  "G(a & F(b))", # not implied
]

# a formula that is too large (likely to crash):
import string
theString = "a"
for i in range(0, 999):
  for c in list(string.ascii_lowercase):
    theString = "X(" + theString + " & " + c + ")"
theSpecList.append(theString)

# see if our properties are sufficient to imply the specifications:
theProperties = "F(a) & G(a -> F(b))"
for theSpec in theSpecList:
  theString  = theProperties + " -> " + theSpec
  theFormula = spot.formula(theString)
  spot.formula.translate(theFormula)  # minimize
  print("Formula " + theString + " is equivalent to " + str(theFormula))

