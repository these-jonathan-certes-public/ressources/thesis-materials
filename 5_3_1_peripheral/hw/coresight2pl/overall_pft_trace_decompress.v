
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* verilator lint_off UNUSED */

/**
 * \brief
 *  Overall PFT trace decompression flow.
 *
 * \details
 *  See ARM Coresight PFT architecture specification, Appendix B: Trace
 *  Decompresser Operation.
 */
module overall_pft_trace_decompress
#(
  parameter p_none      = 4'b0000,
  parameter p_isync     = 4'b0001,
  parameter p_atom      = 4'b0010,
  parameter p_branch    = 4'b0011,
  parameter p_waypoint  = 4'b0100,
  parameter p_trigger   = 4'b0101,
  parameter p_contextid = 4'b0110,
  parameter p_vmid      = 4'b0111,
  parameter p_timestamp = 4'b1000,
  parameter p_except    = 4'b1001,
  parameter p_ignore    = 4'b1010
) (
  input nrst, //!< low level active reset
  input clk,  //!< clock

  output async_enable,  //!< consider boundary alignment packet headers
  input  async_busy,    //!< boundary alignment packet is ongoing
  input  async_ready,   //!< boundary alignment packet has terminated
  //
  output      packets_enable,      //!< enables the packets decompresser
  input [3:0] decompressed_packet, //!< indicates which packet is currently decompressing
  input       packet_ready,        //!< indicates that the packet is decompressed

  output reg decompress_error     //!< when I-sync is periodic and there is a
                                  //!  mismatch between current and last states
);

  /****************************************************************************/

  /**
   * \brief
   *  Changes state to either <tt> state_continue_scan </tt> or <tt>
   *  state_identify_packet </tt> depening on <tt> r_discard_packets </tt>.
   */
  task backToIdentifyPacket();
  begin
    if ( r_discard_packets == 1'b1 ) begin
      state <= state_continue_scan;
    end
    else begin
      state <= state_identify_packet;
    end
  end
  endtask

  /****************************************************************************/

  // to store and check for errors in the decompression:
  reg  [1:0] r_currentstate_isetstate;
  reg [31:0] r_currentstate_address;
  reg        r_currentstate_ns;
  reg [31:0] r_currentstate_contextid;

  // to discard non-isync packets:
  reg r_discard_packets;

  localparam [2:0] state_scan_trace_stream = 3'b000;
  localparam [2:0] state_continue_scan     = 3'b001;
  localparam [2:0] state_identify_packet   = 3'b010;
  localparam [2:0] state_process_async     = 3'b011;
  localparam [2:0] state_process_packets   = 3'b110;
  localparam [2:0] state_error             = 3'b111;
  reg [2:0] state;

  initial begin
    state = state_scan_trace_stream;
    r_discard_packets = 1'b1;
    //
    decompress_error = 1'b0;
  end

  always @( posedge(clk) ) begin
    if ( nrst == 1'b0 ) begin
      state <= state_scan_trace_stream;
      r_discard_packets <= 1'b1;
      //
      decompress_error <= 1'b0;
    end
    else begin
      case ( state )
        state_scan_trace_stream : begin
          if ( async_ready ) begin
            state <= state_continue_scan;
          end
          else begin
            state <= state_scan_trace_stream;
          end
        end

        state_continue_scan : begin
          r_discard_packets <= 1'b1;
               if ( async_busy                    ) state <= state_process_async;
          else if ( decompressed_packet != p_none ) state <= state_process_packets;
          else                                      state <= state_continue_scan;
        end

        state_identify_packet : begin
          r_discard_packets <= 1'b0;
               if ( async_busy                    ) state <= state_process_async;
          else if ( decompressed_packet != p_none ) state <= state_process_packets;
          else                                      state <= state_identify_packet;
        end

        state_process_async : begin
          if ( async_ready ) begin
            backToIdentifyPacket();
          end
          else begin
            state <= state_process_async;
          end
        end

        state_process_packets : begin
          if ( packet_ready ) begin
            state <= state_identify_packet;
          end
          else begin
            state <= state_process_packets;
          end
        end

        state_error : begin
          state <= state_error; // deadlock
          //
          decompress_error <= 1'b1;
        end

        default : begin
          state <= state_scan_trace_stream;
        end
      endcase
    end
  end

  assign async_enable = (
       (state == state_scan_trace_stream)
    || (state == state_continue_scan)
    || (state == state_identify_packet)
    || (state == state_process_async)
  );

  assign packets_enable = (
       (state == state_continue_scan)
    || (state == state_identify_packet)
    || (state == state_process_packets)
  );

endmodule

