
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

`include "parallel2bram.v"

/* verilator lint_off UNUSED */

module logic_analyzer
(
  input mclk, //!< master clock
  input nrst, //!< active-low reset
  input mrst, //!< active-high reset

  // data to monitor:
  input        sw_arvalid,
  input [13:0] sw_araddr,
  input        sw_arready,
  input        kr_axi_arvalid,
  input        xs_axi_arvalid,
  input        xs_axi_awvalid,
  input        xs_axi_wvalid,
  //
  input  [7:0] trace_data,
  input        trace_ctl,

  // infered BRAM, to be connected to an AXI BRAM controller:
  input  [11:0] addr,   //!< address (2 LSB left unused)
  input  clk,           //!< clock
  input  [31:0] wrdata, //!< data to write to memory
  output [31:0] rddata, //!< data read from memory
  input         rst,    //!< active-high reset
  input  [3:0]  we,     //!< write-enable bits, one for each Byte

  output trigger
);

  wire w_parallel2bram_nrst;
  assign w_parallel2bram_nrst = !( !nrst || mrst );

  /** connexions between the parallel2bram and the bram_dual **/
  wire [11:2] w_bram_addr;
  wire        w_bram_clk;
  wire [31:0] w_bram_wrdata;
  wire [31:0] w_bram_rddata;
  wire        w_bram_rst;
  wire  [3:0] w_bram_we;

  // data to monitor:
  wire [31:0] w_data;
  assign w_data = {
    sw_arvalid,
    sw_araddr,
    sw_arready,
    kr_axi_arvalid,
    xs_axi_arvalid,
    xs_axi_awvalid,
    xs_axi_wvalid,
    3'b000,
    trace_data,
    trace_ctl
  };

  // trigger when one signal is set:
  wire w_trigger;
  assign w_trigger = ( sw_arvalid || kr_axi_arvalid || xs_axi_arvalid );
  assign trigger = w_trigger;

  parallel2bram #(
    .ADDR_WIDTH(10)
  ) i_parallel2bram (
    .clk(  mclk                 ),
    .nrst( w_parallel2bram_nrst ),
    //
    .data(        w_data        ),
    .trigger(     w_trigger     ),
    //
    .bram_addr(   w_bram_addr   ),
    .bram_clk(    w_bram_clk    ),
    .bram_wrdata( w_bram_wrdata ),
    .bram_rddata( w_bram_rddata ),
    .bram_rst(    w_bram_rst    ),
    .bram_we(     w_bram_we     )
  );

  // BRAM_TDP_MACRO: True Dual Port RAM
  //                 Artix-7
  // Xilinx HDL Language Template, version 2019.2

  //////////////////////////////////////////////////////////////////////////
  // DATA_WIDTH_A/B | BRAM_SIZE | RAM Depth | ADDRA/B Width | WEA/B Width //
  // ===============|===========|===========|===============|=============//
  //     19-36      |  "36Kb"   |    1024   |    10-bit     |    4-bit    //
  //     10-18      |  "36Kb"   |    2048   |    11-bit     |    2-bit    //
  //     10-18      |  "18Kb"   |    1024   |    10-bit     |    2-bit    //
  //      5-9       |  "36Kb"   |    4096   |    12-bit     |    1-bit    //
  //      5-9       |  "18Kb"   |    2048   |    11-bit     |    1-bit    //
  //      3-4       |  "36Kb"   |    8192   |    13-bit     |    1-bit    //
  //      3-4       |  "18Kb"   |    4096   |    12-bit     |    1-bit    //
  //        2       |  "36Kb"   |   16384   |    14-bit     |    1-bit    //
  //        2       |  "18Kb"   |    8192   |    13-bit     |    1-bit    //
  //        1       |  "36Kb"   |   32768   |    15-bit     |    1-bit    //
  //        1       |  "18Kb"   |   16384   |    14-bit     |    1-bit    //
  //////////////////////////////////////////////////////////////////////////

  BRAM_TDP_MACRO #(
    .BRAM_SIZE("36Kb"), // Target BRAM: "18Kb" or "36Kb"
    .DEVICE("7SERIES"), // Target device: "7SERIES"
    .INIT_FILE ("NONE"),
    .SIM_COLLISION_CHECK ("WARNING_ONLY"), // Collision check enable
    //
    .DOA_REG(0),            // Optional port A output register (0 or 1)
    .INIT_A(32'h00000000),  // Initial values on port A output port
    .READ_WIDTH_A (32),     // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
    .SRVAL_A(32'h00000000), // Set/Reset value for port A output
    .WRITE_MODE_A("WRITE_FIRST"), // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
    .WRITE_WIDTH_A(32),     // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
    //
    .DOB_REG(0),            // Optional port B output register (0 or 1)
    .INIT_B(32'h00000000),  // Initial values on port B output port
    .READ_WIDTH_B (32),     // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
    .SRVAL_B(32'h00000000), // Set/Reset value for port B output
    .WRITE_MODE_B("WRITE_FIRST"), // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
    .WRITE_WIDTH_B(32)      // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
  ) i_bram_dual (
    /** connexions to the parallel2bram **/
    .DOA(w_bram_rddata), // Output port-A data, width defined by READ_WIDTH_A parameter
    .ADDRA(w_bram_addr), // Input port-A address, width defined by Port A depth
    .CLKA(w_bram_clk),   // 1-bit input port-A clock
    .DIA(w_bram_wrdata), // Input port-A data, width defined by WRITE_WIDTH_A parameter
    .ENA(1'b1),          // 1-bit input port-A enable
    .REGCEA(1'b0),       // 1-bit input port-A output register enable
    .RSTA(w_bram_rst),   // 1-bit input port-A reset
    .WEA(w_bram_we),     // Input port-A write enable, width defined by Port A depth
    /** connexions to the AXI BRAM controller **/
    .DOB(rddata),       // Output port-B data, width defined by READ_WIDTH_B parameter
    .ADDRB(addr[11:2]), // Input port-B address, width defined by Port B depth
    .CLKB(clk),         // 1-bit input port-B clock
    .DIB(wrdata),       // Input port-B data, width defined by WRITE_WIDTH_B parameter
    .ENB(1'b1),         // 1-bit input port-B enable
    .REGCEB(1'b0),      // 1-bit input port-B output register enable
    .RSTB(rst),         // 1-bit input port-B reset
    .WEB(we)            // Input port-B write enable, width defined by Port B depth
  );

  // End of BRAM_TDP_MACRO_inst instantiation

endmodule
