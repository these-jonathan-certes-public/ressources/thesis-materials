
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* verilator lint_off UNUSED */

module decode
(
  input        nrst,
  input        clk,
  input [31:0] data
);

  wire        sw_arvalid;
  wire [13:0] sw_araddr;
  wire        sw_arready;
  wire        kr_axi_arvalid;
  wire        xs_axi_arvalid;
  wire        xs_axi_awvalid;
  wire        xs_axi_wvalid;
  //
  wire  [7:0] trace_data;
  wire        trace_ctl;

  assign {
    sw_arvalid,
    sw_araddr,
    sw_arready,
    kr_axi_arvalid,
    xs_axi_arvalid,
    xs_axi_awvalid,
    xs_axi_wvalid
  } = data[31:12];

  assign {
    trace_data,
    trace_ctl
  } = data[8:0];

/******************************************************************************/
/* DEBUG: adding CoreSight packet decompresser and decoder to the testbench so
 * that we can observe the destination addresses.
 */

  // trick the FSM so that it acts as it already received an A-sync packet (in
  // real life, it happens before the logic analyser records):
  initial begin
    `define ASYNC_DONE
    `ifdef ASYNC_DONE
      i_coresight2pl.i_async_decompress.state =
      i_coresight2pl.i_async_decompress.state_end;
      //
      i_coresight2pl.i_async_decompress.count = 1;
    `else
      i_coresight2pl.i_async_decompress.state =
      i_coresight2pl.i_async_decompress.state_header;
      //
      i_coresight2pl.i_async_decompress.count = 4;
    `endif
    i_coresight2pl.i_overall_pft_trace_decompress.state =
    i_coresight2pl.i_overall_pft_trace_decompress.state_continue_scan;
  end

  /* verilator lint_off PINMISSING */
  coresight2pl i_coresight2pl (
    .nrst(           nrst           ),
    .clk(            clk            ),
    .kr_axi_arvalid( kr_axi_arvalid ),
    .xs_axi_arvalid( xs_axi_arvalid ),
    .xs_axi_awvalid( xs_axi_awvalid ),
    .xs_axi_wvalid(  xs_axi_wvalid  ),
    .trace_data(     trace_data     ),
    .trace_ctl(      trace_ctl      )
  );
  /* verilator lint_on  PINMISSING */

endmodule

/* verilator lint_on UNUSED */
