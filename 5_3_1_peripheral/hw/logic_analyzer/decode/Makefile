
#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

TESTBENCH = main.cc
TOP       = decode
ARGS      = ./data.gdb

HDL = decode.v                                          \
      ../../vrased/atomicity.v                          \
      ../../vrased/key_access_control.v                 \
      ../../vrased/exclusive_stack.v                    \
      ../../coresight2pl/async_decompress.v             \
      ../../coresight2pl/overall_pft_trace_decompress.v \
      ../../../../c_1_1_transducteur/transducer.v       \
      ../../../../c_2_1_decodeur/packets_decoder_core.v \
      ../../../../c_2_1_decodeur/packets_decoder.v      \
      ../../../../4_4_0_model/packets_decompresser.v    \
      ../../coresight2pl/coresight2pl.v

#==============================================================================

VERILATOR_MK  = $(addprefix obj_dir/V, $(addsuffix .mk, ${TOP} ))
VERILATOR_BIN = $(addprefix obj_dir/V, ${TOP} )


all : gtkwaverc.tcl output.vcd
	gtkwave --script=gtkwaverc.tcl output.vcd


# (3) run:
output.vcd: ${VERILATOR_BIN} ${ARGS}
	./${VERILATOR_BIN} ${ARGS}

# (2) make with verilator:
${VERILATOR_BIN} : ${VERILATOR_MK} ${TESTBENCH}
	make --directory=$(dir ${VERILATOR_MK}) --file=$(notdir ${VERILATOR_MK})

# (1) translate with verilator (ignore module duplicates because of includes):
${VERILATOR_MK} : ${HDL} ${TESTBENCH}
	verilator -Wall --trace --relative-includes -Wno-MODDUP \
	  --sc ${HDL} --top-module ${TOP} --exe ${TESTBENCH}


clean :
	$(RM) -r obj_dir
	$(RM) output.vcd

