
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __MAIN_H
  #define __MAIN_H

  /**
   * \brief
   *  Data format as given by the logic analyser.
   */
  struct __attribute__((packed)) test_format {
    union {
      struct {
        uint32_t trace_data       :8;
        uint32_t trace_ctl        :1;
        uint32_t kr_axi_arvalid   :1;
        uint32_t xs_axi_arvalid   :1;
        uint32_t xs_axi_awvalid   :1;
        uint32_t xs_axi_wvalid    :1;
        uint32_t reset            :1;
        uint32_t decompress_error :1;
        uint32_t trace_clk_out    :1;
        uint32_t nothing          :16;
      };
      uint32_t raw;
    };
  };

#endif

