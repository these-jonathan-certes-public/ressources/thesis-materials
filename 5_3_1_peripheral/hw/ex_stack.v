
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \brief
 *  Stores the exclusive stack in a read and write mode BRAM.
 */
module ex_stack
#(
  parameter FILE_INIT  = "NONE" //!< path to a file with the hex content
) (
  input  [11:0] addr,   //!< address where to access memory
  input  clk,           //!< clock
  input  [31:0] wrdata, //!< data to write to memory
  output [31:0] rddata, //!< data read from memory
  input  rst,           //!< synchronous reset
  input  [3:0]  we      //!< write-enable bits, one for each Byte
);

  // BRAM_SINGLE_MACRO: Single Port RAM
  //                    Artix-7
  // Xilinx HDL Language Template, version 2019.2

  /////////////////////////////////////////////////////////////////////
  //  READ_WIDTH | BRAM_SIZE | READ Depth  | ADDR Width |            //
  // WRITE_WIDTH |           | WRITE Depth |            |  WE Width  //
  // ============|===========|=============|============|============//
  //    37-72    |  "36Kb"   |      512    |    9-bit   |    8-bit   //
  //    19-36    |  "36Kb"   |     1024    |   10-bit   |    4-bit   //
  //    19-36    |  "18Kb"   |      512    |    9-bit   |    4-bit   //
  //    10-18    |  "36Kb"   |     2048    |   11-bit   |    2-bit   //
  //    10-18    |  "18Kb"   |     1024    |   10-bit   |    2-bit   //
  //     5-9     |  "36Kb"   |     4096    |   12-bit   |    1-bit   //
  //     5-9     |  "18Kb"   |     2048    |   11-bit   |    1-bit   //
  //     3-4     |  "36Kb"   |     8192    |   13-bit   |    1-bit   //
  //     3-4     |  "18Kb"   |     4096    |   12-bit   |    1-bit   //
  //       2     |  "36Kb"   |    16384    |   14-bit   |    1-bit   //
  //       2     |  "18Kb"   |     8192    |   13-bit   |    1-bit   //
  //       1     |  "36Kb"   |    32768    |   15-bit   |    1-bit   //
  //       1     |  "18Kb"   |    16384    |   14-bit   |    1-bit   //
  /////////////////////////////////////////////////////////////////////

  BRAM_SINGLE_MACRO #(
     .BRAM_SIZE("36Kb"),        // Target BRAM, "18Kb" or "36Kb"
     .DEVICE("7SERIES"),        // Target Device: "7SERIES"
     .DO_REG(0),                // Optional output register (0 or 1)
     .INIT(36'h000000000),      // Initial values on output port
     .INIT_FILE(FILE_INIT),
     .WRITE_WIDTH(32),          // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .READ_WIDTH(32),           // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .SRVAL(36'h000000000),     // Set/Reset value for port output
     .WRITE_MODE("WRITE_FIRST") // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
  ) i_bram_single (
     .DO(rddata),       // Output data, width defined by READ_WIDTH parameter
     .ADDR(addr[11:2]), // Input address, width defined by read/write port depth
     .CLK(clk),         // 1-bit input clock
     .DI(wrdata),       // Input data port, width defined by WRITE_WIDTH parameter
     .EN(1'b1),         // 1-bit input RAM enable
     .REGCE(1'b0),      // 1-bit input output register enable
     .RST(rst),         // 1-bit input reset
     .WE(we)            // Input write enable, width defined by write port depth
  );

  // End of BRAM_SINGLE_MACRO_inst instantiation

endmodule
