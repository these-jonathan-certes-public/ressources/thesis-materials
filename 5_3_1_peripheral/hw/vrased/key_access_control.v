
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \brief
 *  Modified version of VRASED automaton for key access control, uses signals
 *  from the transducer and AXI-Lite slaves.
 */
module key_access_control
(
  input clk,
  input kr_axi_arvalid,
  input deduced_PC_in_CR,
  //
  output reset
);

  reg state;
  localparam s_Reset = 1'b0;
  localparam s_Run   = 1'b1;

  always @( posedge(clk) )
  begin
    case( state )
      s_Reset:
        if ( !deduced_PC_in_CR && !kr_axi_arvalid ) begin
          state   <= s_Run;
        end
        else begin
          state   <= s_Reset;
        end

      s_Run:
        if ( !deduced_PC_in_CR && kr_axi_arvalid ) begin
          state <= s_Reset;
        end
        else begin
          state <= s_Run;
        end
    endcase
  end

  /****************************************************************************/

  assign reset = (
      (state == s_Reset)
  ||
    ( (state == s_Run) && !deduced_PC_in_CR && kr_axi_arvalid )
  );

endmodule

