
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \brief
 *  Modified version of VRASED automaton for exclusive stack, uses signals from
 *  the transducer and AXI-Lite slaves.
 *
 * \todo
 *  Detect that W_en when PC is in CR and that we write outside XS or MR.
 */
module exclusive_stack
(
  input clk,
  input xs_axi_arvalid,
  input xs_axi_awvalid,
  input xs_axi_wvalid,
  // input mr_axi_awvalid,
  // input mr_axi_wvalid,
  input deduced_PC_in_CR,
  //
  output reset
);

  reg state;
  localparam s_Reset = 1'b0;
  localparam s_Run   = 1'b1;

  always @( posedge(clk) )
  begin
    case( state )
      s_Reset:
        if ( !deduced_PC_in_CR && !xs_axi_arvalid
                               && !xs_axi_awvalid
                               && !xs_axi_wvalid  ) begin
          state <= s_Run;
        end
        else begin
          state <= s_Reset;
        end

      s_Run:
        if (
            (!deduced_PC_in_CR && (  xs_axi_arvalid
                                  || xs_axi_awvalid
                                  || xs_axi_wvalid  )
            // ) || (
            //   deduced_PC_in_CR && ( /* W_en */ )
            //                    && !(xs_axi_awvalid || xs_axi_wvalid)
            //                    && !(mr_axi_awvalid || mr_axi_wvalid)
            )
           ) begin
          state <= s_Reset;
        end
        else begin
          state <= s_Run;
        end
    endcase
  end

  /****************************************************************************/

  assign reset =
    (state == s_Reset)
      ||
    (state == s_Run) && (
      (!deduced_PC_in_CR && (  xs_axi_arvalid
                            || xs_axi_awvalid
                            || xs_axi_wvalid  )
      // ) || (
      //   deduced_PC_in_CR && ( /* W_en */ )
      //                    && !(xs_axi_awvalid || xs_axi_wvalid)
      //                    && !(mr_axi_awvalid || mr_axi_wvalid)
      )
    );

endmodule
