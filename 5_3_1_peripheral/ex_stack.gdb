
#
# Copyright (C) 2021 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

set architecture armv7
set pagination off

# send commands to the remote monitor: openocd
monitor reset halt
monitor pld load 0 ./hw/design_0_wrapper.bit
monitor gdb_sync

# now controlling gdb:
file  sw/fsbl/main.elf
load  sw/fsbl/main.elf
break _boot
jump  _boot

# add a breakpoint in each entry of the exception vectors table:
break *0x00
break *0x04
break *0x08
break *0x0c
break *0x10
break *0x14
break *0x18
break *0x1c

# run until we reach Loop (ps7_init terminated properly) or FsblHookFallback (an
# error occurred, case of qemu since all devices cannot be initialized):
break Loop
break FsblHookFallback
continue
continue
delete _boot Loop FsblHookFallback

# load the attesting software (has no effect if the button is not pressed):
load sw/app/sw_att.elf

# load the application:
file sw/app/main.elf
load sw/app/main.elf
break _start
jump  _start

break main
continue

layout reg
layout next

# execution of a read/write in the exclusive stack (AXI slave):
break __breakpoint_select
continue
set theSelect = 0

break stack_one
continue
x /wx &__ex_stack_low

break stack_two
continue
x /wx &__ex_stack_low

break __my_breakpoint
continue

