
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* dedicated memories in AXI-Lite slaves: must match the hardware */
KEY_ORIGIN = 0x40000000;
KEY_LENGTH = 0x1000;

SW_ATT_ORIGIN = 0x42000000;
SW_ATT_LENGTH = 0x4000;

EX_STACK_ORIGIN = 0x44000000;
EX_STACK_LENGTH = 0x1000;

LOGICA_ORIGIN = 0x46000000;
LOGICA_LENGTH = 0x1000;

/* dedicated memories in the DDR: */
STACK_LENGTH       = 0x2000;
HEAP_LENGTH        = 0x2000;
CODE_LOADER_LENGTH = 0x20000;
UNDEF_STACK_LENGTH = 0x400;   /* used by xilinx functions that flush the cache */


/* Define Memories in the system */
MEMORY
{
  rom_key (r)       : ORIGIN = KEY_ORIGIN,      LENGTH = KEY_LENGTH
  rom_sw_att (rx)   : ORIGIN = SW_ATT_ORIGIN    LENGTH = SW_ATT_LENGTH
  ram_ex_stack (rw) : ORIGIN = EX_STACK_ORIGIN, LENGTH = EX_STACK_LENGTH
  rom_logica (rw)   : ORIGIN = LOGICA_ORIGIN    LENGTH = LOGICA_LENGTH

  ps7_ddr_MR        : ORIGIN = 0x100000,   LENGTH = 0x100000  /* challenge */
  ps7_ddr_AR        : ORIGIN = 0x200000,   LENGTH = 0x100000  /* attested region */
  ps7_ddr_0         : ORIGIN = 0x300000,   LENGTH = 0x1FF00000
  ps7_qspi_linear_0 : ORIGIN = 0xFC000000, LENGTH = 0x1000000
  ps7_ram_0         : ORIGIN = 0x0,        LENGTH = 0x30000
  ps7_ram_1         : ORIGIN = 0xFFFF0000, LENGTH = 0xFE00
}


/* Specify the default entry point to the program */
ENTRY(_start)


/* Define the sections, and where they are mapped in memory */
SECTIONS
{

  .startup : {
    . = ALIGN(4);
    *startup.o(.text)
  } > ps7_ddr_0


  .MR :{
     /* location where to read the challenge */
    PROVIDE(__challenge_low = .);
     *challenge.o(.data)
    PROVIDE(__challenge_top = .);
  } > ps7_ddr_MR

  .AR :{
    /* memory region to be attested by sw_att */
    PROVIDE(__attested_low = .);
    . = ALIGN(4);
     *attested.o(*)
    PROVIDE(__attested_top = .);
  } > ps7_ddr_AR


  /*****************************************************************************
   * Custom sections for key.
   * To be removed from the binary and placed into an AXI slave.
   */
  .key : {
    PROVIDE(__key_low = .);
    *sw_att/key.o(.rodata)
    PROVIDE(__key_top = .);
  } > rom_key


  /*****************************************************************************
   * Custom sections for sw_att.
   * To be removed from the binary and placed into an AXI slave.
   */

  /* Critical region is wrapped around with specific instructions */
  .sw_att : {
    PROVIDE(__sw_att_low = .);
    . = ALIGN(4);
    *sw_att/wrapper_start.o(.text)
    EXCLUDE_FILE(*sw_att/wrapper_end.o) *sw_att/*.o(*)
    PROVIDE(__sw_att_end_wrapper_low = .);
    *sw_att/wrapper_end.o(.text)
    PROVIDE(__sw_att_top = .);
    FILL(0x00); /* 0x00000000 is "ANDEQ r0, r0, r0" in ARM: equivalent to NOP */
  } > rom_sw_att


  /*****************************************************************************
  /* location of the exclusive stack */
  .ex_stack (NOLOAD) : {
    PROVIDE(__ex_stack_low = .);
    . = . + EX_STACK_LENGTH;
    PROVIDE(__ex_stack_top = .);
  } > ram_ex_stack


  /*****************************************************************************
  /* logic analyser */
  .logica : {
    PROVIDE(__logica_low = .);
    . = . + LOGICA_LENGTH;
    PROVIDE(__logica_top = .);
  } > rom_logica


  /*****************************************************************************
  /* memory region dedicated to code loading: where to copy code from UART and
   * to branch into.
   */
  .loaded_code : {
    PROVIDE(__loaded_code_low = .);
    . = ALIGN(4);
    *loaded.o(*)  /* careful when naming the files!! */
    . = __loaded_code_low + CODE_LOADER_LENGTH;
    PROVIDE(__loaded_code_top = .);
  } > ps7_ddr_0


  /*****************************************************************************
  /* Rest of the code, bare metal application:
   */
  .text : {
    /* rest of the code */
    . = ALIGN(4);
    *(.text)
    *(.data)
    *(.rodata)
    *(.rodata.*)
    *(.bss COMMON)
  } > ps7_ddr_0

  .ARM.exidx : {
     PROVIDE(__exidx_start = .);
    . = ALIGN(4);
     *(.ARM.exidx*)
     *(.gnu.linkonce.armexidix.*.*)
     PROVIDE(__exidx_end = .);
  } > ps7_ddr_0


  /*****************************************************************************
   * heap and stack
   */
  .heap (NOLOAD) : {
    PROVIDE(__heap_low  = .);
    . = . + HEAP_LENGTH;
    PROVIDE(__heap_top  = .);
  } > ps7_ddr_0

  .stack (NOLOAD) : {
    PROVIDE(__stack_low = .);
    PROVIDE(_stack_end  = .);  /* used by xilinx, identical to __stack_low */
    . = . + STACK_LENGTH;
    PROVIDE(__stack_top = .);
  } > ps7_ddr_0


  /*****************************************************************************
   * undef stack is used by xilinx functions that flush the cache
   */
  .undef_stack (NOLOAD) : {
    PROVIDE(_undef_stack_end = .);
    . = . + UNDEF_STACK_LENGTH;
    . = ALIGN(16);
    PROVIDE(__undef_stack    = .);
  } > ps7_ddr_0

}

