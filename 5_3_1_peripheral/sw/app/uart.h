
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file uart.h
 */

#ifndef __UART_H
  #define __UART_H

  #include <stdint.h>

  /**
   * \addtogroup uart
   *
   * \{
   */

  #define UART0_BASE 0xE0000000U //!< Base address of UART0 registers
  #define UART1_BASE 0xE0001000U //!< Base address of UART1 registers
  #define UART_SIZE  0x00000048U //!< Size of UART registers

  /**
   * \brief
   *  Definition of UART control register.
   */
  struct __attribute__((packed)) uart_cr {
    union {
      struct {
        uint32_t Rxrst    : 1;
        uint32_t Txrst    : 1;
        uint32_t Rx_en    : 1;
        uint32_t Rx_dis   : 1;
        uint32_t Tx_en    : 1;
        uint32_t Tx_dis   : 1;
        uint32_t Torst    : 1;
        uint32_t Startbrk : 1;
        uint32_t Stopbrk  : 1;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of UART mode register.
   */
  struct __attribute__((packed)) uart_mr {
    union {
      struct {
        uint32_t Clksel : 1;
        uint32_t Chrl   : 2;
        uint32_t Par    : 3;
        uint32_t Nbstop : 2;
        uint32_t Chmode : 2;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of UART read/write baud rate generator register.
   */
  struct __attribute__((packed)) uart_baudgen {
    union {
      struct {
        uint32_t Cd : 16;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of UART baud rate divider register.
   */
  struct __attribute__((packed)) uart_baud_rate_divider {
    union {
      struct {
        uint32_t Bdiv : 8;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of UART register.
   */
  struct __attribute__((packed)) uart {
    struct  uart_cr                 cr;                 // 0x00000000
    struct  uart_mr                 mr;                 // 0x00000004
    uint8_t dummy0[0x00000018 - 0x00000008];
    struct  uart_baudgen            baudgen;            // 0x00000018
    uint8_t dummy1[0x00000034 - 0x0000001C];
    struct  uart_baud_rate_divider  baud_rate_divider;  // 0x00000034
    uint8_t dummy2[0x00000048 - 0x00000038];
  };

  /**
   * \}
   */

  extern struct uart *uart0;
  extern struct uart *uart1;

/******************************************************************************/

  void uart_init( uint8_t theUartIndex );

#endif
