
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file  etb.c
 */

#include <stdio.h>
#include "etb.h"

/******************************************************************************/

/**
 * \defgroup etb  Embedded Trace Buffer (etb)
 *
 * \brief
 *  Library to manipulate CoreSight Embedded Trace Buffer (etb) under Xilinx
 *  Zynq-7000.
 *
 * \{
 */

struct etb * etb = (struct etb *)( ETB_BASE );

/******************************************************************************/

void etb_init(
  void
) {
  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 1;
  etb->lar             = 0x00000000;
}

/******************************************************************************/

void etb_stop(
  void
) {
  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 0;
  etb->lar             = 0x00000000;
}

/******************************************************************************/

void etb_dump(
  void
) {

  uint16_t i = 0x0000;
  uint32_t * theAddress = 0x00000000;

  for ( i = 0x0000 ; i < ETB_SIZE ; i=i+4 ) {
    theAddress = (uint32_t *)( ETB_BASE + i );
    printf( "(0x%08lx) %08lx %08lx %08lx %08lx\r\n",
      (uint32_t)( theAddress ),
      *(theAddress+3), *(theAddress+2), *(theAddress+1), *(theAddress)
    );
  }

}

/******************************************************************************/

void etb_readAllData(
  void
) {
  uint16_t i = 0x0000;
  uint8_t TraceCaptEn = etb->ctl.TraceCaptEn;

  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 0;
  while ( etb->ffsr.FtStopped == 0 ) {
    // wait
  }

  etb->rrp.value = 0x00000000;
  for ( i = 0x0000 ; i < 0x0400 ; i++ ) {
    if ( i % 4 == 0 ) {
      printf( "(0x%04x)", i );
    }
    printf( " %08lx", etb->rrd.value );
    // read pointer is automatically incremented
    if ( i % 4 == 3 ) {
      printf( "\r\n" );
    }
  }
  etb->rrp.value = 0x00000000;

  etb->ctl.TraceCaptEn = TraceCaptEn;
  etb->lar = 0x00000000;
}

/******************************************************************************/

void etb_writeAllData(
  void
) {
  uint16_t i = 0x0000;
  uint8_t TraceCaptEn = etb->ctl.TraceCaptEn;

  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 0;
  while ( etb->ffsr.FtStopped == 0 ) {
    // wait
  }

  etb->rwp.value = 0x00000000;
  for ( i = 0x0000 ; i < 0x0400 ; i++ ) {
    etb->rwd.value = 0x00000000;
    // write pointer is automatically incremented
    __asm__ __volatile__ ("isb" : : : "memory");
    __asm__ __volatile__ ("dsb" : : : "memory");
  }
  etb->rwp.value = 0x00000000;

  etb->ctl.TraceCaptEn = TraceCaptEn;
  etb->lar = 0x00000000;
}

/******************************************************************************/

/**
 * \}
 */

