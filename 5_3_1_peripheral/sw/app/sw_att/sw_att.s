
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

.text

.extern __challenge_low
.extern __key_low
.extern __ex_stack_top


  /* indirect branch to force coreSight to generate a branch packet */
  movw r5, #0
  movw r6, #0
  movw r7, #0
  movw r8, #0
  movw r9, #0
  add r8, pc, #4
  mov pc, r8
  nop
sw_att_wrapper_CRmin:

  /* adding a delay so that coresight sends its trace before we access the stack
   * and secret */
  nop
  nop
  nop
  nop
  nop
  add r7, r7, #1
  cmp r7, #5
  bls sw_att_wrapper_CRmin

  nop
  nop
  nop
  nop
  nop
  nop
  nop
  nop

  /* saving old stack context */
  mov r5, fp
  mov r6, sp
  /* switching to exclusive stack */
  movw r8, #0x1000
  movt r8, #0x4400
  mov fp, r8
  mov sp, r8
  /* push old stack context */
  push {lr}
  push {r5}

  push {r6}
  nop
  nop
  nop
  nop
  nop
  nop
  nop

  /* compute a XOR between the secret and the challenge */
  // read the challenge:
  movw r8, #0x0000
  movt r8, #0x0010
  ldr r7, [r8]
  // read the secret:
  movw r9, #0x0000
  movt r9, #0x4000
  ldr r9, [r9]
  //
  eor r7, r9    // XOR, we store the results in r7
  str r7, [r8]  // save the results at the address of the challenge

sw_att_wrapper_end:
  /* restoring stack context and old link */
  pop {r6}
  pop {r5}
  pop {lr}
  mov fp, r5
  mov sp, r6
  nop
  /* indirect branch to force coreSight to generate a branch packet */
  add r8, pc, #0
  mov pc, r8
sw_att_wrapper_CRmax:

  /* return */
  bx lr

//   // read the challenge and store it into r9:
//   mov r8, __challenge_low
//   ldr r9, [r8]
//
//   // read the secret and store it into r5:
//   movw r8, #0x0000
//   movt r8, #0x4000
//   ldr  r5, [r8]
