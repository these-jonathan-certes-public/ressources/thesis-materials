
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file  funnel.c
 */

#include "funnel.h"

/******************************************************************************/

/**
 * \defgroup funnel  Trace Funnel (funnel)
 *
 * \brief
 *  Library to manipulate CoreSight Trace Funnel (funnel) under Xilinx
 *  Zynq-7000.
 *
 * \{
 */

struct funnel * funnel = (struct funnel *)( FUNNEL_BASE );

/******************************************************************************/

void funnel_init(
  void
) {
  funnel->lar                  = 0xC5ACCE55;
  funnel->control.EnableSlave0 = 1; // PTM0
  funnel->control.EnableSlave1 = 1; // PTM1
  funnel->control.MinHoldTime  = 0x7;
  funnel->lar                  = 0x00000000;
}

/******************************************************************************/

/**
 * \}
 */

