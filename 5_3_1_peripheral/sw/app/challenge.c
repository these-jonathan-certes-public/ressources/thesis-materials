
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "challenge.h"

/*
 * dd bs=16 count=1 if=/dev/random of=/dev/stdout | xxd -g 1
 */
uint8_t ra_theChallenge[16] = {
  0x00, 0x00, 0xfe, 0xca, 0x00, 0x00, 0xfe, 0xca,
  0x00, 0x00, 0xfe, 0xca, 0x00, 0x00, 0xfe, 0xca
};
