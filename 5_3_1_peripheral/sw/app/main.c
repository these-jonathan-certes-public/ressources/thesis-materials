
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "xil_mmu.h"
#include "xil_cache.h"

#include "funnel.h"
#include "ptm.h"
#include "etb.h"
#include "mmu.h"
#include "uart.h"

/** symbols that are defined in the linker script. **/
extern int __sw_att_low;
extern int __sw_att_top;
extern int __challenge_low;
extern int __ex_stack_low;
extern int __loaded_code_low;
extern int __loaded_code_top;

/** symbols that are defined here in in-line assembly **/
extern int trace_start;
extern int trace_stop;

/******************************************************************************/

/**
 * \brief
 *  Reads and writes in the exclusive stack (AXI slave).
 */
void readwriteAxiSlaves(
  void
) {
  uint32_t *exclusiveStack = (uint32_t *)(&__ex_stack_low);

  *exclusiveStack = 0;
  __asm__ __volatile__(".global stack_one");
  __asm__ __volatile__("stack_one:");
  printf("exclusive stack: 0x%08lx\r\n", *exclusiveStack);

  *exclusiveStack = 0xcafe;
  __asm__ __volatile__(".global stack_two");
  __asm__ __volatile__("stack_two:");
  printf("exclusive stack: 0x%08lx\r\n", *exclusiveStack);

  return;
}

/******************************************************************************/

/**
 * \brief
 *  Function which execution must be traced.
 */
void myFunction(
  void
) {
  void* pc;
  uint8_t myVar  = 0;
  uint8_t myVar2 = 42;

  // beginning of the trace:
  __asm__ __volatile__(".global trace_start");
  __asm__ __volatile__("trace_start:");

  // branches:
  __asm__ __volatile__("mov %0, pc" : "=r"(pc));
  printf("pc = 0x%lx\r\n", (uint32_t)(pc));
  //
  for ( myVar = 0x00 ; myVar < 0x08 ; myVar++ ) {
    myVar2++;
  }
  //
  __asm__ __volatile__("mov %0, pc" : "=r"(pc));
  printf("pc = 0x%lx\r\n", (uint32_t)(pc));

   // indirect branch to generate a branch packet with the address:
   __asm__ __volatile__("mov pc, %0" : : "r"(&trace_stop));

  // end of the trace:
  __asm__ __volatile__(".global trace_stop");
  __asm__ __volatile__("trace_stop:");

  return;
}

/**
 * \brief
 *  Configures CoreSight to trace the execution of function \ref myFunction()
 *  and reads the content of the ETB.
 */
void traceWithCoresight(
  void
) {
  /********************/
  /** configure PTM: **/
  /********************/
  ptm_cpu0->lar = 0xC5ACCE55;
  ptm_cpu0->etmcr.ProgBit = 1;
  while ( ptm_cpu0->etmsr.ProgBit != 1 ) {
    // wait
  }
  // Main Control Register:
  ptm_cpu0->etmcr.BranchOutput   = 1;     // direct branch addresses are output
  ptm_cpu0->etmcr.DebugReqCtrl   = 0;     // not in debug mode
  ptm_cpu0->etmcr.PowerDown      = 0;     // enable PTM
  ptm_cpu0->etmcr.CycleAccurate  = 1;
  ptm_cpu0->etmcr.ContexIDSize   = 3;     // context id size = 32 bits
  // TraceEnable Event Register:
  ptm_cpu0->etmteevr.Function    = 0x0;   // logical operation = A
  ptm_cpu0->etmteevr.ResourceA   = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  ptm_cpu0->etmteevr.ResourceB   = 0x00;  // don't care
  // Trigger Event Register:
  ptm_cpu0->etmtrigger.Function  = 0x0;   // logical operation = A
  ptm_cpu0->etmtrigger.ResourceA = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  ptm_cpu0->etmtrigger.ResourceB = 0x00;  // don't care
  // TraceEnable Control Register 1:
  ptm_cpu0->etmtecr1.TraceSSEn   = 0;     // unaffected by TraceEnable start/stop block
  ptm_cpu0->etmtecr1.ExcIncFlag  = 1;     // exclude
  ptm_cpu0->etmtecr1.AddrCompSel = 0x0;   // no address range
  // CoreSight Trace ID Register:
  ptm_cpu0->etmtraceidr.TraceID  = 0x42;  // non-zero, not in range 0x70-0x7F
  // Address Comparator registers:
  ptm_cpu0->etmacvr1.Address   = (uint32_t)(&trace_start);
  ptm_cpu0->etmacvr2.Address   = (uint32_t)(&trace_stop);
  //
  ptm_cpu0->etmcr.ProgBit = 0;
  while ( ptm_cpu0->etmsr.ProgBit != 0 ) {
    // wait
  }
  ptm_cpu0->lar = 0x00000000;

  /***********************/
  /** configure funnel: **/
  /***********************/
  funnel->lar                  = 0xC5ACCE55;
  funnel->control.EnableSlave0 = 1;
  funnel->control.MinHoldTime  = 0x3;
  funnel->lar                  = 0x00000000;

  /********************/
  /** configure ETB: **/
  /********************/
  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 0;
  while ( etb->ffsr.FtStopped == 0 ) {
    // wait
  }
  // fill the buffer with 0x00:
  etb->rwp.value = 0x000;
  for ( uint16_t i = 0x000 ; i < 0x400 ; i++ ) {
    etb->rwd.value = 0x00000000;  // rwd is automatically incremented
    __asm__ __volatile__ ("isb" : : : "memory");
    __asm__ __volatile__ ("dsb" : : : "memory");
  }
  etb->ctl.TraceCaptEn = 1;
  etb->lar             = 0x00000000;

  /*******************/
  /** to be traced: **/
  /*******************/
  myFunction();

  /*****************************************/
  /** print the beginning of ETB content: **/
  /*****************************************/
  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 0;
  while ( etb->ffsr.FtStopped == 0 ) {
    // wait
  }
  __asm__ __volatile__(".global etb_ready_to_read");
  __asm__ __volatile__("etb_ready_to_read:");
  etb->rrp.value = 0x000;
  for ( uint16_t i = 0x000 ; i < 0x040 ; i++ ) {
    if ( i % 4 == 0 ) {
      printf("(0x%04x)", i);
    }
    printf(" %08lx", etb->rrd.value); // rrd is automatically incremented
    if ( i % 4 == 3 ) {
      printf("\r\n");
    }
  }
  etb->rrp.value       = 0x000;
  etb->ctl.TraceCaptEn = 1;
  etb->lar             = 0x00000000;

  return;
}

/******************************************************************************/

/**
 * \brief
 *  Restricts accesses with MMU and tries to write.
 *
 * \warning
 *  This function generates a fault!
 */
void restrictWithMmu(
  void
) {
  uint32_t i;
  uint32_t data;
  uint32_t *translBaseAddr;
  struct section_descriptor theDescValue;

  Xil_DisableMMU();

  /*********************/
  /** write to memory **/
  /*********************/
  *((uint32_t *)(&__challenge_low)) = 0x00000042;

  /***********************/
  /** set to read-only: **/
  /***********************/
  // write TTBCR:
  data = 0x00000000; // Short-descriptor translation table format ; N = 0
  __asm__ __volatile__("mcr p15, 0, %0, c2, c0, 2" : "=r"(data));

  // write DACR:
  data = 0xfdffffff;  // all domains = Manager, except domain 12 = Client
  __asm__ __volatile__("mcr p15, 0, %0, c3, c0, 0" : "=r"(data));

  // read TTBR0:
  __asm__ __volatile__("mrc p15, 0, %0, c2, c0, 0" : "=r"(data));
  translBaseAddr = (uint32_t *)(data & TTBR0_TRANSL_BASE_ADDR_MASK(0));

  // section base address:
  i = ((uint32_t)(&__challenge_low) >> 20);

  // read the descriptor:
  theDescValue.raw  = translBaseAddr[i];

  // update its value:
  theDescValue.baseaddr = i;  // virtual and physical are identical
  theDescValue.ap_1_0   = 3;  // Read-only at any privilege level
  theDescValue.ap_2     = 1;
  theDescValue.domain   = 12; // MMU generates the fault only if the access is
                              // to memory in the Client domain
  // write the descriptor:
  translBaseAddr[i] = theDescValue.raw;
  __asm__ __volatile__("isb");
  __asm__ __volatile__("dsb");

  Xil_EnableMMU();

  /****************************/
  /** try to write to memory **/
  /****************************/
  // try to write in a memory with read-only access (generates a fault!):
  *((uint32_t *)(&__challenge_low)) = 0xdeadbeef;

  // verification (never reached):
  printf("*(&__challenge_low) = 0x%lx\r\n", (uint32_t)(*(&__challenge_low)));

  return;
}

/******************************************************************************/

int main(
  void
) {
  unsigned int theSelect;

  // selection of the code to run, can be modified from the UART or by gdb:
  theSelect = 0x5d;
  __asm__ __volatile__("__breakpoint_select:");

  if ( theSelect == 0x5d ) {
    /* running from the SD card */
    printf("** Execution of some code. Please enter the number:\n");
    theSelect = (unsigned int)(getchar() - '0');
    printf("You have selected: %u\n", theSelect);
  }

  if ( theSelect == 0 ) {
    readwriteAxiSlaves();
  }
  else
  if ( theSelect == 1 ) {
    traceWithCoresight();
  }
  else
  if ( theSelect == 2 ) {
    restrictWithMmu();
  }

  while (1) {
    // infinite loop
    __asm__ __volatile__("__my_breakpoint:");
  }

  return 0;
}

