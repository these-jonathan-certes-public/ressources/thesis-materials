/*
Copyright (C) 2021  Benoît Morgan

This file is part of abyme

abyme is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

abyme is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with abyme.  If not, see <http://www.gnu.org/licenses/>.

Copyright (C) 2021  Jonathan Certes
Update to fit formally verified remote attestation on microprocessors.

*/

#include "string.h"

void * memset(void *dst, int c, size_t size) {
  size_t i;
  uint8_t *theDest = (uint8_t *)( dst );
  for (i = 0; i < size ; i++) {
    theDest[i] = (uint8_t)( c );
  }
  return dst;
}

void * memcpy(void *dst, const void *src, size_t size) {
  size_t i;
  uint8_t *theDest = (uint8_t *)( dst );
  uint8_t *theSrc  = (uint8_t *)( src );
  for (i = 0; i < size ; i++) {
    theDest[i] = theSrc[i];
  }
  return dst;
}

unsigned int strlen(char *s) {
  unsigned int size = 0, i;
  for (i = 0; s[i] != '\0'; ++i) {
    ++size;
  }
  return size;
}

int strncmp(
  const char *str1,
  const char *str2,
  size_t n
) {
  while ( n > 0 && *str1 != '\0' && (*str1 == *str2) ) {
    str1++;
    str2++;
    n--;
  }
  if ( n == 0 ) {
    return 0;
  } else {
    return ( *(unsigned char *)( str1 ) - *(unsigned char *)( str2 ) );
  }
}
