#!/usr/bin/python3

#
# Copyright (C) 2022 Jonathan Certes
# jonathan.certes@online.fr
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys
# location where spot is installed:
sys.path.append("/usr/local/lib/python3.7/site-packages/")
import spot, buddy

##
# converts a formula to an automaton:
#
f = spot.formula("F(a) && G(a -> F(b))");
theAut = spot.translate(f).postprocess('BA', 'Deterministic')

##
# export to a dot file:
#
theFileName = sys.argv[0]
theFileList = [
                [theFileName[0:-2] + "dot", theAut.to_str("dot")],
              ]
# create files:
for theFile in theFileList:
  print("Creating file: " + theFile[0])
  theBuffer = open(theFile[0], "w")
  theBuffer.write(theFile[1])
  theBuffer.close()

