
/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ETB_H
  #define __ETB_H

  #include <stdint.h>

  /**
   * \addtogroup etb
   *
   * \{
   */

  #define ETB_BASE 0xF8801000  //!< Base address of Embedded Trace Buffer
  #define ETB_SIZE 0x00001000  //!< Size of Embedded Trace Buffer register

  /**
   * \brief
   *  Definition of CoreSight Embedded Trace Buffer (etb) Status Register
   *  (STS).
   */
  struct __attribute__((packed)) etb_sts {
    union {
      struct {
        uint32_t Full      : 1;
        uint32_t Triggered : 1;
        uint32_t AcqComp   : 1;
        uint32_t FtEmpty   : 1;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Embedded Trace Buffer (etb) RAM Read Data Register
   *  (RRD).
   */
  struct __attribute__((packed)) etb_rrd {
    union {
      struct {
        uint32_t value : 32;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Embedded Trace Buffer (etb) RAM Read Pointer
   *  Register (RRP).
   */
  struct __attribute__((packed)) etb_rrp {
    union {
      struct {
        uint32_t value : 10;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Embedded Trace Buffer (etb) RAM Write Pointer
   *  Register (RWP).
   */
  struct __attribute__((packed)) etb_rwp {
    union {
      struct {
        uint32_t value : 10;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Embedded Trace Buffer (etb) Control Register
   *  (CTL).
   */
  struct __attribute__((packed)) etb_ctl {
    union {
      struct {
        uint32_t TraceCaptEn : 1;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Embedded Trace Buffer (etb) RAM Write Data
   *  Register (RWD).
   */
  struct __attribute__((packed)) etb_rwd {
    union {
      struct {
        uint32_t value : 32;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Embedded Trace Buffer (etb) Formatter and Flush
   *  Status Register (FFSR).
   */
  struct __attribute__((packed)) etb_ffsr {
     union {
      struct {
        uint32_t FlInProg  : 1;
        uint32_t FtStopped : 1;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Embedded Trace Buffer (etb) register.
   */
  struct __attribute__((packed)) etb {
    uint8_t  dummy0[0x0000000C];
    struct   etb_sts  sts;
    struct   etb_rrd  rrd;
    struct   etb_rrp  rrp;
    struct   etb_rwp  rwp;
    uint8_t  dummy2[0x00000004];
    struct   etb_ctl  ctl;
    struct   etb_rwd  rwd;
    uint8_t  dummy3[0x00000300 - 0x00000028];
    struct   etb_ffsr ffsr;
    uint8_t  dummy4[0x00000FB0 - 0x00000304];
    uint32_t lar;
  };

  /**
   * \}
   */

  extern struct etb * etb;

/******************************************************************************/

  void etb_init( void );
  void etb_stop( void );
  void etb_dump( void );

  void etb_readAllData(  void );
  void etb_writeAllData( void );

#endif
