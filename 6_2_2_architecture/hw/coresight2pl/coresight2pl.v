/*
 * Copyright (C) 2021 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* verilator lint_off UNUSED */

/**
 * \brief
 *  TODO
 */
module coresight2pl
#(
  parameter p_none      = 4'b0000,
  parameter p_isync     = 4'b0001,
  parameter p_atom      = 4'b0010,
  parameter p_branch    = 4'b0011,
  parameter p_waypoint  = 4'b0100,
  parameter p_trigger   = 4'b0101,
  parameter p_contextid = 4'b0110,
  parameter p_vmid      = 4'b0111,
  parameter p_timestamp = 4'b1000,
  parameter p_except    = 4'b1001,
  parameter p_ignore    = 4'b1010
) (
  input       nrst,       //!< low level active reset
  input       clk,        //!< clock
  input [7:0] trace_data, //!< Byte-aligned trace data
  input       trace_ctl,  //!< active when 0

  output decompress_error //!< same as \ref overall_pft_trace_decompress output
                          //!  with the same name
);

  /* connections between packet decompressers and the overall PFT trace
     decompresser: */
  wire async_enable;  wire async_busy;
                      wire async_ready;
  //
  wire packets_enable;  wire [3:0] available_packet;
                        wire packet_ready;

  /* connections between packet decompressers the packets decoder: */
  wire [3:0] data_size;
  wire [7:0] data__0;
  wire [7:0] data__1;
  wire [7:0] data__2;
  wire [7:0] data__3;
  wire [7:0] data__4;
  wire [7:0] data__5;
  wire [7:0] data__6;
  wire [7:0] data__7;
  wire [7:0] data__8;
  wire [7:0] data__9;
  wire [7:0] data_10;
  wire [7:0] data_11;
  wire [7:0] data_12;
  wire [7:0] data_13;
  wire [7:0] data_14;

  /* connections between the packets decoder and the transducer: */
  wire  [1:0] decoded_isetstate;
  wire [31:0] decoded_address;
  wire        hyp;
  wire        altis;
  wire        ns;
  wire  [1:0] isync_reason;
  wire [31:0] cyclecount;
  wire [31:0] contextid;
  wire        atom_f;
  wire  [8:0] decoded_exception;


  /************************************
   ** overall PFT trace decompresser **
   ************************************/

  overall_pft_trace_decompress i_overall_pft_trace_decompress(
    /* connections to inputs: */
    .nrst( nrst ),
    .clk(  clk  ),
    /* connections to packet decompressers: */
    .async_enable(  async_enable  ),
    .async_busy(    async_busy    ),
    .async_ready(   async_ready   ),
    //
    .packets_enable(   packets_enable   ),
    .available_packet( available_packet ),
    .packet_ready(     packet_ready     ),
    /* connections to outputs: */
    .decompress_error( decompress_error )
  );

  /**************************
   ** packet decompressers **
   **************************/

  async_decompress i_async_decompress (
    /* connections to inputs: */
    .nrst(       nrst & async_enable ),
    .clk(        clk                 ),
    .trace_data( trace_data          ),
    .trace_ctl(  trace_ctl           ),
    /* connections to the decoder: */
    .busy(  async_busy  ),
    .ready( async_ready )
  );

  packets_decompresser #(
    .p_none(       p_none       ),
    .p_isync(      p_isync      ),
    .p_atom(       p_atom       ),
    .p_branch(     p_branch     ),
    .p_waypoint(   p_waypoint   ),
    .p_trigger(    p_trigger    ),
    .p_contextid(  p_contextid  ),
    .p_vmid(       p_vmid       ),
    .p_timestamp(  p_timestamp  ),
    .p_except(     p_except     ),
    .p_ignore(     p_ignore     )
  ) i_packets_decompresser (
    /* connections to inputs: */
    .nrst(       nrst & packets_enable ),
    .clk(        clk                   ),
    .trace_data( trace_data            ),
    .trace_ctl(  trace_ctl             ),
    /* connections to the decoder: */
    .available_packet(  available_packet ),
    .ready(             packet_ready     ),
    .data__0( data__0 ),
    .data__1( data__1 ),
    .data__2( data__2 ),
    .data__3( data__3 ),
    .data__4( data__4 ),
    .data__5( data__5 ),
    .data__6( data__6 ),
    .data__7( data__7 ),
    .data__8( data__8 ),
    .data__9( data__9 ),
    .data_10( data_10 ),
    .data_11( data_11 ),
    .data_12( data_12 ),
    .data_13( data_13 ),
    .data_14( data_14 ),
    //
    .data_size( data_size )
  );

  /*********************
   ** packets decoder **
   *********************/

  packets_decoder i_packets_decoder(
    .nrst( nrst ),
    .clk(  clk  ),
    /* connections to packets decompresser: */
    .available_packet( available_packet ),
    .packet_ready(     packet_ready     ),
    .data__0( data__0 ),
    .data__1( data__1 ),
    .data__2( data__2 ),
    .data__3( data__3 ),
    .data__4( data__4 ),
    .data__5( data__5 ),
    .data__6( data__6 ),
    .data__7( data__7 ),
    .data__8( data__8 ),
    .data__9( data__9 ),
    .data_10( data_10 ),
    .data_11( data_11 ),
    .data_12( data_12 ),
    .data_13( data_13 ),
    .data_14( data_14 ),
    /* connections to the transducer */
    .decoded_isetstate( decoded_isetstate ),
    .decoded_address(   decoded_address   ),
    .hyp(               hyp               ),
    .altis(             altis             ),
    .ns(                ns                ),
    .isync_reason(      isync_reason      ),
    .cyclecount(        cyclecount        ),
    .contextid(         contextid         ),
    .atom_f(            atom_f            ),
    .decoded_exception( decoded_exception )
  );

endmodule
