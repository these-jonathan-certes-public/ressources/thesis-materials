/*
 * Copyright (C) 2021 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* verilator lint_off MODDUP */
`include "packets_decoder_core.v"
/* verilator lint_on  MODDUP */

/**
 * \brief
 *  Wrapper to connect the output vectors in a human readable format.
 */
module packets_decoder
(
  input nrst, //!< low level active reset
  input clk,  //!< clock

  input [3:0] available_packet, //!< indicates which packet is currently decompressing
  input       packet_ready,     //!< indicates that the packet is decompressed
  input [7:0] data__0,  //!< packet byte index  0
  input [7:0] data__1,  //!< packet byte index  1
  input [7:0] data__2,  //!< packet byte index  2
  input [7:0] data__3,  //!< packet byte index  3
  input [7:0] data__4,  //!< packet byte index  4
  input [7:0] data__5,  //!< packet byte index  5
  input [7:0] data__6,  //!< packet byte index  6
  input [7:0] data__7,  //!< packet byte index  7
  input [7:0] data__8,  //!< packet byte index  8
  input [7:0] data__9,  //!< packet byte index  9
  input [7:0] data_10,  //!< packet byte index 10
  input [7:0] data_11,  //!< packet byte index 11
  input [7:0] data_12,  //!< packet byte index 12
  input [7:0] data_13,  //!< packet byte index 13
  input [7:0] data_14,  //!< packet byte index 14

  output  [1:0] decoded_isetstate,//!< instruction set state
  output [31:0] decoded_address,  //!< the target address
  output        hyp,              //!< tells branching into Hyp mode
  output        altis,            //!< gives the instruction set state
  output        ns,               //!< set if in Non-secure state
  output [31:0] cyclecount,       //!< cycle count informations
  output  [1:0] isync_reason,     //!< reason why a I-sync packet was generated
  output [31:0] contextid,        //!< current Context ID
  output        atom_f,           //!< E or N atom
  output  [8:0] decoded_exception //!< exception informations
);

  /****************************************************************************/

  wire decoded_address__0;    wire decoded_address_16;
  wire decoded_address__1;    wire decoded_address_17;
  wire decoded_address__2;    wire decoded_address_18;
  wire decoded_address__3;    wire decoded_address_19;
  wire decoded_address__4;    wire decoded_address_20;
  wire decoded_address__5;    wire decoded_address_21;
  wire decoded_address__6;    wire decoded_address_22;
  wire decoded_address__7;    wire decoded_address_23;
  wire decoded_address__8;    wire decoded_address_24;
  wire decoded_address__9;    wire decoded_address_25;
  wire decoded_address_10;    wire decoded_address_26;
  wire decoded_address_11;    wire decoded_address_27;
  wire decoded_address_12;    wire decoded_address_28;
  wire decoded_address_13;    wire decoded_address_29;
  wire decoded_address_14;    wire decoded_address_30;
  wire decoded_address_15;    wire decoded_address_31;
  //
  wire cyclecount__0; wire cyclecount_16;
  wire cyclecount__1; wire cyclecount_17;
  wire cyclecount__2; wire cyclecount_18;
  wire cyclecount__3; wire cyclecount_19;
  wire cyclecount__4; wire cyclecount_20;
  wire cyclecount__5; wire cyclecount_21;
  wire cyclecount__6; wire cyclecount_22;
  wire cyclecount__7; wire cyclecount_23;
  wire cyclecount__8; wire cyclecount_24;
  wire cyclecount__9; wire cyclecount_25;
  wire cyclecount_10; wire cyclecount_26;
  wire cyclecount_11; wire cyclecount_27;
  wire cyclecount_12; wire cyclecount_28;
  wire cyclecount_13; wire cyclecount_29;
  wire cyclecount_14; wire cyclecount_30;
  wire cyclecount_15; wire cyclecount_31;
  //
  wire [7:0] contextid_0;
  wire [7:0] contextid_1;
  wire [7:0] contextid_2;
  wire [7:0] contextid_3;

  packets_decoder_core i_packets_decoder_core(
    .nrst(           nrst ),
    .clk(            clk  ),
    /* connections to packets decompresser: */
    .available_packet( available_packet ),
    .packet_ready(     packet_ready     ),
    .data__0( data__0 ),
    .data__1( data__1 ),
    .data__2( data__2 ),
    .data__3( data__3 ),
    .data__4( data__4 ),
    .data__5( data__5 ),
    .data__6( data__6 ),
    .data__7( data__7 ),
    .data__8( data__8 ),
    .data__9( data__9 ),
    .data_10( data_10 ),
    .data_11( data_11 ),
    .data_12( data_12 ),
    .data_13( data_13 ),
    .data_14( data_14 ),
    /* outputs: */
    .decoded_isetstate( decoded_isetstate ),
    .hyp(               hyp               ),
    .altis(             altis             ),
    .ns(                ns                ),
    .isync_reason(      isync_reason      ),
    .decoded_address__0( decoded_address__0 ),
    .decoded_address__1( decoded_address__1 ),
    .decoded_address__2( decoded_address__2 ),
    .decoded_address__3( decoded_address__3 ),
    .decoded_address__4( decoded_address__4 ),
    .decoded_address__5( decoded_address__5 ),
    .decoded_address__6( decoded_address__6 ),
    .decoded_address__7( decoded_address__7 ),
    .decoded_address__8( decoded_address__8 ),
    .decoded_address__9( decoded_address__9 ),
    .decoded_address_10( decoded_address_10 ),
    .decoded_address_11( decoded_address_11 ),
    .decoded_address_12( decoded_address_12 ),
    .decoded_address_13( decoded_address_13 ),
    .decoded_address_14( decoded_address_14 ),
    .decoded_address_15( decoded_address_15 ),
    .decoded_address_16( decoded_address_16 ),
    .decoded_address_17( decoded_address_17 ),
    .decoded_address_18( decoded_address_18 ),
    .decoded_address_19( decoded_address_19 ),
    .decoded_address_20( decoded_address_20 ),
    .decoded_address_21( decoded_address_21 ),
    .decoded_address_22( decoded_address_22 ),
    .decoded_address_23( decoded_address_23 ),
    .decoded_address_24( decoded_address_24 ),
    .decoded_address_25( decoded_address_25 ),
    .decoded_address_26( decoded_address_26 ),
    .decoded_address_27( decoded_address_27 ),
    .decoded_address_28( decoded_address_28 ),
    .decoded_address_29( decoded_address_29 ),
    .decoded_address_30( decoded_address_30 ),
    .decoded_address_31( decoded_address_31 ),
    .cyclecount__0(  cyclecount__0 ),
    .cyclecount__1(  cyclecount__1 ),
    .cyclecount__2(  cyclecount__2 ),
    .cyclecount__3(  cyclecount__3 ),
    .cyclecount__4(  cyclecount__4 ),
    .cyclecount__5(  cyclecount__5 ),
    .cyclecount__6(  cyclecount__6 ),
    .cyclecount__7(  cyclecount__7 ),
    .cyclecount__8(  cyclecount__8 ),
    .cyclecount__9(  cyclecount__9 ),
    .cyclecount_10(  cyclecount_10 ),
    .cyclecount_11(  cyclecount_11 ),
    .cyclecount_12(  cyclecount_12 ),
    .cyclecount_13(  cyclecount_13 ),
    .cyclecount_14(  cyclecount_14 ),
    .cyclecount_15(  cyclecount_15 ),
    .cyclecount_16(  cyclecount_16 ),
    .cyclecount_17(  cyclecount_17 ),
    .cyclecount_18(  cyclecount_18 ),
    .cyclecount_19(  cyclecount_19 ),
    .cyclecount_20(  cyclecount_20 ),
    .cyclecount_21(  cyclecount_21 ),
    .cyclecount_22(  cyclecount_22 ),
    .cyclecount_23(  cyclecount_23 ),
    .cyclecount_24(  cyclecount_24 ),
    .cyclecount_25(  cyclecount_25 ),
    .cyclecount_26(  cyclecount_26 ),
    .cyclecount_27(  cyclecount_27 ),
    .cyclecount_28(  cyclecount_28 ),
    .cyclecount_29(  cyclecount_29 ),
    .cyclecount_30(  cyclecount_30 ),
    .cyclecount_31(  cyclecount_31 ),
    .contextid_0(    contextid_0   ),
    .contextid_1(    contextid_1   ),
    .contextid_2(    contextid_2   ),
    .contextid_3(    contextid_3   ),
    .atom_f(            atom_f            ),
    .decoded_exception( decoded_exception )
  );

  assign decoded_address = { decoded_address_31,
                             decoded_address_30,
                             decoded_address_29,
                             decoded_address_28,
                             decoded_address_27,
                             decoded_address_26,
                             decoded_address_25,
                             decoded_address_24,
                             decoded_address_23,
                             decoded_address_22,
                             decoded_address_21,
                             decoded_address_20,
                             decoded_address_19,
                             decoded_address_18,
                             decoded_address_17,
                             decoded_address_16,
                             decoded_address_15,
                             decoded_address_14,
                             decoded_address_13,
                             decoded_address_12,
                             decoded_address_11,
                             decoded_address_10,
                             decoded_address__9,
                             decoded_address__8,
                             decoded_address__7,
                             decoded_address__6,
                             decoded_address__5,
                             decoded_address__4,
                             decoded_address__3,
                             decoded_address__2,
                             decoded_address__1,
                             decoded_address__0
                            };

  assign cyclecount = { cyclecount_31,
                        cyclecount_30,
                        cyclecount_29,
                        cyclecount_28,
                        cyclecount_27,
                        cyclecount_26,
                        cyclecount_25,
                        cyclecount_24,
                        cyclecount_23,
                        cyclecount_22,
                        cyclecount_21,
                        cyclecount_20,
                        cyclecount_19,
                        cyclecount_18,
                        cyclecount_17,
                        cyclecount_16,
                        cyclecount_15,
                        cyclecount_14,
                        cyclecount_13,
                        cyclecount_12,
                        cyclecount_11,
                        cyclecount_10,
                        cyclecount__9,
                        cyclecount__8,
                        cyclecount__7,
                        cyclecount__6,
                        cyclecount__5,
                        cyclecount__4,
                        cyclecount__3,
                        cyclecount__2,
                        cyclecount__1,
                        cyclecount__0
                      };

  assign contextid = { contextid_3,
                       contextid_2,
                       contextid_1,
                       contextid_0
                      };

endmodule

