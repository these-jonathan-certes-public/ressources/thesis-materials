
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __FUNNEL_H
  #define __FUNNEL_H

  #include <stdint.h>

  /**
   * \addtogroup funnel
   *
   * \{
   */

  #define FUNNEL_BASE 0xF8804000  //!< Base address of Funnel
  #define FUNNEL_SIZE 0x00001000  //!< Size of Funnel register

  /**
   * \brief
   *  Definition of CoreSight Trace Funnel (funnel) Control Register (Control).
   */
  struct __attribute__((packed)) funnel_control {
    union {
      struct {
        uint32_t EnableSlave0 : 1;
        uint32_t EnableSlave1 : 1;
        uint32_t EnableSlave2 : 1;
        uint32_t EnableSlave3 : 1;
        uint32_t EnableSlave4 : 1;
        uint32_t EnableSlave5 : 1;
        uint32_t EnableSlave6 : 1;
        uint32_t EnableSlave7 : 1;
        uint32_t MinHoldTime  : 4;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Trace Funnel (funnel) register.
   */
  struct __attribute__((packed)) funnel {
    struct   funnel_control control;  // 0x00000000
    uint8_t  dummy[0x00000FB0 - sizeof(struct funnel_control)];
    uint32_t lar;                     // 0x00000FB0
  };

  /**
   * \}
   */

  extern struct funnel * funnel;

/******************************************************************************/

  void funnel_init( void );
  void funnel_dump( void );

#endif
