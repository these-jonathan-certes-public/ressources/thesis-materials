
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __MMU_H
  #define __MMU_H

  #include <stdint.h>

  /**
   * \addtogroup mmu
   *
   * \{
   */

  // higher bits of TTBR0 function of N:
  #define TTBR0_TRANSL_BASE_ADDR_MASK(N)  (~((1 << (14 - N)) - 1))
  // lower bits of the descriptor address function of N, without the 2 lsb:
  #define TTBR0_TABLE_INDEX_MASK(N)       (((1 << (14 - N)) - 1) & ~0x3)

  /**
   * \brief
   *   Short-descriptor translation table first-level descriptor format for a
   *   section.
   */
  struct __attribute__((packed)) section_descriptor {
    union {
      struct {
        uint32_t type     : 2;
        uint32_t b        : 1;
        uint32_t c        : 1;
        uint32_t xn       : 1;
        uint32_t domain   : 4;
        uint32_t impldef  : 1;
        uint32_t ap_1_0   : 2;
        uint32_t tex      : 3;
        uint32_t ap_2     : 1;
        uint32_t s        : 1;
        uint32_t ng       : 1;
        uint32_t super    : 1;
        uint32_t ns       : 1;
        uint32_t baseaddr : 12;
      };
      uint32_t raw;
    };
  };

/******************************************************************************/

#endif
