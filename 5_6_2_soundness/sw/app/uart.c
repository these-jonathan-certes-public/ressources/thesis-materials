
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file uart.c
 */

#include "xil_cache.h"
#include "uart.h"

/******************************************************************************/

/**
 * \defgroup uart   Universal Asynchronous Receiver Transmitter (uart)
 *
 * \brief
 *  Library to manipulate Universal Asynchronous Receiver Transmitter (uart)
 *  under Xilinx Zynq-7000.
 *
 * \{
 */

struct uart *uart0 = (struct uart *)( UART0_BASE );
struct uart *uart1 = (struct uart *)( UART1_BASE );

/******************************************************************************/

void uart_init(
  uint8_t theUartIndex  //!< index of the UART to activate
) {
  struct uart *theUart;
  switch ( theUartIndex ) {
    case 1:
      theUart = uart1;
      break;
    default:
      theUart = uart0;
      break;
  }

  // Set Baudrate to 115200 (assuming input clock speed UART_Ref_Clk is 100MHz):
  theUart->mr.Clksel              = 0;
  theUart->baud_rate_divider.Bdiv = 6;
  theUart->baudgen.Cd             = 124;

  theUart->mr.Par    = (1 << 2); // 1xx: no parity
  theUart->cr.raw    = 0x00000000;
  theUart->cr.Rx_en  = 1;
  theUart->cr.Tx_en  = 1;

  // software reset (self clearing once the reset has completed):
  theUart->cr.Rxrst = 1;
  while ( theUart->cr.Rxrst ) {
    // do nothing
  }

  theUart->cr.Txrst = 1;
  while ( theUart->cr.Txrst ) {
    // do nothing
  }

  Xil_DCacheFlush();
  Xil_ICacheInvalidate();

  return;
}

/******************************************************************************/

/**
 * \}
 */
