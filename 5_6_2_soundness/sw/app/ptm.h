
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __PTM_H
  #define __PTM_H

  #include <stdint.h>

  /**
   * \addtogroup ptm
   *
   * \{
   */

  #define PTM_BASE_CPU0 0xF889C000  //!< Base address of PTM for CPU 0
  #define PTM_BASE_CPU1 0xF889D000  //!< Base address of PTM for CPU 1
  #define PTM_SIZE      0x00001000  //!< Size of PTM register

  /**
   * \brief
   *  Definition of CoreSight Program Trace Macrocell (ptm) Main Control
   *  Register (ETMCR).
   */
  struct __attribute__((packed)) ptm_etmcr {
    union {
      struct {
        uint32_t PowerDown     : 1;
        uint32_t reserved_0    : 7;
        uint32_t BranchOutput  : 1;
        uint32_t DebugReqCtrl  : 1;
        uint32_t ProgBit       : 1;
        uint32_t reserved_1    : 1;
        uint32_t CycleAccurate : 1;
        uint32_t reserved_2    : 1;
        uint32_t ContexIDSize  : 2;
        uint32_t reserved_3    : 9;
        uint32_t ProcSelect    : 3;
        uint32_t TimestampEn   : 1;
        uint32_t ReturnStackEn : 1;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Program Trace Macrocell (ptm) Trigger Event
   *  Register (ETMTRIGGER).
   */
  struct __attribute__((packed)) ptm_etmtrigger {
    union {
      struct {
        uint32_t ResourceA : 7;
        uint32_t ResourceB : 7;
        uint32_t Function  : 3;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Program Trace Macrocell (ptm) Status Register
   *  (ETMSR).
   */
  struct __attribute__((packed)) ptm_etmsr {
    union {
      struct {
        uint32_t Overflow : 1;
        uint32_t ProgBit  : 1;
        uint32_t TSSRStat : 1;
        uint32_t TrigFlag : 1;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Program Trace Macrocell (ptm) TraceEnable Event
   *  Register (ETMTEEVR).
   */
  struct __attribute__((packed)) ptm_etmteevr {
    union {
      struct {
        uint32_t ResourceA : 7;
        uint32_t ResourceB : 7;
        uint32_t Function  : 3;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Program Trace Macrocell (ptm) TraceEnable Control
   *  Register 1 (ETMTECR1).
   */
  struct __attribute__((packed)) ptm_etmtecr1 {
     union {
      struct {
        uint32_t AddrCompSel : 4;
        uint32_t reserved_0  : 20;
        uint32_t ExcIncFlag  : 1;
        uint32_t TraceSSEn   : 1;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Program Trace Macrocell (ptm) Address Comparator
   *  Value Register X (ETMACVRx).
   */
  struct __attribute__((packed)) ptm_etmacvrX {
     union {
      struct {
        uint32_t Address : 32;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Program Trace Macrocell (ptm) CoreSight Trace ID
   *  Register (ETMTRACEIDR).
   */
  struct __attribute__((packed)) ptm_etmtraceidr {
     union {
      struct {
        uint32_t TraceID : 7;
      };
      uint32_t raw;
    };
  };

  /**
   * \brief
   *  Definition of CoreSight Program Trace Macrocell (ptm) register.
   */
  struct __attribute__((packed)) ptm {
    struct   ptm_etmcr       etmcr;       // 0x00000000
    uint8_t  dummy0[0x00000008 - 0x00000004];
    struct   ptm_etmtrigger  etmtrigger;  // 0x00000008
    uint8_t  dummy1[0x00000010 - 0x0000000c];
    struct   ptm_etmsr       etmsr;       // 0x00000010
    uint8_t  dummy2[0x00000020 - 0x00000014];
    struct   ptm_etmteevr    etmteevr;    // 0x00000020
    struct   ptm_etmtecr1    etmtecr1;    // 0x00000024
    uint8_t  dummy3[0x00000040 - 0x00000028];
    struct   ptm_etmacvrX    etmacvr1;    // 0x00000040
    struct   ptm_etmacvrX    etmacvr2;    // 0x00000044
    struct   ptm_etmacvrX    etmacvr3;    // 0x00000048
    struct   ptm_etmacvrX    etmacvr4;    // 0x0000004c
    struct   ptm_etmacvrX    etmacvr5;    // 0x00000050
    struct   ptm_etmacvrX    etmacvr6;    // 0x00000054
    struct   ptm_etmacvrX    etmacvr7;    // 0x00000058
    struct   ptm_etmacvrX    etmacvr8;    // 0x0000005c
    uint8_t  dummy4[0x00000200 - 0x00000060];
    struct   ptm_etmtraceidr etmtraceidr; // 0x00000200
    uint8_t  dummy5[0x00000FB0 - 0x00000204];
    uint32_t lar;                         // 0x00000FB0
  };

  /**
   * \}
   */

  extern struct ptm* ptm_cpu0;
  extern struct ptm* ptm_cpu1;

/******************************************************************************/

  void ptm_init( uint8_t  theCpuIndex,
                 uint32_t traceRangeStart,
                 uint32_t traceRangeStop );
  void ptm_dump( uint8_t cpuIndex );

#endif

