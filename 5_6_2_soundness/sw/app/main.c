
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "xil_mmu.h"
#include "xil_cache.h"

#include "funnel.h"
#include "ptm.h"
#include "mmu.h"
#include "uart.h"

/** symbols that are defined in the linker script. **/
extern int __sw_att_low;
extern int __sw_att_top;
extern int __challenge_low;
extern int __ex_stack_low;
extern int __loaded_code_low;
extern int __loaded_code_top;

/******************************************************************************/

void configureCoresight(
  void
) {
  /********************/
  /** configure PTM: **/
  /********************/
  ptm_cpu0->lar = 0xC5ACCE55;
  ptm_cpu0->etmcr.ProgBit = 1;
  while ( ptm_cpu0->etmsr.ProgBit != 1 ) {
    // wait
  }
  // Main Control Register:
  ptm_cpu0->etmcr.BranchOutput   = 0;     // direct branch addresses not output
  ptm_cpu0->etmcr.DebugReqCtrl   = 0;     // not in debug mode
  ptm_cpu0->etmcr.PowerDown      = 0;     // enable PTM
  ptm_cpu0->etmcr.CycleAccurate  = 1;
  ptm_cpu0->etmcr.ContexIDSize   = 3;     // context id size = 32 bits
  // TraceEnable Event Register:
  ptm_cpu0->etmteevr.Function    = 0x0;   // logical operation = A
  ptm_cpu0->etmteevr.ResourceA   = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  ptm_cpu0->etmteevr.ResourceB   = 0x00;  // don't care
  // Trigger Event Register:
  ptm_cpu0->etmtrigger.Function  = 0x0;   // logical operation = A
  ptm_cpu0->etmtrigger.ResourceA = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  ptm_cpu0->etmtrigger.ResourceB = 0x00;  // don't care
  // TraceEnable Control Register 1:
  ptm_cpu0->etmtecr1.TraceSSEn   = 0;     // unaffected by TraceEnable start/stop block
  ptm_cpu0->etmtecr1.ExcIncFlag  = 1;     // exclude
  ptm_cpu0->etmtecr1.AddrCompSel = 0x0;   // no address range
  // CoreSight Trace ID Register:
  ptm_cpu0->etmtraceidr.TraceID  = 0x42;  // non-zero, not in range 0x70-0x7F
  // Address Comparator registers:
  ptm_cpu0->etmacvr1.Address   = (uint32_t)( &__sw_att_low );
  ptm_cpu0->etmacvr2.Address   = (uint32_t)( &__sw_att_top );
  //
  ptm_cpu0->etmcr.ProgBit = 0;
  while ( ptm_cpu0->etmsr.ProgBit != 0 ) {
    // wait
  }
  ptm_cpu0->lar = 0x00000000;

  /***********************/
  /** configure funnel: **/
  /***********************/
  funnel->lar                  = 0xC5ACCE55;
  funnel->control.EnableSlave0 = 1;
  funnel->control.MinHoldTime  = 0x3;
  funnel->lar                  = 0x00000000;

  return;
}

/******************************************************************************/

void configureMMU(
  void
) {
  uint32_t i;
  uint32_t data;
  uint32_t *translBaseAddr;
  struct section_descriptor theDescValue;

  Xil_DisableMMU();

  // write TTBCR:
  data = 0x00000000; // Short-descriptor translation table format ; N = 0
  __asm__ __volatile__("mcr p15, 0, %0, c2, c0, 2" : "=r"(data));

  // write DACR:
  data = 0xfdffffff;  // all domains = Manager, except domain 12 = Client
  __asm__ __volatile__("mcr p15, 0, %0, c3, c0, 0" : "=r"(data));

  // read TTBR0:
  __asm__ __volatile__("mrc p15, 0, %0, c2, c0, 0" : "=r"(data));
  translBaseAddr = (uint32_t *)(data & TTBR0_TRANSL_BASE_ADDR_MASK(0));

  // iterates on all indexes of the table:
  for ( i = 0; i <= (TTBR0_TABLE_INDEX_MASK(0) >> 2); i++ ) {
    // read the descriptor:
    theDescValue.raw  = translBaseAddr[i];

    // update its value:
    theDescValue.baseaddr = i; // virtual and physical are identical
    if (
         ( i == ((uint32_t)(&__ex_stack_low)  >> 20) )
      || ( i == ((uint32_t)(&__challenge_low) >> 20) )
    ) {
      // Read/write at any privilege level:
      theDescValue.ap_1_0   = 3;
      theDescValue.ap_2     = 0;
    } else {
      // Read-only at any privilege level:
      theDescValue.ap_1_0   = 3;
      theDescValue.ap_2     = 1;
    }
    // generates the fault only if the access is to memory in the Client domain:
    theDescValue.domain   = 12;

    // write the descriptor:
    translBaseAddr[i] = theDescValue.raw;
    __asm__ __volatile__("isb");
    __asm__ __volatile__("dsb");
  }

  Xil_EnableMMU();
  return;
}

/******************************************************************************/

/**
 * \brief
 *  Execution of attesting software.
 *
 * \details
 *  Reads 16 Bytes at MR, calls \ref sw_att() and reads again.
 */
void swAttTest(
  void
) {
  uint8_t *theBaseAddress = (uint8_t*)( &__challenge_low );

  printf("*** reading from &__challenge_low, address: %p ***\r\n", theBaseAddress);
  for ( uint16_t i = 0 ; i < 16 ; i++ ) {
    printf(" %02x", theBaseAddress[i]);
  }
  printf("\r\n");

  // prerequisites:
  Xil_DCacheDisable();
  Xil_ICacheDisable();
  configureCoresight();
  configureMMU();

  __asm__ __volatile__(".global breakpoint_before");
  __asm__ __volatile__("breakpoint_before:");
  //
  __asm__ __volatile__("bl __sw_att_low");
  //
  __asm__ __volatile__(".global breakpoint_after");
  __asm__ __volatile__("breakpoint_after:");

  // disable MMU (unset bit[0] of SCTLR):
  __asm__ __volatile__("mrc p15, 0, r3, c1, c0, 0");
  __asm__ __volatile__("mov r2, #1");
  __asm__ __volatile__("lsr r3, r2");
  __asm__ __volatile__("lsl r3, r2");
  __asm__ __volatile__("mcr p15, 0, r3, c1, c0, 0");
  __asm__ __volatile__("isb");
  __asm__ __volatile__("dsb");

  Xil_DCacheFlush();
  Xil_ICacheInvalidate();

  printf("*** reading from &__challenge_low, address: %p ***\r\n", theBaseAddress);
  for ( uint16_t i = 0 ; i < 16 ; i++ ) {
    printf(" %02x", theBaseAddress[i]);
  }
  printf("\r\n");

  return;
}

/******************************************************************************/

void uartCodeLoader(
  void
) {
  char theChar = 0x00;  // what is read on UART
  uint8_t* theAddress;

  printf("Send me your code on UART1, I will load it from %p to %p\n",
         &__loaded_code_low, &__loaded_code_top);

  // load what we receive in the loading memory region:
  theAddress = (uint8_t*)( &__loaded_code_low );
  while ( theAddress < (uint8_t*)( &__loaded_code_top ) ) {
    theChar = getchar();
    *theAddress++ = theChar;
  }

  // flush the cache since instruction fetching would be done from the cache if
  // the previous overwritten code had been loaded before:
  Xil_DCacheFlush();
  Xil_ICacheInvalidate();

  return;
}

/******************************************************************************/

/**
 * \brief
 *  Executes asm instruction <tt> bl </tt> to the first instruction of the
 *  loaded code.
 */
void branchInLoaded(
  void
) {
  printf("I branch to address %p\n", &__loaded_code_low);
  __asm__ __volatile__("bl __loaded_code_low");

  return;
}

/******************************************************************************/

int main(
  void
) {
  unsigned int theSelect;

  // selection of the code to run, can be modified from the UART or by gdb:
  theSelect = 0x5d;
  __asm__ __volatile__("__breakpoint_select:");

  if ( theSelect == 0x5d ) {
    /* running from the SD card */
    printf("** Execution of some code. Please enter the number:\n");
    theSelect = (unsigned int)(getchar() - '0');
    printf("You have selected: %u\n", theSelect);
  }

  if ( theSelect == 0 ) {
    swAttTest();
  }
  else
  if ( theSelect == 1 ) {
    uartCodeLoader();
  }
  else
  if ( theSelect == 2 ) {
    branchInLoaded();
  }

  while (1) {
    // infinite loop
    __asm__ __volatile__("__my_breakpoint:");
  }

  return 0;
}

