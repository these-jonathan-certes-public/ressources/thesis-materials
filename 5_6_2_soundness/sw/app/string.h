/*
Copyright (C) 2021  Benoît Morgan

This file is part of abyme

abyme is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

abyme is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with abyme.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __STRING_H__
  #define __STRING_H__

  #include <stdint.h>
  typedef unsigned int size_t;

  void * memset(void *dst, int c, size_t size);
  void * memcpy(void *dst, const void *src, size_t size);
  unsigned int strlen(char *c);
  int strncmp(const char *str1, const char *str2, size_t n);

#endif
