
#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

OCD = ../a_1_2_openocd/openocd_coraz7.tcl

#===============================================================================

all: ${OCD} sw/fsbl/main.elf sw/app/main.elf hw/design_0_wrapper.bit gdbinit.gdb
	make -C sw
	make -C hw
	# TO BE RUN IN AN OTHER TERMINAL:
	# gdb-multiarch -ex "set architecture armv7" -ex "target extended-remote localhost:3333" --command="gdbinit.gdb"
	#
	/usr/bin/openocd -f $<

qemu: gdbinit.gdb
	# TO BE RUN IN AN OTHER TERMINAL:
	# gdb-multiarch -ex "target remote localhost:1234" --command="gdbinit.gdb"
	#
	qemu-system-arm -machine xilinx-zynq-a9 -cpu cortex-a9 -m 1132M -nographic \
	  -serial null -serial mon:stdio -gdb tcp::1234

#=======================
# building the software:
#=======================

sw/fsbl/main.elf sw/app/main.elf : sw
	make -C $^

#=======================
# building the hardware:
#=======================

hw/design_0_wrapper.bit : hw
	make -C $^

#===============================================================================

clean:
	make -C sw clean
	make -C hw clean
	make -C hw/logic_analyzer/decode/ clean
