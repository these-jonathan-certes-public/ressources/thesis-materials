
#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

# this is a gtkwave init script

gtkwave::addSignalsFromList { i_decode.decode.clk             }
gtkwave::addSignalsFromList { i_decode.decode.data[31:0]      }

gtkwave::/Edit/Insert_Blank
gtkwave::/Edit/Insert_Comment "TPIU:"
#
gtkwave::addSignalsFromList { i_decode.decode.trace_data[7:0] }
gtkwave::addSignalsFromList { i_decode.decode.trace_ctl       }
gtkwave::addSignalsFromList { i_decode.decode.trace_clk_out   }

gtkwave::/Edit/Insert_Blank
gtkwave::/Edit/Insert_Comment "Read from AXI slave (sw_att):"
#
gtkwave::addSignalsFromList { i_decode.decode.sw_araddr[13:0] }
gtkwave::addSignalsFromList { i_decode.decode.sw_arvalid      }
gtkwave::addSignalsFromList { i_decode.decode.sw_arready      }
gtkwave::/Edit/Color_Format/Red
gtkwave::addSignalsFromList { i_decode.decode.sw_rvalid       }
gtkwave::addSignalsFromList { i_decode.decode.sw_rready       }
gtkwave::/Edit/Color_Format/Red
gtkwave::addSignalsFromList { i_decode.decode.sw_rdata[31:0]  }
gtkwave::/Edit/Color_Format/Red

gtkwave::/Edit/Insert_Blank
gtkwave::/Edit/Insert_Comment "Read from AXI slave (key and ex_stack):"
#
gtkwave::addSignalsFromList { i_decode.decode.KR_axi_arvalid }
gtkwave::addSignalsFromList { i_decode.decode.XS_axi_arvalid }
gtkwave::addSignalsFromList { i_decode.decode.XS_axi_awvalid }
gtkwave::addSignalsFromList { i_decode.decode.XS_axi_wvalid  }

gtkwave::/Edit/Insert_Blank
gtkwave::/Edit/Insert_Comment "Not from data but from SystemC:"
#
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.decompressed_packet[3:0] }
gtkwave::/Edit/Color_Format/Blue
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.packet_ready             }
gtkwave::/Edit/Color_Format/Blue
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.decoded_address[31:0]    }
gtkwave::/Edit/Color_Format/Blue
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.decoded_exception[8:0]   }
gtkwave::/Edit/Color_Format/Blue
#
gtkwave::/Edit/Insert_Blank
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.deduced_PC_in_CR    }
gtkwave::/Edit/Color_Format/Blue
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.deduced_PC_eq_CRmin }
gtkwave::/Edit/Color_Format/Blue
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.deduced_PC_eq_CRmax }
gtkwave::/Edit/Color_Format/Blue
gtkwave::/Edit/Insert_Blank
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.reset_atomicity }
gtkwave::/Edit/Color_Format/Blue
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.reset_exclusive_stack }
gtkwave::/Edit/Color_Format/Blue
gtkwave::addSignalsFromList { i_decode.decode.i_coresight2pl.reset_key_access_control }
gtkwave::/Edit/Color_Format/Blue

# zoom to fit:
gtkwave::/Time/Zoom/Zoom_Full
