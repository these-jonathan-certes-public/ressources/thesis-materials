
#
# Copyright (C) 2021 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

#
# This script must be sourced when a bd_design has been created.
#

set_property source_mgmt_mode All [current_project]

## IMPORTANT! Use digilent pre-configuration for the board, see
## vivadoMakefile.tcl, option --board.

##
# ports from the FPGA we are going to use, see constraints file:
#
create_bd_port -dir "I" "btn_0"
create_bd_port -dir "I" "btn_1"
#
create_bd_port -dir "O" "led_0"
create_bd_port -dir "O" "led_1"


##
# adding zynq processing system to the design:
#
set theInstName "processing_system7_0"
create_bd_cell -type "ip" \
               -vlnv "xilinx.com:ip:processing_system7:5.5" \
               ${theInstName}
# run block automation:
apply_bd_automation -rule "xilinx.com:bd_rule:processing_system7" -config {
  make_external      "FIXED_IO, DDR"
  apply_board_preset 1
  Master             "Disable"
  Slave              "Disable"
} [get_bd_cells ${theInstName}]
# connect clock:
connect_bd_net [get_bd_pins "${theInstName}/FCLK_CLK0"] \
               [get_bd_pins "${theInstName}/M_AXI_GP0_ACLK"]
# enables EMIO GPIO and sets its size:
set_property -dict {
  CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE 1
  CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE     1
  CONFIG.PCW_GPIO_EMIO_GPIO_IO         2
} [get_bd_cells ${theInstName}]
# save its instance name:
set theZynq ${theInstName}


##
# instantiating a module:
#
set theInstName "i_connexions"
create_bd_cell -type "module" -reference "connexions" ${theInstName}
#
connect_bd_net [get_bd_pins "${theInstName}/gpio_o"] \
               [get_bd_pins "${theZynq}/GPIO_O"]
connect_bd_net [get_bd_pins "${theZynq}/GPIO_I"] \
               [get_bd_pins "${theInstName}/gpio_i"]
#
connect_bd_net [get_bd_ports "btn_0"] [get_bd_pins "${theInstName}/btn_0"]
connect_bd_net [get_bd_pins "${theInstName}/led_0"] [get_bd_ports "led_0"]


##
# instantiating a module:
#
set theInstName "i_flipflop"
create_bd_cell -type "module" -reference "flipflop" ${theInstName}
# using clock from the PS7:
connect_bd_net [get_bd_pins "${theZynq}/FCLK_CLK0"] \
               [get_bd_pins "${theInstName}/clk"]
#
connect_bd_net [get_bd_ports "btn_1"] [get_bd_pins "${theInstName}/btn"]
connect_bd_net [get_bd_pins "${theInstName}/led"] [get_bd_ports "led_1"]


regenerate_bd_layout

