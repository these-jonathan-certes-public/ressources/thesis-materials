(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

(******************************************************************************)
(******************************************************************************)

(**
 * Definition of all the predicates:
 *)

Axiom clk       : CV0 Bit (eq O).
Axiom done      : CV0 Bit (eq O).
Axiom verifying : CV0 Bit (eq O).
Axiom sw_arvalid: CV0 Bit (eq O).

Axiom rddata : (BitVect 64).
Axiom i      : (BitVect 10).

Definition END_ADDR := uwconst(0x3ff, 10).

(******************************************************************************)

(**
 * Model of the environment:
 *)

(**
 * The clock in the FPGA is always active.
 *)
Axiom clk_is_on:
  ∀ π : Path, π ⊨ G('clk).


(**
 * At initial state, the microprocessor is not asking for data from the ROM.
 *)
Axiom init_sw_arvalid :
  ∀ π : Path, π ⊨ (negP ('sw_arvalid)).


(**
 * We define that we can access a 64-bit vector from a natural in the ROM.
 *)
Axiom ROM : nat -> BitVect 64.


(**
 * This models the behaviour of the ROM.
 *)
Axiom ROM_model:
forall n : nat,
(n < 2^10) -> (
  ∀ π : Path, π ⊨ G(
    (i '== uwconst(n, 10)) --> X(rddata '== (ROM n))
  )
) /\ (
  ∀ π : Path, π ⊨ G(
    X(rddata '== (ROM n)) --> (i '== uwconst(n, 10))
  )
).
