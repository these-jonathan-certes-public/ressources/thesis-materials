(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.decide.

(******************************************************************************)
(******************************************************************************)

(**
 * This is a manual translation of an invariant that cannot be converted by
 * psl2coq.
 *)
Axiom decide_rom_i_induction:
forall n : nat,
∀ π : Path, π ⊨ G(
  (
    ('clk) ∧ X('verifying) ∧ (i '== uwconst(n, 10))
  )
  -->
  (
    X(i '== uwconst(n + 1, 10))
  )
).

(******************************************************************************)

Lemma decide_i_init_clk_on:
∀ π : Path, π ⊨ G(
  (
    (negP ('verifying)) ∧ (negP ('done)) ∧ X('verifying)
  )
  -->
  (i '== uwconst(0, 10))
).
Proof.
  intros π.
  pose proof (Dependencies.decide._decide_rom_i_init_ π) as H1.
  pose proof (Dependencies.axioms.clk_is_on π) as H2.

  rewrite Thesis.toolbox.globally_to_state.
  rewrite Thesis.toolbox.globally_to_state in H1, H2.
  intros s.
  pose proof (H1 s) as H1.
  pose proof (H2 s) as H2.
  destruct H1 as [H1 | H1].
  - destruct H1 as [H1 | H1].
    + destruct H1 as [H1 | H1].
      * destruct H1 as [H1 | H1].
        -- simpl in H1, H2.
          rewrite H1 in H2.
          discriminate.
        -- left; left; left; exact H1.
      *  left; left; right; exact H1.
    +  left; right; exact H1.
  - right; exact H1.
Qed.


Theorem read_rom_init:
∀ π : Path, π ⊨ G(
  (
    (negP ('verifying)) ∧ (negP ('done)) ∧ X('verifying)
  )
  -->
  (
    X(rddata '== (ROM 0))
  )
).
Proof.
  intros π.

  pose proof (decide_i_init_clk_on π) as H.
  rewrite Thesis.toolbox.globally_to_state.
  rewrite Thesis.toolbox.globally_to_state in H.
  intros s.
  pose proof (H s) as H.
  destruct H as [H | H].
  - left; exact H.
  - assert (A0: 0 < 2^10).
    + unfold lt.
      simpl.
      intuition.
    + pose proof (Dependencies.axioms.ROM_model 0 A0) as ROM_model.
      destruct ROM_model as (H1 & ROM_model).
      pose proof (H1 π) as H1.
      rewrite Thesis.toolbox.globally_to_state in H1.
      pose proof (H1 s) as H1.
      destruct H1 as [H1 | H1].
      * apply Thesis.LTL.negP_forms in H1.
        rewrite Thesis.toolbox.negP_invol in H1.
        apply H1 in H.
        case H.
      * right.
        exact H1.
Qed.



Theorem read_rom_induction:
forall n : nat,
  (n < (2^10 - 1)) ->
∀ π : Path, π ⊨ G(
  (
    ('verifying) ∧ (rddata '== (ROM n))
  )
  -->
  (
    X(rddata '== (ROM (n+1)))
  )
).
Proof.
  intros n.
  intros Hn.
  intros π.

  pose proof (decide_rom_i_induction n π) as H.
  rewrite Thesis.toolbox.globally_to_state.
  rewrite Thesis.toolbox.globally_to_state in H.
  intros s.

  assert (A0: forall n: nat, n + 1 = S n) by intuition.

  induction s.
  - pose proof (Dependencies.decide._decide_rom_verifying_init_ π) as H1.
    simpl in H1.
    left; left.
    simpl.
    rewrite path_sub_j_0.
    pose proof (Dependencies.axioms.init_sw_arvalid π) as H2.
    destruct H1 as [H1 | H1].
    + simpl in H2.
      rewrite H1 in H2.
      discriminate.
    + exact H1.

  - pose proof (H s) as H.
    destruct H as [H | H].
      + destruct H as [H | H].
        * destruct H as [H | H].

          (* clk *)
          -- pose proof (Dependencies.axioms.clk_is_on π) as H2.
            rewrite Thesis.toolbox.globally_to_state in H2.
            pose proof (H2 s) as H2.
            simpl in H, H2.
            rewrite H in H2.
            discriminate.

          (* verifying *)
          -- left; left.
            simpl.
            simpl in H.
            rewrite Thesis.LTL.path_plus_i_j in H.
            rewrite A0 in H.
            exact H.

        (* value of i at previous state *)
        * assert (Hn1: n < 2^10).
          -- intuition.
          -- pose proof (Dependencies.axioms.ROM_model n Hn1) as ROM_model.
            destruct ROM_model as (H1 & H2).
            pose proof (H2 π) as H2.
            rewrite Thesis.toolbox.globally_to_state in H2.
            pose proof (H2 s) as H2.
            destruct H2 as [H2 | H2].
            ++ left; right.
              simpl.
              simpl in H2.
              rewrite Thesis.LTL.path_plus_i_j in H2.
              rewrite A0 in H2.
              exact H2.
            ++ apply Thesis.LTL.negP_forms in H.
              rewrite Thesis.toolbox.negP_invol in H.
              apply H in H2.
              case H2.

      (* value of i at current state *)
      + assert (Hn1: (n+1) < 2^10).
        * intuition.
        * pose proof (Dependencies.axioms.ROM_model (n+1) Hn1) as ROM_model.
          destruct ROM_model as (H1 & H2).
          pose proof (H1 π) as H1.
          rewrite Thesis.toolbox.globally_to_state in H1.
          pose proof (H1 (S s)) as H1.
          destruct H1 as [H1 | H1].
          -- rewrite Thesis.toolbox.next_at_state in H.
            rewrite A0 in H.
            apply Thesis.LTL.negP_forms in H1.
            rewrite Thesis.toolbox.negP_invol in H1.
            apply H1 in H.
            case H.
          -- right.
            exact H1.
Qed.
