#!/usr/bin/python3

#
# Copyright (C) 2022 Jonathan Certes
# jonathan.certes@online.fr
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys
# location where spot is installed:
sys.path.append("/usr/local/lib/python3.7/site-packages/")
import spot, buddy

##
# creates the automaton:
#
theBdict = spot.make_bdd_dict();
theAut = spot.make_twa_graph(theBdict)

# registers all predicates:
pA = buddy.bdd_ithvar(theAut.register_ap("a"))
pB = buddy.bdd_ithvar(theAut.register_ap("b"))

# indicates that the automaton should output as if it were state-based:
theAut.prop_state_acc(True)
# creates a finite number of states:
theAut.new_states(3)
# choses a state to be an init state:
theAut.set_init_state(2)
# creates the edges:
theAut.new_edge(2, 0,  pA & -pB)
theAut.new_edge(2, 1, -pA &  pB)
theAut.new_edge(1, 0,  pA & -pB)
theAut.new_edge(0, 1, -pA &  pB)

##
# creates the property and its negation:
#
theFormula    = spot.formula("F(a && F(b))");   # true
#theFormula    = spot.formula("G(a && F(b))");   # false
theNegFormula = spot.formula.Not(theFormula)
theFormulaAut = spot.translate(theNegFormula, dict=theBdict)

##
# create the product of the automata:
#
theProd = spot.product(theAut, theFormulaAut)

##
# export to a dot file:
#
theFileName = sys.argv[0]
theFileList = [
                [theFileName[0:-2] + "aut.dot",  theAut.to_str("dot")],
                [theFileName[0:-2] + "form.dot", theFormulaAut.to_str("dot")],
                [theFileName[0:-2] + "prod.dot", theProd.to_str("dot")],
              ]
# create files:
for theFile in theFileList:
  print("Creating file: " + theFile[0])
  theBuffer = open(theFile[0], "w")
  theBuffer.write(theFile[1])
  theBuffer.close()

