
--
-- Copyright (C) 2022 Jonathan Certes
--
-- This program is free software: you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your option) any later
-- version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU General Public License along with
-- this program. If not, see <http://www.gnu.org/licenses/>.
--

MODULE main

PSLSPEC
  NAME "When ready, received bytes are copied in order on output vectors. Case
        of data_3." :=
forall i_3 in {0:255} :
  always(
     -- starting from a reset or the end of a packet:
     ( ("clk" && !"nrst") || ("clk" && ("ready" == 0ub1_1)) )
     -- no reset until a new packet is decompressed:
  && ( next( "nrst" until ("clk" && ("ready" == 0ub1_1)) ))
     -- received byte (with index 4) is equal to i_3:
  && ( next( next_event(  "clk" && ("ready" == 0ub1_1)) ("data_size" >= 0ud4_4) ))
  && ( next( next_event_a("clk" && !"trace_ctl")[1:1] (("trace_data" & 0ub8_00000001) == 0ub8_00000001) )) -- branch packet
  && ( next( next_event_a("clk" && !"trace_ctl")[1:1] (("trace_data" & 0ub8_10000000) == 0ub8_10000000) )) -- presence of Byte 1
  && ( next( next_event_a("clk" && !"trace_ctl")[2:2] (("trace_data" & 0ub8_10000000) == 0ub8_10000000) )) -- presence of Byte 2
  && ( next( next_event_a("clk" && !"trace_ctl")[3:3] (("trace_data" & 0ub8_10000000) == 0ub8_10000000) )) -- presence of Byte 3
  && ( next( next_event_a("clk" && !"trace_ctl")[4:4] ("trace_data" == uwconst(i_3, 8)) ))
  ->
     ( next( next_event(  "clk" && ("ready" == 0ub1_1)) ("data__3" == uwconst(i_3, 8)) ))
  );

