
#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

# location where spot is installed:
SPOT="/usr/local/lib/python3.7/site-packages/"

#===============================================================================

all : proof.log
	grep tautology $<


##
# 3) running the proof:
#
proof.log : proof.py
	chmod +x ./$^
	./$^ > $@


##
# 2) translation from SMV to Spot and creation of an implication of the overall
# specifications by the conjunction of the filtered left side properties:
#
proof.py : left_side.spec.smv ${SPEC}
	../../../b_2_5_nusmvall/psl2spot.py \
	  --properties $<                   \
	  --specifications ${SPEC}          \
	  --filter ${FILTER}                \
	  --to $@
	# location where spot is installed:
	sed -i '2a import sys' $@
	sed -i '3a sys.path.append(${SPOT})' $@
	sed -i '4G' $@


##
# 1) on the left side of the implication, we have the axioms and the properties
# verified through model-checking:
#
left_side.spec.smv : ${AXIOMS} ${PROP}
	cat $^ > $@

#===============================================================================

clean :
	$(RM) left_side.spec.smv proof.py proof.log
