(*
 * Copyright (C) 2022 Guillaume Dupont
 * guillaume.dupont@toulouse-inp.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Coq.Init.Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Arith.Peano_dec.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import natutil.
Require Import bit.
Import bit.Bit.
Require Import bitvect.
Import bitvect.BitVect.
Require Import nat2bit.

(**
 * Definition of the nat2vect function, that transforms a natural into a vector of given size.
 * A few theorems and also mainly an indirect correctness proof of the function.
 * Finally the strict monotony of nat2vect (main result needed).
 *)
Section Nat2BitVect.

(**
 * Obtain the next bit in the bit vector for the given value, return that bit (as a number) and
 * the remainder of value to be transformed.
 *
 * This is essentially the inductive step for the nat2vect function.
 *)
Definition next_bit (size : nat) (value : nat) : nat * nat :=
  let q := value / 2 ^ size in
  let r := value mod 2 ^ size in
  (q, r).

(** Transforms a natural number into a bit vector of given size. *)
Fixpoint nat2vect (size : nat) (value : nat) : BitVect size :=
  match size with
  | 0 => bnil
  | S n =>
    let (b,rem) := next_bit n value in
      bcons n (nat2bit b) (nat2vect n rem)
  end.

(**
 * At every step, the bit and remainder can be recomposed into the initial value. This is essentially
 * an (informal) inductive invariant.
 *
 * To be honest this is mostly unnecessary but it is reassuring.
 *)
Theorem next_bit_step_correctness : forall size : nat, forall value : nat, value < 2 ^ (S size) ->
  let (b,r) := next_bit size value in value = b * 2 ^ size + r /\ (b <= 1) /\ (r < 2 ^ size).
Proof.
intros size value H.
unfold next_bit.
assert (2 ^ size <> 0) as Hp by (apply pow_neq_0; lia).
pose proof (Nat.div_mod value (2 ^ size)) as H0.
assert (2 ^ size - 1 <= 2 ^ size - 1) as H1 by (apply Nat.le_refl).
pose proof (H0 Hp).
split.
lia.
split.
destruct (pow_dich value size H) as [(_,H3)|(H31,H32)].
apply Nat.div_small in H3; lia.
(* lia needs a ''little bit'' of help... *)
simpl in H32.
rewrite Nat.add_0_r in H32.
assert (2 ^ size <= value < 2 ^ size + 2 ^ size) as H00 by (split; assumption).
destruct (lt_add (2 ^ size) value H00) as (m,(H41,H42)).
rewrite H42.
replace (2 ^ size + m) with (1 * 2 ^ size + m) by (rewrite Nat.mul_1_l; trivial).
rewrite Nat.div_add_l.
apply Nat.div_small in H41; rewrite H41; lia.
apply pow_neq_0; lia.
apply (Nat.mod_bound_pos value (2 ^ size)); lia.
Qed.

(** The dual of next_bit, used in vect2nat. *)
Definition bit_next (size : nat) (bitrem : nat * nat) : nat :=
  let '(b,rem) := bitrem in
    b * 2 ^ size + rem.

(**
 * bit_next and next_bit are reciprocal, provided the input value is below 2 ^ (S size).
 * (informally, if the input is greater than 2 ^ (S size) you will lose bits of information)/
 *
 * This is again not very necessary but quite useful.
 *)
Theorem bit_next_next_bit : forall size value : nat, value < 2 ^ (S size) ->
  bit_next size (next_bit size value) = value.
Proof.
intros size value H.
unfold next_bit; unfold bit_next.
symmetry.
apply next_bit_step_correctness; assumption.
Qed.

(**
 * next_bit and bit_next are reciprocal; similar to the previous theorem.
 *)
Theorem next_bit_bit_next : forall size b r : nat, b <= 1 -> r < 2 ^ size ->
  next_bit size (bit_next size (b,r)) = (b,r).
Proof.
intros size b r Hb Hr.
unfold bit_next; unfold next_bit.
f_equal.
rewrite Nat.div_add_l.
apply Nat.div_small in Hr; rewrite Hr; apply Nat.add_0_r.
apply pow_neq_0; lia.
rewrite Nat.add_comm; rewrite Nat.mod_add.
apply Nat.mod_small in Hr; rewrite Hr; reflexivity.
apply pow_neq_0; lia.
Qed.

(**
 * Transforms a vector into a natural number.
 *
 * The main problem with functions on dependent types is to carry on information from the input
 * into the output. Doing pattern matching on the vector removes the size information, that we need
 * to call bit_next (to divmod by 2 ^ size).
 *
 * The 'trick' is to pattern match on the size and decompose the vector (the decomposition is exhaustive
 * by virtue of its inductive definition). This allows to get the head, the tail, and the size of the vector.
 *)
Fixpoint vect2nat (size : nat) (vect : BitVect size) : nat :=
  match size return BitVect size -> nat with
  | 0 => fun _ => 0
  | (S n) => fun v => bit_next n (bit2nat (bvhd n v), vect2nat n (bvtl n v))
  end vect.

(**
 * vect2nat and nat2vect are reciprocal for numbers that are below 2 ^ size.
 * (again, if the output is >= 2 ^ size some bits will be missing from the result and the dual transformation
 * cannot retrieve them).
 *)
Theorem vect2nat_nat2vect : forall size n : nat, n < 2 ^ size ->
  vect2nat size (nat2vect size n) = n.
Proof.
intros size n H.
(* We need dependent induction otherwise we will have an inductive hypothesis with "n". We need an hypothesis
   with "forall n : nat" (because the whole goal of next_bit is to decompose n).
 *)
dependent induction size.
simpl; simpl in H; lia.
unfold vect2nat; unfold nat2vect.
simpl.
rewrite bit2nat_nat2bit.
rewrite IHsize. (* we use the inductive hypothesis on n := n mod 2 ^ size *)
symmetry; rewrite Nat.mul_comm; apply Nat.div_mod; apply pow_neq_0; lia.
apply Nat.mod_bound_pos.
lia.
pose proof (pow_neq_0 2 size); lia.
(* We note that 0 <= 2 ^ S size < n and deduce that either 0 <= n < 2 ^ size or 2 ^ size <= n < 2 ^ S size.
   In the first case, "div_small" tells us that n / 2 ^ size = 0.
   In the second case, we replace n with 2 ^ size + (n - 2 ^ size), with (n - 2 ^ size) < 2 ^ size, then reduce
   (2 ^ size + (n - 2 ^ size) / 2 ^ size) = 1 + 0 = 1 (<= 1).
*)
destruct (pow_dich n size H) as [(H1,H2)|(H1,H2)].
apply Nat.div_small in H2; rewrite H2; lia.
simpl in H2.
rewrite Nat.add_0_r in H2.
assert (2 ^ size <= n < 2 ^ size + 2 ^ size) by lia.
apply lt_add in H0.
destruct H0 as (m,(H31,H32)).
rewrite H32.
replace (2 ^ size + m) with (1 * 2 ^ size + m) by (rewrite Nat.mul_1_l; trivial).
rewrite Nat.div_add_l.
apply Nat.div_small in H31; rewrite H31; lia.
apply pow_neq_0; lia.
Qed.

(** vect2nat_bound is bounded by 2 ^ size. *)
Theorem vect2nat_bound : forall size : nat, forall v : BitVect size,
  vect2nat size v < 2 ^ size.
Proof.
intros size v.
bvinduction size.
simpl; lia.
simpl.
pose proof (IHsize v).
pose proof (bit2nat_bound b) as (H1,H2).
apply Nat.mul_le_mono_pos_r with (p := 2 ^ size) in H2.
rewrite Nat.mul_1_l in H2.
lia.
lia.
Qed.

(** nat2vect and vect2nat are reciprocal. *)
Theorem nat2vect_vect2nat : forall size : nat, forall v : BitVect size,
  nat2vect size (vect2nat size v) = v.
Proof.
intros size v.
bvinduction size.
simpl; reflexivity.
simpl.
assert (vect2nat size v < 2 ^ size) by (apply vect2nat_bound).
f_equal; simpl.
rewrite Nat.div_add_l.
apply Nat.div_small in H; rewrite H.
rewrite Nat.add_0_r.
apply nat2bit_bit2nat.
apply pow_neq_0; lia.
rewrite Nat.add_comm.
rewrite Nat.mod_add.
apply Nat.mod_small in H; rewrite H.
apply IHsize.
apply pow_neq_0; lia.
Qed.

(** vect2nat is monotonous. *)
Theorem vect2nat_mono : forall size : nat, forall v1 v2 : BitVect size,
  leq_vect size v1 v2 -> vect2nat size v1 <= vect2nat size v2.
Proof.
intros size v1 v2 H.
bvinduction size.
apply Nat.le_refl.
simpl.
destruct (bit2nat_bound b) as (H1,H2).
destruct (bit2nat_bound b0) as (H3,H4).
dependent destruction H.
pose proof (vect2nat_bound size v1).
pose proof (vect2nat_bound size v2).
pose proof (bit2nat_mono_strict b b0 H).
pose proof (pow_neq_0 2 size Nat.lt_0_2).
replace (bit2nat b0) with 1 by lia.
replace (bit2nat b) with 0 by lia.
simpl; lia.
pose proof (IHsize v1 v2 H0) as H5.
lia.
Qed.

(** nat2vect is monotonous. *)
Lemma nat2vect_mono : forall size : nat, forall i j : nat, i < 2 ^ size -> j < 2 ^ size ->
  i <= j -> leq_vect size (nat2vect size i) (nat2vect size j).
Proof.
intros size i j H1 H2 H3.
dependent induction size. (* we are going to need a proof in forall i j *)
(* Trivial case : bnil <= bnil *)
simpl; apply leq_vect_nil.
(* Not so trivial :| *)
simpl.
(* Assert that i / 2 ^ size <= j / 2 ^ size (because i <= j) *)
assert (i / 2 ^ size <= j / 2 ^ size).
apply Nat.div_le_mono.
apply pow_neq_0; apply Nat.lt_0_2.
assumption.
(* Assert that nat2bit (i / 2 ^ size) <= nat2bit (j / 2 ^ size) (because nat2bit is monotonous). *)
apply nat2bit_mono in H.
(* Decompose "<=" as "<" and "=". This is to line up with the cases of leq_vect (_lt, _eq) *)
apply leq_lt_invert in H.
destruct H as [H4|H4].
(* As expected, the case for "<" is not inductive so the proof is immediate. *)
apply leq_vect_cons_lt; assumption.
(* Case for "=". *)
apply leq_vect_cons_eq.
assumption. (* equality by construction in the hypotheses *)
(* 2 ^ size <> 0 is used a lot so we assert it early (often appears as a divisor). *)
pose proof (pow_neq_0 2 size Nat.lt_0_2) as Hpow2size.
(* We can use the inductive hypothesis (again, thank you dependent induction) *)
apply IHsize.
apply Nat.mod_bound_pos; lia. (* a mod b < b if b > 0 (mod_bound_pos); the rest is taken care of by lia. *)
apply Nat.mod_bound_pos; lia. (* idem *)
(* This part of the proof consists in showing that i mod 2 ^ size <= j mod 2 ^ size under our hypotheses.
   We have i <= j, so it "looks trivial", but in fact it is not, because i and/or j may be below or above
   2 ^ size (since 0 <= i < 2 ^ S size and similarly for j).
   The strategy is to split the domain of j in three:
     [0,2 ^ size[ U [2 ^ size, 2 ^ size] U ]2 ^ size,2 ^ S size[
   and perform case analysis.
   The next issue is that i moves freely below j; we in fact perform multiple "smaller" case analyses,
   exploiting the fact that nat2bit (i / 2 ^ size) = nat2bit (j / 2 ^ size) together with the properties
   of nat2bit.
*)
assert (j = 2 ^ size \/ j < 2 ^ size \/ 2 ^ size < j < 2 ^ S size) as H5 by lia. (* Partitioning *)
destruct H5 as [H5|[H5|H5]]. (* Case analysis *)
(* j = 2 ^ size *)
  (* We have nat2bit (i / 2 ^ size) = nat2bit (j / 2 ^ size) = nat2bit 1, so i / 2 ^ size > 0 *)
  rewrite H5 in H4.
  rewrite Nat.div_same in H4 by lia.
  rewrite nat2bitS in H4.
  apply nat2bit_0_lt in H4.
  (* i / 2 ^ size -> i >= 2 ^ size *)
  apply (Nat.div_str_pos_iff i (2 ^ size) Hpow2size) in H4.
  (* i <= j, j = 2 ^ size, i >= 2 ^ size -> i = 2 ^ size *)
  assert (i = 2 ^ size) by lia.
  (* i = j, proof is done! *)
  rewrite H5; rewrite H; apply Nat.le_refl.
(* j < 2 ^ size *)
  (* i <= j, j < 2 ^ size -> i < 2 ^ size *)
  assert (i < 2 ^ size) as H6 by lia.
  (* when a < b, a mod b = a (applied on i and j) *)
  apply Nat.mod_small in H5; apply Nat.mod_small in H6.
  (* i mod 2 ^ size = i and similarly for j, rewrite the goal as i <= j, exactly H3 *)
  rewrite H5; rewrite H6; assumption.
(* 2 ^ size < j < 2 ^ S size *)
  (* Prove that 2 ^ size <= i kind of my absurd (i < 2 ^ size leads to contradiction) *)
  assert (2 ^ size <= i).
  assert (2 ^ size <= i \/ i < 2 ^ size) by lia.
  destruct H as [H7|H7].
  exact H7.
  (* Now assumint i < 2 ^ size *)
    (* Replace nat2bit (i / 2 ^ size) with Z *)
    apply Nat.div_small in H7.
    rewrite H7 in H4.
    rewrite nat2bit0 in H4.
    (* 2 ^ size < j, so j / 2 ^ size > 0, so nat2bit (j / 2 ^ size) = O *)
    destruct H5 as (H5,_).
    assert (0 < j / 2 ^ size).
    apply (Nat.div_str_pos_iff j (2 ^ size)).
    assumption.
    lia.
    apply nat2bit_0_lt in H.
    (* Rewrite H, get O = Z => contradiction! *)
    rewrite H in H4.
    inversion H4.
  (* Decompose <= into = and < *)
  assert (i = 2 ^ size \/ 2 ^ size < i) by lia.
  destruct H0 as [H6|H6].
    (* i = 2 ^ size *)
    rewrite H6; rewrite Nat.mod_same; lia.
    (* 2 ^ size < i *)
    (* The strategy is similar as in the proof of vect2nat_nat2vect: we find mi, mj : nat with
         i = 2 ^ size + mi, 0 <= mi < 2 ^ size
         j = 2 ^ size + mj, 0 <= mj < 2 ^ size
         mi <= mj
       Then:
         (mi + 2 ^ size) mod 2 ^ size = mi
         (mj + 2 ^ size) mod 2 ^ size = mj
       And with mi <= mj proof is done.
     *)
    simpl in H5; rewrite Nat.add_0_r in H5.
    assert (2 ^ size < i < 2 ^ size + 2 ^ size) by lia.
    assert (exists mi mj : nat, mi <= mj /\ mi < 2 ^ size /\ mj < 2 ^ size /\ i = 2 ^ size + mi /\ j = 2 ^ size + mj).
    exists (i - 2 ^ size); exists (j - 2 ^ size).
    repeat split; try lia. (* By chance lia is able to deal with every goals. *)
    destruct H7 as (mi,(mj,(H7,(H8,(H9,(H10,H11)))))).
    rewrite H10; rewrite H11.
    rewrite !mod_red; lia.
Qed.

(** nat2vect preserve non equality. *)
Lemma nat2vect_neq : forall size : nat, forall i j : nat, i < 2 ^ size -> j < 2 ^ size ->
  i <> j -> nat2vect size i <> nat2vect size j.
Proof.
intros size i j H1 H2 H3.
(* Proof is by contradiction *)
intro H4.
(* The trick is to introduce vect2nat by forward function extensionality (x = y -> f x = f y) *)
assert (vect2nat size (nat2vect size i) = vect2nat size (nat2vect size j)).
rewrite H4; reflexivity.
(* Then we just have to use reciprocality of vect2nat and nat2vect and that's it! *)
rewrite !vect2nat_nat2vect in H by assumption.
contradiction.
Qed.

(**
 * nat2vect is strictly monotonic. This comes from the fact that nat2vect preserves <= and <>, so it preserves
 * < (which is <= /\ <>)
 *)
Theorem nat2vect_mono_strict : forall size : nat, forall i j : nat, i < 2 ^ size -> j < 2 ^ size ->
  i < j -> lt_vect size (nat2vect size i) (nat2vect size j).
Proof.
intros size i j H1 H2 H3.
split.
apply nat2vect_mono; (assumption || lia).
apply nat2vect_neq; (assumption || lia).
Qed.


End Nat2BitVect.



