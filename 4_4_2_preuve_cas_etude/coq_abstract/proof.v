(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import LTL.
Require Import bit.
Require Import bitvectgen.
Require Import nat2bitvect.
Require Import psl.

Import bit.Bit.
Import bitvect.BitVect.

(**
 * Function to convert from Bit to path formula.
 *)
Definition Bit2pathF
  (b : Bit)
: PathF :=
  match b with
  | Z => bot
  | O => top
  end.

(**
 * Selection of a range in a BitVector with implicit size.
 *)
Definition range_BitVector
  {size : nat} (* implicit arg since coq can deduce it from the size of bv *)
  (v : BitVect size)
  (from to : nat)
:= bitvect.BitVect.range size v from to.

Notation "' b"     :=   (Bit2pathF b)           (at level 100).
Notation "a '== b" :=   (eq_BitVector a b)      (at level 100).
Notation "a '<= b" :=   (le_BitVector a b)      (at level 100).
Notation "a '<  b" :=   (lt_BitVector a b)      (at level 100).
Notation "uwconst( v , s )" := (nat2vect s v)   (at level 100).

(******************************************************************************)

(**
 * Here is a proof that, if we declare predicates as uninterpreted function, we
 * can use these predicates to obtain abstract PSL properties and prove an
 * implication of the specification.
 *)
Section proof_abstraction.

(**
 * Declaration of predicates from the inputs/outputs of the decompresser.
 * Note: we renamed "in" as "input" to avoid using keywords from coq.
 *)
Variable clk reset ready enable : Bit.
Variable data_size : (BitVect 4).
Variable input     : (BitVect 8).
Variable data      : (BitVect 120).

(**
 * Declaration of predicates to abstract uninterpreted functions from PSL.
 *)
Variable abstract_next_event_a_0 abstract_next_event_a_1 abstract_next_event_a_2
         abstract_next_event_a_3 abstract_next_event_a_4 abstract_next_event_a_5
         abstract_next_event_a_6 : PathF.
Variable abstract_next_event_0   abstract_next_event_1   abstract_next_event_2
         abstract_next_event_3   abstract_next_event_4   abstract_next_event_5
         abstract_next_event_6   : PathF.

(**
 * Definition of the abstractions. These are axioms.
 *)
Axiom uninterpreted_input_0:
  ∀ i : nat, (next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i, 8)))
                = abstract_next_event_a_0.

Axiom uninterpreted_input_1:
  ∀ i : nat, (next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i, 8)))
                = abstract_next_event_a_1.

Axiom uninterpreted_input_2:
  ∀ i : nat, (next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i, 8)))
                = abstract_next_event_a_2.

Axiom uninterpreted_input_3:
  ∀ i : nat, (next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i, 8)))
                = abstract_next_event_a_3.

Axiom uninterpreted_input_4:
  ∀ i : nat, (next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i, 8)))
                = abstract_next_event_a_4.

Axiom uninterpreted_input_5:
  ∀ i : nat, (next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i, 8)))
                = abstract_next_event_a_5.

Axiom uninterpreted_input_6:
  ∀ i : nat, (next_event_a (('clk) ∧ ('enable)) 7 7 (input '== uwconst(i, 8)))
                = abstract_next_event_a_6.

Axiom uninterpreted_data_0:
  ∀ i : nat, (next_event (('clk) ∧ ('ready)) 1
                  ((range_BitVector data 0 7) '== uwconst(i, 8)))
                = abstract_next_event_0.

Axiom uninterpreted_data_1:
  ∀ i : nat, (next_event (('clk) ∧ ('ready)) 1
                  ((range_BitVector data 8 15) '== uwconst(i, 8)))
                = abstract_next_event_1.

Axiom uninterpreted_data_2:
  ∀ i : nat, (next_event (('clk) ∧ ('ready)) 1
                  ((range_BitVector data 16 23) '== uwconst(i, 8)))
                = abstract_next_event_2.

Axiom uninterpreted_data_3:
  ∀ i : nat, (next_event (('clk) ∧ ('ready)) 1
                  ((range_BitVector data 24 31) '== uwconst(i, 8)))
                = abstract_next_event_3.

Axiom uninterpreted_data_4:
  ∀ i : nat, (next_event (('clk) ∧ ('ready)) 1
                  ((range_BitVector data 32 39) '== uwconst(i, 8)))
                = abstract_next_event_4.

Axiom uninterpreted_data_5:
  ∀ i : nat, (next_event (('clk) ∧ ('ready)) 1
                  ((range_BitVector data 40 47) '== uwconst(i, 8)))
                = abstract_next_event_5.

Axiom uninterpreted_data_6:
  ∀ i : nat, (next_event (('clk) ∧ ('ready)) 1
                  ((range_BitVector data 48 55) '== uwconst(i, 8)))
                = abstract_next_event_6.


(**
 * Seven local properties that are verified through model-checking.
 *)

Hypothesis model_checked_property_0:
  ∀ π  : Path,
  ∀ i₀ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(1, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 0 7) '== uwconst(i₀, 8)))
      )
    ).

Hypothesis model_checked_property_1:
  ∀ π  : Path,
  ∀ i₁ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(2, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 8 15) '== uwconst(i₁, 8)))
      )
    ).

Hypothesis model_checked_property_2:
  ∀ π  : Path,
  ∀ i₂ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(3, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8)))
      )
    ).

Hypothesis model_checked_property_3:
  ∀ π  : Path,
  ∀ i₃ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(4, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8)))
      )
    ).

Hypothesis model_checked_property_4:
  ∀ π  : Path,
  ∀ i₄ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(5, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8)))
      )
    ).

Hypothesis model_checked_property_5:
  ∀ π  : Path,
  ∀ i₅ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(6, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i₅, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i₅, 8)))
      )
    ).

Hypothesis model_checked_property_6:
  ∀ π  : Path,
  ∀ i₆ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 7 7 (input '== uwconst(i₆, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 48 55) '== uwconst(i₆, 8)))
      )
    ).

(**
 * Proof that we can substitute our PSL uninterpreted functions from the seven
 * local properties. This is step 1 of our proof strategy.
 *)

Theorem equivalence_uninterpreted_0:
  ∀ π  : Path,
  ∀ i₀ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(1, 4) '<= data_size))
        ∧ abstract_next_event_a_0
      )
    -->
      X(
        abstract_next_event_0
      )
    ).
Proof.
  intros π.
  intros i₀.
  rewrite <- uninterpreted_input_0 with (i := i₀).
  rewrite <- uninterpreted_data_0  with (i := i₀).
  apply model_checked_property_0.
Qed.

Theorem equivalence_uninterpreted_1:
  ∀ π  : Path,
  ∀ i₁ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(2, 4) '<= data_size))
        ∧ abstract_next_event_a_1
      )
    -->
      X(
        abstract_next_event_1
      )
    ).
Proof.
  intros π.
  intros i₁.
  rewrite <- uninterpreted_input_1 with (i := i₁).
  rewrite <- uninterpreted_data_1  with (i := i₁).
  apply model_checked_property_1.
Qed.

Theorem equivalence_uninterpreted_2:
  ∀ π  : Path,
  ∀ i₂ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(3, 4) '<= data_size))
        ∧ abstract_next_event_a_2
      )
    -->
      X(
        abstract_next_event_2
      )
    ).
Proof.
  intros π.
  intros i₂.
  rewrite <- uninterpreted_input_2 with (i := i₂).
  rewrite <- uninterpreted_data_2  with (i := i₂).
  apply model_checked_property_2.
Qed.

Theorem equivalence_uninterpreted_3:
  ∀ π  : Path,
  ∀ i₃ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(4, 4) '<= data_size))
        ∧ abstract_next_event_a_3
      )
    -->
      X(
        abstract_next_event_3
      )
    ).
Proof.
  intros π.
  intros i₃.
  rewrite <- uninterpreted_input_3 with (i := i₃).
  rewrite <- uninterpreted_data_3  with (i := i₃).
  apply model_checked_property_3.
Qed.

Theorem equivalence_uninterpreted_4:
  ∀ π  : Path,
  ∀ i₄ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(5, 4) '<= data_size))
        ∧ abstract_next_event_a_4
      )
    -->
      X(
        abstract_next_event_4
      )
    ).
Proof.
  intros π.
  intros i₄.
  rewrite <- uninterpreted_input_4 with (i := i₄).
  rewrite <- uninterpreted_data_4  with (i := i₄).
  apply model_checked_property_4.
Qed.

Theorem equivalence_uninterpreted_5:
  ∀ π  : Path,
  ∀ i₅ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(6, 4) '<= data_size))
        ∧ abstract_next_event_a_5
      )
    -->
      X(
        abstract_next_event_5
      )
    ).
Proof.
  intros π.
  intros i₅.
  rewrite <- uninterpreted_input_5 with (i := i₅).
  rewrite <- uninterpreted_data_5  with (i := i₅).
  apply model_checked_property_5.
Qed.

Theorem equivalence_uninterpreted_6:
  ∀ π  : Path,
  ∀ i₆ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ abstract_next_event_a_6
      )
    -->
      X(
        abstract_next_event_6
      )
    ).
Proof.
  intros π.
  intros i₆.
  rewrite <- uninterpreted_input_6 with (i := i₆).
  rewrite <- uninterpreted_data_6  with (i := i₆).
  apply model_checked_property_6.
Qed.


(**
 * Proof that the specification is implied by the conjunction of local
 * properties. This uses predicates instead of PSL functions that are left
 * uninterpreted. This is step 2 of our proof strategy.
 *
 * Here we consider that this is proven using Spot, so we do not prove it.
 * But this can be done here as well.
 *)

Hypothesis implication_specification:
  ∀ π  : Path,
  ∀ i₀ i₁ i₂ i₃ i₄ i₅ i₆ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ abstract_next_event_a_0
        ∧ abstract_next_event_a_1
        ∧ abstract_next_event_a_2
        ∧ abstract_next_event_a_3
        ∧ abstract_next_event_a_4
        ∧ abstract_next_event_a_5
        ∧ abstract_next_event_a_6
      )
    -->
      X(
          abstract_next_event_0
        ∧ abstract_next_event_1
        ∧ abstract_next_event_2
        ∧ abstract_next_event_3
        ∧ abstract_next_event_4
        ∧ abstract_next_event_5
        ∧ abstract_next_event_6
      )
    ).


(**
 * Proof that we can substitute our PSL uninterpreted functions from the
 * specification. This is step 3 of our proof strategy.
 *)

Theorem equivalence_uninterpreted_specification:
  ∀ π  : Path,
  ∀ i₀ i₁ i₂ i₃ i₄ i₅ i₆ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8)))
        ∧ (next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8)))
        ∧ (next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8)))
        ∧ (next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8)))
        ∧ (next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8)))
        ∧ (next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i₅, 8)))
        ∧ (next_event_a (('clk) ∧ ('enable)) 7 7 (input '== uwconst(i₆, 8)))
      )
    -->
      X(
          (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8)))
        ∧ (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8)))
        ∧ (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8)))
        ∧ (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8)))
        ∧ (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8)))
        ∧ (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i₅, 8)))
        ∧ (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 48 55) '== uwconst(i₆, 8)))
      )
    ).
Proof.
  intros π.
  intros i₀ i₁ i₂ i₃ i₄ i₅ i₆.

  rewrite uninterpreted_input_0 with (i := i₀).
  rewrite uninterpreted_data_0  with (i := i₀).
  rewrite uninterpreted_input_1 with (i := i₁).
  rewrite uninterpreted_data_1  with (i := i₁).
  rewrite uninterpreted_input_2 with (i := i₂).
  rewrite uninterpreted_data_2  with (i := i₂).
  rewrite uninterpreted_input_3 with (i := i₃).
  rewrite uninterpreted_data_3  with (i := i₃).
  rewrite uninterpreted_input_4 with (i := i₄).
  rewrite uninterpreted_data_4  with (i := i₄).
  rewrite uninterpreted_input_5 with (i := i₅).
  rewrite uninterpreted_data_5  with (i := i₅).
  rewrite uninterpreted_input_6 with (i := i₆).
  rewrite uninterpreted_data_6  with (i := i₆).
  apply implication_specification.
  - exact i₀.
  - exact i₁.
  - exact i₂.
  - exact i₃.
  - exact i₄.
  - exact i₅.
  - exact i₆.
Qed.

End proof_abstraction.
