(*
 * Copyright (C) 2022 Guillaume Dupont
 * guillaume.dupont@toulouse-inp.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Coq.Init.Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Arith.Peano_dec.
Require Import Coq.Program.Equality.
Require Import LTL.
Require Import bitvect.
Import bitvect.BitVect.

Section WrapGen.

Inductive wrap_gen (phi : forall s : nat, BitVect s -> BitVect s -> Prop) :
  forall m n : nat, BitVect m -> BitVect n -> Prop :=
    | wrap_gen_same_size : forall m n : nat, forall H : m = n, forall v1 : BitVect m, forall v2 : BitVect n,
    phi m v1 (cast m n H v2) -> wrap_gen phi m n v1 v2.

Definition wrap_ungen (phi : forall s : nat, BitVect s -> BitVect s -> Prop) :
  forall s : nat, forall v1 v2 : BitVect s, wrap_gen phi s s v1 v2 <-> phi s v1 v2.
Proof.
intros s v1 v2; split; intro H.
dependent destruction H; rewrite <- cast_id in H0; assumption.
apply wrap_gen_same_size with (H := eq_refl); rewrite <- cast_id; assumption.
Qed.

Definition wrap_gen_dec
  (phi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (phi_dec : forall s : nat, forall v1 v2 : BitVect s, { phi s v1 v2 } + { ~ phi s v1 v2 }) :
    forall m n : nat, forall v1 : BitVect m, forall v2 : BitVect n,
      { wrap_gen phi m n v1 v2 } + { ~wrap_gen phi m n v1 v2 }.
Proof.
intros m n v1 v2.
destruct (Nat.eq_dec m n) as [H1|H1].
destruct (phi_dec m v1 (cast m n H1 v2)) as [H2|H2].
left; apply wrap_gen_same_size with (H := H1); exact H2.
(* The idea now is to build leq_vect m v1 (cast m n H1 v2) which contradicts H2. *)
right; intro H3.
destruct H3.
(* We have cast wtih H but we want cast with H1... that's okay because they are the same, in fact! *)
rewrite (eq_irr m n H H1) in H0.
contradiction.
(* This one is more trivial. *)
right; intro H; destruct H; contradiction.
Qed.

Definition wrap_gen_dec_eval_true
  {A}
  (phi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (phi_dec : forall s : nat, forall v1 v2 : BitVect s, { phi s v1 v2 } + { ~ phi s v1 v2 }) :
  forall a b : A, forall m n : nat, forall v1 : BitVect m, forall v2 : BitVect n,
    wrap_gen phi m n v1 v2 -> (if wrap_gen_dec phi phi_dec m n v1 v2 then a else b) = a.
Proof.
intros a b m n v1 v2 Hphi.
destruct (wrap_gen_dec phi phi_dec m n v1 v2).
reflexivity.
contradiction.
Qed.

Definition wrap_gen_dec_eval_true'
  {A}
  (phi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (phi_dec : forall s : nat, forall v1 v2 : BitVect s, { phi s v1 v2 } + { ~ phi s v1 v2 }) :
  forall a b : A, forall s : nat, forall v1 v2 : BitVect s,
    phi s v1 v2 -> (if wrap_gen_dec phi phi_dec s s v1 v2 then a else b) = a.
Proof.
intros a b s v1 v2 Hphi.
apply wrap_gen_dec_eval_true.
apply wrap_gen_same_size with (H := eq_refl).
rewrite <- cast_id; assumption.
Qed.

Definition wrap_gen_dec_eval_false
  {A}
  (phi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (phi_dec : forall s : nat, forall v1 v2 : BitVect s, { phi s v1 v2 } + { ~ phi s v1 v2 }) :
  forall a b : A, forall m n : nat, forall v1 : BitVect m, forall v2 : BitVect n,
    ~wrap_gen phi m n v1 v2 -> (if wrap_gen_dec phi phi_dec m n v1 v2 then a else b) = b.
Proof.
intros a b m n v1 v2 H.
destruct (wrap_gen_dec phi phi_dec m n v1 v2).
contradiction.
reflexivity.
Qed.

Definition wrap_gen_lift_imp
  (phi psi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (Himp : forall s : nat, forall v1 v2 : BitVect s, phi s v1 v2 -> psi s v1 v2) :
    forall m n : nat, forall v1 : BitVect m, forall v2 : BitVect n,
      wrap_gen phi m n v1 v2 -> wrap_gen psi m n v1 v2.
Proof.
intros m n v1 v2 H.
dependent destruction H.
apply wrap_gen_same_size with (H := H).
apply Himp; assumption.
Qed.

Definition wrap_bool
  (phi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (phi_dec : forall s : nat, forall v1 v2 : BitVect s, { phi s v1 v2 } + { ~ phi s v1 v2 })
  { m n : nat } (v1 : BitVect m) (v2 : BitVect n) : bool  :=
    if wrap_gen_dec phi phi_dec m n v1 v2 then true else false.

Definition wrap_bool_lift_imp
  (phi psi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (phi_dec : forall s : nat, forall v1 v2 : BitVect s, { phi s v1 v2 } + { ~ phi s v1 v2 })
  (psi_dec : forall s : nat, forall v1 v2 : BitVect s, { psi s v1 v2 } + { ~ psi s v1 v2 })
  (Himp : forall s : nat, forall v1 v2 : BitVect s, phi s v1 v2 -> psi s v1 v2) :
    forall m n : nat, forall v1 : BitVect m, forall v2 : BitVect n,
      wrap_bool phi phi_dec v1 v2 = true -> wrap_bool psi psi_dec v1 v2 = true.
Proof.
intros m n v1 v2.
unfold wrap_bool.
intro H.
case (wrap_gen_dec psi psi_dec m n v1 v2).
intro; reflexivity.
intro H1.
destruct (wrap_gen_dec phi phi_dec m n v1 v2) as [H2|H2].
apply (wrap_gen_lift_imp phi psi Himp) in H2; contradiction.
inversion H.
Qed.

Definition unwrap_bool
  (phi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (phi_dec : forall s : nat, forall v1 v2 : BitVect s, { phi s v1 v2 } + { ~ phi s v1 v2 }) :
  forall m n : nat, forall v1 : BitVect m, forall v2 : BitVect n,
    wrap_bool phi phi_dec v1 v2 = true <-> wrap_gen phi m n v1 v2.
Proof.
intros m n v1 v2; split; intro H.
unfold wrap_bool in H.
destruct (wrap_gen_dec phi phi_dec m n v1 v2).
assumption.
inversion H.
unfold wrap_bool.
apply wrap_gen_dec_eval_true.
assumption.
Qed.

Definition wrap_pathF
  (phi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (phi_dec : forall s : nat, forall v1 v2 : BitVect s, { phi s v1 v2 } + { ~ phi s v1 v2 })
  { m n : nat } (v1 : BitVect m) (v2 : BitVect n) : PathF :=
    if wrap_gen_dec phi phi_dec m n v1 v2 then top else bot.

Theorem wrap_pathF_lift_imp
  (phi psi : forall s : nat, BitVect s -> BitVect s -> Prop)
  (phi_dec : forall s : nat, forall v1 v2 : BitVect s, { phi s v1 v2 } + { ~ phi s v1 v2 })
  (psi_dec : forall s : nat, forall v1 v2 : BitVect s, { psi s v1 v2 } + { ~ psi s v1 v2 })
  (Himp : forall s : nat, forall v1 v2 : BitVect s, phi s v1 v2 -> psi s v1 v2) :
    forall pi : Path, forall s : State, forall m n : nat, forall v1 : BitVect m, forall v2 : BitVect n,
      (path_up pi s) ⊨ wrap_pathF phi phi_dec v1 v2
        -> (path_up pi s) ⊨ wrap_pathF psi psi_dec v1 v2.
Proof.
intros pi s m n v1 v2.
unfold wrap_pathF.
intro H.
destruct (wrap_gen_dec phi phi_dec m n v1 v2) as [H1|H1].
destruct (wrap_gen_dec psi psi_dec m n v1 v2) as [H2|H2].
assumption.
apply (wrap_gen_lift_imp phi psi Himp) in H1; contradiction.
apply state_top in H; contradiction.
Qed.


End WrapGen.






