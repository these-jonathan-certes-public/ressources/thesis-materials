(*
 * Copyright (C) 2022 Guillaume Dupont
 * guillaume.dupont@toulouse-inp.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Coq.Program.Equality.
Require Import LTL.
Require Import bit.
Import bit.Bit.
Require Import bitvect.
Import bitvect.BitVect.
Require Import wrapgen.

Section BitVectGen.

Definition leq_vect_gen { m n : nat } : BitVect m -> BitVect n -> Prop := wrap_gen leq_vect m n.
Definition lt_vect_gen  { m n : nat } : BitVect m -> BitVect n -> Prop := wrap_gen lt_vect m n.

Definition leq_vect_gen_dec { m n : nat } : forall v1 : BitVect m, forall v2 : BitVect n,
  { leq_vect_gen v1 v2 } + { ~leq_vect_gen v1 v2} := wrap_gen_dec leq_vect leq_vect_dec m n.
Definition lt_vect_gen_dec { m n : nat } : forall v1 : BitVect m, forall v2 : BitVect n,
  { lt_vect_gen v1 v2 } + { ~lt_vect_gen v1 v2} := wrap_gen_dec lt_vect lt_vect_dec m n.

Definition leq_vect_bool { m n : nat } : BitVect m -> BitVect n -> bool := wrap_bool leq_vect leq_vect_dec.
Definition lt_vect_bool { m n : nat } : BitVect m -> BitVect n -> bool := wrap_bool lt_vect lt_vect_dec.

Definition le_BitVector { m n : nat } : BitVect m -> BitVect n -> PathF := wrap_pathF leq_vect leq_vect_dec.
Definition lt_BitVector { m n : nat } : BitVect m -> BitVect n -> PathF := wrap_pathF lt_vect lt_vect_dec.

Definition eq_vect (s : nat) (v1 v2 : BitVect s) : Prop := v1 = v2.
Definition eq_vect_gen { m n : nat } : BitVect m -> BitVect n -> Prop := wrap_gen eq_vect m n.
Definition eq_vect_gen_dec { m n : nat } : forall v1 : BitVect m, forall v2 : BitVect n,
  { eq_vect_gen v1 v2 } + { ~eq_vect_gen v1 v2 } := wrap_gen_dec eq_vect eq_vect_dec m n.
Definition eq_BitVector { m n : nat } : BitVect m -> BitVect n -> PathF := wrap_pathF eq_vect eq_vect_dec.

Definition eq_vect_gen_eq : forall s : nat, forall v1 v2 : BitVect s, eq_vect_gen v1 v2 <-> v1 = v2 :=
  wrap_ungen eq_vect.

End BitVectGen.

