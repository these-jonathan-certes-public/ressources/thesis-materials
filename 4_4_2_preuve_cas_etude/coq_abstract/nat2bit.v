(*
 * Copyright (C) 2022 Guillaume Dupont
 * guillaume.dupont@toulouse-inp.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Coq.Init.Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Arith.Peano_dec.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import natutil.
Require Import bit.
Import bit.Bit.

(**
 * Definition of the relationship between bits and natural numbers.
 * nat2bit transforms a natural into a bit, and bit2nat does the opposite.
 *
 * Note that nat2bit and bit2nat are not exactly reciprocal, because the domain of nat2bit is the
 * entire nat, but the codomain of bit2nat is {0,1} (nat and {0,1} don't have the same cardinality).
 *)
Section Nat2Bit.

(** The (decidable) test that a number is 0. *)
Definition is_zero (n : nat) : { n = 0 } + { n > 0 }.
Proof.
destruct (Nat.eq_dec n 0) as [H|H]. (* Equality is decidable on naturals, i.e. { n = 0 } + { n <> 0 } *)
left; assumption.
right; lia. (* n <> 0 implies n > 0 for naturals *)
Qed.

(** Evaluate is_zero 0. *)
Lemma is_zero_0 {A} : forall a b : A, (if is_zero 0 then a else b) = a.
Proof.
intros a b.
destruct (is_zero 0) as [H|H].
reflexivity. (* 0 = 0 *)
inversion H. (* 0 > 0 leads to a contradiction *)
Qed.

(** Evaluate is_zero (S n) (for any n). *)
Lemma is_zero_S {A} : forall a b : A, forall n : nat, (if is_zero (S n) then a else b) = b.
Proof.
intros a b n.
destruct (is_zero (S n)) as [H|H].
inversion H. (* S n = 0 is a contradiction (because 0 and S are separate constructors of the type nat *)
reflexivity. (* b = b *)
Qed.

(** Evaluate is_zero n when n > 0. *)
Lemma is_zero_lt_0 {A} : forall a b : A, forall n : nat, n > 0 -> (if is_zero n then a else b) = b.
Proof.
intros a b n H.
destruct n.
inversion H. (* 0 > 0 leads to a contradiction *)
apply is_zero_S.
Qed.

(**
 * Transform a natural to a bit. This function is *total*, in the sense that it accepts *any* natural as
 * input. We could go with a *partial* function (only defined on {0,1}) but it is not necessary for any proof
 * and tends to make proof a bit more cumbersome.
 *
 * As a tradeoff, nat2bit is not *injective* (and so not *bijective* and so we canno build a reciprocal
 * function).
 *)
Definition nat2bit (n : nat) : Bit :=
  if is_zero n then Z else O.

(** If the output of nat2bit is Z then its input must be 0. *)
Theorem nat2bit_Z : forall n : nat, nat2bit n = Z -> n = 0.
Proof.
intro n.
unfold nat2bit.
destruct n.
intro; reflexivity.
rewrite is_zero_S; intro H; inversion H. (* This leads to Z = O (contradiction) *)
Qed.

(** Evaluate nat2bit 0. *)
Theorem nat2bit0 : nat2bit 0 = Z.
Proof.
unfold nat2bit.
rewrite is_zero_0; reflexivity.
Qed.

(** Evaluate nat2bit (S n) for any n. *)
Theorem nat2bitS : forall n : nat, nat2bit (S n) = O.
Proof.
intro n.
unfold nat2bit.
rewrite is_zero_S; reflexivity.
Qed.

(**
 * Evaluate nat2bit n when n > 0. Also deduce that if nat2bit n = O then n > 0.
 *)
Theorem nat2bit_0_lt : forall n : nat, 0 < n <-> nat2bit n = O.
Proof.
intros n; split; intro H.
unfold nat2bit.
rewrite is_zero_lt_0.
reflexivity.
lia.
destruct n.
rewrite nat2bit0 in H; inversion H.
lia.
Qed.

(** nat2bit is monotonous (from (nat, <=) to (Bit, <=)). *)
Theorem nat2bit_mono : forall i j : nat, i <= j -> (nat2bit i <= nat2bit j)%B.
Proof.
intros i j H.
destruct i.
destruct j.
apply leq_bit_refl.
rewrite nat2bit0.
apply leq_bit_min.
replace j with (S (pred j)) by lia. (* A lil trick: j = S (pred j) where pred is predecessor function (if j > 0) *)
rewrite !nat2bitS.
apply leq_bit_refl.
Qed.

(** bit2nat transforms a bit into a natural. This is the logical dual of nat2bit. *)
Definition bit2nat (b : Bit) : nat :=
  match b with
  | O => 1
  | Z => 0
  end.

(** The output of bit2nat is bounded. *)
Theorem bit2nat_bound : forall b : Bit, 0 <= bit2nat b <= 1.
Proof.
intro b.
case b; simpl; lia. (* Simple case alaysis *)
Qed.

(** IF THE INPUT IS 0 OR 1 then bit2nat and nat2bit are reciprocal for that particular input. *)
Theorem bit2nat_nat2bit : forall n : nat, 0 <= n <= 1 ->
  bit2nat (nat2bit n) = n.
Proof.
intros n H.
unfold bit2nat; unfold nat2bit.
assert (n = 0 \/ n = 1) by lia. (* Because 0 <= n <= 1, either n = 0 or n = 1 *)
destruct H0 as [H0|H0]; rewrite H0. (* Case analysis *)
rewrite is_zero_0; reflexivity.
rewrite is_zero_S; reflexivity.
Qed.

(** This order of composition always works (because the output of nat2bit is bounded). *)
Lemma nat2bit_bit2nat : forall b : Bit,
  nat2bit (bit2nat b) = b.
Proof.
intro b.
unfold bit2nat; unfold nat2bit.
case b.
apply is_zero_0; reflexivity.
apply is_zero_S; reflexivity.
Qed.

(** bit2nat is monotonous (from (Bit,<=) to (nat,<=)). *)
Theorem bit2nat_mono : forall b1 b2 : Bit, (b1 <= b2)%B -> bit2nat b1 <= bit2nat b2.
Proof.
intros b1 b2 H.
inversion H.
case b2; simpl; apply Nat.le_refl.
simpl; lia.
Qed.

(** bit2nat is *strictly* monotonous, i.e. it preserves strict orders (from (Bit,<) to (nat,<)). *)
Theorem bit2nat_mono_strict : forall b1 b2 : Bit, (b1 < b2)%B -> bit2nat b1 < bit2nat b2.
Proof.
unfold lt_bit.
intros b1 b2 (H1,H2).
inversion H1.
contradiction.
simpl; lia.
Qed.


End Nat2Bit.



