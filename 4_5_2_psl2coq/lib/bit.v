(*
 * Copyright (C) 2022 Guillaume Dupont
 * guillaume.dupont@toulouse-inp.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Coq.Init.Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Arith.Peano_dec.
Require Import Psatz.
Require Import natutil.

(**
 * Thid module define the Bit type as well as a comparison operator and a few
 * useful theorems.
 *
 * A bit is either a zero (Z) or a one (O). There exists a total order on bits (leq_bit)
 * associated to a strict order (lt_bit). Equality and orders are decidable (xxx_dec definitions).
 *)
Module Bit.

(* We define a custom scope for operators defined on bits (for readbaility) *)
Declare Scope Bit_scope.
Delimit Scope Bit_scope with B.

Open Scope Bit_scope.

(**
 * Definition of a Bit.
 *)
Inductive Bit : Set :=
  | Z : Bit   (** Z = a Bit equal to zero *)
  | O : Bit   (** O = a Bit equal to one  *)
  .

(**
 * Definition of the order on bits (<=) as an inductive predicate. This order follows general
 * intuition (b <= b and 0 <= 1).
 *)
Inductive leq_bit : Bit -> Bit -> Prop :=
  | leq_bit_refl : forall b : Bit, leq_bit b b  (** any bit is lower or equal to itself *)
  | leq_bit_Z_O  : leq_bit Z O                  (** 0 <= 1 *)
  .

(** A notation for the order *)
Infix "<=" := leq_bit : Bit_scope.

(**
 * Definition/proof that equality is decidable for bits. This relies on the fact that the
 * type Bit is a non-inductive, finite enumeration, and the proof is simply a big case
 * analysis.
 *)
Definition eq_bit_dec : forall b1 b2 : Bit, { b1 = b2 } + { b1 <> b2 }.
Proof.
intros b1 b2.
destruct b1; destruct b2.
(* We get the 4 cases (Z,Z),(Z,O),(O,Z),(O,O). Cases (Z,Z) and (O,O) are solved by reflexivity,
 * cases (Z,O) and (O,Z) are solved with "intro H; inversion H" that makes the proof that having
 * "Z = O" (or "O = Z") leads to a contradiction.
 *
 * "inversion" is used here to use the fact that different constructors of an inductive type are
 * incompatible (different, not coercible).
 *)
left; reflexivity.
right; intro H; inversion H.
right; intro H; inversion H.
left; reflexivity.
Qed.

(** A notation for decidable equality (can be used in if/then/else typically) *)
Infix "=?" := eq_bit_dec : Bit_scope.

(**
 * Definition/proof that leq is decidable for bits. Again, Bit is finite and leq is non-inductive,
 * so we proceed with exhaustive case analysis.
 *)
Definition leq_bit_dec : forall b1 b2 : Bit, { b1 <= b2 } + { ~ (b1 <= b2) }.
Proof.
intros b1 b2.
destruct b1; destruct b2.
(* We get the 4 cases (Z,Z),(Z,O),(O,Z),(O,O). We solve (O,Z) by using the inversion technique:
 * no constructor allows to deduce (O,Z), so the predicate is false for this pair (similar to a proof
 * by absurd).
 *
 * For (Z,O) we use the leq_bit_Z_O case/constructor of leq_bit, and for (Z,Z)/(O,O) we use leq_bit_refl.
 *)
left; apply leq_bit_refl.
left; apply leq_bit_Z_O.
right; intro H; inversion H.
left; apply leq_bit_refl.
Qed.

(** A notation for decidable order (can be used in if/then/else) *)
Infix "<=?" := leq_bit_dec : Bit_scope.

(** The order "<=" has a minimum (Z). *)
Theorem leq_bit_min : forall b : Bit, Z <= b.
Proof.
intro b.
destruct b.
apply leq_bit_refl.
apply leq_bit_Z_O.
Qed.

(** The order "<=" is antisymetrical. *)
Theorem leq_bit_antisym : forall b1 b2 : Bit, b1 <= b2 -> b2 <= b1 -> b1 = b2.
Proof.
intros b1 b2 H1 H2.
destruct b1; destruct b2.
reflexivity.
inversion H2. (* Having O <= Z in hypothesis leads to a contradiction (=> proof ex falso) *)
inversion H1. (* idem *)
reflexivity.
Qed.

(** The order "<=" is transitive. *)
Theorem leq_bit_tran : forall b1 b2 b3 : Bit, b1 <= b2 -> b2 <= b3 -> b1 <= b3.
Proof.
intros b1 b2 b3 H1 H2.
destruct b1.
apply leq_bit_min.
(*
 This usage of inversion performs a form of 'case analysis' for "<=".
 Basically, inversion here finds "under which circumpstances H1 holds?". It deduces that
 O <= b2 is only true via leq_bit_refl and so b2 = O (because otherwise leq_bit_Z_O does not have O
 on the left).
*)
inversion H1.
rewrite H0; assumption.
Qed.

(* Note: the order "<=" is reflexive by construction (leq_bit_refl in the inductive predicate) *)

(** The order "<=" is total. *)
Theorem leq_bit_total : forall b1 b2 : Bit, { b1 <= b2 } + { b2 <= b1 }.
Proof.
intros b1 b2.
destruct b1.
left; apply leq_bit_min.
destruct b2.
right; apply leq_bit_min.
left; apply leq_bit_refl.
Qed.

(** Definition of the associated strict order "<" *)
Definition lt_bit (b1 b2 : Bit) : Prop := b1 <= b2 /\ b1 <> b2.

(** Notation for the strict order *)
Infix "<" := lt_bit : Bit_scope.

(**
 * Definition/proof that "<" is decidable. This is trivial since "<=" and "=" are both decidable.
 *)
Definition lt_bit_dec : forall b1 b2 : Bit, { b1 < b2 } + { ~(b1 < b2) }.
Proof.
intros b1 b2.
unfold "<".
destruct (leq_bit_dec b1 b2) as [H1|H1].
destruct (eq_bit_dec b1 b2) as [H2|H2].
right; intros (H3,H4); contradiction.
left; split; assumption.
right; intros (H3,H4); contradiction.
Qed.

(** Notation for decidable strict order (can be used in if/then/else). *)
Infix "<?" := lt_bit_dec : Bit_scope.

(** A few theorems to transform "<" and "<=", namely. *)
Theorem leq_not_lt_bit : forall b1 b2 : Bit, b1 <= b2 -> ~(b2 < b1).
Proof.
intros b1 b2 H1.
unfold "<".
intros (H2,H3).
pose proof (leq_bit_antisym b1 b2 H1 H2) as H4; symmetry in H4; contradiction.
Qed.

Theorem lt_not_leq_bit : forall b1 b2 : Bit, b1 < b2 -> ~(b2 <= b1).
Proof.
intros b1 b2;
unfold "<".
intros (H1,H2) H3.
pose proof (leq_bit_antisym b1 b2 H1 H3) as H4; contradiction.
Qed.

Theorem not_lt_leq_bit : forall b1 b2 : Bit, ~(b1 < b2) -> b2 <= b1.
Proof.
unfold "<".
intros b1 b2 H1.
destruct (leq_bit_total b1 b2) as [H2|H2].
destruct (eq_bit_dec b1 b2) as [H3|H3].
rewrite H3; apply leq_bit_refl.
assert (b1 <= b2 /\ b1 <> b2) by (split; assumption); apply H1 in H; contradiction.
assumption.
Qed.

Theorem not_leq_lt_bit : forall b1 b2 : Bit, ~(b1 <= b2) -> b2 < b1.
Proof.
intros b1 b2 H1.
split.
apply not_lt_leq_bit; intro H2.
unfold "<" in H2; destruct H2 as (H2,H3); contradiction.
intro H2; assert (b1 <= b2) as H3 by (rewrite H2; apply leq_bit_refl); contradiction.
Qed.

(** As a strict order, "<" is asymetrical. *)
Theorem lt_bit_asym : forall b : Bit, ~(b < b).
Proof.
unfold "<".
intros b (H1,H2).
contradiction. (* b <> b is always a contradiction (for obvious reasons) *)
Qed.

(** Another formulation of asymetry, that follows antisymetry. *)
Theorem lt_bit_asym' : forall b1 b2 : Bit, b1 < b2 -> b2 < b1 -> False.
Proof.
unfold "<".
intros b1 b2 (H1,H2) (H3,H4).
pose proof (leq_bit_antisym b1 b2 H1 H3).
contradiction.
Qed.

(** Other formulation of the totality of "<=". *)
Theorem leq_lt_bit : forall b1 b2 : Bit, { b1 <= b2 } + { b2 < b1 }.
Proof.
intros b1 b2.
destruct (leq_bit_total b1 b2) as [H1|H1].
left; assumption.
destruct (eq_bit_dec b2 b1) as [H2|H2].
left; rewrite H2; apply leq_bit_refl.
right; unfold "<"; split; assumption.
Qed.

(** Expression of "<=" using "<". *)
Theorem leq_lt_invert : forall b1 b2 : Bit, b1 <= b2 -> (b1 < b2 \/ b1 = b2).
Proof.
unfold "<".
intros b1 b2 H1.
destruct H1.
right; reflexivity.
left; split.
apply leq_bit_Z_O.
intro H; inversion H.
Qed.

Close Scope Bit_scope.

End Bit.




