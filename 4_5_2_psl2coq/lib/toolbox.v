(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import LTL.
Require Import bit.
Require Import bitvect.
Require Import nat2bitvect.
Require Import truefor.
Require Import bitvectltl.
Require Import psl.

Import bit.Bit.
Import bitvect.BitVect.


(**
 * Selection of a range in a BitVector with implicit size.
 *
 * \warning
 *  bitvect.BitVect.range is excluding argument "to": [from,to[, so here we
 *  select from "from" to "(S to)".
 *)
Definition range_BitVector
  {size : nat} (* implicit arg since coq can deduce it from the size of bv *)
  (v : BitVect size)
  (from to : nat)
:= bitvect.BitVect.range size v from (S to).


(**
 * \brief
 *  Simplify an implication in a globally operator.
 *)
Theorem simpl_globally:
  ∀ π : Path,
  ∀ h p : PathF,
  (
    π ⊨ G(h --> p)
  <->
    ∀ s : State, path_up π s ⊨ negP h \/ path_up π s ⊨ p
  ).
Proof.
  intros π.
  intros h p.
  split.
  - intros H.
    simpl in H.
    destruct H as [H1 | H2].
    + exact H1.
    + destruct H2 as (s0  & H2 ).
      destruct H2 as (H2a & H2b).
      apply state_top in H2a.
      case H2a.
  - intros H.
    simpl.
    left.
    exact H.
Qed.


Theorem top_eq_not_bot:
  top = negP bot.
Proof.
  unfold top.
  unfold bot.
  unfold negP.
  tauto.
Qed.

Theorem bot_eq_not_top:
  bot = negP top.
Proof.
  reflexivity.
Qed.

(**
 * \brief
 *  Proof that applying negP twice on one formula is like not apllying at all.
 *)
Theorem negP_invol :
  forall p : PathF,
    negP (negP p) = p.
Proof.
  intros p.

  induction p;
  (* we apply these two tactics on each branch of the induction: *)
  (simpl; intuition).
  (* this application removes the two first branches. Then: *)
  - rewrite IHp1.
    rewrite IHp2.
    reflexivity.
  - rewrite IHp1; rewrite IHp2; reflexivity.
  - rewrite IHp1; rewrite IHp2; reflexivity.
  - rewrite IHp1; rewrite IHp2; reflexivity.
  - rewrite IHp; reflexivity.
Qed.

(**
 * \brief
 *  Distributivity of the negation over the conjunction.
 *)
Theorem distr_negP_and:
  forall p q : PathF,
    negP (p ∧ q) = (negP p) ∨ (negP q).
Proof.
  intros p q.
  reflexivity.
Qed.

(**
 * \brief
 *  Distributivity of the negation over the disjuction.
 *)
Theorem distr_negP_or:
  forall p q : PathF,
    negP (p ∨ q) = (negP p) ∧ (negP q).
Proof.
  intros p q.
  reflexivity.
Qed.

Theorem and_definition :
  forall a b : PathF,
    a ∧ b = negP (negP a ∨ negP b).
Proof.
  intros a b.
  rewrite distr_negP_or.
  rewrite negP_invol.
  rewrite negP_invol.
  reflexivity.
Qed.

Theorem or_definition :
  forall a b : PathF,
    a ∨ b = negP (negP a ∧ negP b).
Proof.
  intros a b.
  rewrite distr_negP_and.
  rewrite negP_invol.
  rewrite negP_invol.
  reflexivity.
Qed.

(*****************************************************************************)

(* a tactic to simplify hypothesis with boolean equalities in it: *)

Lemma true_eq_false:
  (true = false) <-> False.
Proof.
  split.
  - intros H; discriminate.
  - intros H. case H.
Qed.

Lemma false_eq_true:
  (false = true) <-> False.
Proof.
  split.
  - intros H; discriminate.
  - intros H. case H.
Qed.

Lemma true_eq_true:
  (true = true) <-> True.
Proof.
  split.
  - intros H; intuition.
  - intros H. reflexivity.
Qed.

Lemma false_eq_false:
  (false = false) <-> True.
Proof.
  split.
  - intros H; intuition.
  - intros H. reflexivity.
Qed.

Lemma Prop_False_or:
  forall A : Prop,
    (False \/ A)
  <->
    (A \/ False).
Proof.
  intros A.
  split.
  - intros H.
    destruct H as [H | H].
    + right; exact H.
    + left;  exact H.
  - intros H.
    destruct H as [H | H].
    + right; exact H.
    + left;  exact H.
Qed.

Lemma Prop_True_or:
  forall A : Prop,
    (True \/ A)
  <->
    (A \/ True).
Proof.
  intros A.
  split.
  - intros H.
    destruct H as [H | H].
    + right; exact H.
    + left;  exact H.
  - intros H.
    destruct H as [H | H].
    + right; exact H.
    + left;  exact H.
Qed.

Lemma Prop_False_and:
  forall A : Prop,
    (False /\ A)
  <->
    (A /\ False).
Proof.
  intros A.
  split.
  - intros H.
    destruct H as (H1 & H2).
    split.
    + exact H2.
    + exact H1.
  - intros H.
    destruct H as (H1 & H2).
    split.
    + exact H2.
    + exact H1.
Qed.

Lemma Prop_True_and:
  forall A : Prop,
    (True /\ A)
  <->
    (A /\ True).
Proof.
  intros A.
  split.
  - intros H.
    destruct H as (H1 & H2).
    split.
    + exact H2.
    + exact H1.
  - intros H.
    destruct H as (H1 & H2).
    split.
    + exact H2.
    + exact H1.
Qed.

Lemma Prop_or_False:
  forall A : Prop,
    (A \/ False)
  <->
    A.
Proof.
  intros A.
  split.
  - intros H.
    destruct H.
    + exact H.
    + case H.
  - intros H.
    left.
    exact H.
Qed.

Lemma Prop_or_True:
  forall A : Prop,
    (A \/ True)
  <->
    True.
Proof.
  intros A.
  split.
  - intros H.
    destruct H.
    + intuition.
    + exact H.
  - intros H.
    right.
    exact H.
Qed.

Lemma Prop_and_False:
  forall A : Prop,
    (A /\ False)
  <->
    False.
Proof.
  intros A.
  split.
  - intros H.
    destruct H.
    + case H0.
  - intros H.
    case H.
Qed.

Lemma Prop_and_True:
  forall A : Prop,
    (A /\ True)
  <->
    A.
Proof.
  intros A.
  split.
  - intros H.
    destruct H.
    + exact H.
  - intros H.
    split.
    + exact H.
    + intuition.
Qed.

Ltac simpl_all_bool_in H :=
  try repeat rewrite true_eq_false  in H;
  try repeat rewrite false_eq_true  in H;
  try repeat rewrite true_eq_true   in H;
  try repeat rewrite false_eq_false in H;
  try repeat rewrite Prop_False_or  in H;
  try repeat rewrite Prop_True_or   in H;
  try repeat rewrite Prop_False_and in H;
  try repeat rewrite Prop_True_and  in H;
  try repeat rewrite Prop_or_False  in H;
  try repeat rewrite Prop_or_True   in H;
  try repeat rewrite Prop_and_False in H;
  try repeat rewrite Prop_and_True  in H;
  try repeat rewrite Prop_or_False  in H;
  try repeat rewrite Prop_or_True   in H.

Ltac simpl_all_bool :=
  try repeat rewrite true_eq_false;
  try repeat rewrite false_eq_true;
  try repeat rewrite true_eq_true;
  try repeat rewrite false_eq_false;
  try repeat rewrite Prop_False_or;
  try repeat rewrite Prop_True_or;
  try repeat rewrite Prop_False_and;
  try repeat rewrite Prop_True_and;
  try repeat rewrite Prop_or_False;
  try repeat rewrite Prop_or_True;
  try repeat rewrite Prop_and_False;
  try repeat rewrite Prop_and_True;
  try repeat rewrite Prop_or_False;
  try repeat rewrite Prop_or_True.

(*****************************************************************************)

Theorem equiv_and:
  forall π : Path,
    forall p q : PathF,
      (π ⊨ (p ∧ q))
    <->
      (π ⊨ (p) /\ π ⊨ (q)).
Proof.
  intros π.
  intros p q.

  split.  (* equivalence is two implications *)
  - simpl.
    intros H.
    exact H.

  - simpl.
    intros H.
    exact H.
Qed.

Theorem equiv_or:
  forall π : Path,
    forall p q : PathF,
      (π ⊨ (p ∨ q))
    <->
      (π ⊨ (p) \/ π ⊨ (q)).
Proof.
  intros π.
  intros p q.

  split.
  (* equivalence is two implications *)
  - simpl.
    intros H.
    exact H.

  - simpl.
    intros H.
    exact H.
Qed.

(**
 * \brief
 *  Proves the distributivity of the conjunction over the disjunction for
 *  temporal formulae.
 *)
Lemma distr_and_or :
  forall π : Path,
    forall ф₁ ф₂ ф₃ : PathF,
      π ⊨ ф₁ ∧ (ф₂ ∨ ф₃)
    <->
      π ⊨ (ф₁ ∧ ф₂) ∨ (ф₁ ∧ ф₃).
Proof.
  split.

  (* -> *)
  - intros.
    apply equiv_or.
    destruct H as (H1 & H2).
    case H2.
    * intros H3.
      left.
      split.
        + exact H1.
        + exact H3.
    * intros H3.
      right.
      split.
        + exact H1.
        + exact H3.

  (* <- *)
  - intros.
    apply equiv_and.
    case H.
    * intros H3.
      split.
      + destruct H3 as (H3 & H4).
        exact H3.
      + destruct H3 as (H3 & H4).
        left.
        exact H4.
    * intros H3.
      split.
      + destruct H3 as (H3 & H4).
        exact H3.
      + destruct H3 as (H3 & H4).
        right.
        exact H4.
Qed.


Theorem and_commutes:
  forall π : Path,
    forall p q : PathF,
      (π ⊨ (p ∧ q))
    <->
      (π ⊨ (q ∧ p)).
Proof.
  intros.

  split;
  (* we apply these tactics on each branch of the split: *)
  ( intro;
    rewrite equiv_and in H;
    destruct H as (H1 & H2);
    rewrite equiv_and;
    split
  ).
    - exact H2.
    - exact H1.
    - exact H2.
    - exact H1.
Qed.


Theorem or_commutes:
  forall π : Path,
    forall p q : PathF,
      (π ⊨ (p ∨ q))
    <->
      (π ⊨ (q ∨ p)).
Proof.
  intros.

  split;
  (* we apply these tactics on each branch of the split: *)
  ( intro;
    rewrite equiv_or in H;
    destruct H as [H1 | H2];
    rewrite equiv_or
  ).
  - right; exact H1.
  - left;  exact H2.
  - right; exact H1.
  - left;  exact H2.
Qed.


Theorem bot_and_p:
  forall π : Path,
    forall p : PathF,
        (π ⊨ bot ∧ p)
      <->
        (π ⊨ bot).
Proof.
  intros π.
  intros p.

  simpl.
  split.
  - intros H.
    destruct H as (H1 & H2).
    exact H1.
  - intros H.
    split.
    * exact H.
    * apply state_top in H.
      case H.
Qed.


Theorem bot_is_false:
  forall π : Path, ( π ⊨ bot ) -> False.
Proof.
  intros π.
  intros H.
  unfold bot in H.
  apply state_top in H.
  exact H.
Qed.


Theorem top_or_p:
  forall π : Path,
    forall p : PathF,
        (π ⊨ top ∨ p)
      <->
        (π ⊨ top).
Proof.
  intros π.
  intros p.

  simpl.
  split.
  - intros H.
    destruct H as [H1 | H2].
    * exact H1.
    * apply state_bot.
  - intros H.
    left.
    exact H.
Qed.


Theorem top_is_true:
  forall π : Path, ( π ⊨ top ).
Proof.
  intros π.
  rewrite top_eq_not_bot.
  unfold bot.
  unfold negP.
  apply state_bot.
Qed.

(*****************************************************************************)

(**
 * \brief
 *  Relation between eventually and globally temporal operators.
 *  Proof that F(p) <-> ¬G(¬p).
 *)
Theorem eventually_globally:
  forall π : Path,
    forall p : PathF,
      ( π ⊨ F(p) )
    <->
      ( π ⊨ negP (G(negP p)) ).
Proof.
  intros π.
  intros p.

  split.
  - intros H.
    unfold F in H.
    unfold G.
    simpl negP.
    unfold top in H.
    rewrite negP_invol.
    exact H.

  - intros H.
    unfold F.
    unfold G in H.
    simpl negP in H.
    unfold top.
    rewrite negP_invol in H.
    exact H.
Qed.

(**
 * \brief
 *  Relation between eventually and globally temporal operators.
 *  Proof that G(p) <-> ¬F(¬p).
 *)
Theorem globally_eventually:
  forall π : Path,
    forall p : PathF,
      ( π ⊨ G(p) )
    <->
      ( π ⊨ negP (F(negP p)) ).
Proof.
  intros π.
  intros p.

  split.
  - intros H.
    unfold G in H.
    unfold F.
    simpl negP.
    unfold bot in H.
    rewrite negP_invol.
    exact H.

  - intros H.
    unfold G.
    unfold F in H.
    simpl negP in H.
    unfold bot.
    rewrite negP_invol in H.
    exact H.
Qed.

(**
 * \brief
 *  Relation between the negation of eventually and globally temporal
 *  operators.
 *  Proof that ¬F(p) <-> G(¬p).
 *)
Theorem negP_eventually:
  forall π : Path,
    forall p : PathF,
      ( π ⊨ negP (F(p)) )
    <->
      ( π ⊨ G(negP p) ).
Proof.
  intros π.
  intros p.

  split.
  - intros H.
    rewrite globally_eventually.
    rewrite negP_invol.
    exact H.
  - intros H.
    rewrite globally_eventually in H.
    rewrite negP_invol in H.
    exact H.
Qed.

(**
 * \brief
 *  Relation between the negation of globally and eventually temporal
 *  operators.
 *  Proof that ¬G(p) <-> F(¬p).
 *)
Theorem negP_globally:
  forall π : Path,
    forall p : PathF,
      ( π ⊨ negP (G(p)) )
    <->
      ( π ⊨ F(negP p) ).
Proof.
  intros π.
  intros p.

  split.
  - intros H.
    rewrite eventually_globally.
    rewrite negP_invol.
    exact H.
  - intros H.
    rewrite eventually_globally in H.
    rewrite negP_invol in H.
    exact H.
Qed.


(**
 *  Proof that using coq and operator "/\" on two LTL globally true properties
 *  is equivalent to having one globally true LTL propery with a conjunction.
 *)
Theorem globally_equiv_and:
  forall π : Path,
    forall p q : PathF,
      (π ⊨ G(p ∧ q))
    <->
      (π ⊨ G(p) /\ π ⊨ G(q)).
Proof.
  intros π.
  intros p q.

  split.  (* equivalence is two implications *)
  - intros H.
    apply distr_G_ConjP in H.
    destruct H as (H1 & H2).
    split.
    * exact H1.
    * exact H2.

  - intros H.
    apply distr_G_ConjP in H.
    exact H.
Qed.

Theorem eventually_equiv_or:
  forall π : Path,
    forall p q : PathF,
      (π ⊨ F(p ∨ q))
    <->
      (π ⊨ F(p) \/ π ⊨ F(q)).
Proof.
  intros π.
  intros p q.

  split.  (* equivalence is two implications *)
  - intros H.
    apply distr_F_DisyP.
    exact H.
  - intros H.
    case H;
    ( intros H1;
      apply distr_F_DisyP in H;
      exact H
    ).
Qed.

(**
 * \brief
 *  Distributivity of the negation over the conjunction in globally.
 *)
Theorem globally_distr_negP_and:
  forall π : Path,
    forall p q : PathF,
      π ⊨ G(negP (p ∧ q))
    <->
      π ⊨ G( (negP p) ∨ (negP q) ).
Proof.
  intros π.
  intros p q.
  reflexivity.
Qed.

(**
 * \brief
 *  Distributivity of the negation over the disjuction in globally.
 *)
Theorem globally_distr_negP_or:
  forall π : Path,
    forall p q : PathF,
      π ⊨ G(negP (p ∨ q))
    <->
      π ⊨ G( (negP p) ∧ (negP q) ).
Proof.
  intros π.
  intros p q.
  reflexivity.
Qed.

(**
 * \brief
 *  Distributivity of the negation over the conjunction in eventually.
 *)
Theorem eventually_distr_negP_and:
  forall π : Path,
    forall p q : PathF,
      π ⊨ F(negP (p ∧ q))
    <->
      π ⊨ F( (negP p) ∨ (negP q) ).
Proof.
  intros π.
  intros p q.
  reflexivity.
Qed.

(**
 * \brief
 *  Distributivity of the negation over the disjuction in eventually.
 *)
Theorem eventually_distr_negP_or:
  forall π : Path,
    forall p q : PathF,
      π ⊨ F(negP (p ∨ q))
    <->
      π ⊨ F( (negP p) ∧ (negP q) ).
Proof.
  intros π.
  intros p q.
  reflexivity.
Qed.


Theorem globally_and_commutes:
  forall π : Path,
    forall p q : PathF,
      (π ⊨ G(p ∧ q))
    <->
      (π ⊨ G(q ∧ p)).
Proof.
  intros π.
  intros p q.

  split.
  - intros.
    rewrite globally_equiv_and in *.
    destruct H as (H1 & H2).
    split.
    * exact H2.
    * exact H1.
  - intros.
    rewrite globally_equiv_and in *.
    destruct H as (H1 & H2).
    split.
    * exact H2.
    * exact H1.
Qed.

Theorem eventually_or_commutes:
  forall π : Path,
    forall p q : PathF,
      (π ⊨ F(p ∨ q))
    <->
      (π ⊨ F(q ∨ p)).
Proof.
  intros π.
  intros p q.

  split.
  - intros.
    rewrite eventually_equiv_or in *.
    destruct H as [H1 | H2].
    * right; exact H1.
    * left;  exact H2.
  - intros.
    rewrite eventually_equiv_or in *.
    destruct H as [H1 | H2].
    * right; exact H1.
    * left;  exact H2.
Qed.

Theorem eventually_next:
  forall π : Path,
    forall p : PathF,
      ( π ⊨ F(p) )
    <->
      ( π ⊨ p ∨ (X (F(p))) ).
Proof.
  intros π.
  intros p.

  split.
  - intros.
    apply FP_U         in H.
    apply distr_or_and in H.
    destruct H as (H1 & H2).
    exact H2.
  - intros.
    apply FP_U.
    apply distr_or_and.
    unfold F in H.
    rewrite equiv_and.
    split.
    * rewrite equiv_or.
      right.
      apply top_is_true.
    * exact H.
Qed.


Theorem globally_next:
  forall π : Path,
    forall p : PathF,
      ( π ⊨ G(p) )
    <->
      ( π ⊨ p ∧ (X (G(p))) ).
Proof.
  intros π.
  intros p.

  split.
  - intros.
    apply FP_R in H.
    apply distr_and_or in H.
    destruct H as [H1 | H2].
    * rewrite equiv_and in H1.
      destruct H1 as (H1 & H2).
      apply bot_is_false in H2.
      case H2.
    * unfold G.
      exact H2.

  - intros.
    apply FP_R.
    apply distr_and_or.
    unfold G in H.
    rewrite equiv_or.
    right.
    exact H.
Qed.


Theorem globally_bot_and_p:
  forall π : Path,
    forall p : PathF,
        (π ⊨ G (bot ∧ p))
      <->
        (π ⊨ G (bot)).
Proof.
  intros π.
  intros p.

  split.
  - intro H.
    apply globally_equiv_and in H.
    destruct H as (H1 & H2).
    exact H1.
  - intro H.
    rewrite globally_next in H.
    destruct H as (H1 & H2).
    apply bot_is_false in H1.
    case H1.
Qed.

Theorem eventually_bot_and_p:
  forall π : Path,
    forall p : PathF,
        (π ⊨ F (bot ∧ p))
      <->
        (π ⊨ F (bot)).
Proof.
  intros π.
  intros p.

  split.
  - intro H.
    rewrite eventually_next in H.
    destruct H as [H1 | H2].
    * destruct H1 as (H1 & H2).
      apply bot_is_false in H1.
      case H1.
    * destruct H2 as (H1 & H2).
      simpl in H2.
      destruct H2 as (H2 & H3).
      destruct H2 as (H2 & H4).
      apply state_top in H2.
      case H2.
  - intro H.
    simpl in *.
    destruct H as (H1 & H2).
    destruct H2 as (H2 & H3).
    apply state_top in H2.
    case H2.
Qed.

(*****************************************************************************)

Theorem replace_with_global_implication:
  forall π : Path,
    forall a b : PathF,
        (π ⊨ G (a --> b))
      ->
        ((π ⊨ a) -> (π ⊨ b)).
Proof.
  intros π.
  intros a b.
  intros H.

  rewrite globally_next in H.
  destruct H as (H1 & H2).
  unfold impP in H1.
  destruct H1 as [H1 | H3].
  - intros H3.
    apply negP_forms   in H1.
    rewrite negP_invol in H1.
    unfold not in H1.
    apply H1 in H3.
    case H3.
  - intros H4.
    exact H3.
Qed.

Theorem globally_impP_x2:
  forall π : Path,
    forall A₀ A₁ A₂ P₁ P₂ : PathF,
      (π ⊨ G (A₀ ∧ A₁ --> P₁)) /\ (π ⊨ G (A₀ ∧ A₂ --> P₂))
    ->
      (π ⊨ G (A₀ ∧ A₁ ∧ A₂ --> (P₁ ∧ P₂))).
Proof.
  intros π.
  intros A₀ A₁ A₂ P₁ P₂.
  intros H1.

  destruct H1 as (H1 & H2). (* hypothesis is a conjunction *)
  simpl in *.
  left. (* right side has "state_top" in it, so it is always False *)
  intros i.

  destruct H1 as [H1 | H1_false].   (* G(p) <-> (∀i : State, path_up π i ⊨ p)
                                                  \/ False *)

  (* case of H1 := G(p) -> (∀i : State, path_up π i ⊨ p) *)
  - destruct H2 as [H2 | H2_false]. (* G(p) <-> (∀i : State, path_up π i ⊨ p)
                                                  \/ False *)
    (* case of H2 := G(p) -> (∀i : State, path_up π i ⊨ p) *)
    * pose (H1_state := H1 i).  (* for each State i, H1_state is True *)
      pose (H2_state := H2 i).  (* for each State i, H2_state is True *)

      (* in H1, "h --> p" <-> "¬h ∨ p" *)
      destruct H1_state as [H1_notH | H1_P].

      (* case where "¬h" in H1 *)
      + left; left; exact H1_notH.

      (* case where "p" in H1 *)
      + (* in H2, "h --> p" <-> "¬h ∨ p" *)
        destruct H2_state as [H2_notH | H2_P].

        (* case where "¬h" in H2 *)
        --  destruct H2_notH as [H2_notA₀ | H2_notA₂].
            ** left; left; left; exact H2_notA₀.
            ** left; right;      exact H2_notA₂.

        (* case where "p" in H2 *)
        --  right.
            split.  (* P₁ ∧ P₂ *)
            ** exact H1_P.
            ** exact H2_P.

    (* case of H2 := G(p) -> False *)
    * (* I know there exists a State where H2_false is True, so I pose
         "j: State" and I remove ∃i from H2b. *)
      destruct H2_false as (j        & H2_false).
      destruct H2_false as (H2_false & H2_else).
      apply state_top in H2_false.
      case H2_false. (* at this State, my hypothesis is False *)

  (* case of H1 := G(p) -> False *)
  - destruct H1_false as (j        & H1_false).
    destruct H1_false as (H1_false & H1_else).
    apply state_top in H1_false.
    case H1_false.
Qed.

(**
 * \brief
 *  Associtivity of the conjunction in a global implication.
 *)
Theorem globally_and_assoc_H:
  forall π : Path,
    forall A₀ A₁ A₂ P₁ : PathF,
      (π ⊨ G (A₀ ∧ (A₁ ∧ A₂) --> P₁))
    <->
      (π ⊨ G (A₀ ∧ A₁ ∧ A₂ --> P₁)).
Proof.
  intros π.
  intros A₀ A₁ A₂ P₁.
  split.

  (* -> *)
  - intros H.
    simpl.
    left.
    intros i.
    simpl in H.
    destruct H as [H1 | H2].
    + assert (A0:
        (path_up π i ⊨ negP A₀  \/
         path_up π i ⊨ negP A₁  \/
         path_up π i ⊨ negP A₂) \/
         path_up π i ⊨ P₁
      ).
      * apply H1.
      * destruct A0 as [A0 | A1].
        destruct A0 as [A0 | A1].
        -- left; left; left; exact A0.
        -- destruct A1 as [A0 | A1].
          ++ left; left; right; exact A0.
          ++ left; right; exact A1.
        -- right; exact A1.
    + destruct H2 as (s & H2).
      destruct H2 as (H_false & H2).
      apply state_top in H_false.
      case H_false.

  (* <- *)
  - intros H.
    simpl.
    left.
    intros i.
    simpl in H.
    destruct H as [H1 | H2].
    + assert (A0:
        ((path_up π i ⊨ negP A₀  \/
          path_up π i ⊨ negP A₁) \/
          path_up π i ⊨ negP A₂) \/
          path_up π i ⊨ P₁
      ).
      * apply H1.
      * destruct A0 as [A0 | A1].
        destruct A0 as [A0 | A1].
        destruct A0 as [A0 | A1].
        -- left; left; exact A0.
        -- left; right; left; exact A1.
        -- left; right; right; exact A1.
        -- right; exact A1.
    + destruct H2 as (s & H2).
      destruct H2 as (H_false & H2).
      apply state_top in H_false.
      case H_false.
Qed.


Theorem globally_impP_x2_to_conj:
  ∀ π : Path,
    ∀ a b c : PathF,
      (π ⊨ G (a --> b)) /\ (π ⊨ G (b --> c))
    ->
      (π ⊨ G (a --> (b ∧ c))).
Proof.
  intros π.
  intros a b c.
  intros H.
  destruct H as (H1 & H2).
  rewrite simpl_globally in H1, H2.
  rewrite simpl_globally.
  intros s.
  pose proof (H1 s) as H1s.
  pose proof (H2 s) as H2s.
  destruct H1s as [H1a | H1b].
  - left; exact H1a.
  - destruct H2s as [H2a | H2b].
    + apply negP_forms in H1b.
      apply H1b in H2a.
      case H2a.
    + right.
      simpl.
      split.
      * exact H1b.
      * exact H2b.
Qed.

Theorem globally_impP_conj_commutes:
  ∀ π : Path,
    ∀ a b c : PathF,
      (π ⊨ G (a --> (b ∧ c)))
    <->
      (π ⊨ G (a --> (c ∧ b))).
Proof.
  intros π.
  intros a b c.
  split.

  (* -> *)
  - intros H.
    rewrite simpl_globally in H.
    rewrite simpl_globally.
    intros s.
    pose proof (H s) as H0.
    destruct H0 as [H0a | H0b].
    + left; exact H0a.
    + right.
      simpl in H0b.
      simpl.
      destruct H0b as (H1 & H2).
      split.
      * exact H2.
      * exact H1.

  (* <- *)
  - intros H.
    rewrite simpl_globally in H.
    rewrite simpl_globally.
    intros s.
    pose proof (H s) as H0.
    destruct H0 as [H0a | H0b].
    + left; exact H0a.
    + right.
      simpl in H0b.
      simpl.
      destruct H0b as (H1 & H2).
      split.
      * exact H2.
      * exact H1.
Qed.

Theorem globally_and_assoc_P:
  forall π : Path,
    forall h a b c : PathF,
      (π ⊨ G ( h --> (a ∧ (b ∧ c)) ))
    <->
      (π ⊨ G ( h --> ((a ∧ b) ∧ c) )).
Proof.
  intros π.
  intros h a b c.
  split.

  (* -> *)
  - intros H.
    rewrite simpl_globally in H.
    rewrite simpl_globally.
    intros s.
    pose proof (H s) as H0.
    destruct H0 as [H0a | H0b].
    + left; exact H0a.
    + right.
      simpl in H0b.
      simpl.
      destruct H0b as (H1 & H2).
      destruct H2  as (H2 & H3).
      split.
      * split.
        -- exact H1.
        -- exact H2.
      * exact H3.

  (* <- *)
  - intros H.
    rewrite simpl_globally in H.
    rewrite simpl_globally.
    intros s.
    pose proof (H s) as H0.
    destruct H0 as [H0a | H0b].
    + left; exact H0a.
    + right.
      simpl in H0b.
      simpl.
      destruct H0b as (H1 & H3).
      destruct H1  as (H1 & H2).
      split.
      * exact H1.
      * split.
        -- exact H2.
        -- exact H3.
Qed.

Theorem globally_impP_conj_to_single:
  ∀ π : Path,
    ∀ a b c : PathF,
      (π ⊨ G (a --> (b ∧ c)))
    ->
      (π ⊨ G (a --> b)).
Proof.
  intros π.
  intros a b c.
  intros H.
  rewrite simpl_globally in H.
  rewrite simpl_globally.
  intros s.
  pose proof (H s) as Hs.
  destruct Hs as [Ha | Hb].
  - left; exact Ha.
  - right.
    simpl in Hb.
    destruct Hb as (Hb & dummy).
    exact Hb.
Qed.


Theorem globally_impP_x2_to_disj:
  ∀ π : Path,
  ∀ a b c d e : PathF,
    π ⊨ G(a ∧ b --> d)
  /\
    π ⊨ G(a ∧ c --> e)
  ->
    π ⊨ G(a ∧ (b ∨ c) --> (d ∨ e)).
Proof.
  intros π.
  intros a b c d e.
  intros H.
  destruct H as (H1 & H2).
  rewrite Thesis.toolbox.simpl_globally in H1.
  rewrite Thesis.toolbox.simpl_globally in H2.
  rewrite Thesis.toolbox.simpl_globally.
  intros s.
  pose proof (H1 s) as H1.
  pose proof (H2 s) as H2.
  destruct H1 as [H1a | H1b], H2 as [H2a | H2b].
  - left.
    simpl in H1a, H2a.
    simpl.
    destruct H1a as [H1a | H1b], H2a as [H2a | H2b].
    + left; exact H1a.
    + left; exact H1a.
    + left; exact H2a.
    + right.
      split.
      * exact H1b.
      * exact H2b.

  - simpl in H1a.
    simpl.
    destruct H1a as [H1a | H1b].
    + left;  left;  exact H1a.
    + right; right; exact H2b.

  - simpl in H2a.
    simpl.
    destruct H2a as [H2a | H2b].
    + left;  left;  exact H2a.
    + right; left;  exact H1b.

  - right.
    simpl.
    left; exact H1b.
Qed.


(**
 * \brief
 *  Simplify a globally operator to a property for all states.
 *)
Lemma globally_to_state:
  ∀ π : Path,
  ∀ p : PathF,
  (
    π ⊨ G(p)
  <->
    ∀ s : State, path_up π s ⊨ p
  ).
Proof.
  intros π.
  intros p.
  split.
  - intros H.
    simpl in H.
    destruct H as [H1 | H2].
    + exact H1.
    + destruct H2 as (s0  & H2 ).
      destruct H2 as (H2a & H2b).
      apply state_top in H2a.
      case H2a.
  - intros H.
    simpl.
    left.
    exact H.
Qed.


(**
 * \brief
 *  At a given state, if we have a next operator, then the property is verfied
 *  at the next state.
 *)
Theorem next_at_state:
∀ π : Path,
∀ s : State,
∀ P : PathF,
  path_up π s ⊨ X(P) = path_up π (s + 1) ⊨ P.
Proof.
  intros π s P.
  simpl.
  rewrite Thesis.LTL.path_plus_i_j.
  reflexivity.
Qed.


(**
 * \brief
 *  From two equalities of 3 vectors at the same state, we can create a third.
 *)
Lemma my_eq_vect_tran:
  ∀ size : nat,
  ∀ v1 v2 v3 : BitVect size,
  ∀ π : Path,
  ∀ s : State,
path_up π s ⊨ (v1 '== v2) /\ path_up π s ⊨ (v2 '== v3)
  ->
path_up π s ⊨ (v1 '== v3).
Proof.
  intros size v1 v2 v3 π s H.
  destruct H as (H1 & H2).
  rewrite <- eq_vect_true_iff_path_up in H1, H2.
  rewrite <- eq_vect_true_iff_path_up.
  apply (eq_vect_tran_TF size v1 v2 v3).
  - exact H1.
  - exact H2.
Qed.

