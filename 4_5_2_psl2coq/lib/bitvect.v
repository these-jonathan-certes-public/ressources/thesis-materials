(*
 * Copyright (C) 2022 Guillaume Dupont
 * guillaume.dupont@toulouse-inp.fr
 *
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Coq.Init.Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Arith.Peano_dec.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import natutil.
Require Import bit.
Import bit.Bit.

(**
 * This module defines bit vectors, plus an order on bit vectors and a few useful theorems.
 *)
Module BitVect.

Declare Scope BitVect_scope.
Delimit Scope BitVect_scope with BV.
Open Scope BitVect_scope.

(**
 * Definition of a Bit vector.
 *
 *  Inductive type: from one Bit vector of size n and one Bit, I can obtain a
 *  Bit vector of size (n + 1).
 *)
Inductive BitVect : nat -> Set := (** Depends on a nat value for the size.   *)
  | bnil  : BitVect 0             (** Empty vector is a vector of size n = 0 *)
  | bcons : forall (n : nat), Bit -> BitVect n
              -> BitVect (S n)
            (**
             * Inductive type: from one Bit vector of size n and one Bit, I can
             * obtain a Bit vector of size (n + 1).
             * Also, depending on n.
             *)
  .

(** Get the head of the bit vector. *)
Definition bvhd (n : nat) (v : BitVect (S n)) : Bit :=
  match v with
  | bcons _ b _ => b
  end.

(** Get the tail of the bit vector. *)
Definition bvtl (n : nat) (v : BitVect (S n)) : BitVect n :=
  match v with
  | bcons _ _ v' => v'
  end.

(**
 * Utility tactic (not intended to be called other than by bvinduction) that decompose every
 * bit vector of the given size variable. Replaces "v : BitVect 0" with "bnil", and "v : BitVect (S n)"
 * with "v : BitVect n, b : Bit" (i.e. performs destruction).
 *)
Ltac destruct_all s := repeat match goal with
  | [ v : BitVect (S s) |- _ ] => dependent destruction v
  | [ v : BitVect 0 |- _ ] => dependent destruction v
  end.

(**
 * A very useful tactic, that basically allows to perform "linked induction" (dependent induction) on
 * every vector indexed by the same size.
 *)
Ltac bvinduction s := dependent induction s; destruct_all s.

(**
 * Equality on vectors is decidable. The reason why is that vectors have a known and finite size, and
 * equality is decidable on bits. Equality on vectors is "implicitly defined" inductively.
 *)
Definition eq_vect_dec : forall size : nat, forall v1 v2 : BitVect size, { v1 = v2 } + { v1 <> v2 }.
Proof.
intros size v1 v2.
bvinduction size.
left; reflexivity.
destruct (eq_bit_dec b b0) as [H1|H1].
destruct (IHsize v1 v2) as [H2|H2].
left; f_equal; assumption.
(*
 "intro H3" generates the hypothesis "H3 : bcons size b v1 = bcons size b0 v2".

 "dependent destruction" allows to split H3 as b = b0 and v1 = v2. It does something similar to
 "f_equal", except "f_equal" only works on the goal.

 For the first goal, we obtain "v1 = v2" and "v1 <> v2" (obvious contradiction) and for the second
 goal, we obtain "b = b0" and "b <> b0" (obvious contradiction as well).
*)
all: right; intro H3; dependent destruction H3; contradiction.
Qed.

(** Notation for decidable equality on vectors (to be used in if/then/else) *)
Infix "=?" := eq_vect_dec : BitVect_scope.

(**
 * Order on vect, as an inductive predicates. This factually implements a left-to-right lexicographical order.
 * This also implies that BitVect represent words that are stored MSB first.
 *
 * Intuitively, the order is defined as so :
 *   - (leq_vect_nil)       [] <= []
 *   - (leq_vect_cons_lt)   b1 :: v1 <= b2 :: v2 if b1 < b2
 *   - (leq_vect_cons_eq)   b1 :: v1 <= b2 :: v2 if b1 = b2 and v1 <= v2  [induction]
 *
 * Note that this predicate is only defined on vectors of the same size (to avoid very tricky situations).
 * It is possible to have it on vectors of different size but only have cases for vectors of the same size
 * (but this formulation is slightly more efficient).
 *)
Inductive leq_vect : forall size : nat, BitVect size -> BitVect size -> Prop :=
  | leq_vect_nil : leq_vect 0 bnil bnil
  | leq_vect_cons_lt : forall size : nat, forall b1 b2 : Bit, forall v1 v2 : BitVect size,
    (b1 < b2)%B -> leq_vect (S size) (bcons size b1 v1) (bcons size b2 v2)
  | leq_vect_cons_eq : forall size : nat, forall b1 b2 : Bit, forall v1 v2 : BitVect size,
    b1 = b2 -> leq_vect size v1 v2 -> leq_vect (S size) (bcons size b1 v1) (bcons size b2 v2)
  .

(**
 * Definition/proof that leq_vect is decidable. This relies on the fact that "<" and "=" are decidable
 * for bits.
 *)
Definition leq_vect_dec : forall size : nat, forall v1 v2 : BitVect size,
  { leq_vect size v1 v2 } + { ~ leq_vect size v1 v2 }.
Proof.
intros size v1 v2.
bvinduction size.
left; apply leq_vect_nil.
destruct (lt_bit_dec b b0) as [H1|H1].
left; apply leq_vect_cons_lt; assumption.
destruct (eq_bit_dec b b0) as [H2|H2].
(* Note: "bvinduction" performs a *dependent* induction, which allows the induction hypothesis to be
   generalized (i.e. "forall v1 v2 : BitVect size, P v1 v2" instead of just "P v1 v2"). This is absolutely
   crucial as otherwise we cannot apply it in most proofs!
*)
destruct (IHsize v1 v2) as [IH|IH].
left; apply leq_vect_cons_eq; assumption.
right; intro H.
(* "dependent destruction" inverts H (it 'fills in the hole so that H holds') as "inversion" would do.
   Here "inversion" does not work; it creates this weird "existsT" stuff in the hypotheses, that are here because
   the different parts of the hypothesis are linked together (dependent type). "dependent destruction" takes care
   of the link and does not generate the "existsT" stuff.
*)
dependent destruction H; contradiction.
apply not_lt_leq_bit in H1.
right; intro H3; dependent destruction H3.
apply lt_not_leq_bit in H; contradiction.
contradiction.
Qed.

(** leq_vect is reflexive. *)
Theorem leq_vect_refl : forall size : nat, forall v : BitVect size, leq_vect size v v.
Proof.
intros size v.
bvinduction size.
apply leq_vect_nil.
apply leq_vect_cons_eq.
reflexivity.
apply IHsize.
Qed.

(** leq_vect is antisymetrical. *)
Theorem leq_vect_antisym : forall size : nat, forall v1 v2 : BitVect size,
  leq_vect size v1 v2 -> leq_vect size v2 v1 -> v1 = v2.
Proof.
intros size v1 v2 H1 H2.
bvinduction size.
reflexivity.
(* Similarly as for the proof of leq_vect_dec, simple inversion will not work because BitVect is a dependent type
   and leq_vect is a dependent predicate (size runs through the leq_vect and cannot be dissociated).
*)
dependent destruction H1.
dependent destruction H2.
pose proof (lt_bit_asym' b b0 H H0); contradiction.
apply lt_bit_asym in H; contradiction.
(* Given a constructor C, if you have C x y = C w z, f_equal 'cuts the goal' and deduces x = w and y = z. *)
f_equal.
apply IHsize.
assumption.
dependent destruction H2.
apply lt_bit_asym in H; contradiction.
assumption.
Qed.

(** leq_vect is transitive. *)
Theorem leq_vect_tran : forall size : nat, forall v1 v2 v3 : BitVect size,
  leq_vect size v1 v2 -> leq_vect size v2 v3 -> leq_vect size v1 v3.
Proof.
intros size v1 v2 v3 H1 H2.
bvinduction size.
apply leq_vect_refl.
dependent destruction H1; dependent destruction H2.
apply leq_vect_cons_lt.
unfold lt_bit; unfold lt_bit in H; unfold lt_bit in H0.
destruct H as (H1,H2).
destruct H0 as (H3,H4).
split.
apply (leq_bit_tran b b0 b1 H1 H3).
intro H.
rewrite <- H in H3.
pose proof (leq_bit_antisym b b0 H1 H3); contradiction.
apply leq_vect_cons_lt; assumption.
apply leq_vect_cons_lt; assumption.
apply leq_vect_cons_eq.
reflexivity.
apply (IHsize v1 v2 v3); assumption.
Qed.

(** Definition of the strict order associated to leq_vect. *)
Definition lt_vect (size : nat) (v1 v2 : BitVect size) : Prop :=
  leq_vect size v1 v2 /\ v1 <> v2.

(**
 * Definition/proof that lt_vect is decidable. This proof relies on the fact that leq_vect is decidable and
 * so is "=" on vectors.
 *)
Definition lt_vect_dec : forall size : nat, forall v1 v2 : BitVect size,
  { lt_vect size v1 v2 } + { ~ lt_vect size v1 v2 }.
Proof.
intros size v1 v2.
unfold lt_vect.
destruct (leq_vect_dec size v1 v2) as [H1|H1]; destruct (eq_vect_dec size v1 v2).
right; intros (H2,H3); contradiction.
left; split; assumption.
right; intros (H2,H3); contradiction.
right; intros (H2,H3); contradiction.
Qed.

Lemma not_leq_cons : forall size : nat, forall b1 b2 : Bit, forall v1 v2 : BitVect size,
  ~leq_vect (S size) (bcons size b1 v1) (bcons size b2 v2) ->
    ((b2 < b1)%B \/ ((b1 <= b2)%B /\ ~leq_vect size v1 v2)).
Proof.
intros size b1 b2 v1 v2 H1.
destruct (leq_lt_bit b1 b2) as [H2|H2].
- right.
  split.
  assumption.
  intro H3.
  destruct (leq_lt_invert b1 b2 H2) as [H4|H4].
  + pose proof (leq_vect_cons_lt size b1 b2 v1 v2 H4); contradiction.
  + pose proof (leq_vect_cons_eq size b1 b2 v1 v2 H4 H3); contradiction.
- left; assumption.
Qed.

Theorem not_leq_lt : forall size : nat, forall v1 v2 : BitVect size,
  ~ leq_vect size v1 v2 -> lt_vect size v2 v1.
Proof.
unfold lt_vect.
intros size v1 v2 Hnlt.
bvinduction size.
(* "contradiction" is not powerful enough to guess that "~ leq_vect 0 bnil bnil" is contradictory so
 * we summon leq_vect_nil manually
 *)
- pose proof leq_vect_nil; contradiction.
(* *)
- destruct (not_leq_cons size b b0 v1 v2 Hnlt) as [H1|(H1,H2)]. (* Result of lemma is of the form "A \/ (B /\ C)" *)
  + split.
    * apply leq_vect_cons_lt; assumption.
    * intro H2; inversion H2.  (* generates b0 = b *)
      destruct H1 as (H1,H1'). (* generates b0 <> b *)
      contradiction.
  + destruct (IHsize v1 v2 H2) as (H3,H4).
    split.
    * destruct (leq_lt_invert b b0 H1) as [H5|H5].
      -- pose proof (leq_vect_cons_lt size b b0 v1 v2 H5); contradiction.
      -- apply leq_vect_cons_eq.
         ++ symmetry; assumption.
         ++ assumption.
    * intro H5.
      dependent destruction H5. (* Guess v1 = v2 *)
      contradiction.
Qed.

Theorem lt_not_leq : forall size : nat, forall v1 v2 : BitVect size,
  lt_vect size v2 v1 -> ~ leq_vect size v1 v2.
Proof.
intros size v1 v2 (H1,H2) H3.
pose proof (leq_vect_antisym size v1 v2 H3 H1).
symmetry in H.
contradiction.
Qed.


(** lt_vect is transitive. *)
Theorem lt_vect_tran : forall size : nat, forall v1 v2 v3 : BitVect size,
  lt_vect size v1 v2 -> lt_vect size v2 v3 -> lt_vect size v1 v3.
Proof.
  intros size v1 v2 v3 H1 H2.
  unfold lt_vect in *.
  destruct H1 as (H1 & H3).
  destruct H2 as (H2 & H4).
  split.
  - apply leq_vect_tran with (v2 := v2).
    + exact H1.
    + exact H2.
  - destruct (eq_vect_dec size v1 v3).
    + rewrite e in H1.
      assert (H_false: v2 = v3).
      * apply leq_vect_antisym.
        -- exact H2.
        -- exact H1.
      * contradiction.
    + exact n.
Qed.


Lemma eq_vect_tran :
forall size : nat, forall v1 v2 v3 : BitVect size,
  v1 = v2 -> v2 = v3 -> v1 = v3.
Proof.
  intros size v1 v2 v3.
  intros H1 H2.
  rewrite H2 in H1.
  exact H1.
Qed.


(**
 * The mega boilerplate. Because of the way the proofs are written, we always suppose that we handle
 * vectors of the same size. Coq cannot really coerce BitVect m and BitVect n by itself (even if somewhere
 * m = n); we need to do it "by hand"...
 *
 * It is probably possible to write a more subtle version of this without "Program" (which allows to leave
 * 'holes' in the function and deal with correctness later) but this works.
 *)
Program Fixpoint cast (m n : nat) (H : m = n) (v : BitVect n) : BitVect m :=
  match m, n with
  | 0, _ | _, 0 => bnil (* By construction, this pattern matching is always only activated when m = n = 0 *)
  | (S m'), (S n') => bcons m' (bvhd n' v) (cast m' n' _ (bvtl n' v)) (* Similar technique as for vect2nat *)
  end.
(* "Program" generates proof obligations, but they are all trivial (oof) *)

(** This lemma says that "cast size size eq_refl" is the identity *)
Theorem cast_id : forall size : nat, forall v : BitVect size, v = cast size size eq_refl v.
Proof.
intros size v.
dependent induction v.
simpl.
reflexivity.
simpl.
f_equal.
assumption. (* This is in fact IHv (yes) *)
Qed.


(**
 * The smaller boilerplate, local proof irrelevance for reflexivity. Because "cast" takes a *proof* as argument,
 * we want to say that "any proof of m = n is equivalent" so that we interchange them as we see fit.
 *)
Theorem eq_irr : forall m n : nat, forall p q : m = n, p = q.
Proof.
intros m n p q.
(* "=" is, in fact, defined inductively as "| eq_refl : forall n, n = n", so we can destruct it *)
destruct p.
dependent destruction q. (* we need dependent here because of some obscure black magic *)
reflexivity.
Qed.

(**
 * A function that returns the minimum between (m - n) and 0. In other words, if n > m, returns 0, otherwise
 * return m - n.
 *)
Definition minsub (m n : nat) : nat :=
  if n <=? m then m - n else 0.

(**
 * Drop the first n elements of a vector. If n is greater than the size, returns nbil. If n is 0, returns
 * the vector unchanged.
 *)
Fixpoint drop (size : nat) (v : BitVect size) (n : nat) : BitVect (minsub size n) :=
  match size,n return BitVect size -> BitVect (minsub size n) with
  | _, 0 => fun v' => v'
  | 0, _ => fun _ => bnil
  | (S size'), (S n') => fun v' => drop size' (bvtl size' v') n'
  end v.

(**
 * Take the n first elements of a vector. If n is greater than the size, returns the entire vector. If n is 0,
 * return bnil.
 *)
Fixpoint take (size : nat) (v : BitVect size) (n : nat) : BitVect (Nat.min n size) :=
  match size,n return BitVect size -> BitVect (Nat.min n size) with
  | _, 0 => fun _ => bnil
  | 0, (S n') => fun _ => bnil
  | (S size'),(S n') => fun v' =>
    bcons (Nat.min n' size') (bvhd size' v') (take size' (bvtl size' v') n')
  end v.

(**
 * Take a sequence of elements from a vector, starting at index 'from' (included) and until index 'to'
 * (excluded).
 * If to <= from, return bnil. If to >= size, equivalent to "drop from".
 *)
(* from included, to excluded *)
Definition range (size : nat) (v : BitVect size) (from to : nat) :
  BitVect (Nat.min (minsub to from) (minsub size from)) :=
    take (minsub size from) (drop size v from) (minsub to from).

(* Eval compute in (range 4 (bcons 3 O (bcons 2 Z (bcons 1 Z (bcons 0 O bnil)))) 1 6). *)



End BitVect.


