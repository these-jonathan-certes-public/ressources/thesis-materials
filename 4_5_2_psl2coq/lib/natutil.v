(*
 * Copyright (C) 2022 Guillaume Dupont
 * guillaume.dupont@toulouse-inp.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Coq.Init.Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Arith.Peano_dec.
Require Import Psatz.

(**
 * A bunch of useful results/shortcuts for proofs with naturals.
 *
 * The main tactic to know is "lia". It is an arithmetic solver that is extremely useful and quite
 * powerful. Usually proofs goes according to the following scheme:
 *   1. clean up goal
 *   2. generate useful hypotheses
 *   3. summon lia
 *)
Section NatUtil.

(** Power is not equal to 0 (if the basis is not 0) *)
Theorem pow_neq_0 : forall b n : nat, b > 0 -> b ^ n <> 0.
Proof.
intros b n H0.
induction n.
rewrite Nat.pow_0_r; intro H2; inversion H2.
rewrite Nat.pow_succ_r; lia.
Qed.

(**
 * Splits the interval [0,2 ^ S s[ in [0,2 ^ s[ U [2 ^ s,2 ^ S s[.
 * This is typically useful in various case analyses.
 *)
Theorem pow_dich : forall i s : nat, i < 2 ^ S s -> (0 <= i < 2 ^ s \/ 2 ^ s <= i < 2 ^ S s).
Proof.
intros i s H; lia.
Qed.

(**
 * Generated the "missing steps" between two numbers. This allows to transform a problem into
 * another simpler problem without much overhead.
 *)
Theorem lt_add : forall n m : nat, n <= m < n + n -> exists p : nat, p < n /\ m = n + p.
Proof.
intros n m H.
exists (m - n).
split; lia.
Qed.

(** Simple identity on modulo. *)
Theorem mod_red : forall n m : nat, 0 <= m -> m < n -> (n + m) mod n = m.
Proof.
intros n m H1 H2.
replace (n + m) with (m + 1 * n) by lia.
rewrite Nat.mod_add.
apply Nat.mod_small; assumption.
lia.
Qed.

(** Simple identity on division. *)
Theorem div_red : forall n m : nat, 0 <= m -> m < n -> (n + m) / n = 1 + (m / n).
Proof.
intros n m H1 H2.
replace (n + m) with (1 * n + m) by (rewrite Nat.mul_1_l; reflexivity).
rewrite Nat.div_add_l.
reflexivity.
lia.
Qed.

End NatUtil.


