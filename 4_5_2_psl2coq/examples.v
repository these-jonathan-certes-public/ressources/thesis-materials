(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import PeanoNat.
Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import bit.Bit.
Import bitvect.BitVect.

Section examples.

(**
 * Declaration of predicates from the inputs/outputs of the decompresser.
 *)

(* inputs: *)
Variable clk nrst trace_ctl : CV0 Bit (eq O).
Variable input : (BitVect 8).

(* outputs: *)
Variable ready     : (BitVect 1).
Variable data_size : (BitVect 4).
Variable data__0   : (BitVect 8).
Variable decompressed_packet : (BitVect 4).

(* internal registers: *)
Variable i_packets_decompresser_state     : (BitVect  4).
Variable i_packets_decompresser_r_data__8 : (BitVect  8).

Definition p_branch := (nat2vect 4 3).

(**
 * insert the results of the conversion here and finish with "End examples."
 *)

