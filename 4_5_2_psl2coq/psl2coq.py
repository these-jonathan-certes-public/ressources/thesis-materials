#!/usr/local/bin/python3

#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import os
import re
import sys
import pynusmv
import pynusmv.nusmv.parser.psl.psl
import pynusmv.nusmv.sexp.sexp

##
# \brief
#   Stores the path to the SMV module file containing verified properties after
#   \ref psl2coq_parseArgv() has been called.
#
psl2coq_propertiesFilePath = str()

##
# \brief
#   Stores the path to the file to print the python script to after \ref
#   psl2coq_parseArgv() has been called.
#
psl2coq_outputFilePath = str()

##
# \brief
#   Stores the content of the string used to filter the properties by their
#   names.
#
psl2coq_filterString = str()

#===============================================================================

##
# \brief
#   Prints help to the standard output and then exits.
#
def psl2coq_printHelpAndExit(
  theCode #!< the exit code
):
  theText = """
Creates a coq syntax file from a SMV module where all properties from PSLSPEC,
LTLSPEC and INVARSPEC are converted to coq Axioms.

SYNOPSIS:
  ./psl2coq.py --properties FILE1 [--to FILE2]

DESCRIPTION:
  Removes double quotes from the PSL expressions. Then puts atom names between
  strings "(' " and ")" except if their parent is a vector relation.

  Input SMV modules must be correctly written as their content is not verified.

OPTIONS:
  -p OR --properties     : the argument following this option gives a path to
                           the file containing verified properties.

  -t OR --to             : the argument following this option gives a path to
                           the file to print the output to. If this argument is
                           omitted, the output is printed to the standard
                           output.

  -F OF --filter         : the argument following this option gives a regular
                           expression to filter the verified properties by the
                           content of their NAME attribute.

  -h OR --help           : prints this help and exits

EXAMPLE:
  ./psl2coq.py --properties prop.smv --to hypothesis.v
"""
  print(str.strip(theText))
  exit(int(theCode))

#===============================================================================

##
# \brief
#   Creates a list of NuSMV node pointers that all share the same type
#   attribute.
#
# \details
#   Recursively accesses NuSMV nodes from one specific node and compares their
#   type: if it matches the expected type, adds them to the list that is given
#   as a parameter. Stops recursively accessing nodes once the type cannot have
#   a hierarchy.
#
# \returns void
#
def recursivelyGetAllNodes(
  theTopNode,   #!< the node to get in the hierarchy from
  theNodeType,  #!< the type of nodes to add to the list
  theList       #!< the list to append
):
  if ( theTopNode.type == theNodeType ):
    list.append(theList, theTopNode)
  else:
    if theTopNode.type not in {
      pynusmv.nusmv.parser.parser.NUMBER_SIGNED_WORD,
      pynusmv.nusmv.parser.parser.NUMBER_UNSIGNED_WORD,
      pynusmv.nusmv.parser.parser.NUMBER_FRAC,
      pynusmv.nusmv.parser.parser.NUMBER_EXP,
      pynusmv.nusmv.parser.parser.NUMBER_REAL,
      pynusmv.nusmv.parser.parser.NUMBER,
      pynusmv.nusmv.parser.parser.ATOM,
      pynusmv.nusmv.parser.parser.FAILURE
    }:
      if ( pynusmv.nusmv.node.node.car(theTopNode) is not None ):
        recursivelyGetAllNodes( pynusmv.nusmv.node.node.car(theTopNode),
                                theNodeType,
                                theList
                              )
      if ( pynusmv.nusmv.node.node.cdr(theTopNode) is not None ):
        recursivelyGetAllNodes( pynusmv.nusmv.node.node.cdr(theTopNode),
                                theNodeType,
                                theList
                              )

#===============================================================================

##
# \brief
#   Uses regular expression matching to extract the size and value from constant
#   that is formatted with NuSMV syntax.
#
def pslNumberSizeAndValue(
  theNodeText
):
  theSize  = "ERROR_SIZE"
  theValue = "ERROR_VALUE"

  # case of value in decimal:
  thePattern = "0[su][dD](\d+)_(\d+)"
  theMatch   = re.search(thePattern, theNodeText)
  if ( theMatch != None ):
    theSize  = theMatch.group(1)
    theValue = theMatch.group(2)

  return theSize + " " + theValue

#===============================================================================

##
# \brief
#   Recursive function to walk an AST describing a temporal property and appends
#   Coq formatted strings to a list.
#
def psl2coq_walkPsl(
  theNode,  #!< the node to walk from
  theList   #!< the list to append Coq formatted strings to
):
  theCar = pynusmv.nusmv.node.node.car(theNode)
  theCdr = pynusmv.nusmv.node.node.cdr(theNode)

  if ( theNode.type == pynusmv.nusmv.parser.psl.psl.PSL_REPLPROP ):
    # creates a forall replicator:
    theReplicator = pynusmv.nusmv.parser.psl.psl.\
                            psl_node_repl_prop_get_replicator(theNode)
    theProperty   = pynusmv.nusmv.parser.psl.psl.\
                            psl_node_repl_prop_get_property(theNode)
    theIdentifier = pynusmv.nusmv.node.node.sprint_node(
                      pynusmv.nusmv.node.node.car(theReplicator))
    theRange = pynusmv.nusmv.node.node.cdr(pynusmv.nusmv.node.node.cdr(theReplicator))
    theRange = str(pynusmv.nusmv.node.node.sprint_node(theRange))
    theLow  = theRange.split(":")[0]
    theHigh = theRange.split(":")[1]
    theCoqString = "forall " + theIdentifier + """ : nat,
    """ + theLow        + " <= " + theIdentifier + " /\\ " + \
         str(theIdentifier) + " <= " + theHigh + "\n  ->\n"
    #
    theList.append(theCoqString)
    if ( theProperty.type != pynusmv.nusmv.parser.psl.psl.PSL_REPLPROP ):
      theList.append("forall π : Path, π ⊨\n  ")
    psl2coq_walkPsl(theProperty, theList)

  if ( theNode.type == pynusmv.nusmv.parser.parser.IMPLIES ):
    theList.append("(")
    if ( theCar.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCar, theList)
    theList.append(") --> (")
    if ( theCdr.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.NOT ):
    theList.append("negP (")
    if ( theCar.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCar, theList)
    theList.append(")")

  # TODO: see if this is a disjunction or a logic or:
  if ( theNode.type == pynusmv.nusmv.parser.parser.OR ):
    theList.append("(")
    if ( theCar.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCar, theList)
    theList.append(") ∨ (")
    if ( theCdr.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(")")

  # TODO: see if this is a conjunction or a logic and:
  if ( theNode.type == pynusmv.nusmv.parser.parser.AND ):
    theList.append("(")
    if ( theCar.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCar, theList)
    theList.append(") ∧ (")
    if ( theCdr.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.OP_GLOBAL ) or \
     ( theNode.type == pynusmv.nusmv.parser.psl.psl.PSL_ALWAYS ):
    theList.append("G(")
    if ( theCar.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCar, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.OP_NEXT ) or \
     ( theNode.type == pynusmv.nusmv.parser.psl.psl.PSL_NEXT ):
    theList.append("X(")
    if ( theCar.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCar, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.UNTIL ) or \
     ( theNode.type == pynusmv.nusmv.parser.psl.psl.PSL_UNTIL ):
    theList.append("(")
    if ( theCar.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCar, theList)
    theList.append(") U (")
    if ( theCdr.type == pynusmv.nusmv.parser.parser.ATOM ):
      theList.append("'")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.EQUAL ):
    theList.append("(eq_BitVector ")
    psl2coq_walkPsl(theCar, theList)
    theList.append(" ")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.NOTEQUAL ):
    theList.append("(negP (eq_BitVector ")
    psl2coq_walkPsl(theCar, theList)
    theList.append(" ")
    psl2coq_walkPsl(theCdr, theList)
    theList.append("))")

  if ( theNode.type == pynusmv.nusmv.parser.parser.LE ):
    theList.append("(le_BitVector ")
    psl2coq_walkPsl(theCar, theList)
    theList.append(" ")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.LT ):
    theList.append("(lt_BitVector ")
    psl2coq_walkPsl(theCar, theList)
    theList.append(" ")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.GE ):
    theList.append("(le_BitVector ")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(" ")
    psl2coq_walkPsl(theCar, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.GT ):
    theList.append("(lt_BitVector ")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(" ")
    psl2coq_walkPsl(theCar, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.psl.psl.PSL_NEXT_EVENT ):
    theList.append("(next_event (")
    psl2coq_walkPsl(pynusmv.nusmv.node.node.cdr(theCdr), theList)
    theList.append(") 1 (")
    psl2coq_walkPsl(theCar, theList)
    theList.append("))")

  if ( theNode.type == pynusmv.nusmv.parser.psl.psl.PSL_NEXT_EVENT_A ):
    theList.append("(next_event_a (")
    psl2coq_walkPsl(pynusmv.nusmv.node.node.cdr(theCdr), theList)
    theRange = pynusmv.nusmv.node.node.sprint_node(
                 pynusmv.nusmv.node.node.car(theCdr)).split(":")
    theList.append(") " + theRange[0] + " " + theRange[1] + " (")
    psl2coq_walkPsl(theCar, theList)
    theList.append("))")

  if ( theNode.type == pynusmv.nusmv.parser.parser.UWCONST ) or \
     ( theNode.type == pynusmv.nusmv.parser.parser.SWCONST ):
    theList.append("(nat2vect ")
    psl2coq_walkPsl(theCdr, theList)
    theList.append(" ")
    psl2coq_walkPsl(theCar, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.BIT_SELECTION ):
    theList.append("(range_BitVector ")
    psl2coq_walkPsl(theCar, theList)
    theFrom = pynusmv.nusmv.node.node.sprint_node(pynusmv.nusmv.node.node.cdr(theCdr))
    theTo   = pynusmv.nusmv.node.node.sprint_node(pynusmv.nusmv.node.node.car(theCdr))
    theList.append(" " + theFrom + " " + theTo + ")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.ARRAY ):
    theList.append("(range_BitVector ")
    psl2coq_walkPsl(theCar, theList)
    theSel = pynusmv.nusmv.node.node.sprint_node(theCdr)
    theList.append(" " + theSel + " " + theSel + ")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.CAST_WORD1 ):
    theList.append("(bcons 0 ")
    psl2coq_walkPsl(theCar, theList)
    theList.append(" bnil)")

  if ( theNode.type == pynusmv.nusmv.parser.parser.CAST_BOOL ):
    theList.append("('")
    psl2coq_walkPsl(theCar, theList)
    theList.append(")")

  if ( theNode.type == pynusmv.nusmv.parser.parser.ATOM ):
    theAtomText = theNode.left.strtype.text
    theAtomText = str.replace(theAtomText, ".", "_")  # avoid Coq syntax error
    if ( theAtomText[0] == '"' and theAtomText[len(theAtomText)-1] == '"' ):
      theList.append(theAtomText[1:len(theAtomText)-1])
    else:
      theList.append(theAtomText)

  if ( theNode.type == pynusmv.nusmv.parser.parser.NUMBER_FRAC ) or \
     ( theNode.type == pynusmv.nusmv.parser.parser.NUMBER_EXP  ) or \
     ( theNode.type == pynusmv.nusmv.parser.parser.NUMBER_REAL ) or \
     ( theNode.type == pynusmv.nusmv.parser.parser.NUMBER      ):
    theList.append(pynusmv.nusmv.node.node.sprint_node(theNode))

  if ( theNode.type == pynusmv.nusmv.parser.parser.NUMBER_SIGNED_WORD   ) or \
     ( theNode.type == pynusmv.nusmv.parser.parser.NUMBER_UNSIGNED_WORD ):
    theList.append("(nat2vect ")
    theList.append(pslNumberSizeAndValue(pynusmv.nusmv.node.node.sprint_node(theNode)))
    theList.append(")")

#===============================================================================

##
# \brief
#   Uses NuSMV parser to extract PSL, LTL and INVAR expressions.
#
# \details
#   Calls \ref psl2coq_walkPsl to create the Coq formatted string from the top
#   node of the expression.
#
# \returns a list of elements to be taken 3 by 3: the first element is the name,
#   the second is the PSL expression and the third is its LTL version.
#
# \bug
#   There is a crash of NuSMV if the input file is not correctly written.
#
def psl2coq_parseSmvFile(
  theFilePath #!< path to the input file containing PSL expressions
):
  pynusmv.init.reset_nusmv()
  # causes a segmentation fault if the input file is not correctly written
  # (catching the error cannot avoid NuSMV API to stop):
  pynusmv.nusmv.parser.parser.Parser_ReadSMVFromFile(str(theFilePath))

  ##
  # accesses all PSL properties in the file:
  theTree = pynusmv.nusmv.parser.parser.cvar.parsed_tree
  thePslSpecNodeList = list()
  recursivelyGetAllNodes(
    theTree, pynusmv.nusmv.parser.parser.PSLSPEC, thePslSpecNodeList )

  ##
  # accesses all LTL properties in the file:
  theTree = pynusmv.nusmv.parser.parser.cvar.parsed_tree
  theLtlSpecNodeList = list()
  recursivelyGetAllNodes(
    theTree, pynusmv.nusmv.parser.parser.LTLSPEC, theLtlSpecNodeList )

  ##
  # accesses all invariant properties in the file:
  theTree = pynusmv.nusmv.parser.parser.cvar.parsed_tree
  theInvarSpecNodeList = list()
  recursivelyGetAllNodes(
    theTree, pynusmv.nusmv.parser.parser.INVARSPEC, theInvarSpecNodeList )

  ##
  # first, get the names and expressions with no modification:
  theNameList           = list()
  theUnmodifiedPropList = list()
  #
  for thePslSpecNode in thePslSpecNodeList:
    # saves the name of the property (left node):
    theNameNode   = pynusmv.nusmv.node.node.cdr(thePslSpecNode)
    theNameString = pynusmv.nusmv.node.node.sprint_node(theNameNode)
    list.append(theNameList, str.replace(theNameString, " ", "_"))
    # accesses the property (right node):
    thePslNode   = pynusmv.nusmv.node.node.car(thePslSpecNode)
    thePslString = pynusmv.nusmv.node.node.sprint_node(thePslNode)
    list.append(theUnmodifiedPropList, "PSLSPEC " + str.strip(thePslString))
  #
  for theLtlSpecNode in theLtlSpecNodeList:
    #
    theNameNode   = pynusmv.nusmv.node.node.cdr(theLtlSpecNode)
    theNameString = pynusmv.nusmv.node.node.sprint_node(theNameNode)
    list.append(theNameList, str.replace(theNameString, " ", "_"))
    #
    theLtlSpecNode = pynusmv.nusmv.node.node.car(theLtlSpecNode)
    theLtlString   = pynusmv.nusmv.node.node.sprint_node(theLtlSpecNode)
    list.append(theUnmodifiedPropList, "LTLSPEC " + str.strip(theLtlString))
  #
  for theInvarSpecNode in theInvarSpecNodeList:
    #
    theNameNode   = pynusmv.nusmv.node.node.cdr(theInvarSpecNode)
    theNameString = pynusmv.nusmv.node.node.sprint_node(theNameNode)
    list.append(theNameList, str.replace(theNameString, " ", "_"))
    #
    theInvarSpecNode = pynusmv.nusmv.node.node.car(theInvarSpecNode)
    theInvarString   = pynusmv.nusmv.node.node.sprint_node(theInvarSpecNode)
    list.append(theUnmodifiedPropList, "INVARSPEC " + str.strip(theInvarString))


  ##
  # second, get the converted properties in the same order:
  thePropList = list() # list of converted properties

  for thePslSpecNode in thePslSpecNodeList:
    # accesses the property (right node):
    thePslNode = pynusmv.nusmv.node.node.car(thePslSpecNode)
    #
    theTextList = list()
    psl2coq_walkPsl(thePslNode, theTextList)
    theCoqString = "".join(theTextList)
    if ( "forall π : Path, π ⊨" not in theCoqString ):
      theCoqString = "forall π : Path, π ⊨\n  " + theCoqString
    list.append(thePropList, str.strip(theCoqString))

  for theLtlSpecNode in theLtlSpecNodeList:
    # accesses the property (right node):
    theLtlNode = pynusmv.nusmv.node.node.car(theLtlSpecNode)
    #
    theTextList = list()
    psl2coq_walkPsl(theLtlNode, theTextList)
    theCoqString = "".join(theTextList)
    if ( "forall π : Path, π ⊨" not in theCoqString ):
      theCoqString = "forall π : Path, π ⊨\n  " + theCoqString
    list.append(thePropList, str.strip(theCoqString))

  for theInvarSpecNode in theInvarSpecNodeList:
    # accesses the property (right node):
    theInvarSpecNode = pynusmv.nusmv.node.node.car(theInvarSpecNode)
    theInvarString = pynusmv.nusmv.node.node.sprint_node(theInvarSpecNode)
    # replace next() operators from INVARSPEC to X() operators from LTLSPEC:
    theNextList = list()
    recursivelyGetAllNodes(
      theInvarSpecNode, pynusmv.nusmv.parser.parser.NEXT, theNextList )
    for theNextNode in theNextList:
      theNextNode.type = pynusmv.nusmv.parser.parser.OP_NEXT
    # wrap the INVARSPEC node into a new node to say that the invariant is
    # globally true:
    theGloballyNode = pynusmv.nusmv.node.node.node()
    pynusmv.nusmv.node.node.node_set_type(theGloballyNode,
                                          pynusmv.nusmv.parser.parser.OP_GLOBAL)
    pynusmv.nusmv.node.node.setcar(theGloballyNode, theInvarSpecNode)
    #
    theTextList = list()
    psl2coq_walkPsl(theGloballyNode, theTextList)
    theCoqString = "".join(theTextList)
    if ( "forall π : Path, π ⊨" not in theCoqString ):
      theCoqString = "forall π : Path, π ⊨\n  " + theCoqString
    list.append(thePropList, str.strip(theCoqString))

  ##
  # in the end, create a list with elements taken 3 by 3:
  if ( (len(theNameList) != len(theUnmodifiedPropList)) or
       (len(theNameList) != len(thePropList))
  ):
    print("DEBUG: error in psl2coq_parseSmvFile(), verify property lists.")
  else:
    the3PropList = list()
    for i in range(0, len(theNameList)):
      list.append(the3PropList, theNameList[i])
      list.append(the3PropList, theUnmodifiedPropList[i])
      list.append(the3PropList, thePropList[i])

  return the3PropList

#===============================================================================

##
# \brief
#   Creates a coq script with an hypothesis for each converted property.
#
# \returns the text to be printed in the script.
#
def psl2coq_getOutput(
  thePropList   #!< list of elements to be taken 3 by 3, verified properties
):

  theText = ""
  for i in range(0, len(thePropList), 3):
    theName      = thePropList[i]
    thePslString = thePropList[i+1]
    theLtlString = thePropList[i+2]
    #
    # filter the properties:
    try:
      theMatch = re.search(psl2coq_filterString, theName)
    except:
      print("** error ** with regexp pattern: " + psl2coq_filterString)
    else:
      if ( theMatch != None ):
        # post-processing for the name:
        theName = "".join([c if c.isalnum() else "_" for c in theName])
        theName = re.sub(r"_+", "_", theName)

        #
        theText += "\n"
        theText += "(**\n * " + str(thePslString) + "\n *)\n"
        theText += "Axiom " + str(theName) + ":\n"
        theText += str(theLtlString) + "." + "\n"
        theText += "\n("
        for j in range(0, 78): theText += "*"
        theText += ")\n"

  return theText

#===============================================================================

##
# \brief
#   Parses the vector of arguments and updates global variables \ref
#   psl2coq_propertiesFilePath and \ref psl2coq_outputFilePath.
#
# \details
#   More informations about the syntax are given in the text printed by \ref
#   psl2coq_printHelpAndExit().
#
def psl2coq_parseArgv(
  argv  #!< vector of arguments as accessed with <tt> sys.argv </tt>
):
  global psl2coq_propertiesFilePath
  global psl2coq_outputFilePath
  global psl2coq_filterString

  if ( len(argv) < 2 ):
    print("** Error ** you must specify at least one argument")
    exit(1)

  previousArg = str()
  for theArg in argv[1:len(argv)]:
    if ( str.find(theArg, "-") == 0 ):
      if   ( str(theArg) == "-p" or str(theArg) == "--properties"     ):
        previousArg = "properties"
      elif ( str(theArg) == "-t" or str(theArg) == "--to"             ):
        previousArg = "to"
      elif ( str(theArg) == "-F" or str(theArg) == "--filter"         ):
        previousArg = "filter"
      elif ( str(theArg) == "-h" or str(theArg) == "--help"           ):
        psl2coq_printHelpAndExit(0)
      else:
        print("** Error ** unable to parse argument: " + str(theArg))
        exit(2)
    else:
      if   ( str(previousArg) == "properties" ):
        psl2coq_propertiesFilePath = str(theArg)
        previousArg = ""
      elif ( str(previousArg) == "to" ):
        psl2coq_outputFilePath = str(theArg)
        previousArg = ""
      elif ( str(previousArg) == "filter" ):
        psl2coq_filterString = str(theArg)
        previousArg = ""
      else:
        print("** Error ** unable to parse argument: " \
                + str(previousArg) + str(theArg) )

  fileExists = True
  if ( not os.path.exists(str(psl2coq_propertiesFilePath)) ):
    fileExists = False

  if ( not fileExists ):
    print("** Error ** you must specify existing files for the properties.")
    exit(3)

#===============================================================================
#===============================================================================

psl2coq_parseArgv(sys.argv)

pynusmv.init.init_nusmv()

thePropList = psl2coq_parseSmvFile(psl2coq_propertiesFilePath)
theText     = psl2coq_getOutput(thePropList)

if ( psl2coq_outputFilePath == str() ):
  print(theText)
else:
  try :
    theBuffer = open(psl2coq_outputFilePath, "w")
    theBuffer.write(theText)
    theBuffer.close()
  except :
    print("** Error ** Unable to write to file: " + psl2coq_outputFilePath)
    exit(1)
  else :
    print("Creation of file: " + psl2coq_outputFilePath)

