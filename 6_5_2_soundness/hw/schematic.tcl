
#
# Copyright (C) 2021 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

#
# This script must be sourced when a bd_design has been created.
#

set_property source_mgmt_mode All [current_project]

## IMPORTANT! Use digilent pre-configuration for the board, see
## vivadoMakefile.tcl, option --board.

##
# ports from the FPGA we are going to use, see constraints file:
#
create_bd_port -dir "I" "clk" -type "CLK"
create_bd_port -dir "I" "btn_0"
create_bd_port -dir "O" "led_0"
#
create_bd_port -dir "I" "btn_1"
create_bd_port -dir "O" "led_1"

# witness:
connect_bd_net [get_bd_ports "btn_0"] [get_bd_ports "led_0"]


##
# adding zynq processing system to the design:
#
set theInstName "processing_system7_0"
create_bd_cell -type "ip" \
               -vlnv "xilinx.com:ip:processing_system7:5.5" \
               ${theInstName}
# run block automation:
apply_bd_automation -rule "xilinx.com:bd_rule:processing_system7" -config {
  make_external      "FIXED_IO, DDR"
  apply_board_preset 1
  Master             "Disable"
  Slave              "Disable"
} [get_bd_cells ${theInstName}]
# connect AXI GP0 input clock to an external clock:
connect_bd_net [get_bd_ports "clk"] \
               [get_bd_pins "${theInstName}/M_AXI_GP0_ACLK"]
# deactivate output clock for PL:
set_property -dict {
  CONFIG.PCW_EN_CLK0_PORT 0
} [get_bd_cells ${theInstName}]
# enable trace:
set_property -dict {
  CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE 1
  CONFIG.PCW_TRACE_PERIPHERAL_ENABLE   1
  CONFIG.PCW_TRACE_GRP_2BIT_ENABLE     1
  CONFIG.PCW_TRACE_GRP_4BIT_ENABLE     1
  CONFIG.PCW_TRACE_GRP_8BIT_ENABLE     1
} [get_bd_cells ${theInstName}]
#
connect_bd_net [get_bd_ports "clk"] \
               [get_bd_pins "/${theInstName}/TRACE_CLK"]
# enables EMIO GPIO and sets its size:
set_property -dict {
  CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE 1
  CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE     1
  CONFIG.PCW_GPIO_EMIO_GPIO_IO         2
} [get_bd_cells ${theInstName}]
# save its instance name:
set theZynq ${theInstName}

# split the GPIO vector:
set theInstName "i_connexions"
create_bd_cell -type "module" -reference "connexions" ${theInstName}
#
connect_bd_net [get_bd_pins "${theInstName}/gpio_o"] \
               [get_bd_pins "${theZynq}/GPIO_O"]
# save its instance name:
set theGpio ${theInstName}

#===============================================================================

set theSlaveNameList [list key sw_att ex_stack logic_analyzer]
foreach theSlaveName ${theSlaveNameList} {

  ##
  # instantiate a BRAM to store the slave
  set theInstName "i_${theSlaveName}"
  create_bd_cell -type "module" -reference ${theSlaveName} ${theInstName}
  # saving its instance name:
  set theSlave ${theInstName}


  ##
  # adding a BRAM controller:
  set theInstName "i_bram_ctrl_${theSlaveName}"
  create_bd_cell -type "ip" \
                 -vlnv "xilinx.com:ip:axi_bram_ctrl:4.1" \
                 ${theInstName}
  # setting it to single port AXI Lite:
  set_property -dict {
    CONFIG.SINGLE_PORT_BRAM 1
    CONFIG.PROTOCOL         AXI4LITE
    CONFIG.ECC_TYPE         0
  } [get_bd_cells ${theInstName}]
  # connect clock:
  connect_bd_net [get_bd_ports "clk"] [get_bd_pins "${theInstName}/s_axi_aclk"]
  # saving its instance name:
  set theBramController ${theInstName}


  ##
  # run block automation:
  apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config [list \
    Clk_master "/clk (100 MHz)"                  \
    Clk_slave  "/clk (100 MHz)"                  \
    Clk_xbar   "/clk (100 MHz)"                  \
    Master     "/${theZynq}/M_AXI_GP0"           \
    Slave      "/${theBramController}/S_AXI"     \
    ddr_seg    "Auto"                            \
    intc_ip    "New AXI SmartConnect"            \
    master_apm 0
  ] [get_bd_intf_pins "${theBramController}/S_AXI"]


  ##
  # changes the addresses range according to the infered BRAM:
  switch $theSlaveName {
    key      { set theRange "4K"  }
    sw_att   { set theRange "16K" }
    ex_stack { set theRange "4K"  }
    default  { set theRange "4K"  }
  }
  set_property range ${theRange} \
    [get_bd_addr_segs "${theZynq}/Data/SEG_${theBramController}_Mem0"]
  #
  regenerate_bd_layout

  ##
  # connections between the BRAM controller and the slave:
  set thePort "a" ;# either "a" or "b"
  connect_bd_net [get_bd_pins "${theBramController}/bram_addr_${thePort}"]   \
                 [get_bd_pins "${theSlave}/addr"]
  connect_bd_net [get_bd_pins "${theBramController}/bram_clk_${thePort}"]    \
                 [get_bd_pins "${theSlave}/clk"]
  connect_bd_net [get_bd_pins "${theBramController}/bram_rddata_${thePort}"] \
                 [get_bd_pins "${theSlave}/rddata"]
  # for key and sw_att, write enable from software AND hardware:
  if { $theSlaveName == "key" || $theSlaveName == "sw_att" } {
    connect_bd_net [get_bd_pins "${theBramController}/bram_wrdata_${thePort}"] \
                   [get_bd_pins "${theSlave}/wrdata"]
    connect_bd_net [get_bd_pins "${theBramController}/bram_we_${thePort}"] \
                   [get_bd_pins "${theSlave}/we"]
    # hardware write-enable for the attesting software, btn_0 on the board:
    connect_bd_net [get_bd_ports "btn_0"] [get_bd_pins  "${theSlave}/hard_we"]
  } else {
  # for all other BRAM, write enable from software alone:
    connect_bd_net [get_bd_pins "${theBramController}/bram_wrdata_${thePort}"] \
                   [get_bd_pins "${theSlave}/wrdata"]
    connect_bd_net [get_bd_pins "${theBramController}/bram_we_${thePort}"] \
                   [get_bd_pins "${theSlave}/we"]
    connect_bd_net [get_bd_pins "${theBramController}/bram_rst_${thePort}"] \
                   [get_bd_pins "${theSlave}/rst"]
  }

}

# reset of the AXI slaves by the PS7:
apply_bd_automation -rule xilinx.com:bd_rule:board -config [list  \
  Manual_Source "/${theZynq}/FCLK_RESET0_N (ACTIVE_LOW)"          \
] [get_bd_pins "rst_clk_100M/ext_reset_in"]

#===============================================================================

##
# instantiate a Verilog module:
#
set theInstName "i_coresight2pl"
create_bd_cell -type "module" -reference "coresight2pl" ${theInstName}
#
connect_bd_net [get_bd_pins "${theInstName}/nrst"]       \
               [get_bd_pins "rst_*/peripheral_aresetn"]
#
connect_bd_net [get_bd_pins "${theInstName}/mrst"]       \
               [get_bd_ports "btn_1"]
#
connect_bd_net [get_bd_pins "${theInstName}/clk"]        \
               [get_bd_ports "clk"]

# connexions with CoreSight:
connect_bd_net [get_bd_pins "${theInstName}/trace_data"] \
               [get_bd_pins "${theZynq}/TRACE_DATA"]
#
connect_bd_net [get_bd_pins "${theInstName}/trace_ctl"]  \
               [get_bd_pins "${theZynq}/TRACE_CTL"]

# when we tap into the AXI interface, we need to keep the connexions between the
# SmartConnect and the AXI BRAM Controller:
set theIndex [lsearch ${theSlaveNameList} key]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arvalid"] \
               [get_bd_pins "i_bram_ctrl_key/s_axi_arvalid"]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arvalid"] \
               [get_bd_pins "${theInstName}/KR_axi_arvalid"]
#
set theIndex [lsearch ${theSlaveNameList} ex_stack]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arvalid"] \
               [get_bd_pins "i_bram_ctrl_ex_stack/s_axi_arvalid"]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arvalid"] \
               [get_bd_pins "${theInstName}/XS_axi_arvalid"]
#
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_awvalid"] \
               [get_bd_pins "i_bram_ctrl_ex_stack/s_axi_awvalid"]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_awvalid"] \
               [get_bd_pins "${theInstName}/XS_axi_awvalid"]
#
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_wvalid"]  \
               [get_bd_pins "i_bram_ctrl_ex_stack/s_axi_wvalid"]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_wvalid"]  \
               [get_bd_pins "${theInstName}/XS_axi_wvalid"]
#
set theIndex [lsearch ${theSlaveNameList} sw_att]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_araddr"] \
               [get_bd_pins "i_bram_ctrl_sw_att/s_axi_araddr"]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_araddr"] \
               [get_bd_pins "${theInstName}/sw_araddr"]
#
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arvalid"] \
               [get_bd_pins "i_bram_ctrl_sw_att/s_axi_arvalid"]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arvalid"] \
               [get_bd_pins "${theInstName}/sw_arvalid"]

# save its instance name:
set theModule ${theInstName}


##
# adding a BRAM controller to access the expected data (for debug purposes):
set theInstName "i_bram_ctrl_calibration"
create_bd_cell -type "ip" \
               -vlnv "xilinx.com:ip:axi_bram_ctrl:4.1" \
               ${theInstName}
# setting it to single port AXI Lite:
set_property -dict {
  CONFIG.SINGLE_PORT_BRAM 1
  CONFIG.PROTOCOL         AXI4LITE
  CONFIG.ECC_TYPE         0
} [get_bd_cells ${theInstName}]
# connect clock:
connect_bd_net [get_bd_ports "clk"] [get_bd_pins "${theInstName}/s_axi_aclk"]
# saving its instance name:
set theBramController ${theInstName}

# run block automation:
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config [list \
  Clk_master "/clk (100 MHz)"                  \
  Clk_slave  "/clk (100 MHz)"                  \
  Clk_xbar   "/clk (100 MHz)"                  \
  Master     "/${theZynq}/M_AXI_GP0"           \
  Slave      "/${theBramController}/S_AXI"     \
  ddr_seg    "Auto"                            \
  intc_ip    "New AXI SmartConnect"            \
  master_apm 0
] [get_bd_intf_pins "${theBramController}/S_AXI"]

# change the address range:
set_property range "8K" \
  [get_bd_addr_segs "${theZynq}/Data/SEG_${theBramController}_Mem0"]

# connections between the BRAM controller and the module:
set thePort "a" ;# either "a" or "b"
connect_bd_net [get_bd_pins "${theBramController}/bram_addr_${thePort}"]   \
               [get_bd_pins "${theModule}/rom_addr"]
connect_bd_net [get_bd_pins "${theBramController}/bram_clk_${thePort}"]    \
               [get_bd_pins "${theModule}/rom_clk"]
connect_bd_net [get_bd_pins "${theBramController}/bram_rddata_${thePort}"] \
               [get_bd_pins "${theModule}/rom_rddata"]
connect_bd_net [get_bd_pins "${theBramController}/bram_wrdata_${thePort}"] \
               [get_bd_pins "${theModule}/rom_wrdata"]
connect_bd_net [get_bd_pins "${theBramController}/bram_we_${thePort}"] \
               [get_bd_pins "${theModule}/rom_we"]
connect_bd_net [get_bd_pins "${theBramController}/bram_rst_${thePort}"] \
               [get_bd_pins "${theModule}/rom_rst"]

# connexions between the GPIO and the module:
connect_bd_net [get_bd_pins "${theGpio}/gpio_1"] \
               [get_bd_pins "${theModule}/calibration"]

#===============================================================================

##
# connect the inputs of the logic analyser (instantiated in the foreach loop):
#
set theLogicA "i_logic_analyzer"
#
connect_bd_net [get_bd_pins "${theLogicA}/mclk"] \
               [get_bd_ports "clk"]
#
connect_bd_net [get_bd_pins "${theLogicA}/nrst"] \
               [get_bd_pins "rst_*/peripheral_aresetn"]
#
connect_bd_net [get_bd_pins "${theGpio}/gpio_0"] \
               [get_bd_pins "${theLogicA}/mrst"]

# when we tap into the AXI interface, we need to keep the connexions between the
# SmartConnect and the AXI BRAM Controller:
set theIndex [lsearch ${theSlaveNameList} sw_att]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arvalid"] \
               [get_bd_pins "${theLogicA}/sw_arvalid"]
#
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_araddr"] \
               [get_bd_pins "${theLogicA}/sw_araddr"]
#
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arready"] \
               [get_bd_pins "${theLogicA}/sw_arready"]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arready"] \
               [get_bd_pins "i_bram_ctrl_sw_att/s_axi_arready"]
#
set theIndex [lsearch ${theSlaveNameList} key]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arvalid"] \
               [get_bd_pins "${theLogicA}/KR_axi_arvalid"]
#
set theIndex [lsearch ${theSlaveNameList} ex_stack]
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_arvalid"] \
               [get_bd_pins "${theLogicA}/XS_axi_arvalid"]
#
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_awvalid"] \
               [get_bd_pins "${theLogicA}/XS_axi_awvalid"]
#
connect_bd_net [get_bd_pins "/axi_smc/M0${theIndex}_AXI_wvalid"]  \
               [get_bd_pins "${theLogicA}/XS_axi_wvalid"]

# coresight outputs:
connect_bd_net [get_bd_pins "${theLogicA}/trace_data"] \
               [get_bd_pins "${theZynq}/TRACE_DATA"]
#
connect_bd_net [get_bd_pins "${theLogicA}/trace_ctl"]  \
               [get_bd_pins "${theZynq}/TRACE_CTL"]

#===============================================================================

##
# debug: LED to witness the reset of the zynq
#
set theLockUp "i_lock_up"
create_bd_cell -type "module" -reference "lock_up" ${theLockUp}
#
connect_bd_net \
  [get_bd_ports "clk"]                     [get_bd_pins "${theLockUp}/clk"]
connect_bd_net \
  [get_bd_pins "rst_*/peripheral_aresetn"] [get_bd_pins "${theLockUp}/nrst"]
connect_bd_net \
  [get_bd_ports "btn_1"]                   [get_bd_pins "${theLockUp}/rst"]
connect_bd_net \
  [get_bd_pins "${theModule}/reset"]       [get_bd_pins "${theLockUp}/in"]
connect_bd_net \
  [get_bd_pins "${theLockUp}/out"]         [get_bd_ports "led_1"]

regenerate_bd_layout

