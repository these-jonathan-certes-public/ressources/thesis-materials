
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

module decode
(
  input        nrst,
  input        clk,
  input [31:0] data,
  input [31:0] expected0,
  input [31:0] expected1
);

  /***********************************
   * signals from the logic analyser:
   ***********************************/
  wire        sw_arvalid;
  wire [13:0] sw_araddr;
  /* verilator lint_off UNUSED */
  wire        sw_arready;
  /* verilator lint_on  UNUSED */
  wire        KR_axi_arvalid;
  wire        XS_axi_arvalid;
  wire        XS_axi_awvalid;
  wire        XS_axi_wvalid;
  /* verilator lint_off UNUSED */
  wire  [2:0] dummy0;
  /* verilator lint_on  UNUSED */
  wire  [7:0] trace_data;
  wire        trace_ctl;

  assign {
    sw_arvalid,
    sw_araddr,
    sw_arready,
    KR_axi_arvalid,
    XS_axi_arvalid,
    XS_axi_awvalid,
    XS_axi_wvalid
  } = data[31:12];

  assign dummy0 = data[11:9];

  assign {
    trace_data,
    trace_ctl
  } = data[8:0];


  /********************
   * expected signals:
   ********************/
  /* verilator lint_off UNUSED */
  wire [13:0] expected_sw_araddr;
  wire        expected_sw_arvalid;
  wire        expected_trace_ctl;
  wire  [7:0] expected_trace_data;
  wire [31:0] expected_decoded_address;
  wire  [7:0] dummy1;
  /* verilator lint_on  UNUSED */

  assign {
    dummy1,
    expected_trace_data,
    expected_trace_ctl,
    expected_sw_arvalid,
    expected_sw_araddr
  } = expected0;

  assign expected_decoded_address = expected1;

/******************************************************************************/
/* DEBUG: adding CoreSight packet decompresser and decoder to the testbench so
 * that we can observe the destination addresses.
 */

  // trick the FSM so that it acts as it already received an A-sync packet (in
  // real life, it happens before the logic analyser records):
  initial begin
    //`define ASYNC_DONE
    `ifdef ASYNC_DONE
      i_coresight2pl.i_async_decompress.state =
      i_coresight2pl.i_async_decompress.state_end;
      //
      i_coresight2pl.i_async_decompress.count = 1;
    `else
      i_coresight2pl.i_async_decompress.state =
      i_coresight2pl.i_async_decompress.state_header;
      //
      i_coresight2pl.i_async_decompress.count = 4;
    `endif
    i_coresight2pl.i_overall_pft_trace_decompress.state =
    i_coresight2pl.i_overall_pft_trace_decompress.state_continue_scan;
  end

  /* verilator lint_off UNUSED */
  wire [31:0] w_rom_rddata;
  wire        w_reset;
  wire        w_decompress_error;
  /* verilator lint_on  UNUSED */

  coresight2pl #(
    .CRMIN_ADDR( 32'h0020_0020 ),
    .CRMAX_ADDR( 32'h0020_0060 )
  ) i_coresight2pl (
    .nrst(           nrst           ),
    .mrst(          !nrst           ),
    .clk(            clk            ),
    .KR_axi_arvalid( KR_axi_arvalid ),
    .XS_axi_arvalid( XS_axi_arvalid ),
    .XS_axi_awvalid( XS_axi_awvalid ),
    .XS_axi_wvalid(  XS_axi_wvalid  ),
    .trace_data(     trace_data     ),
    .trace_ctl(      trace_ctl      ),
    .sw_araddr(      sw_araddr      ),
    .sw_arvalid(     sw_arvalid     ),
    // ROM is not present in the simulation:
    .calibration(1'b0          ),
    .rom_clk(    clk           ),
    .rom_rst(    1'b0          ),
    .rom_addr(   13'h000       ),
    .rom_wrdata( 32'h0000_0000 ),
    .rom_rddata( w_rom_rddata  ),
    .rom_we(     4'b0000       ),
    //
    .reset(            w_reset            ),
    .decompress_error( w_decompress_error )
  );

endmodule

