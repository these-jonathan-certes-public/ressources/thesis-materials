
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* verilator lint_off UNUSED */

/**
 * \brief
 *  Reads data from a 32-bit vector and writes it to a BRAM interface.
 *
 * \details
 *  Starts writing when a trigger is received, ends when the BRAM is full.
 *
 * \warning
 *  Last 16 bits are not written.
 */
module parallel2bram
#(
  parameter ADDR_WIDTH = 13 //!< block RAM address bus width, number of bits
) (
  input clk,  //!< master clock, identical to block RAM clock
  input nrst, //!< active when 0, restore bram_addr and bram_we to zero
  //
  input [31:0] data,
  input trigger,
  // bram:
  output [ADDR_WIDTH+1:2] bram_addr, //!< assigned to MSB of the Byte index
  output bram_clk,                   //!< is assigned to clk
  output [31:0] bram_wrdata,         //!< is assigned to data
  input  [31:0] bram_rddata,         //!< unused
  output        bram_rst,            //!< active-high reset
  output  [3:0] bram_we              //!< assigned to LSB of the Byte index
);

  reg [ADDR_WIDTH+1:0] r_address;
  reg r_enable;
  reg r_done;

  initial begin
    r_address = { (ADDR_WIDTH+2){1'b0} };
    r_enable  = 1'b0;
    r_done    = 1'b0;
  end

  always @( posedge(clk) ) begin
    if ( nrst == 1'b0 ) begin
      r_address <= { (ADDR_WIDTH+2){1'b0} };
      r_enable  <= 1'b0;
      r_done    <= 1'b0;
    end
    else begin

      /* stops writing when the BRAM is full: */
      if ( r_address == { (ADDR_WIDTH+2){1'b0} } ) begin
        if ( r_enable ) begin
          r_done    <= 1'b1;
          r_address <= { (ADDR_WIDTH+2){1'b0} };
        end
        else if ( !r_done ) begin
          if ( trigger ) begin
            r_enable  <= 1'b1;
            r_address <= r_address + 4;
          end
          else begin
            r_enable  <= 1'b0;
            r_address <= { (ADDR_WIDTH+2){1'b0} };
          end
        end
      end
      else begin
        if ( !r_done && (trigger || r_enable) ) begin
          r_address <= r_address + 4;
        end
      end

    end
  end

  assign bram_addr   = r_address[ADDR_WIDTH+1:2];
  assign bram_clk    = clk;
  assign bram_wrdata = data;
  assign bram_rst    = !nrst;

  /* stops writing when the BRAM is full: */
  assign bram_we =
    ( r_done )                                              ? 4'b0000 :
    ( !trigger && !r_enable )                               ? 4'b0000 :
    ( (r_address == { (ADDR_WIDTH+2){1'b0} }) && !trigger ) ? 4'b0000 : 4'b1111;

endmodule
