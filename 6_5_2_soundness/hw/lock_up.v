
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \brief
 *  Locks signal <tt> out </tt> to 1 when <tt> in </tt> is set.
 *
 * \details
 *  Initial value for signal <tt> out </tt> is 0.
 */
module lock_up(
  input  clk,   //!< clock
  input  nrst,  //!< active low reset
  input  rst,   //!< active high reset
  input  in,    //!< input to latch up
  output out    //!< latch output
);

  reg r_out;

  initial begin
    r_out = 1'b0;
  end

  always @( posedge(clk) ) begin
    if ( rst || !nrst ) begin
      r_out <= 1'b0;
    end
    else if ( in ) begin
      r_out <= 1'b1;
    end
  end

  /****************************************************************************/

  assign out = r_out;

endmodule
