
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \brief
 *  Stores the sw_att in a read-only mode BRAM.
 *
 * \bug
 *  Initialization does not work as it must be done after the synthesis.
 *  Execution script or Makefile must be updated accordingly.
 */
module sw_att
#(
  parameter FILE_INIT  = ""  //!< path to a file with the hex content
) (
  input  [13:0] addr,   //!< address where to access memory (2 LSB left unused)
  input  clk,           //!< clock
  input  [31:0] wrdata, //!< data to write to memory
  output [31:0] rddata, //!< data read from memory
  input  [3:0]  we,     //!< write-enable bits, one for each Byte
  input  hard_we        //!< if unset, write-enable bits are forced to 0
);

  wire rst;
  assign rst = 1'b0;

  /* local write-enable: reads "we" only if "hard_we" is set */
  wire [3:0] we_local;
  assign we_local = ( hard_we ) ? we : 4'b0000;

  /* we instantiate four 4K BRAM to achieve 16K */
  wire  [3:0] bram0_we;
  wire  [3:0] bram1_we;
  wire  [3:0] bram2_we;
  wire  [3:0] bram3_we;
  assign bram0_we = ( addr[13:12] == 2'b00 ) ? we_local     : 4'b0000;
  assign bram1_we = ( addr[13:12] == 2'b01 ) ? we_local     : 4'b0000;
  assign bram2_we = ( addr[13:12] == 2'b10 ) ? we_local     : 4'b0000;
  assign bram3_we = ( addr[13:12] == 2'b11 ) ? we_local     : 4'b0000;
  //
  wire [31:0] bram0_rddata;
  wire [31:0] bram1_rddata;
  wire [31:0] bram2_rddata;
  wire [31:0] bram3_rddata;
  assign rddata   = ( addr[13:12] == 2'b00 ) ? bram0_rddata :
                    ( addr[13:12] == 2'b01 ) ? bram1_rddata :
                    ( addr[13:12] == 2'b10 ) ? bram2_rddata : bram3_rddata;

  // BRAM_SINGLE_MACRO: Single Port RAM
  //                    Artix-7
  // Xilinx HDL Language Template, version 2019.2

  /////////////////////////////////////////////////////////////////////
  //  READ_WIDTH | BRAM_SIZE | READ Depth  | ADDR Width |            //
  // WRITE_WIDTH |           | WRITE Depth |            |  WE Width  //
  // ============|===========|=============|============|============//
  //    37-72    |  "36Kb"   |      512    |    9-bit   |    8-bit   //
  //    19-36    |  "36Kb"   |     1024    |   10-bit   |    4-bit   //
  //    19-36    |  "18Kb"   |      512    |    9-bit   |    4-bit   //
  //    10-18    |  "36Kb"   |     2048    |   11-bit   |    2-bit   //
  //    10-18    |  "18Kb"   |     1024    |   10-bit   |    2-bit   //
  //     5-9     |  "36Kb"   |     4096    |   12-bit   |    1-bit   //
  //     5-9     |  "18Kb"   |     2048    |   11-bit   |    1-bit   //
  //     3-4     |  "36Kb"   |     8192    |   13-bit   |    1-bit   //
  //     3-4     |  "18Kb"   |     4096    |   12-bit   |    1-bit   //
  //       2     |  "36Kb"   |    16384    |   14-bit   |    1-bit   //
  //       2     |  "18Kb"   |     8192    |   13-bit   |    1-bit   //
  //       1     |  "36Kb"   |    32768    |   15-bit   |    1-bit   //
  //       1     |  "18Kb"   |    16384    |   14-bit   |    1-bit   //
  /////////////////////////////////////////////////////////////////////

  BRAM_SINGLE_MACRO #(
.INIT_00(256'he1a0f003e28f3000e320f000e320f000e320f000e320f000e320f000e320f000),
.INIT_01(256'he1a0f003e28f3000e320f000e5832000e2822001e5932000e3403100e3003000),
.INIT_02(256'he1a0f003e28f3000e320f000e320f000e320f000e320f000e320f000e320f000),
.INIT_03(256'he320f000e320f000e320f000e320f000e320f000e320f000e320f000e12fff1e),
.INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .BRAM_SIZE("36Kb"),        // Target BRAM, "18Kb" or "36Kb"
     .DEVICE("7SERIES"),        // Target Device: "7SERIES"
     .DO_REG(0),                // Optional output register (0 or 1)
     .INIT(36'h000000000),      // Initial values on output port
     .INIT_FILE (FILE_INIT),
     .WRITE_WIDTH(32),          // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .READ_WIDTH(32),           // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .SRVAL(36'h000000000),     // Set/Reset value for port output
     .WRITE_MODE("WRITE_FIRST") // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
  ) i_bram0_single (
     .DO(bram0_rddata), // Output data, width defined by READ_WIDTH parameter
     .ADDR(addr[11:2]), // Input address, width defined by read/write port depth
     .CLK(clk),         // 1-bit input clock
     .DI(wrdata),       // Input data port, width defined by WRITE_WIDTH parameter
     .EN(1'b1),         // 1-bit input RAM enable
     .REGCE(1'b0),      // 1-bit input output register enable
     .RST(rst),         // 1-bit input reset
     .WE(bram0_we)      // Input write enable, width defined by write port depth
  );

  BRAM_SINGLE_MACRO #(
.INIT_00(256'h060039412d786574726f43050000001c0100696261656100000026410144012a),
.INIT_01(256'h0000000000000000000000000000000000000000000144012a0209010841070a),
     .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .BRAM_SIZE("36Kb"),        // Target BRAM, "18Kb" or "36Kb"
     .DEVICE("7SERIES"),        // Target Device: "7SERIES"
     .DO_REG(0),                // Optional output register (0 or 1)
     .INIT(36'h000000000),      // Initial values on output port
     .INIT_FILE (FILE_INIT),
     .WRITE_WIDTH(32),          // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .READ_WIDTH(32),           // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .SRVAL(36'h000000000),     // Set/Reset value for port output
     .WRITE_MODE("WRITE_FIRST") // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
  ) i_bram1_single (
     .DO(bram1_rddata), // Output data, width defined by READ_WIDTH parameter
     .ADDR(addr[11:2]), // Input address, width defined by read/write port depth
     .CLK(clk),         // 1-bit input clock
     .DI(wrdata),       // Input data port, width defined by WRITE_WIDTH parameter
     .EN(1'b1),         // 1-bit input RAM enable
     .REGCE(1'b0),      // 1-bit input output register enable
     .RST(rst),         // 1-bit input reset
     .WE(bram1_we)      // Input write enable, width defined by write port depth
  );

  BRAM_SINGLE_MACRO #(
     .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .BRAM_SIZE("36Kb"),        // Target BRAM, "18Kb" or "36Kb"
     .DEVICE("7SERIES"),        // Target Device: "7SERIES"
     .DO_REG(0),                // Optional output register (0 or 1)
     .INIT(36'h000000000),      // Initial values on output port
     .INIT_FILE (FILE_INIT),
     .WRITE_WIDTH(32),          // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .READ_WIDTH(32),           // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .SRVAL(36'h000000000),     // Set/Reset value for port output
     .WRITE_MODE("WRITE_FIRST") // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
  ) i_bram2_single (
     .DO(bram2_rddata), // Output data, width defined by READ_WIDTH parameter
     .ADDR(addr[11:2]), // Input address, width defined by read/write port depth
     .CLK(clk),         // 1-bit input clock
     .DI(wrdata),       // Input data port, width defined by WRITE_WIDTH parameter
     .EN(1'b1),         // 1-bit input RAM enable
     .REGCE(1'b0),      // 1-bit input output register enable
     .RST(rst),         // 1-bit input reset
     .WE(bram2_we)      // Input write enable, width defined by write port depth
  );

  BRAM_SINGLE_MACRO #(
     .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
     .BRAM_SIZE("36Kb"),        // Target BRAM, "18Kb" or "36Kb"
     .DEVICE("7SERIES"),        // Target Device: "7SERIES"
     .DO_REG(0),                // Optional output register (0 or 1)
     .INIT(36'h000000000),      // Initial values on output port
     .INIT_FILE (FILE_INIT),
     .WRITE_WIDTH(32),          // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .READ_WIDTH(32),           // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
     .SRVAL(36'h000000000),     // Set/Reset value for port output
     .WRITE_MODE("WRITE_FIRST") // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
  ) i_bram3_single (
     .DO(bram3_rddata), // Output data, width defined by READ_WIDTH parameter
     .ADDR(addr[11:2]), // Input address, width defined by read/write port depth
     .CLK(clk),         // 1-bit input clock
     .DI(wrdata),       // Input data port, width defined by WRITE_WIDTH parameter
     .EN(1'b1),         // 1-bit input RAM enable
     .REGCE(1'b0),      // 1-bit input output register enable
     .RST(rst),         // 1-bit input reset
     .WE(bram3_we)      // Input write enable, width defined by write port depth
  );

  // End of BRAM_SINGLE_MACRO_inst instantiation

endmodule
