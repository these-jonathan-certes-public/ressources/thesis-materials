/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

`timescale 1ns / 1ps

/******************************************************************************/

/**
 * \brief
 *  Establishes the connexions between Z7 EMIO GPIO and some wires.
 */
module connexions
(
  input  [1:0] gpio_o,
  output gpio_0,
  output gpio_1
);
  assign gpio_0 = gpio_o[0];
  assign gpio_1 = gpio_o[1];
endmodule

