
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \brief
 *  Modified version of VRASED automaton for atomicity, uses signals from the
 *  transducer.
 */
module atomicity
(
  input clk,
  //
  input deduced_PC_eq_CRmin,
  input deduced_PC_eq_CRmax,
  input deduced_PC_in_CR,
  input deduced_irq_in_CR,
  //
  output reset
);

  reg [2:0] state;
  localparam [2:0] s_Reset  = 3'b000;
  localparam [2:0] s_notCR  = 3'b001;
  localparam [2:0] s_fstCR  = 3'b010;
  localparam [2:0] s_midCR  = 3'b011;
  localparam [2:0] s_lastCR = 3'b100;

  always @( posedge(clk) ) begin
    case( state )
      s_Reset:
        if (
          !deduced_PC_in_CR && !deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax
        ) begin
          state   <= s_notCR;
        end
        else begin
          state   <= s_Reset;
        end

      s_notCR:
        if (
          deduced_PC_in_CR && deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax
        ) begin
          state   <= s_fstCR;
        end
        else if (
          !deduced_PC_in_CR && !deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax
        ) begin
          state   <= s_notCR;
        end
        else begin
          state   <= s_Reset;
        end

      s_fstCR:
        if (
          deduced_PC_in_CR && !deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax
        ) begin
          state   <= s_midCR;
        end
        else if (
          deduced_PC_in_CR && deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax
        ) begin
          state   <= s_fstCR;
        end
        else begin
          state   <= s_Reset;
        end

      s_midCR:
        if (
          deduced_PC_in_CR && !deduced_PC_eq_CRmin && deduced_PC_eq_CRmax
        ) begin
          state   <= s_lastCR;
        end
        else if (
          deduced_PC_in_CR && !deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax
        ) begin
          state   <= s_midCR;
        end
        else begin
          state   <= s_Reset;
        end

      s_lastCR:
        if (
          !deduced_PC_in_CR && !deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax
        ) begin
          state   <= s_notCR;
        end
        else if (
          deduced_PC_in_CR && !deduced_PC_eq_CRmin && deduced_PC_eq_CRmax
        ) begin
          state   <= s_lastCR;
        end
        else begin
          state   <= s_Reset;
        end

      default:
        state <= s_Reset;

    endcase
  end

  /****************************************************************************/

  assign reset =
    deduced_irq_in_CR
      ||

    ( state == s_Reset )
      ||

    ( state == s_notCR  && (
         !( deduced_PC_in_CR &&  deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax)
      && !(!deduced_PC_in_CR && !deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax)
    )) ||

    ( state == s_fstCR  && (
         !( deduced_PC_in_CR && !deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax)
      && !( deduced_PC_in_CR &&  deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax)
    )) ||

    ( state == s_midCR  && (
         !( deduced_PC_in_CR && !deduced_PC_eq_CRmin &&  deduced_PC_eq_CRmax)
      && !( deduced_PC_in_CR && !deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax)
    )) ||

    ( state == s_lastCR && (
         !(!deduced_PC_in_CR && !deduced_PC_eq_CRmin && !deduced_PC_eq_CRmax)
      && !( deduced_PC_in_CR && !deduced_PC_eq_CRmin &&  deduced_PC_eq_CRmax)
    )) ||

    // handling incorrect state value:
    ( state != s_Reset  &&
      state != s_notCR  &&
      state != s_fstCR  &&
      state != s_midCR  &&
      state != s_lastCR
    );

endmodule

