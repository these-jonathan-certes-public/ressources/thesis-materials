
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* verilator lint_off UNUSED */
module monitor
#(
  parameter END_ADDR = 10'h13d  //!< address after which we consider that
                                //!  configuration is done
) (
  input mclk, //!< a clock that is external to the PS7
  input nrst, //!< active-low reset
  input mrst, //!< active-high reset

  input calibration,  //!< if set, we write the signals in the BRAM
                      // (only present for debug pruposes, must be removed
                      // before final release)

  input deduced_PC_eq_CRmax,  //!< given by the transducer

  // to monitor:
  input [13:0] sw_araddr,       //!< ARADDR  to AXI slave containing software
  input        sw_arvalid,      //!< ARVALID to AXI slave containing software
  input        trace_ctl,       //!< CoreSight enable bit
  input  [7:0] trace_data,      //!< CoreSight data
  input [31:0] decoded_address, //!< given by the decoder

  // to read the ROM (for debug purposes):
  input  [12:0] addr,   //!< address (2 LSB left unused)
  input         clk,    //!< clock
  input  [31:0] wrdata, //!< data to write to memory
  output [31:0] rddata, //!< data read from memory
  input         rst,    //!< active-high reset
  input  [3:0]  we,     //!< write-enable bits, one for each Byte

  output i_eq_end_addr, //!< boolean indicates that i = END_ADDR
  output verifying,     //!< says that the monitor is currently verifying
  output done,          //!< says that the monitor is expecting to reach CR_max
  output reset          //!< a mismatch occurs on a signal
);

  wire w_nrst;
  assign w_nrst = !( !nrst || mrst );

  wire w_verifying;
  assign verifying = w_verifying;

  /* we instantiate two 4K BRAM:
   * - one with all data except "decoded_address",
   * - the other with only "decoded_address"
   */
  wire  [3:0] bram0_we;
  wire  [3:0] bram1_we;
  assign bram0_we = ( addr[12] == 1'b0 ) ? we : 4'b0000;
  assign bram1_we = ( addr[12] == 1'b1 ) ? we : 4'b0000;
  //
  wire [31:0] bram0_rddata;
  wire [31:0] bram1_rddata;
  assign rddata = ( addr[12] == 1'b0 ) ? bram0_rddata : bram1_rddata;

  /* Connexions between the ROM containing the data and the decide module: */
  wire  [9:0] i;
  //
  wire [31:0] w_bram0_rddata;
  wire [31:0] w_bram1_rddata;
  wire [63:0] w_rddata;
  assign w_rddata = {
    8'b00000000,
    w_bram1_rddata[31:0],   // expected decoded_address
    w_bram0_rddata[23:16],  // expected trace_data
    w_bram0_rddata[15],     // expected trace_ctl
    w_bram0_rddata[14],     // expected sw_arvalid
    w_bram0_rddata[13:0]    // expected sw_araddr
  };

  /* during calibration, we overwrite the data in the BRAM: */
  wire [31:0] w_bram0_wrdata;
  wire [31:0] w_bram1_wrdata;
  assign w_bram0_wrdata = {
    8'b00000000,
    trace_data,
    trace_ctl,
    sw_arvalid,
    sw_araddr
  };
  assign w_bram1_wrdata = decoded_address;
  /*
   * we only overwrite when decide module is verifying.
   */
  wire [3:0] w_bram0_we;
  wire [3:0] w_bram1_we;
  assign w_bram0_we = (calibration && w_verifying) ? 4'b1111 : 4'b0000;
  assign w_bram1_we = (calibration && w_verifying) ? 4'b1111 : 4'b0000;
  /*
   * during calibration, we write the value at the address before to compensate
   * the time we take to read the data (one clock cycle):
   */
  wire [9:0] w_i;
  assign w_i = (calibration && w_verifying) ? (i - 1) : i;


  /**
   * formally verified module to ensure security:
   */
  decide_wrapper #(
    .END_ADDR(END_ADDR)
  ) i_decide_wrapper (
    .clk(mclk),
    .deduced_PC_eq_CRmax(deduced_PC_eq_CRmax || !w_nrst),
    //
    .sw_araddr      (sw_araddr      ),
    .sw_arvalid     (sw_arvalid     ),
    .trace_ctl      (trace_ctl      ),
    .trace_data     (trace_data     ),
    .decoded_address(decoded_address),
    //
    .i(i),
    .rddata(w_rddata),
    //
    .i_eq_end_addr(i_eq_end_addr),
    .verifying    (w_verifying  ),
    .done         (done         ),
    .reset        (reset        )
  );

  // BRAM_TDP_MACRO: True Dual Port RAM
  //                 Artix-7
  // Xilinx HDL Language Template, version 2019.2

  //////////////////////////////////////////////////////////////////////////
  // DATA_WIDTH_A/B | BRAM_SIZE | RAM Depth | ADDRA/B Width | WEA/B Width //
  // ===============|===========|===========|===============|=============//
  //     19-36      |  "36Kb"   |    1024   |    10-bit     |    4-bit    //
  //     10-18      |  "36Kb"   |    2048   |    11-bit     |    2-bit    //
  //     10-18      |  "18Kb"   |    1024   |    10-bit     |    2-bit    //
  //      5-9       |  "36Kb"   |    4096   |    12-bit     |    1-bit    //
  //      5-9       |  "18Kb"   |    2048   |    11-bit     |    1-bit    //
  //      3-4       |  "36Kb"   |    8192   |    13-bit     |    1-bit    //
  //      3-4       |  "18Kb"   |    4096   |    12-bit     |    1-bit    //
  //        2       |  "36Kb"   |   16384   |    14-bit     |    1-bit    //
  //        2       |  "18Kb"   |    8192   |    13-bit     |    1-bit    //
  //        1       |  "36Kb"   |   32768   |    15-bit     |    1-bit    //
  //        1       |  "18Kb"   |   16384   |    14-bit     |    1-bit    //
  //////////////////////////////////////////////////////////////////////////

  BRAM_TDP_MACRO #(
.INIT_00(256'h000180040001c00400018004000180040001800000018000000180000001c000),
.INIT_01(256'h0001800c0001c00c0001800800018008000180080001c0080001800400018004),
.INIT_02(256'h000180140001c0140001801000018010000180100001c0100001800c0001800c),
.INIT_03(256'h0001801c0001c01c0001801800018018000180180001c0180001801400018014),
.INIT_04(256'h0001801c0001801c0001801c0001801c0001801c0001801c0001801c0001801c),
.INIT_05(256'h0001801c0001801c0001801c0001801c0001801c0001801c0001801c0001801c),
.INIT_06(256'h00018024000180240001c0240001802000018020000180200001c0200001801c),
.INIT_07(256'h0080002c0000002c0001c02c0001802800018028000180280001c02800018024),
.INIT_08(256'h00fa003400f8003400214034000000300020003000000030000040300008002c),
.INIT_09(256'h0068003c0011003c0000403c0000003800000038000000380002403800f60034),
.INIT_0A(256'h0001803c0001803c0001803c0001803c0001803c0001803c000c003c0010003c),
.INIT_0B(256'h0001803c0001803c0001803c0001803c0001803c0001803c0001803c0001803c),
.INIT_0C(256'h0001802400018024000180240001c0240001802000018020000180200001c020),
.INIT_0D(256'h0001802c0001802c0001802c0001c02c0001802800018028000180280001c028),
.INIT_0E(256'h0001803400018034000180340001c0340001803000018030000180300001c030),
.INIT_0F(256'h0001803c0001803c0001803c0001c03c0001803800018038000180380001c038),
.INIT_10(256'h0001803c0001803c0001803c0001803c0001803c0001803c0001803c0001803c),
.INIT_11(256'h0001c0400001803c0001803c0001803c0001803c0001803c0001803c0001803c),
.INIT_12(256'h0001c0480001804400018044000180440001c044000180400001804000018040),
.INIT_13(256'h0001c0500001804c0001804c0001804c0001c04c000180480001804800018048),
.INIT_14(256'h0001c0580001805400018054000180540001c054000180500001805000018050),
.INIT_15(256'h0001805c0001805c0001805c0001805c0001c05c000180580001805800018058),
.INIT_16(256'h0001805c0001805c0001805c0001805c0001805c0001805c0001805c0001805c),
.INIT_17(256'h000180400001c0400001805c0001805c0001805c0001805c0001805c0001805c),
.INIT_18(256'h000180480001c0480001804400018044000180440001c0440001804000018040),
.INIT_19(256'h000180500001c0500001804c0001804c0001804c0001c04c0001804800018048),
.INIT_1A(256'h000180580001c0580001805400018054000180540001c0540001805000018050),
.INIT_1B(256'h0001805c0001805c0001805c0001805c0001805c0001c05c0001805800018058),
.INIT_1C(256'h0001805c0001805c0001805c0001805c0001805c0001805c0001805c0001805c),
.INIT_1D(256'h00018060000180600001c0600001805c0001805c0001805c0001805c0001805c),
.INIT_1E(256'h00018068000180680001c0680001806400018064000180640001c06400018060),
.INIT_1F(256'h000180700001807000314070001e006c0064006c0021006c0001c06c00018068),
.INIT_20(256'h00018078000180780001c0780001807400018074000180740001c07400018070),
.INIT_21(256'h0001807c0001807c0001807c0001807c0001807c0001807c0001c07c00018078),
.INIT_22(256'h0001807c0001807c0001807c0001807c0001807c0001807c0001807c0001807c),
.INIT_23(256'h0001806000018060000180600001c0600001807c0001807c0001807c0001807c),
.INIT_24(256'h0001806800018068000180680001c0680001806400018064000180640001c064),
.INIT_25(256'h0001807000018070000180700001c0700001806c0001806c0001806c0001c06c),
.INIT_26(256'h0001807800018078000180780001c0780001807400018074000180740001c074),
.INIT_27(256'h0000000000000000000000000001807c0001807c0001807c0001807c0001c07c),
    .BRAM_SIZE("36Kb"), // Target BRAM: "18Kb" or "36Kb"
    .DEVICE("7SERIES"), // Target device: "7SERIES"
    .INIT_FILE ("NONE"),
    .SIM_COLLISION_CHECK ("WARNING_ONLY"), // Collision check enable
    //
    .DOA_REG(0),            // Optional port A output register (0 or 1)
    .INIT_A(32'h00000000),  // Initial values on port A output port
    .READ_WIDTH_A (32),     // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
    .SRVAL_A(32'h00000000), // Set/Reset value for port A output
    .WRITE_MODE_A("WRITE_FIRST"), // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
    .WRITE_WIDTH_A(32),     // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
    //
    .DOB_REG(0),            // Optional port B output register (0 or 1)
    .INIT_B(32'h00000000),  // Initial values on port B output port
    .READ_WIDTH_B (32),     // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
    .SRVAL_B(32'h00000000), // Set/Reset value for port B output
    .WRITE_MODE_B("WRITE_FIRST"), // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
    .WRITE_WIDTH_B(32)      // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
  ) i_bram_dual_0 (
    /** connexions to the decide module **/
    .DOA(w_bram0_rddata),// Output port-A data, width defined by READ_WIDTH_A parameter
    .ADDRA(w_i),         // Input port-A address, width defined by Port A depth
    .CLKA(mclk),         // 1-bit input port-A clock
    .DIA(w_bram0_wrdata),// Input port-A data, width defined by WRITE_WIDTH_A parameter
    .ENA(1'b1),          // 1-bit input port-A enable
    .REGCEA(1'b0),       // 1-bit input port-A output register enable
    .RSTA(1'b0),         // 1-bit input port-A reset
    .WEA(w_bram0_we),    // Input port-A write enable, width defined by Port A depth
    /** connexions to the AXI BRAM controller **/
    .DOB(bram0_rddata), // Output port-B data, width defined by READ_WIDTH_B parameter
    .ADDRB(addr[11:2]), // Input port-B address, width defined by Port B depth
    .CLKB(clk),         // 1-bit input port-B clock
    .DIB(wrdata),       // Input port-B data, width defined by WRITE_WIDTH_B parameter
    .ENB(1'b1),         // 1-bit input port-B enable
    .REGCEB(1'b0),      // 1-bit input port-B output register enable
    .RSTB(rst),         // 1-bit input port-B reset
    .WEB(bram0_we)      // Input port-B write enable, width defined by Port B depth
  );

  BRAM_TDP_MACRO #(
.INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_09(256'h0020000000200000002000000000000000000000000000000000000000000000),
.INIT_0A(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_0B(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_0C(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_0D(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_0E(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_0F(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_10(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_11(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_12(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_13(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_14(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_15(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_16(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_17(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_18(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_19(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_1A(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_1B(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_1C(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_1D(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_1E(256'h0020002000200020002000200020002000200020002000200020002000200020),
.INIT_1F(256'h0020004000200040002000400020004000200020002000200020002000200020),
.INIT_20(256'h0020004000200040002000400020004000200040002000400020004000200040),
.INIT_21(256'h0020004000200040002000400020004000200040002000400020004000200040),
.INIT_22(256'h0020004000200040002000400020004000200040002000400020004000200040),
.INIT_23(256'h0020004000200040002000400020004000200040002000400020004000200040),
.INIT_24(256'h0020004000200040002000400020004000200040002000400020004000200040),
.INIT_25(256'h0020004000200040002000400020004000200040002000400020004000200040),
.INIT_26(256'h0020004000200040002000400020004000200040002000400020004000200040),
.INIT_27(256'h0000000000000000000000000020004000200040002000400020004000200040),
    .BRAM_SIZE("36Kb"), // Target BRAM: "18Kb" or "36Kb"
    .DEVICE("7SERIES"), // Target device: "7SERIES"
    .INIT_FILE ("NONE"),
    .SIM_COLLISION_CHECK ("WARNING_ONLY"), // Collision check enable
    //
    .DOA_REG(0),            // Optional port A output register (0 or 1)
    .INIT_A(32'h00000000),  // Initial values on port A output port
    .READ_WIDTH_A (32),     // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
    .SRVAL_A(32'h00000000), // Set/Reset value for port A output
    .WRITE_MODE_A("WRITE_FIRST"), // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
    .WRITE_WIDTH_A(32),     // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
    //
    .DOB_REG(0),            // Optional port B output register (0 or 1)
    .INIT_B(32'h00000000),  // Initial values on port B output port
    .READ_WIDTH_B (32),     // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
    .SRVAL_B(32'h00000000), // Set/Reset value for port B output
    .WRITE_MODE_B("WRITE_FIRST"), // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
    .WRITE_WIDTH_B(32)      // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
  ) i_bram_dual_1 (
    /** connexions to the decide module **/
    .DOA(w_bram1_rddata),// Output port-A data, width defined by READ_WIDTH_A parameter
    .ADDRA(w_i),         // Input port-A address, width defined by Port A depth
    .CLKA(mclk),         // 1-bit input port-A clock
    .DIA(w_bram1_wrdata),// Input port-A data, width defined by WRITE_WIDTH_A parameter
    .ENA(1'b1),          // 1-bit input port-A enable
    .REGCEA(1'b0),       // 1-bit input port-A output register enable
    .RSTA(1'b0),         // 1-bit input port-A reset
    .WEA(w_bram1_we),    // Input port-A write enable, width defined by Port A depth
    /** connexions to the AXI BRAM controller **/
    .DOB(bram1_rddata), // Output port-B data, width defined by READ_WIDTH_B parameter
    .ADDRB(addr[11:2]), // Input port-B address, width defined by Port B depth
    .CLKB(clk),         // 1-bit input port-B clock
    .DIB(wrdata),       // Input port-B data, width defined by WRITE_WIDTH_B parameter
    .ENB(1'b1),         // 1-bit input port-B enable
    .REGCEB(1'b0),      // 1-bit input port-B output register enable
    .RSTB(rst),         // 1-bit input port-B reset
    .WEB(bram1_we)      // Input port-B write enable, width defined by Port B depth
  );

  // End of BRAM_TDP_MACRO_inst instantiation

endmodule
/* verilator lint_on UNUSED */
