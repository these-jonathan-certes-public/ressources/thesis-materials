/**
 * \file  etb.c
 */

/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "etb.h"

/******************************************************************************/

/**
 * \defgroup etb  Embedded Trace Buffer (etb)
 *
 * \brief
 *  Library to manipulate CoreSight Embedded Trace Buffer (etb) under Xilinx
 *  Zynq-7000.
 *
 * \{
 */

struct etb * etb = (struct etb *)( ETB_BASE );

/******************************************************************************/

void etb_init(
  void
) {
  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 1;
  etb->lar             = 0x00000000;
}

/******************************************************************************/

void etb_stop(
  void
) {
  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 0;
  etb->lar             = 0x00000000;
}

/******************************************************************************/

/**
 * \}
 */

