
/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "gpio.h"

/******************************************************************************/

/**
 * \brief
 *  Updates two <tt> uint8_t </tt>, which addresses are given as arguments, to
 *  be both the bank index and the bit index for a given GPIO.
 *
 * \details
 *  The appropriate bank is deduced from the GPIO index:
 * - Bank0, 32-bit bank controlling MIO pins[31:0]      : indexes from 0 to 31
 * - Bank1, 22-bit bank controlling MIO pins[53:32]     : indexes from 32 to 53
 * - Bank2, 32-bit bank controlling EMIO signals[31:0]  : indexes from 54 to 85
 * - Bank3, 32-bit bank controlling EMIO signals[63:32] : indexes from 86 to 117
 */
void gpio_getBankAndBitIndex(
  uint8_t theGpioIndex,   //!< integer that refers to a GPIO pin index
  uint8_t * theBank,      //!< address of the bank index to update
  uint8_t * theBitIndex   //!< address of the bit index to update
) {
  if ( theGpioIndex < 32 ) {
    *theBank     = 0;
    *theBitIndex = theGpioIndex;
  } else if ( theGpioIndex < 54 ) {
    *theBank     = 1;
    *theBitIndex = theGpioIndex - 32;
  } else if ( theGpioIndex < 86 ) {
    *theBank     = 2;
    *theBitIndex = theGpioIndex - 54;
  } else {
    *theBank     = 3;
    *theBitIndex = theGpioIndex - 86;
  }
}

/******************************************************************************/

/**
 * \brief
 *  Sets the direction of a GPIO from the \ref XGPIOPS_DIRM_OFFSET register of
 *  the appropriate bank.
 */
void gpio_setDirection(
  uint8_t theGpioIndex, //!< integer that refers to the GPIO index
  uint8_t theDirection  //!< output if non-zero
) {
  uint8_t theBank;        // bank index for the GPIO
  uint8_t theBitIndex;    // bit index in the bank for the GPIO
  uint32_t * theAddress;  // register address to read the GPIO value from

  gpio_getBankAndBitIndex(theGpioIndex, &theBank, &theBitIndex);

  theAddress = (uint32_t *)(XGPIOPS_BASE_ADDR + XGPIOPS_DIRM_OFFSET)
                + 0x10*theBank;

  if ( theDirection != 0 ) {
    *theAddress = (*theAddress & ~(1 << theBitIndex)) | (1 << theBitIndex);
  } else {
    *theAddress = (*theAddress & ~(1 << theBitIndex)) | (0 << theBitIndex);
  }

  return;
}

/******************************************************************************/

/**
 * \brief
 *  Sets the output enable bit of a GPIO from the \ref XGPIOPS_OUTEN_OFFSET
 *  register of the appropriate bank.
 */
void gpio_setOuputEnable(
  uint8_t theGpioIndex, //!< integer that refers to the GPIO index
  uint8_t theEnable     //!< enabled if non-zero
) {
  uint8_t theBank;        // bank index for the GPIO
  uint8_t theBitIndex;    // bit index in the bank for the GPIO
  uint32_t * theAddress;  // register address to read the GPIO value from

  gpio_getBankAndBitIndex(theGpioIndex, &theBank, &theBitIndex);

  theAddress = (uint32_t *)(XGPIOPS_BASE_ADDR + XGPIOPS_OUTEN_OFFSET)
                + 0x10*theBank;

  if ( theEnable != 0 ) {
    *theAddress = (*theAddress & ~(1 << theBitIndex)) | (1 << theBitIndex);
  } else {
    *theAddress = (*theAddress & ~(1 << theBitIndex)) | (0 << theBitIndex);
  }

  return;
}

/******************************************************************************/

/**
 * \brief
 *  Reads the value of a GPIO from the \ref XGPIOPS_DATA_OFFSET register of the
 *  appropriate bank.
 *
 * \returns
 *  The value of the GPIO.
 */
uint32_t gpio_read(
  uint8_t theGpioIndex  //!< integer that refers to the GPIO index
) {
  uint8_t theBank;        // bank index for the GPIO
  uint8_t theBitIndex;    // bit index in the bank for the GPIO
  uint32_t * theAddress;  // register address to read the GPIO value from

  gpio_getBankAndBitIndex(theGpioIndex, &theBank, &theBitIndex);

  theAddress = (uint32_t *)(XGPIOPS_BASE_ADDR + XGPIOPS_DATA_OFFSET) + theBank;

  return ((*theAddress & (1 << theBitIndex)) >> theBitIndex);
}

/******************************************************************************/

/**
 * \brief
 *  Writes the value of a GPIO in the \ref XGPIOPS_DATA_OFFSET register of the
 *  appropriate bank.
 */
void gpio_write(
  uint8_t theGpioIndex, //!< integer that refers to the GPIO index
  uint8_t theValue      //!< sets the GPIO if non-zero
) {
  uint8_t theBank;        // bank index for the GPIO
  uint8_t theBitIndex;    // bit index in the bank for the GPIO
  uint32_t * theAddress;  // register address to read the GPIO value from

  gpio_getBankAndBitIndex(theGpioIndex, &theBank, &theBitIndex);

  theAddress = (uint32_t *)(XGPIOPS_BASE_ADDR + XGPIOPS_DATA_OFFSET) + theBank;

  if ( theValue != 0 ) {
    *theAddress = (*theAddress & ~(1 << theBitIndex)) | (1 << theBitIndex);
  } else {
    *theAddress = (*theAddress & ~(1 << theBitIndex)) | (0 << theBitIndex);
  }

  return;
}

/******************************************************************************/

