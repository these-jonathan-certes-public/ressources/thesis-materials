
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h>
#include <stdio.h>

#include "xil_mmu.h"
#include "xil_cache.h"

#include "gpio.h"
#include "coresight/ptm.h"
#include "coresight/funnel.h"
#include "coresight/etb.h"

/**
 * Location where to copy the code and reconfigure the MMU.
 */
#define ATTACK_ADDRESS 0x00200000

/**
 * size of one page when we reconfigure the MMU.
 */
#define MMU_PAGE_SIZE (1 << 20)

/**
 * \brief
 *  Integer that refers to the output pin index: <tt> EMIO[0] </tt>. See details
 *  of \ref gpio_read().
 */
#define GPIO_LOGIC_ANALYSER_RESET 54

/**
 * \brief
 *  Integer that refers to the output pin index: <tt> EMIO[1] </tt>. See details
 *  of \ref gpio_read().
 */
#define GPIO_CALIBRATION 55

/** symbols that are defined in the linker script. **/
extern int __sw_att_low;
extern int __sw_att_top;
//
extern int __logica_low;
extern int __logica_top;

/******************************************************************************/

void configureCoresight(
  uint32_t *theStartAddress,
  uint32_t *theStopAddress
) {
  /********************/
  /** configure PTM: **/
  /********************/
  ptm_cpu0->lar = 0xC5ACCE55;
  ptm_cpu0->etmcr.ProgBit = 1;
  while ( ptm_cpu0->etmsr.ProgBit != 1 ) {
    // wait
  }
  // Main Control Register:
  ptm_cpu0->etmcr.BranchOutput   = 0;     // direct branch addresses not output
  ptm_cpu0->etmcr.DebugReqCtrl   = 0;     // not in debug mode
  ptm_cpu0->etmcr.PowerDown      = 0;     // enable PTM
  ptm_cpu0->etmcr.CycleAccurate  = 1;
  ptm_cpu0->etmcr.ContexIDSize   = 3;     // context id size = 32 bits
  // TraceEnable Event Register:
  ptm_cpu0->etmteevr.Function    = 0x0;   // logical operation = A
  ptm_cpu0->etmteevr.ResourceA   = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  ptm_cpu0->etmteevr.ResourceB   = 0x00;  // don't care
  // Trigger Event Register:
  ptm_cpu0->etmtrigger.Function  = 0x0;   // logical operation = A
  ptm_cpu0->etmtrigger.ResourceA = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  ptm_cpu0->etmtrigger.ResourceB = 0x00;  // don't care
  // TraceEnable Control Register 1:
  ptm_cpu0->etmtecr1.TraceSSEn   = 0;     // unaffected by TraceEnable start/stop block
  ptm_cpu0->etmtecr1.ExcIncFlag  = 1;     // exclude
  ptm_cpu0->etmtecr1.AddrCompSel = 0x0;   // no address range
  // CoreSight Trace ID Register:
  ptm_cpu0->etmtraceidr.TraceID  = 0x42;  // non-zero, not in range 0x70-0x7F
  // Address Comparator registers:
  ptm_cpu0->etmacvr1.Address   = (uint32_t)( theStartAddress );
  ptm_cpu0->etmacvr2.Address   = (uint32_t)( theStopAddress  );
  //
  ptm_cpu0->etmcr.ProgBit = 0;
  while ( ptm_cpu0->etmsr.ProgBit != 0 ) {
    // wait
  }
  ptm_cpu0->lar = 0x00000000;

  /***********************/
  /** configure funnel: **/
  /***********************/
  funnel->lar                  = 0xC5ACCE55;
  funnel->control.EnableSlave0 = 1;
  funnel->control.MinHoldTime  = 0x3;
  funnel->lar                  = 0x00000000;

  /********************/
  /** configure ETB: **/
  /********************/
  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 0;
  while ( etb->ffsr.FtStopped == 0 ) {
    // wait
  }
  // fill the buffer with 0x00:
  etb->rwp.value       = 0x000;
  for ( uint16_t i = 0x000 ; i < 0x400 ; i++ ) {
    etb->rwd.value = 0x00000000;
    // write pointer is automatically incremented
    __asm__ __volatile__ ("isb" : : : "memory");
    __asm__ __volatile__ ("dsb" : : : "memory");
  }
  etb->ctl.TraceCaptEn = 1;
  etb->lar             = 0x00000000;

  return;
}

/******************************************************************************/

void* mmuSetAddress(
  intptr_t baseVirtAddr,
  intptr_t basePhysAddr,
  size_t size
) {
  uint32_t data;
  uint32_t addrSize;
  uint32_t *ttAddr;
  uint32_t tableIndex;
  uint32_t desc;
  uint32_t i;
  const uint32_t pageMask = MMU_PAGE_SIZE - 1;

  //__asm__ __volatile__("mrs %0, cpsr" : "=r"(data));              // CPSR
  __asm__ __volatile__("mrc p15, 0, %0, c2, c0, 2" : "=r"(data)); // TTBCR
  addrSize = data & 0x7;

  // Lecture du registre TTBR0
  __asm__ __volatile__("mrc p15, 0, %0, c2, c0, 0" : "=r"(data));
  ttAddr = (uint32_t *)(data & ~((1 << (14 - addrSize)) - 1));

  if ( !ttAddr ) {
    return (void *)basePhysAddr;
  }

  for ( i = 0; i < size; i += MMU_PAGE_SIZE ) {
    uint32_t sectionBaseAddr;
    tableIndex = ((baseVirtAddr + i) >> 20) & ((1 << (31-addrSize-20+1))-1);
    desc = ttAddr[tableIndex];

    sectionBaseAddr = (basePhysAddr + i) & ~pageMask;
    desc = (desc & pageMask) | sectionBaseAddr;
    ttAddr[tableIndex] = desc;

    __asm__ __volatile__("isb");
    __asm__ __volatile__("dsb");
  }

  return (void *)(baseVirtAddr & ~pageMask);
}

/******************************************************************************/

/**
 * \brief
 *  Configures the GPIO that controls the reset of the logic analyser and the
 *  calibration.
 */
void gpio_configure(
  void
) {
  gpio_setDirection(  GPIO_LOGIC_ANALYSER_RESET, GPIO_DIRECTION_OUTPUT);
  gpio_setOuputEnable(GPIO_LOGIC_ANALYSER_RESET, 1);
  //
  gpio_setDirection(  GPIO_CALIBRATION,          GPIO_DIRECTION_OUTPUT);
  gpio_setOuputEnable(GPIO_CALIBRATION,          1);
}

/******************************************************************************/

/**
 * \brief
 *  Sets the GPIO that controls the reset of the logic analyser, waits a bit and
 *  unsets the GPIO.
 */
void logicAnalyser_reset(
  void
) {
  uint32_t *theAddress;

  gpio_write(GPIO_LOGIC_ANALYSER_RESET, 1);

  /**
   * Fills the content of the BRAM with zeros.
   * Also we keep the reset high to make sure the clock of the FPGA issued at
   * least one rising edge.
   */
  theAddress = (uint32_t *)(&__logica_low);
  while ( theAddress < (uint32_t *)(&__logica_top) ) {
    *theAddress = 0x00000000;
    theAddress++;
  }

  gpio_write(GPIO_LOGIC_ANALYSER_RESET, 0);

  return;
}

/******************************************************************************/

/**
 * \brief
 *  Prints the content of the BRAM to stdout with the same format as gdb.
 */
void logicAnalyser_printf(
  void
) {
  uint32_t *theAddress;

  theAddress = (uint32_t *)(&__logica_low);
  while ( theAddress < (uint32_t *)(&__logica_top) ) {
    printf("%p:\t", theAddress);
    printf("0x%08lx\t", *theAddress++);
    printf("0x%08lx\t", *theAddress++);
    printf("0x%08lx\t", *theAddress++);
    printf("0x%08lx\n", *theAddress++);
  }

  return;
}

/******************************************************************************/

int main(
  void
) {
  unsigned int theSelect;
  uint32_t      *theBramAddress     = (uint32_t *)(&__sw_att_low);
  uint32_t      *theAttackAddress   = (uint32_t *)(ATTACK_ADDRESS);
  const uint32_t theCoreSightOffset = (uint32_t)(&__sw_att_top - &__sw_att_low);


  Xil_DisableMMU();
  gpio_configure();

  logicAnalyser_reset();


  // selection of the code to run, can be modified from the UART or by gdb:
  theSelect = 0x5d;
  __asm__ __volatile__("__breakpoint_select:");

  if ( theSelect == 0x5d ) {
    /* running from the SD card */
    printf("** Execution of some code. Please enter the number:\n");
    theSelect = (unsigned int)(getchar() - '0');
    printf("You have selected: %u\n", theSelect);
  }

  if ( theSelect != 0 ) {
    // calibration:
    theSelect = 1;
  }
  gpio_write(GPIO_CALIBRATION, theSelect);

  // MMU, switch the location of the BRAM with the location of the attack:
  Xil_DisableMMU();
  mmuSetAddress( (uint32_t)(theBramAddress),   (uint32_t)(theAttackAddress),
                 MMU_PAGE_SIZE );
  mmuSetAddress( (uint32_t)(theAttackAddress), (uint32_t)(theBramAddress),
                 MMU_PAGE_SIZE );
  Xil_EnableMMU();
  //
  theBramAddress   = (uint32_t *)(ATTACK_ADDRESS);
  theAttackAddress = (uint32_t *)(&__sw_att_low);

  /* execution of the code in the BRAM */
  configureCoresight(theBramAddress, theBramAddress + theCoreSightOffset);
  __asm__ __volatile__("nop");
  __asm__ __volatile__("mov r1, %0" :: "r"(theBramAddress));
  __asm__ __volatile__("mov lr, pc");
  __asm__ __volatile__("mov pc, r1");

  // end:
  gpio_write(GPIO_CALIBRATION, 0);
  logicAnalyser_printf();

  while (1) {
    // infinite loop
    __asm__ __volatile__("__my_breakpoint:");
  }

  return 0;
}

