
#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

#
ifndef LD
	echo "You must specify a linker script in variable LD."
	exit 1
endif
#
ifndef SRC
	echo "You must specify the source to compile in variable SRC."
	exit 1
endif
#
ifndef BIN
	echo "You must specify the binary name in variable BIN."
	exit 1
endif

#===============================================================================

INCLUDE = $(addprefix -I , $(sort $(dir ${SRC})))
CCOUT   = $(addsuffix .o, $(basename ${SRC}))

ARMGNU = arm-none-eabi-
ARCH   = -mcpu=cortex-a9
ASOPS  = -g
COPS   = -g -no-pie -Wall -O0 -nostdlib
LOPS   = -L "/usr/lib/arm-none-eabi/newlib/" -lc \
         -L "/usr/lib/gcc/arm-none-eabi/8.3.1/" -lgcc -lnosys

#===============================================================================

all : ${BIN}.elf sw_att.elf


# (3) creates an ELF without key and sw_att:
${BIN}.elf : ${BIN}.full.elf
	$(ARMGNU)objcopy \
	  --set-section-flags .key=noload     \
	  --set-section-flags .sw_att=noload  $^ $@

# (2) extract sw_att from the full ELF:
sw_att.elf : ${BIN}.full.elf
	$(ARMGNU)objcopy --only-section=.sw_att* $^ $@

# (1) link all *.o into a full ELF (including key and sw_att):
${BIN}.full.elf : ${LD} ${CCOUT}
	$(ARMGNU)ld -T ${LD} ${CCOUT} -o $@ ${LOPS}


qemu : ${BIN}.qemu.elf
	# connect with:
	# gdb-multiarch $^ -ex "set architecture armv7" -ex "target remote localhost:1234"
	qemu-system-arm -machine xilinx-zynq-a9 -cpu cortex-a9 -m 1132M -nographic \
	  -serial null -serial mon:stdio -gdb tcp::1234 \
	  -kernel $^

${BIN}.qemu.elf : ${LD} $(subst main.o,main.qemu.o,${CCOUT})
	$(ARMGNU)ld -T ${LD} $(subst main.o,main.qemu.o,${CCOUT}) -o $@ ${LOPS}

%ain.qemu.o : %ain.c
	# DEFINE _QEMU to compile $^
	$(ARMGNU)gcc ${ARCH} ${INCLUDE} -c ${COPS} -D _QEMU $^ -o $@


%.o : %.c
	$(ARMGNU)gcc ${ARCH} ${INCLUDE} -c ${COPS} $^ -o $@

%.o : %.s
	$(ARMGNU)as  ${ARCH} ${ASOPS} $^ -o $@


clean :
	$(RM) ${CCOUT}
	$(RM) ${BIN}.full.elf ${BIN}.elf sw_att.elf
	$(RM) ${BIN}.qemu.elf main.qemu.o

