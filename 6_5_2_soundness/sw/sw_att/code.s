
/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

.text

  nop
  nop
  nop
  nop
  nop
  nop
  /**
   * indirect branch to force CoreSight to output a trace:
   */
  add r3, pc, #0  // pc = pc + 4; r3 = pc + 4 + 0;
  mov pc, r3
  // <- destination for the indirect branch


  /**
   * witness: increment a value at address 0x01000000:
   */
  movw r3, #0x0000
  movt r3, #0x0100
  ldr  r2, [r3]
  add  r2, r2, #1
  str r2, [r3]
  //
  nop
  add r3, pc, #0  // pc = pc + 4; r3 = pc + 4 + 0;
  mov pc, r3


  nop
  nop
  nop
  nop
  nop
  nop
  add r3, pc, #0  // pc = pc + 4; r3 = pc + 4 + 0;
  mov pc, r3


  bx  lr          // return
  nop
  nop
  nop
  nop
  nop
  nop
  nop

