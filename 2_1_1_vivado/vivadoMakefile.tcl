#!/bin/sh
# the next line restarts using vivado \
exec vivado -mode batch -source "$0" -tclargs ${1+"$@"}

#
# Copyright (C) 2021 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

set theHdlList        [list]
set theSchematicList  [list]
set theConstraintList [list]
set theTopLevelName   ""
set theFpga           "xc7z007sclg400-1"
set theBoardConf      ""

set dryRun   0
set doExport 0
set theExportedOutput  "output.xsa"

#===============================================================================
# parse argv:
#===============================================================================

if { [info exists argv] } {

  set theOption ""
  foreach theArg $argv {
    if { [string first "--" $theArg] == 0 } {

      set theOption $theArg
      switch $theOption {
        "--hdl"       {}
        "--schem"     {}
        "--constr"    {}
        "--top"       {}
        "--output"    {}
        "--fpga"      {}
        "--board"     {}
        "--dry-run"   { set dryRun   1 }
        "--do-export" { set doExport 1 }
        default       {
          puts "unknown option \"$theOption\", ignoring"
        }
      }

    } else {

      switch $theOption {
        "--hdl"       { lappend theHdlList        $theArg }
        "--schem"     { lappend theSchematicList  $theArg }
        "--constr"    { lappend theConstraintList $theArg }
        "--fpga"      { set theFpga               $theArg }
        "--board"     { set theBoardConf          $theArg }
        "--top"       { set theTopLevelName       $theArg }
        "--output"    { set theExportedOutput     $theArg }
        default       {
          puts "unknown option \"$theOption\", ignoring arg: $theArg"
        }
      }

    }
  }

}

set hasError 0
foreach theHdl $theHdlList {
  if { ![file exists $theHdl] || ![file isfile $theHdl] } {
    puts "** Error ** HDL file does not exist: $theHdl"
    set hasError 1
  }
}
#
foreach theSchematic $theSchematicList {
  if { ![file exists $theSchematic] || ![file isfile $theSchematic] } {
    puts "** Error ** schematic file does not exist: $theSchematic"
    set hasError 1
  }
}
#
foreach theConstraint $theConstraintList {
  if { ![file exists $theConstraint] || ![file isfile $theConstraint] } {
    puts "** Error ** constraint file does not exist: $theConstraint"
    set hasError 1
  }
}

if { $hasError } {
  exit 0
}


#===============================================================================
# use vivado API to create/update the project and export:
#===============================================================================

set theWorkspace "vivado.workspace"
set theProject   [file tail [pwd]]

set theHdlList        [lsort ${theHdlList}]
set theSchematicList  [lsort ${theSchematicList}]
set theConstraintList [lsort ${theConstraintList}]
set needToRun 0


##
# creates or opens the project:
#
set theProjectFile [file join ${theWorkspace} "${theProject}.xpr"]
if { [file exists ${theProjectFile}] } {
  open_project ${theProjectFile}
} else {
  create_project ${theProject} vivado.workspace -part ${theFpga} -force
}


##
# set board configuration:
#
if { [string trim ${theBoardConf}] != "" } {
  set_property board_part ${theBoardConf} [current_project]
}


##
# adds the HDL files to the project if not already in it:
#
set alreadyInList [get_files -filter { FILE_TYPE == Verilog ||
                                       FILE_TYPE == VHDL       }]
#
foreach theHdl $theHdlList {
  set theHdl [file normalize ${theHdl}]
  if { [lsearch ${alreadyInList} ${theHdl}] == -1 } {
    add_files ${theHdl}
    set needToRun 1
  }
  #
  if { ![file exists ${theTopLevelName}.bit] ||
        [file mtime ${theHdl}] > [file mtime ${theTopLevelName}.bit] } {
    set needToRun 1
  }
}


##
# creates the schematics in the project if not already in it:
#
set alreadyInList [get_files -filter { FILE_TYPE == "Block Designs" }]
set alreadyInList [lsort ${alreadyInList}]
#
if { [llength ${alreadyInList}] != [llength ${theSchematicList}] } {
  set needToRun 1
} else {
  for { set i 0 } { ${i} < [llength ${alreadyInList}] } { incr i } {
    set theDestination [lindex ${alreadyInList}     ${i}]
    set theSchematic   [lindex ${theSchematicList} ${i}]
    if { [file mtime ${theSchematic}] > [file mtime ${theDestination}] } {
      set needToRun 1
      break
    }
  }
}
#
if { ${needToRun} } {
  foreach theBlockDesign ${alreadyInList} {
    remove_files ${theBlockDesign}
    file delete -force ${theBlockDesign}
  }

  for { set j 0 } { ${j} < [llength ${theSchematicList}] } { incr j } {
    set theSchematic [lindex ${theSchematicList} ${j}]
    create_bd_design "design_${j}"
    set theError [catch {
      source ${theSchematic}
    } theReturn]
    #
    if { $theError } {
      puts "** Error ** Source of the following script: ${theSchematic}"
      puts ${theReturn}
    }
    save_bd_design  "design_${j}"
    # export as pdf:
    write_bd_layout -format "pdf"            \
                    -orientation "landscape" \
                    -force "design_${j}.pdf"
    #
    close_bd_design "design_${j}"
  }

  set theFiles [get_files -filter { FILE_TYPE == "Block Designs" }]
  make_wrapper -files ${theFiles} -top -import
}


##
# adds the contstraints files to the project if not already in it:
#
set alreadyInList [get_files -filter {FILE_TYPE == XDC}]
#
foreach theConstraint ${theConstraintList} {
  set theConstraint [file normalize ${theConstraint}]
  if { [lsearch ${alreadyInList} ${theConstraint}] == -1 } {
    add_files ${theConstraint}
    set needToRun 1
  }
  #
  if { ![file exists ${theTopLevelName}.bit] ||
        [file mtime ${theConstraint}] > [file mtime ${theTopLevelName}.bit] } {
    set needToRun 1
  }
}


##
# selects top level:
#
set_property "source_mgmt_mode" "None" [current_project] ;# manual compile order
if { [get_property top [current_fileset]] != ${theTopLevelName} } {
  set_property "top" ${theTopLevelName} [current_fileset]
  set needToRun 1
}


##
# synthesis:
#
if { ${needToRun} } {
  if { ${dryRun} != 0 } {
    puts "dry-run: generation of the bitstream!"
  } else {
    reset_runs *
    launch_runs "impl_1" -to_step "write_bitstream" -jobs 1
    wait_on_run "impl_1"
    if { ![file exists ${theTopLevelName}.bit] } {
      file link -symbolic ${theTopLevelName}.bit \
[file join ${theWorkspace} ${theProject}.runs "impl_1" ${theTopLevelName}.bit]
    }
  }
} else {
  puts "No modification since last time."
}


##
# export:
#
if { ${doExport} } {
  if { ${dryRun} != 0 } {
    puts "dry-run: export of xsa"
  } else {
    write_hw_platform -fixed -force -file ${theExportedOutput}
  }
}


close_project

