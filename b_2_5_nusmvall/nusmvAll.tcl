#!/bin/sh
# the next line restarts using tclsh \
exec tclsh "$0" ${1+"$@"}

#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

#===============================================================================

##
# \brief
#   Stores the program enviromnement variables.
#
namespace eval nusmvall {

  ##
  # \brief
  #   List of elements that must be taken 3 by 3:
  #   - the command to execute
  #   - the arguments that must be given to the command to test its availability
  #   - the expected string that must be printed by the command when testing
  #
  # \details
  #   This variable is set by function \ref nusmvall::init.
  #
  set theAvailTests [list]

  ##
  # \brief
  #   Path to the VPI source file, used by icarus to retrive the inputs/outputs
  #   of the verilog input file.
  #
  # \details
  #   This variable is set by function \ref nusmvall::init.
  #
  set theVpiSrc ""

  ##
  # \brief
  #   Path to both verilog input file and LTL specifications input file.
  #
  # \details
  #   These variables are set by function \ref nusmvall::parseArgv.
  #
  set theVerilog        ""
  set theSpecifications ""

  ##
  # \brief
  #   Boolean to define if counterexamples must be converted to VCD.
  #
  # \details
  #   This variable is set by function \ref nusmvall::parseArgv.
  #
  set convertToVcd 0

  ##
  # \brief
  #   Boolean to define if the executed commands are printed to stdout.
  #
  # \details
  #   This variable is set by function \ref nusmvall::parseArgv.
  #
  set verbose 0

  set theOutputFile "stdout"

}

#===============================================================================

##
# \brief
#   Prints help to the standard output and then exits.
#
proc nusmvall::printHelpAndExit {
  {theCode 0}
} {
  puts "Model-checking of complex verilog modules

SYNOPSIS:
  ./nusmvAll.tcl \[OPTIONS\] --verilog FILE1 --spec FILE2

DESCRIPTION:
  Uses external tools such as icarus verilog, verilog2SMV and NuSMV to formally
  verify LTL specifications.
  From a complex verilog module and multiple LTL specifications, filters the
  module outputs to avoid combinatorial blow up of the state-space (state
  explosion problem) and performs model-checking on these specifications.

OPTIONS:
  -t OR --to      : the argument following this option gives a path to the file
                    to print NuSMV results to. If this argument is ommited, the
                    results are printed to the standard output.

  -i OR --verilog : the argument following this option gives a path to the
                    verilog input file to formally verify.

  -s OR --spec    : the argument following this option gives a path to the file
                    which contains the LTL specifications.

  -c OR --vcd     : activates conversion to VCD for counterexamples. Each time
                    NuSMV results contain a counterexample, a *.vcd file is
                    generated.

  -v OR --verbose : activates verbose mode ; every call to an external program
                    is now printed to the standard output

  -h OR --help    : prints this help and exits

EXAMPLE:
  ./nusmvAll.tcl --verbose              \\
                 --to      hardware.log \\
                 --verilog hardware.v   \\
                 --spec    hardware.spec.smv
"
  exit $theCode
}

#===============================================================================

##
# \brief
#   Initializes namespace \ref nusmvall and makes sure that all the
#   dependencies are available.
#
# \details
#   Tests an execution of each commands by executing them.
#
proc nusmvall::init {
} {
  # adding external commands to the dependencies:
  lappend nusmvall::theAvailTests verilog2smv  {} "Usage:"
  lappend nusmvall::theAvailTests NuSMV {} "This version of NuSMV is linked to"
  lappend nusmvall::theAvailTests iverilog     -V "Icarus Verilog version"
  lappend nusmvall::theAvailTests vvp          -V "Icarus Verilog runtime"
  lappend nusmvall::theAvailTests iverilog-vpi {} "Usage:"

  # adding other scripts in the same directory to the dependencies:
  set here [file dirname [file normalize [info script]]]
  #
  lappend nusmvall::theAvailTests \
    "[info nameofexecutable] [file join $here "createVerilogWrapper.tcl"]" \
    "--help" "Creation of a verilog wrapper"
  #
  lappend nusmvall::theAvailTests \
    "[info nameofexecutable] [file join $here "splitLtl.tcl"]" \
    "--help" "Split LTLSPEC into seprated files"
  #
  lappend nusmvall::theAvailTests \
    "[info nameofexecutable] [file join $here "smvTrace2vcd.tcl"]" \
    "--help" "Converts SMV counterexample traces to VCD"
  #
  lappend nusmvall::theAvailTests \
    "python3 [file join $here "equivalence.py"]" \
    "--help" "Verifies the equivalence between transition functions"

  set nusmvall::theVpiSrc [file join $here "vpi_listIO.c"]

  # verify that the dependencies are available:
  set dependenciesOk 1
  #
  foreach { theCommand theArgs theExpectedText } ${nusmvall::theAvailTests} {
    catch { exec -- {*}${theCommand} {*}${theArgs} } theReturn
    if { [string first ${theExpectedText} $theReturn] < 0 } {
      puts "** Error ** command \"${theCommand}\" is not available:"
      puts $theReturn
      set dependenciesOk 0
    }
  }

  if { ![file exists ${nusmvall::theVpiSrc}] } {
    puts "** Error ** dependency file is not available: ${nusmvall::theVpiSrc}"
    set dependenciesOk 0
  }

  if { !$dependenciesOk } {
    exit 1
  }
  return 0
}

#===============================================================================

##
# \brief
#   Parses the vector of arguments and updates the variables in namespace \ref
#   nusmvall.
#
# \details
#   More informations about the syntax are given in the text printed by \ref
#   nusmvall::printHelpAndExit.
#
# \param  argv  Vector of arguments.
#
proc nusmvall::parseArgv {
  argv
} {
  if { [llength $argv] < 1 } {
    puts "** Error ** you must specify at least one argument:"
    nusmvall::printHelpAndExit 1
  }

  set previousArg ""
  foreach theArg [string trim $argv] {
    if { [string first "-" $theArg] == 0 } {
      switch $theArg {
        "-t" - "--to"      { set previousArg "to"         }
        "-i" - "--verilog" { set previousArg "verilog"    }
        "-s" - "--spec"    { set previousArg "spec"       }
        "-c" - "--vcd"     { set nusmvall::convertToVcd 1 }
        "-v" - "--verbose" { set nusmvall::verbose 1      }
        "-h" - "--help"    { nusmvall::printHelpAndExit   }
        default  {
          puts "** Error ** unable to parse argument: $theArg"
          nusmvall::printHelpAndExit 2
        }
      }
    } else {
      switch $previousArg {
        "to"      { set nusmvall::theOutputFile $theArg
                    set previousArg ""
                  }
        "verilog" { set nusmvall::theVerilog $theArg
                    set previousArg ""
                  }
        "spec"    { set nusmvall::theSpecifications $theArg
                    set previousArg ""
                  }
        default  {
          puts "** Error ** unable to parse argument: $previousArg $theArg"
          nusmvall::printHelpAndExit 3
        }
      }
    }
  }

  if { ![file exists $nusmvall::theVerilog]        ||
       ![file exists $nusmvall::theSpecifications] } {
    puts "** Error ** you must specify existing files for both verilog and SMV\
      specifications."
  }

  return 0
}

#===============================================================================

##
# \brief
#   Calls exec command. If verbose is activated, prints the exec command to
#   stdout first.
#
proc nusmvall::execCall {
  args
} {
  if { $nusmvall::verbose != 0 } {
    puts "# $args"
  }
  eval exec $args
}

#===============================================================================

##
# \brief
#   Uses icarus verilog and a VPI module to extract the inputs, outputs and
#   module names.
#
# \details
#   This function calls external programs and remove files that are created by
#   these calls.
#
proc nusmvall::getWrapperArgs {
} {
  set theArgs ""

  set theVpiBin [file rootname [file tail ${nusmvall::theVpiSrc}]].vpi
  set theVpiO   [file rootname [file tail ${nusmvall::theVpiSrc}]].o
  #
  set theCode [catch {
    nusmvall::execCall iverilog-vpi ${nusmvall::theVpiSrc}
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
    return 1
  }
  #
  set theCode [catch {
    nusmvall::execCall iverilog -o "binary" \
                                -I [file dirname ${nusmvall::theVerilog}] \
                                  ${nusmvall::theVerilog}
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
    return 2
  }
  #
  set theCode [catch {
    nusmvall::execCall vvp -M. -m[file rootname $theVpiBin] "binary"
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
    return 3
  } else {
    set theArgs $theReturn
  }

  # clean:
  set theCode [catch {
    file delete -force $theVpiBin $theVpiO "binary"
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
    return 4
  }

  return [string trim $theArgs]
}

#===============================================================================

##
# \brief
#   Executes TCL interpreter whilst sourcing \c splitLtl.tcl in the same
#   dirercory of this script.
#
# \details
#   Also parses its standard output to retreive the list of created LTL files.
#
# \returns a list of paths to created LTL files
#
proc nusmvall::splitLtl {
} {
  set ltlFilesList [list]

  set here [file dirname [file normalize [info script]]]
  set theCode [catch {
    nusmvall::execCall [info nameofexecutable] \
      [file join $here "splitLtl.tcl"] --expand ${nusmvall::theSpecifications}
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
    return 1
  }

  foreach theLine [split $theReturn "\n"] {
    if { [string first "Creation of file:" $theLine] != -1 } {
      set theLtlFile [string trim [lindex [split $theLine ":"] 1]]
      lappend ltlFilesList [file normalize $theLtlFile]
    }
  }
  return $ltlFilesList
}

#===============================================================================

##
# \brief
#   Uses NuSMV to retrieve the list of LTL variables.
#
# \details
#   Calls NuSMV on the created LTL files. This file must only contain a <tt>
#   MODULE main </tt> definition and one \c LTLSPEC. NuSMV then returns an
#   undefined identifier error for each LTL variables: error output is parsed so
#   that all variables are retrieved.
#
# \param theLtlFile
#   Path to the input LTL file.
#
proc nusmvall::getLtlVarNames {
  theLtlFile
} {
  set theLtlVarNames [list]

  set theCode [catch {
    nusmvall::execCall NuSMV $theLtlFile 2>@1
  } theReturn]
  # expecting an error:
  if { $theCode != 1 } {
    puts "** Error ** NuSMV must return an undefined identifier error so that\
      LTL variables names can be retrieved."
  }

  foreach theLine [split $theReturn "\n"] {
    if { [string first "undefined identifier" $theLine] != -1 } {
      set theLtlVar [string trim [lindex [split $theLine   ":"] end]]
      set theLtlVar [string trim [lindex [split $theLtlVar "'"]   1]]
      lappend theLtlVarNames $theLtlVar
    }
  }
  return $theLtlVarNames
}

#===============================================================================

##
# \brief
#   Creates a verilog file dedicated to one LTL specification.
#
# \details
#   Concatenates the content of the verilog file with what is printed by \c
#   createVerilogWrapper.tcl into a file with the same root name as the LTL
#   file.
#
# \param  theLtlFile
#   Path to the LTL file to determine the name of the verilog output.
#
# \param  theWrapperArgs
#   Arguments to give to \c createVerilogWrapper.tcl. These are given by
#   function \ref nusmvall::getWrapperArgs.
#
# \param  theLtlVarNames
#   List of LTL variables names to filter the outputs of the verilog wrapper.
#   These are given by function \ref nusmvall::getLtlVarNames.
#
# \returns the path the created verilog file.
#
proc nusmvall::createVerilogWrapper {
  theLtlFile
  theWrapperArgs
  theLtlVarNames
} {
  set theVerilogFile [file rootname $theLtlFile].v

  # reads the verilog input:
  set theCode [catch {
    set theBuffer      [open $nusmvall::theVerilog "r"]
    set theVerilogText [read $theBuffer]
    close $theBuffer
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** Unable to read verilog file: $nusmvall::theVerilog"
    puts "$theReturn"
    return 1
  }

  # creates a top level wrapper:
  set    theArgs $theWrapperArgs
  append theArgs " --filter " [join $theLtlVarNames " "]
  #
  set here [file dirname [file normalize [info script]]]
  set theCode [catch {
    nusmvall::execCall [info nameofexecutable] \
      [file join $here "createVerilogWrapper.tcl"] {*}$theArgs
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
    return 2
  }

  # concatenates the two verilog modules into one file:
  set theCode [catch {
    set    theBuffer [open $theVerilogFile "w"]
    puts  $theBuffer $theVerilogText
    puts  $theBuffer $theReturn
    close $theBuffer
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** Unable to write verilog file: $theVerilogFile"
    puts "$theReturn"
    return 3
  }

  return $theVerilogFile
}

#===============================================================================

##
# \brief
#   Uses verilog2smv to convert the verilog input into SMV. Also concatenates
#   the output with the LTL specification.
#
# \details
#   When concatenating  the LTL specification, ignores the lines starting with
#   \c MODULE. Also replaces IVAR by VAR in the SMV file as it introduces an
#   error to PSL specifications.
#
# \param theVerilogInputFile
#   Path to the verilog input file.
#
# \param theLtlFile
#   Path to the input LTL file.
#
# \param theTopModule
#   Name of the verilog top module to convert to SMV.
#
# \returns the path the created SMV file.
#
proc nusmvall::verilog2smv {
  theVerilogInputFile
  theLtlFile
  {theTopModule "top"}
} {
  set theSmvFile [file rootname $theVerilogInputFile].smv

  set here [pwd]

  # converts verilog to SMV:
  set theCode [catch {
    cd [file dirname $nusmvall::theVerilog]
nusmvall::execCall verilog2smv $theVerilogInputFile $theSmvFile $theTopModule
  } theReturn]
  cd $here
  set theSmvFile [file join [file dirname $nusmvall::theVerilog] $theSmvFile]
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
    return 1
  }

  # replace IVAR by VAR in the SMV file as it introduces an error to PSL
  # specifications:
  set theCode [catch {
    set theBuffer [open $theSmvFile "r"]
    set theText   [read $theBuffer]
    close $theBuffer
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** Unable to read verilog2smv output file: $theSmvFile"
    return 2
  }
  regsub -all -lineanchor {^IVAR$} $theText {VAR} theText

  # concatenates the LTL specification:
  set theCode [catch {
    set theBuffer  [open $theLtlFile "r"]
    set theLtlText [read $theBuffer]
    close $theBuffer
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** Unable to read LTL specification file: $theLtlFile"
    puts "$theReturn"
    return 4
  }
  #
  set theCode [catch {
    set   theBuffer [open $theSmvFile "w"]
    puts $theBuffer $theText
    foreach theLine [split $theLtlText "\n"] {
      if { [string first "MODULE" $theLine] != 0 } {
        puts $theBuffer $theLine
      }
    }
    close $theBuffer
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** Unable to concatenate LTL specification to file:\
      $theSmvFile"
    puts "$theReturn"
    return 5
  }

  return $theSmvFile
}

#===============================================================================

##
# \brief
#   Creates a VCD file with the same root name as a specification file if the
#   NuSMV output contains a counterexample.
#
# \details
#   Concatenates NuSMV output to the input of \c smvTrace2vcd.tcl then writes
#   its output to a VCD file.
#
# \param  theSmvInputFile
#   Path to the specification file that has been passed to NuSMV.
#
# \param  theSmvOutputText
#   Output of NuSMV when it processed the specification file.
#
proc nusmvall::smvTrace2vcd {
  theSmvInputFile
  theSmvOutputText
} {
  # make sure that a counterexample is available:
  set toBeCreated 0
  foreach theLine [split $theSmvOutputText "\n"] {
    if { [string first "-- specification" $theLine] == 0 &&
         [string first "is false"         $theLine]  > 0 } {
      set toBeCreated 1
      break
    }
    if { [string first "-- invariant" $theLine] == 0 &&
         [string first "is false"     $theLine]  > 0 } {
      set toBeCreated 1
      break
    }
  }

  if { $toBeCreated != 0 } {
    # executes external program:
    set here [file dirname [file normalize [info script]]]
    set theCode [catch {
      nusmvall::execCall [info nameofexecutable] \
        [file join $here "smvTrace2vcd.tcl"] << $theSmvOutputText
    } theReturn]
    if { $theCode != 0 } {
      puts "** Error ** $theReturn"
      return 1
    }

    # creates output file:
    set theText     $theReturn
    set theFilePath [file rootname $theSmvInputFile].vcd
    set theCode [catch {
      set    theBuffer [open $theFilePath "w"]
      puts  $theBuffer ${theText}
      close $theBuffer
    } theReturn]
    if { $theCode != 0 } {
      puts "** Error ** $theReturn"
      return 2
    }
  }

  return 0
}

#===============================================================================
#===============================================================================

set errorCount 0

nusmvall::init
nusmvall::parseArgv $::argv
set theWrapperArgs  [nusmvall::getWrapperArgs]
set ltlFilesList    [nusmvall::splitLtl]

# create verilog files whilst filtering their outputs and converts them to SMV:
set theVerilogFilesList [list]
set theSmvFilesList     [list]
#
foreach theLtlFile $ltlFilesList {
  set theLtlVarNames [nusmvall::getLtlVarNames $theLtlFile]
  #
  set theVerilogFile \
    [nusmvall::createVerilogWrapper $theLtlFile $theWrapperArgs $theLtlVarNames]
  lappend theVerilogFilesList $theVerilogFile
  #
  set theSmvFile [nusmvall::verilog2smv $theVerilogFile $theLtlFile]
  lappend theSmvFilesList $theSmvFile
}

# create a SMV file for the concrete model to verify the soundness:
set theSoundnessFile \
  [file join [file dirname $nusmvall::theVerilog] "soundness.v"]
set theVerilogFile \
  [nusmvall::createVerilogWrapper $theSoundnessFile $theWrapperArgs [list]]
lappend theVerilogFilesList $theVerilogFile
#
set theConcreteSmvFile [nusmvall::verilog2smv [file tail $theVerilogFile] "/dev/null"]

# use pynusmv to verify the soundness and NuSMV for the model checking:
set theText ""
#
foreach theSmvFile $theSmvFilesList {
  # verify the soundness with pynusmv:
  set    theArgs ""
  append theArgs " --concrete " ${theConcreteSmvFile}
  append theArgs " --abstract " ${theSmvFile}
  #
  set here [file dirname [file normalize [info script]]]
  #
  set theCode [catch {
    nusmvall::execCall [file join $here "equivalence.py"] {*}$theArgs
  } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** Verification of soundness returned an error:"
    puts ${theReturn}
    continue
  }
  #
  foreach theLine [split $theReturn "\n"] {
    if { $theLine != "" && [string first "***" $theLine] < 0 } {
      append theText $theLine "\n"
    }
  }

  # model checking with NuSMV:
  set theCode [catch {
    nusmvall::execCall NuSMV $theSmvFile
  } theReturn]
  #
  if { $theCode != 0 } {
    puts "** Error ** Execution of NuSMV returned an non-zero code. Input:\
       $theSmvFile"
    incr errorCount
  }
  #
  if { $nusmvall::convertToVcd != 0 } {
    nusmvall::smvTrace2vcd $theSmvFile $theReturn
  }
  foreach theLine [split $theReturn "\n"] {
    if { $theLine != "" && [string first "***" $theLine] < 0 } {
      append theText $theLine "\n"
    }
  }
  append theText "\n"
}

# clean:
foreach theLtlFile \
          [concat $ltlFilesList $theVerilogFilesList $theConcreteSmvFile] {
  set theCode [catch { file delete -force $theLtlFile } theReturn]
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
  }
  # only if parent directory is empty:
  catch { file delete [file dirname $theLtlFile] }
}

# prints the output:
if { $nusmvall::theOutputFile == "stdout" } {
  puts $theText
} else {
  set theParentFolder [file dirname [file normalize ${nusmvall::theOutputFile}]]
  if { ![file isdirectory $theParentFolder] } {
    set theCode [catch { file mkdir $theParentFolder } theReturn]
    if { $theCode != 0 } {
      puts "** Error ** $theReturn"
      exit $theCode
    }
  }

  set theCode [catch {
    set    theBuffer [open ${nusmvall::theOutputFile} "w"]
    puts  $theBuffer ${theText}
    close $theBuffer
  } theReturn]
  #
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
  } else {
    puts "Creation of file: ${nusmvall::theOutputFile}"
  }
}

exit $errorCount

