#!/bin/sh
# the next line restarts using tclsh \
exec tclsh "$0" ${1+"$@"}

#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

#===============================================================================

##
# \brief
#   Stores the variables that describe the wrapper to create.
#
namespace eval wrapper {
  set theOutputFile "stdout"
  set theModuleName "top"
  set theInputList  [list]
  set theOutputList [list]
  set theFilterList [list]
}

#===============================================================================

##
# \brief
#   Prints help to the standard output and then exits.
#
proc wrapper::printHelpAndExit {
  {theCode 0}
} {
  puts "Creation of a verilog wrapper:

SYNOPSIS:
  ./createVerilogWrapper.tcl \[OPTIONS\]

DESCRIPTION:
  Creates a verilog wrapper from a list of inputs and outputs. It instanciates a
  module with a given name and connects its inputs and outputs to the wrapper
  inputs and outputs.
  If a filter is provided, outputs NOT having their name matching one filter are
  ignored. Data between brackets \[\] in the output names is ignored when
  matching.

OPTIONS:
  -t OR --to     : the argument following this option gives a path to the file
                   to print the verilog wrapper to. If this argument is ommited,
                   the verilog wrapper is printed to the standard output.

  -m OR --module : the argument following this option gives the name of the
                   module to instanciate.

  -i OR --input  : all arguments following this option are considered as
                   inputs. If its ends with data between brackets \[\], their
                   content is considered as a vector range.

  -o OR --output : all arguments following this option are considered as
                   outputs. If its ends with data between brackets \[\], their
                   content is considered as a vector range.

  -F OR --filter : all arguments following this option are considered as
                   filters.

  -h OR --help   : prints this help and exits

EXAMPLE:
  ./createVerilogWrapper.tcl --to     outputfile.v \\
                             --module \"modulename\" \\
                             --input  a \"b\[1:0\]\" c \\
                             --output e f \"g\[2:0\]\" \\
                             --filter f
"
  exit $theCode
}

#===============================================================================

##
# \brief
#   Parses the vector of arguments and updates the variables in namespace \ref
#   wrapper.
#
# \details
#   More informations about the syntax are given in the text printed by \ref
#   wrapper::printHelpAndExit.
#
# \param  argv  Vector of arguments.
#
proc wrapper::parseArgv {
  argv
} {
  set previousArg ""
  foreach theArg [string trim $argv] {
    if { [string first "-" $theArg] == 0 } {
      switch $theArg {
        "-t" - "--to"     { set previousArg "to"      }
        "-m" - "--module" { set previousArg "module"  }
        "-i" - "--input"  { set previousArg "input"   }
        "-o" - "--output" { set previousArg "output"  }
        "-F" - "--filter" { set previousArg "filter"  }
        "-h" - "--help"   { wrapper::printHelpAndExit }
        default  {
          puts "** Error ** unable to parse argument: $theArg"
          wrapper::printHelpAndExit 1
        }
      }
    } else {
      switch $previousArg {
        "to"     { set wrapper::theOutputFile $theArg ; set previousArg "" }
        "module" { set wrapper::theModuleName $theArg ; set previousArg "" }
        "input"  { lappend wrapper::theInputList  $theArg }
        "output" { lappend wrapper::theOutputList $theArg }
        "filter" { lappend wrapper::theFilterList $theArg }
        default  {
          puts "** Error ** unable to parse argument: $previousArg $theArg"
          wrapper::printHelpAndExit 2
        }
      }
    }
  }

  return 0
}

#===============================================================================

##
# \brief
#   Creates the content of the verilog wrapper according to the variables in
#   \ref wrapper namespace.
#
# \returns string: the content of the verilog wrapper.
#
proc wrapper::createText {} {
  if { ${wrapper::theModuleName} != "top" } {
    set theText "
module top(
"
  } else {
    set theText "
module toplevel(
"
  }

  ##
  # filter the output list
  #
  if { [llength ${wrapper::theFilterList}] > 0 } {
    set theOutputList [list]
    foreach theOutput ${wrapper::theOutputList} {
      regsub -all {([^\[\]]*)(\[.*\])} $theOutput {\1} theOutputName
      set isIgnored 1
      foreach theFilter ${wrapper::theFilterList} {
        if { [string match $theFilter $theOutputName] } {
          set isIgnored 0
          break
        }
      }
      if { $isIgnored != 1 } {
        lappend theOutputList $theOutput
      }
    }
    set wrapper::theOutputList $theOutputList
  }

  ##
  # inputs and outputs list:
  #
  set theInputLength  [llength ${wrapper::theInputList}]
  set theOutputLength [llength ${wrapper::theOutputList}]
  #
  for {set i 0} {$i < $theInputLength} {incr i} {
    set theInput [lindex ${wrapper::theInputList} $i]
    regsub -all {([^\[\]]*)(\[.*\])} $theInput {\2 \1} theInput
    #
    append theText "  input  $theInput"
    if { $i < [expr {$theInputLength - 1}] } {
      append theText ",\n"
    } else {
      if { $theOutputLength > 0 } {
        append theText ",\n"
      } else {
        append theText "\n"
      }
    }
  }
  #
  for {set i 0} {$i < $theOutputLength} {incr i} {
    set theOutput [lindex ${wrapper::theOutputList} $i]
    regsub -all {([^\[\]]*)(\[.*\])} $theOutput {\2 \1} theOutput
    #
    append theText "  output $theOutput"
    if { $i < [expr {$theOutputLength - 1}] } {
      append theText ",\n"
    } else {
      append theText "\n"
    }
  }
  append theText ");\n"
  append theText "\n"

  ##
  # instance of the module:
  #
  append theText "  ${wrapper::theModuleName} i_${wrapper::theModuleName} (\n"
  #
  for {set i 0} {$i < $theInputLength} {incr i} {
    set theInput [lindex ${wrapper::theInputList} $i]
    regsub -all {([^\[\]]*)(\[.*\])} $theInput {\1} theInput
    #
    append theText "    .${theInput}( ${theInput} )"
    if { $i < [expr {$theInputLength - 1}] } {
      append theText ",\n"
    } else {
      if { $theOutputLength > 0 } {
        append theText ",\n"
      } else {
        append theText "\n"
      }
    }
  }
  #
  for {set i 0} {$i < $theOutputLength} {incr i} {
    set theOutput [lindex ${wrapper::theOutputList} $i]
    regsub -all {([^\[\]]*)(\[.*\])} $theOutput {\1} theOutput
    #
    append theText "    .${theOutput}( ${theOutput} )"
    if { $i < [expr {$theOutputLength - 1}] } {
      append theText ",\n"
    } else {
      append theText "\n"
    }
  }
  append theText "  );\n"

  append theText "\n"
  append theText "endmodule"

  return $theText
}

#===============================================================================
#===============================================================================

wrapper::parseArgv $::argv
set theText [wrapper::createText]

if { $wrapper::theOutputFile == "stdout" } {
  puts $theText
} else {
  set theParentFolder [file dirname [file normalize ${wrapper::theOutputFile}]]
  if { ![file isdirectory $theParentFolder] } {
    set theCode [catch { file mkdir $theParentFolder } theReturn]
    if { $theCode != 0 } {
      puts "** Error ** $theReturn"
      exit $theCode
    }
  }

  set theCode [catch {
    set    theBuffer [open ${wrapper::theOutputFile} "w"]
    puts  $theBuffer ${theText}
    close $theBuffer
  } theReturn]
  #
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
  } else {
    puts "Creation of file: ${wrapper::theOutputFile}"
  }
  exit $theCode
}

