#!/usr/local/bin/python3

#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import os
import re
import sys
import pynusmv
import pynusmv.nusmv.parser.psl.psl

##
# \brief
#   Stores a list of path to the SMV module file containing verified properties
#   after \ref psl2spot_parseArgv() has been called.
#
psl2spot_propertiesFilePath = list()

##
# \brief
#   Stores the path to the SMV module file containing overall specifications
#   after \ref psl2spot_parseArgv() has been called.
#
psl2spot_specificationsFilePath = str()

##
# \brief
#   Stores the path to the file to print the python script to after \ref
#   psl2spot_parseArgv() has been called.
#
psl2spot_outputFilePath = str()

##
# \brief
#   Stores the content of the string used to filter the properties by their
#   names.
#
psl2spot_filterString = str()

#===============================================================================

##
# \brief
#   Prints help to the standard output and then exits.
#
def psl2spot_printHelpAndExit(
  theCode #!< the exit code
):
  theText = """
Creates a python script from two SMV modules to verify using Spot that a
conjunction of properties implies overall specifications.

SYNOPSIS:
  ./psl2spot.py --properties FILE1 [FILE]... --specifications FILE2 [--to FILE3]

DESCRIPTION:
  Uses pynusmv tool to parse model files and converts PSLSPEC, INVARSPEC and
  LTLSPEC into LTL so that Spot can process them.

  Does not expand the forall replicators so that we can reason about the
  replicated properties (i.e. "out = uwconst(i,32)"). As a consequence, we
  assume that model checking has been processed in the appropriate range for the
  replicated variable.

  Removes double quotes from the expressions. Then puts equalities and
  inequalities into double quotes so that Spot cat interpret them as a single
  atomic proposition.

  Both input SMV modules must be correctly written as their content is not
  verified.

OPTIONS:
  -p OR --properties     : all arguments following this option are considered as
                           a path to the file containing verified properties.

  -s OR --specifications : the argument following this option gives a path to
                           the file containing overall specifications to prove.

  -t OR --to             : the argument following this option gives a path to
                           the file to print the python script to. If this
                           argument is omitted, the script is printed to the
                           standard output.

  -F OF --filter         : the argument following this option gives a regular
                           expression to filter the verified properties by the
                           content of their NAME attribute.

  -h OR --help           : prints this help and exits

EXAMPLE:
  ./psl2spot.py --properties     prop0.smv prop1.smv \\
                --specifications spec.smv            \\
                --to             proof.py
"""
  print(str.strip(theText))
  exit(int(theCode))

#===============================================================================

##
# \brief
#   Creates a list of NuSMV node pointers that all share the same type
#   attribute.
#
# \details
#   Recursively accesses NuSMV nodes from one specific node and compares their
#   type: if it matches the expected type, adds them to the list that is given
#   as a parameter. Stops recursively accessing nodes once the type cannot have
#   a hierarchy.
#
# \returns void
#
def recursivelyGetAllNodes(
  theTopNode,   #!< the node to get in the hierarchy from
  theNodeType,  #!< the type of nodes to add to the list
  theList       #!< the list to append
):
  if ( theTopNode.type == theNodeType ):
    list.append(theList, theTopNode)
  else:
    if theTopNode.type not in {
      pynusmv.nusmv.parser.parser.NUMBER_SIGNED_WORD,
      pynusmv.nusmv.parser.parser.NUMBER_UNSIGNED_WORD,
      pynusmv.nusmv.parser.parser.NUMBER_FRAC,
      pynusmv.nusmv.parser.parser.NUMBER_EXP,
      pynusmv.nusmv.parser.parser.NUMBER_REAL,
      pynusmv.nusmv.parser.parser.NUMBER,
      pynusmv.nusmv.parser.parser.ATOM,
      pynusmv.nusmv.parser.parser.FAILURE
    }:
      if ( pynusmv.nusmv.node.node.car(theTopNode) is not None ):
        recursivelyGetAllNodes( pynusmv.nusmv.node.node.car(theTopNode),
                                theNodeType,
                                theList
                              )
      if ( pynusmv.nusmv.node.node.cdr(theTopNode) is not None ):
        recursivelyGetAllNodes( pynusmv.nusmv.node.node.cdr(theTopNode),
                                theNodeType,
                                theList
                              )

#===============================================================================

##
# \brief
#   From a NuSMV node pointer, extracts its string and formats it with Spot
#   syntax.
#
# \warning
#   Replacing equalities so that they are into double quotes is done at semantic
#   level, it should be done on NuSMV symbols.
#
# \returns
#   A string: LTL specification written with Spot syntax.
#
def psl2spot_getSpotString(
 theNode
):
  # removes the double quotes introduced by verilog2smv:
  theAtomList = list()
  recursivelyGetAllNodes(
    theNode, pynusmv.nusmv.parser.parser.ATOM, theAtomList )
  #
  for theAtom in theAtomList:
    theAtomText = theAtom.left.strtype.text
    if ( theAtomText[0] == '"' and theAtomText[len(theAtomText)-1] == '"' ):
      theAtom.left.strtype.text = theAtomText[1:len(theAtomText)-1]

  theLtlString = pynusmv.nusmv.node.node.sprint_node(theNode)

  # gets equalities in the property:
  theEqualList = list()
  recursivelyGetAllNodes(
    theNode, pynusmv.nusmv.parser.parser.EQUAL, theEqualList )
  recursivelyGetAllNodes(
    theNode, pynusmv.nusmv.parser.parser.NOTEQUAL, theEqualList )
  recursivelyGetAllNodes(
    theNode, pynusmv.nusmv.parser.parser.GT, theEqualList )
  recursivelyGetAllNodes(
    theNode, pynusmv.nusmv.parser.parser.GE, theEqualList )
  recursivelyGetAllNodes(
    theNode, pynusmv.nusmv.parser.parser.LT, theEqualList )
  recursivelyGetAllNodes(
    theNode, pynusmv.nusmv.parser.parser.LE, theEqualList )
  # puts them into double quotes:
  for theEqualNode in theEqualList:
    theEqualString    = pynusmv.nusmv.node.node.sprint_node(theEqualNode)

    # using regular expressions to detect that we already treated an equality:
    thePattern = '([^"]*)' + re.escape(theEqualString) + '([^"]*)'
    theMatch   = re.search(thePattern, theLtlString)
    if ( theMatch != None ):
      theReplace   = theMatch.group(1) + '("' + \
                        theEqualString + '")' + theMatch.group(2)
      # (HACK) replace the first occurrence so we do not replace 'a = 0ud8_1' in
      # 'a = 0ud8_10'. This only works if the values go increasingly in the
      # property.
      theLtlString = re.sub(thePattern, theReplace, theLtlString, 1)
    else :
      print("** Error ** Unable to find pattern: " + thePattern)
      print("            in: " + theLtlString)

  return theLtlString

#===============================================================================

##
# \brief
#   Uses NuSMV parser to convert PSL expressions into LTL so that spot can
#   process them.
#
# \details
#   Does not expand the <tt> forall </tt> replicators so that we can reason
#   about the replicated properties (i.e. "out = uwconst(i,32)"). As a
#   consequence, we assume that model checking has been processed in the
#   appropriate range for the replicated variable.
#
# \returns a list of elements to be taken two by two: the first element is the
#   PSL expression and the second is its LTL version.
#
# \warning
#   Removes double quotes from the PSL expressions. Then puts equalities and
#   inequalities into double quotes so that spot cat interpret them as a single
#   atomic proposition. There might be a bug since the substitution occurs on
#   the string describing the expression.
#
# \bug
#   There is a crash of NuSMV if the input file is not correctly written.
#
def psl2spot_parseSmvFile(
  theFilePath #!< path to the input file containing PSL expressions
):
  pynusmv.init.reset_nusmv()
  # causes a segmentation fault if the input file is not correctly written
  # (catching the error cannot avoid NuSMV API to stop):
  pynusmv.nusmv.parser.parser.Parser_ReadSMVFromFile(str(theFilePath))

  ##
  # accesses all PSL properties in the file:
  theTree = pynusmv.nusmv.parser.parser.cvar.parsed_tree
  thePslSpecNodeList = list()
  recursivelyGetAllNodes(
    theTree, pynusmv.nusmv.parser.parser.PSLSPEC, thePslSpecNodeList )

  thePropList = list() # list of converted properties (3 by 3: in PSL, in LTL)
  for thePslSpecNode in thePslSpecNodeList:

    # saves the name of the property (left node):
    theNameNode   = pynusmv.nusmv.node.node.cdr(thePslSpecNode)
    theNameString = pynusmv.nusmv.node.node.sprint_node(theNameNode)
    #
    theNewName = str()
    for theLine in str.split(theNameString, "\n"):
      theNewName += str.strip(theLine) + " "
    theNameString= str.strip(theNewName)

    # accesses the property (right node):
    thePslNode   = pynusmv.nusmv.node.node.car(thePslSpecNode)

    # gets the top level REPLICATOR PROPERTY node, if any:
    theReplPropNodeList = list()
    recursivelyGetAllNodes( thePslNode,
                            pynusmv.nusmv.parser.psl.psl.PSL_REPLPROP,
                            theReplPropNodeList )
    #
    if ( len(theReplPropNodeList) < 1 ):
      theNode = pynusmv.nusmv.parser.psl.psl.\
                  PslNode_convert_psl_to_core(thePslNode)
    else:
      theReplPropNode = theReplPropNodeList[0]
      # separates the replicator and the property:
      theReplicator = pynusmv.nusmv.parser.psl.psl.\
                            psl_node_repl_prop_get_replicator(theReplPropNode);
      theProperty   = pynusmv.nusmv.parser.psl.psl.\
                            psl_node_repl_prop_get_property(theReplPropNode);
      theReplVar    = pynusmv.nusmv.parser.psl.psl.\
                            psl_node_get_replicator_id(theReplicator)
      # converts to LTL so that spot can read it:
      theNode = pynusmv.nusmv.parser.psl.psl.\
                            PslNode_convert_psl_to_core(theProperty)
      # # DEBUG
      # print(pynusmv.nusmv.node.node.sprint_node(theReplicator))
      # print(pynusmv.nusmv.node.node.sprint_node(theReplVar   ))
      # print(pynusmv.nusmv.node.node.sprint_node(theProperty  ))
      # print("")
      # print(pynusmv.nusmv.node.node.sprint_node(theNode))

    # gets the string from the node and converts it to Spot syntax (modifies the
    # content of the node so the original string is also modified):
    theSpotString = psl2spot_getSpotString(theNode)
    thePslString  = pynusmv.nusmv.node.node.sprint_node(thePslNode)

    list.append(thePropList, "PSLSPEC " + str.strip(thePslString))
    list.append(thePropList,              str.strip(theSpotString))
    list.append(thePropList,              str.strip(theNameString))


  ##
  # accesses all LTL properties in the file:
  theTree = pynusmv.nusmv.parser.parser.cvar.parsed_tree
  theLtlSpecNodeList = list()
  recursivelyGetAllNodes(
    theTree, pynusmv.nusmv.parser.parser.LTLSPEC, theLtlSpecNodeList )

  for theLtlSpecNode in theLtlSpecNodeList:

    # saves the name of the property (left node):
    theNameNode   = pynusmv.nusmv.node.node.cdr(theLtlSpecNode)
    theNameString = pynusmv.nusmv.node.node.sprint_node(theNameNode)
    #
    theNewName = str()
    for theLine in str.split(theNameString, "\n"):
      theNewName += str.strip(theLine) + " "
    theNameString = str.strip(theNewName)

    # accesses the property (right node):
    theLtlSpecNode = pynusmv.nusmv.node.node.car(theLtlSpecNode)

    theSpotString  = psl2spot_getSpotString(theLtlSpecNode)
    theLtlString   = pynusmv.nusmv.node.node.sprint_node(theLtlSpecNode)
    #
    list.append(thePropList, "LTLSPEC " + str.strip(theLtlString))
    list.append(thePropList,              str.strip(theSpotString))
    list.append(thePropList,              str.strip(theNameString))


  ##
  # accesses all invariant properties in the file:
  theTree = pynusmv.nusmv.parser.parser.cvar.parsed_tree
  theInvarSpecNodeList = list()
  recursivelyGetAllNodes(
    theTree, pynusmv.nusmv.parser.parser.INVARSPEC, theInvarSpecNodeList )

  for theInvarSpecNode in theInvarSpecNodeList:

    # saves the name of the property (left node):
    theNameNode   = pynusmv.nusmv.node.node.cdr(theInvarSpecNode)
    theNameString = pynusmv.nusmv.node.node.sprint_node(theNameNode)
    #
    theNewName = str()
    for theLine in str.split(theNameString, "\n"):
      theNewName += str.strip(theLine) + " "
    theNameString = str.strip(theNewName)

    # accesses the property (right node):
    theInvarSpecNode = pynusmv.nusmv.node.node.car(theInvarSpecNode)
    theInvarString = pynusmv.nusmv.node.node.sprint_node(theInvarSpecNode)

    # replace next() operators from INVARSPEC to X() operators from LTLSPEC:
    theNextList = list()
    recursivelyGetAllNodes(
      theInvarSpecNode, pynusmv.nusmv.parser.parser.NEXT, theNextList )
    for theNextNode in theNextList:
      theNextNode.type = pynusmv.nusmv.parser.parser.OP_NEXT

    # wrap the INVARSPEC node into a new node to say that the invariant is
    # globally true:
    theGloballyNode = pynusmv.nusmv.node.node.node()
    pynusmv.nusmv.node.node.node_set_type(theGloballyNode,
                                          pynusmv.nusmv.parser.parser.OP_GLOBAL)
    pynusmv.nusmv.node.node.setcar(theGloballyNode, theInvarSpecNode)

    theSpotString  = psl2spot_getSpotString(theGloballyNode)
    #
    list.append(thePropList, "INVARSPEC " + str.strip(theInvarString))
    list.append(thePropList,                str.strip(theSpotString))
    list.append(thePropList,                str.strip(theNameString))

  return thePropList

#===============================================================================

##
# \brief
#   Creates a python script using spot to prove an implication of the overall
#   specifications.
#
# \details
#   In this script, a formula is created such as the conjunction of a list of
#   verified properties implies the conjunction of a list of overall
#   specifications. Spot processes the implication with objective to reduce it
#   to a tautology.
#
#   The output script can be edited later so that axioms can be added to help
#   proving the implication.
#
# \returns the text to be printed in the script.
#
def psl2spot_getPythonOutput(
  thePropList,  #!< list of elements to be taken 3 by 3, verified properties
  theSpecList   #!< list of elements to be taken 3 by 3, overall specifications
):

  theText  = "#!" + str(sys.executable) + "\n"
  theText += """
#--------
# axioms:
#--------
theAxioms = []

#------------
# properties:
#------------
theProperties = [
"""

  for i in range(0, len(thePropList), 3):
    thePslString = thePropList[i]
    theLtlString = thePropList[i+1]
    theName      = thePropList[i+2]
    #
    # filter the properties:
    try:
      theMatch = re.search(psl2spot_filterString, theName)
    except:
      print("** error ** with regexp pattern: " + psl2spot_filterString)
    else:
      if ( theMatch != None ):
        theText += "\n" + "# " + str(theName)      + "\n"
        theText +=        "# " + str(thePslString) + "\n"
        theText += "\"\"\"" + str(theLtlString) + "\"\"\"," + "\n"

  theText += """]

#------------------------
# overall specifications:
#------------------------
theSpecification = [
"""

  for i in range(0, len(theSpecList), 3):
    thePslString = theSpecList[i]
    theLtlString = theSpecList[i+1]
    theName      = theSpecList[i+2]
    #
    theText += "\n" + "# " + str(theName)      + "\n"
    theText +=        "# " + str(thePslString) + "\n"
    theText += "\"\"\"" + str(theLtlString) + "\"\"\"," + "\n"

  theText += """]

#-------
# proof:
#-------
if ( len(theAxioms) > 0 ):
  theLeftSide  = ("(" + str.join(") & (", theAxioms) + ")")
  theLeftSide += " & "
else:
  theLeftSide = ""
#
theLeftSide += ("(" + str.join(") & (", theProperties)    + ")")

theFormula  = theLeftSide + " -> "
theFormula += ("(" + str.join(") & (", theSpecification) + ")")
#
import spot
theFormula = spot.formula(theFormula)
spot.formula.translate(theFormula)  # processes the implication
#
if ( theFormula.is_tt() ) :
  print("Implication is true, now verifying...")
  theLeftSide = spot.formula(theLeftSide)
  spot.formula.translate(theLeftSide)
  if ( str(theLeftSide) == str(0) ):
    print("The system is not alive.")
  else:
    print("Implication is a tautology.")
else :
  print("Implication is equivalent to: ")
  print(theFormula.to_str())
  exit(1)
"""

  return theText

#===============================================================================

##
# \brief
#   Parses the vector of arguments and updates global variables \ref
#   psl2spot_propertiesFilePath, \ref psl2spot_specificationsFilePath and \ref
#   psl2spot_outputFilePath.
#
# \details
#   More informations about the syntax are given in the text printed by \ref
#   psl2spot_printHelpAndExit().
#
def psl2spot_parseArgv(
  argv  #!< vector of arguments as accessed with <tt> sys.argv </tt>
):
  global psl2spot_propertiesFilePath
  global psl2spot_specificationsFilePath
  global psl2spot_outputFilePath
  global psl2spot_filterString

  if ( len(argv) < 2 ):
    print("** Error ** you must specify at least one argument")
    exit(1)

  previousArg = str()
  for theArg in argv[1:len(argv)]:
    if ( str.find(theArg, "-") == 0 ):
      if   ( str(theArg) == "-p" or str(theArg) == "--properties"     ):
        previousArg = "properties"
      elif ( str(theArg) == "-s" or str(theArg) == "--specifications" ):
        previousArg = "specifications"
      elif ( str(theArg) == "-t" or str(theArg) == "--to"             ):
        previousArg = "to"
      elif ( str(theArg) == "-F" or str(theArg) == "--filter"         ):
        previousArg = "filter"
      elif ( str(theArg) == "-h" or str(theArg) == "--help"           ):
        psl2spot_printHelpAndExit(0)
      else:
        print("** Error ** unable to parse argument: " + str(theArg))
        exit(2)
    else:
      if   ( str(previousArg) == "properties" ):
        list.append(psl2spot_propertiesFilePath, str(theArg))
      elif ( str(previousArg) == "specifications" ):
        psl2spot_specificationsFilePath = str(theArg)
        previousArg = ""
      elif ( str(previousArg) == "to" ):
        psl2spot_outputFilePath = str(theArg)
        previousArg = ""
      elif ( str(previousArg) == "filter" ):
        psl2spot_filterString = str(theArg)
        previousArg = ""
      else:
        print("** Error ** unable to parse argument: " \
                + str(previousArg) + str(theArg) )

  fileExists = True
  for thePath in psl2spot_propertiesFilePath:
    if ( not os.path.exists(str(thePath)) ):
      fileExists = False
      break
  #
  if ( not os.path.exists(str(psl2spot_specificationsFilePath)) ):
    fileExists = False

  if ( not fileExists ):
    print("** Error ** you must specify existing files for the properties and" \
          " specifications.")
    exit(3)

#===============================================================================
#===============================================================================

psl2spot_parseArgv(sys.argv)

pynusmv.init.init_nusmv()

thePropList = list()
for theFilePath in psl2spot_propertiesFilePath:
  thePropList += psl2spot_parseSmvFile(theFilePath)
#
theSpecList = psl2spot_parseSmvFile(psl2spot_specificationsFilePath)

theText = psl2spot_getPythonOutput(thePropList, theSpecList)

if ( psl2spot_outputFilePath == str() ):
  print(theText)
else:
  try :
    theBuffer = open(psl2spot_outputFilePath, "w")
    theBuffer.write(theText)
    theBuffer.close()
  except :
    print("** Error ** Unable to write to file: " + psl2spot_outputFilePath)
    exit(1)
  else :
    print("Creation of file: " + psl2spot_outputFilePath)
