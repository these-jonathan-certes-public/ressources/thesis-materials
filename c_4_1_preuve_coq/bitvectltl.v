(*
 * Copyright (C) 2022 Guillaume Dupont
 * guillaume.dupont@toulouse-inp.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Coq.Init.Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Arith.Peano_dec.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Lists.List.
Import ListNotations.
Require Import LTL.
Require Import natutil.
Require Import bit.
Import bit.Bit.
Require Import bitvect.
Import bitvect.BitVect.
Require Import nat2bit.
Require Import nat2bitvect.
(*Require Import BitVectGen.*)
Require Import truefor.

(* We use the TrueFor module to lift a bunch of results to the LTL realm. This is a "true" lift, in the sense that
 * we actually transpose Prop notions to LTL notions.
 *)

(** We create a wrapping of leq_vect and eq_vect. These are axioms because we do not need to construct it. *)
Axiom leq_vect_CVP : forall size : nat, forall a b : BitVect size, CVP (leq_vect size a b).
Axiom eq_vect_CVP : forall size : nat, forall a b : BitVect size, CVP (a = b).

(** We now define the "pathF predicate" le_BitVector and eq_BitVector using the wrappers. *)
Definition le_BitVector {size : nat} (v1 v2 : BitVect size) : PathF :=
  Var (strP (leq_vect size v1 v2) (leq_vect_CVP size v1 v2)).
Definition eq_BitVector {size : nat} (v1 v2 : BitVect size) : PathF :=
  Var (strP (v1 = v2) (eq_vect_CVP size v1 v2)).

(** A few notations *)
Notation "a '<= b" := (le_BitVector a b) (at level 100).
Notation "a '== b" := (eq_BitVector a b) (at level 100).
Notation "uwconst( v , s )" := (nat2vect s v)   (at level 100).

(** Theorem "not_leq_lt" lifted for [[.]] *)
Theorem not_leq_lt_TF : forall size : nat, forall v1 v2 : BitVect size, forall pi : Path, forall s : State,
  [[~leq_vect size v1 v2]] pi s <-> [[lt_vect size v2 v1]] pi s.
Proof.
intros size v1 v2 pi s; split; intro H.
apply (TrueFor_lift_thm (not_leq_lt size v1 v2)).
assumption.
apply (TrueFor_lift_thm (lt_not_leq size v1 v2)).
assumption.
Qed.

(** Theorem "lt_vect_tran" lifted for [[.]] *)
Theorem lt_vect_tran_TF : forall size : nat, forall v1 v2 v3 : BitVect size, forall pi : Path, forall s : State,
  [[lt_vect size v1 v2]] pi s -> [[lt_vect size v2 v3]] pi s -> [[lt_vect size v1 v3]] pi s.
Proof.
intros size v1 v2 v3 pi s H1 H2.
apply (TrueFor_lift_thm2 (lt_vect_tran size v1 v2 v3)); assumption.
Qed.

(** Transforming "pi |= a '<= b" into "[[a <= b]] pi s" *)
Theorem le_vect_true_iff_path_up:
  forall pi : Path, forall s : State, forall size: nat, forall bv1 bv2 : BitVect size,
    [[leq_vect size bv1 bv2]] pi s <-> (path_up pi s ⊨ (bv1 '<= bv2)).
Proof.
intros pi s size bv1 bv2.
split; intro H.
* apply (corrP _ (leq_vect_CVP size bv1 bv2)).
  apply (TrueFor_compat H).
  unfold path_sub.
  unfold path_up.
  rewrite Nat.add_0_l.
  reflexivity.
* apply (corrP _ (leq_vect_CVP size bv1 bv2)).
  simpl in H.
  assumption.
Qed.

(** Transforming "pi |= negP (a '<= b)" into "[[~ a <= b]] pi s" *)
Theorem le_vect_false_iff_neg_path_up:
  forall pi : Path, forall s : State, forall size: nat, forall bv1 bv2 : BitVect size,
    [[~leq_vect size bv1 bv2]] pi s <-> (path_up pi s) ⊨ negP (bv1 '<= bv2).
Proof.
intros pi s size bv1 bv2.
split; intro H.
* simpl.
  apply (anti_corrP _ (leq_vect_CVP size bv1 bv2)).
  apply TrueFor_not_compat.
  apply TrueFor_mono with (pi := pi) (s := s) (pi' := path_sub (path_up pi s)) (s' := 0).
  + unfold path_sub; unfold path_up; rewrite Nat.add_0_l; reflexivity.
  + assumption.
* simpl in H.
  apply (anti_corrP _ (leq_vect_CVP size bv1 bv2)) in H.
  apply TrueFor_not_compat'.
  + apply leq_vect_dec.
  + intro H2.
    apply TrueFor_mono with (pi' := path_sub (path_up pi s)) (s' := 0) in H2.
    - contradiction.
    - unfold path_sub; unfold path_up; rewrite Nat.add_0_l; reflexivity.
Qed.

