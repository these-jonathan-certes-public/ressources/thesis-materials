(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import PeanoNat.
Require Import Nat.
Require Import LTL.
Require Import psl.
Require Import bit.
Require Import bitvect.
Require Import nat2bitvect.
Require Import truefor.
Require Import bitvectltl.

Import bit.Bit.
Import bitvect.BitVect.

(**
 * Selection of a range in a BitVector with implicit size.
 *
 * \warning
 *  bitvect.BitVect.range is excluding argument "to": [from,to[, so here we
 *  select from "from" to "(S to)".
 *)
Definition range_BitVector
  {size : nat} (* implicit arg since coq can deduce it from the size of bv *)
  (v : BitVect size)
  (from to : nat)
:= bitvect.BitVect.range size v from (S to).

(******************************************************************************)
(******************************************************************************)

Section proof.

(**
 * Declaration of predicates from the inputs/outputs of the decompresser.
 * Note: we renamed "in" as "input" to avoid using keywords from coq.
 *)

(**
 * clk, reset, ready, and enable are single bits; we wrap them in a CV0 to be
 * able to use them in LTL formulas.
 * "('clk)" is then a variable that is true iff the corresponding bit of clk is
 * equal to O (one).
 *)
Variable clk reset ready enable : CV0 Bit (eq O).

Variable data_size : (BitVect 4).
Variable input     : (BitVect 8).
Variable data      : (BitVect 120).


(**
 * Seven local properties that are verified through model-checking.
 *)

Hypothesis model_checked_property_0:
  ∀ π  : Path,
  ∀ i₀ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(1, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 0 7) '== uwconst(i₀, 8)))
      )
    ).

Hypothesis model_checked_property_1:
  ∀ π  : Path,
  ∀ i₁ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(2, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 8 15) '== uwconst(i₁, 8)))
      )
    ).

Hypothesis model_checked_property_2:
  ∀ π  : Path,
  ∀ i₂ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(3, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8)))
      )
    ).

Hypothesis model_checked_property_3:
  ∀ π  : Path,
  ∀ i₃ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(4, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8)))
      )
    ).

Hypothesis model_checked_property_4:
  ∀ π  : Path,
  ∀ i₄ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(5, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8)))
      )
    ).

Hypothesis model_checked_property_5:
  ∀ π  : Path,
  ∀ i₅ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(6, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i₅, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i₅, 8)))
      )
    ).

Hypothesis model_checked_property_6:
  ∀ π  : Path,
  ∀ i₆ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 7 7 (input '== uwconst(i₆, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 48 55) '== uwconst(i₆, 8)))
      )
    ).

(******************************************************************************)
(******************************************************************************)

(**
 * First step: proof that we have the same seven local properties if "data_size"
 * is greater or equal to 7.
 *)


(**
 * \brief
 *  Theorem compatible with all possible local properties so that we only have
 *  to apply it.
 *)
Theorem data_size_update:
  ∀ size i j : nat,
       (i < j)
    /\ (j < 2^size)
  ->
    ∀ π : Path,
    ∀ ф₀ ф₁ ф₂ ф₃ : PathF,
    ∀ bv : BitVect size,
      π ⊨ G( ф₀ ∧ X(ф₁ ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(i, size) '<= bv)) ∧ ф₂) --> ф₃ )
    ->
      π ⊨ G( ф₀ ∧ X(ф₁ ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(j, size) '<= bv)) ∧ ф₂) --> ф₃ ).
Proof.
  intros size i j.
  intros i_lt_j.
  intros π.
  intros ф₀ ф₁ ф₂ ф₃.
  intros bv.
  intros H.

  simpl in H.
  left. (* right side has "state_top" in it, so it is always False *)
  intros s0.

  destruct H as [H | H_false]. (* G(p) <-> (∀i : State, path_up π i ⊨ p) \/ False *)

  (* case of H := G(p) -> (∀i : State, path_up π i ⊨ p) *)
  - pose (H_state := H s0).  (* for each State s0, H_state is True *)

    (* in H, "h --> p" <-> "¬h ∨ p" *)
    destruct H_state as [H_notH | H_P].

    (* case where "¬h" in H *)
    + left.
      destruct H_notH as [H_not_phi_0 | H_not_next].  (* ¬ф₀ \/ ¬X(...) *)

      (* case of ¬ф₀ *)
      * left; exact H_not_phi_0.

      (* case of ¬X(...) *)
      * destruct H_not_next as [H_not_phi_1_data_size | H_not_phi_2].
        -- destruct H_not_phi_1_data_size as [H_not_phi_1 | H_not_data_size].

          (* here we have ¬ф₁ *)
          ++ right; left; left; exact H_not_phi_1.

          (* here we have ¬(next_event (('clk) ∧ ('ready)) 1 ( uwconst(i, size) '<= bv)) *)
          ++ right; left; right.
            destruct H_not_data_size as (H_not_data_size & never_reset).
            split.
            ** destruct H_not_data_size as [H1 | H2].
              --- left.
                intros s.
                assert (A0:
                  (path_up (path_up (path_up π s0) 1) s ⊨ negP (' clk) \/
                   path_up (path_up (path_up π s0) 1) s ⊨ negP (' ready)) \/
                   path_up (path_up (path_up π s0) 1) s ⊨ negP ((uwconst( i, size)) '<= bv)
                ).
                  +++ apply H1.
                  +++ destruct A0 as [A0 | A1].
                    *** left; exact A0.
                    *** right.
                      rewrite <- le_vect_false_iff_neg_path_up.
                      rewrite not_leq_lt_TF.

                      rewrite path_plus_i_j in A1.
                      rewrite <- le_vect_false_iff_neg_path_up in A1.
                      rewrite not_leq_lt_TF in A1.

                      assert (A0: lt_vect size (nat2vect size i) (nat2vect size j)).
                      ---- destruct i_lt_j as (ij1 & ij2).
                         apply nat2vect_mono_strict.
                        ++++ apply Nat.lt_trans with (n := i) (m := j) (p := 2^size).
                          **** exact ij1.
                          **** exact ij2.
                        ++++ exact ij2.
                        ++++ exact ij1.
                      ---- apply TrueFor_universal with (pi := path_up π s0) (s := 1 + s) in A0.
                        apply lt_vect_tran_TF with (v2 := (uwconst( i, size))).
                        ++++ compat A1.
                        ++++ compat A0.

              --- right.
                destruct H2 as (s & H2).
                exists s.
                destruct H2 as (H1 & H2).
                split.
                +++ exact H1.
                +++ intros s1.
                  assert (A0:
                    s1 <= s
                      ->
                    (path_up (path_up (path_up π s0) 1) s1 ⊨ negP (' clk) \/
                     path_up (path_up (path_up π s0) 1) s1 ⊨ negP (' ready)) \/
                     path_up (path_up (path_up π s0) 1) s1 ⊨ negP ((uwconst( i, size)) '<= bv)
                  ).
                  *** apply H2.
                  *** intros H3.
                    apply A0 in H3.
                    destruct H3 as [H3 | A1].
                    ---- left; exact H3.
                    ---- right.
                      rewrite <- le_vect_false_iff_neg_path_up.
                      rewrite not_leq_lt_TF.

                      rewrite path_plus_i_j in A1.
                      rewrite <- le_vect_false_iff_neg_path_up in A1.
                      rewrite not_leq_lt_TF in A1.

                      assert (A2: lt_vect size (nat2vect size i) (nat2vect size j)).
                      ++++ destruct i_lt_j as (ij1 & ij2).
                         apply nat2vect_mono_strict.
                        **** apply Nat.lt_trans with (n := i) (m := j) (p := 2^size).
                          ----- exact ij1.
                          ----- exact ij2.
                        **** exact ij2.
                        **** exact ij1.
                      ++++ apply TrueFor_universal with (pi := path_up π s0) (s := 1 + s1) in A2.
                        apply lt_vect_tran_TF with (v2 := (uwconst( i, size))).
                        **** truefor.compat A1.
                        **** truefor.compat A2.

            ** destruct never_reset as (s & never_reset).
              exists s.
              exact never_reset.

        (* here we have ¬ф₁ *)
        -- right; right; exact H_not_phi_2.

    (* case where "p" in H *)
    + right; exact H_P.

  (* case of H := G(p) -> False *)
  - destruct H_false as (s1      & H_false).
    destruct H_false as (H_false & H_else).
    apply state_top in H_false.
    case H_false.
Qed.


Lemma data_size_property_0:
  ∀ π  : Path,
  ∀ i₀ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 0 7) '== uwconst(i₀, 8)))
      )
    ).
Proof.
  intros π.
  intros i₀.

  (* first we apply our theorem to our hypothesis: *)
  apply data_size_update with
    (π    := π)
    (ф₀   := (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)))
    (ф₁   := (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready)))
    (ф₂   := (next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))))
    (ф₃   := X((next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 0 7) '== uwconst(i₀, 8)))))
    (size := 4)
    (i    := 1)
    (j    := 7)
    (bv   := data_size)
  in model_checked_property_0.

  (* then we must be sure that this hypothesis is sufficient: *)
  - exact model_checked_property_0.
  (* and we must be sure that the values of our integers are correct: *)
  - simpl. intuition.
Qed.

Lemma data_size_property_1:
  ∀ π : Path,
  ∀ i : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 8 15) '== uwconst(i, 8)))
      )
    ).
Proof.
  intros π.
  intros i.

  apply data_size_update with
    (π    := π)
    (ф₀   := (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)))
    (ф₁   := (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready)))
    (ф₂   := (next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i, 8))))
    (ф₃   := X((next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 8 15) '== uwconst(i, 8)))))
    (size := 4)
    (i    := 2)
    (j    := 7)
    (bv   := data_size)
  in model_checked_property_1.

  - exact model_checked_property_1.
  - simpl. intuition.
Qed.

Lemma data_size_property_2:
  ∀ π : Path,
  ∀ i : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i, 8)))
      )
    ).
Proof.
  intros π.
  intros i.

  apply data_size_update with
    (π    := π)
    (ф₀   := (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)))
    (ф₁   := (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready)))
    (ф₂   := (next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i, 8))))
    (ф₃   := X((next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i, 8)))))
    (size := 4)
    (i    := 3)
    (j    := 7)
    (bv   := data_size)
  in model_checked_property_2.

  - exact model_checked_property_2.
  - simpl. intuition.
Qed.

Lemma data_size_property_3:
  ∀ π : Path,
  ∀ i : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i, 8)))
      )
    ).
Proof.
  intros π.
  intros i.

  apply data_size_update with
    (π    := π)
    (ф₀   := (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)))
    (ф₁   := (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready)))
    (ф₂   := (next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i, 8))))
    (ф₃   := X((next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i, 8)))))
    (size := 4)
    (i    := 4)
    (j    := 7)
    (bv   := data_size)
  in model_checked_property_3.

  - exact model_checked_property_3.
  - simpl. intuition.
Qed.

Lemma data_size_property_4:
  ∀ π : Path,
  ∀ i : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i, 8)))
      )
    ).
Proof.
  intros π.
  intros i.

  apply data_size_update with
    (π    := π)
    (ф₀   := (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)))
    (ф₁   := (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready)))
    (ф₂   := (next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i, 8))))
    (ф₃   := X((next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i, 8)))))
    (size := 4)
    (i    := 5)
    (j    := 7)
    (bv   := data_size)
  in model_checked_property_4.

  - exact model_checked_property_4.
  - simpl. intuition.
Qed.

Lemma data_size_property_5:
  ∀ π : Path,
  ∀ i : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i, 8)))
      )
    ).
Proof.
  intros π.
  intros i.

  apply data_size_update with
    (π    := π)
    (ф₀   := (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)))
    (ф₁   := (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready)))
    (ф₂   := (next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i, 8))))
    (ф₃   := X((next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i, 8)))))
    (size := 4)
    (i    := 6)
    (j    := 7)
    (bv   := data_size)
  in model_checked_property_5.

  - exact model_checked_property_5.
  - simpl. intuition.
Qed.

Lemma data_size_property_6:
  ∀ π : Path,
  ∀ i : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ (next_event_a (('clk) ∧ ('enable)) 7 7 (input '== uwconst(i, 8)))
      )
    -->
      X(
        (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 48 55) '== uwconst(i, 8)))
      )
    ).
Proof.
  intros π.
  intros i.

  apply model_checked_property_6.
Qed.

(******************************************************************************)

(**
 * Second step: proof that the specification is implied by merging the
 * "next_event" operators one after the other.
 *)

(**
 * \brief
 *  Theorem compatible with all possible merge so that we only have to apply it.
 *)
Theorem globally_impP_x2:
  forall π : Path,
    forall A₀ A₁ A₂ P₁ P₂ : PathF,
      (π ⊨ G (A₀ ∧ X(A₁) --> X(P₁))) /\ (π ⊨ G (A₀ ∧ X(A₂) --> X(P₂)))
    ->
      (π ⊨ G (A₀ ∧ X(A₁ ∧ A₂) --> X(P₁ ∧ P₂))).
Proof.
  intros π.
  intros A₀ A₁ A₂ P₁ P₂.
  intros H1.

  destruct H1 as (H1 & H2). (* hypothesis is a conjunction *)
  simpl in H1, H2.
  simpl.
  left. (* right side has "state_top" in it, so it is always False *)
  intros i.

  destruct H1 as [H1 | H1_false].   (* G(p) <-> (∀i : State, path_up π i ⊨ p)
                                                  \/ False *)

  (* case of H1 := G(p) -> (∀i : State, path_up π i ⊨ p) *)
  - destruct H2 as [H2 | H2_false]. (* G(p) <-> (∀i : State, path_up π i ⊨ p)
                                                  \/ False *)
    (* case of H2 := G(p) -> (∀i : State, path_up π i ⊨ p) *)
    * pose (H1_state := H1 i).  (* for each State i, H1_state is True *)
      pose (H2_state := H2 i).  (* for each State i, H2_state is True *)

      (* in H1, "h --> p" <-> "¬h ∨ p" *)
      destruct H1_state as [H1_notH | H1_P].

      (* case where "¬h" in H1 *)
      + destruct H1_notH as [H1a | H1b].
        -- left; left; exact H1a.
        -- left; right; left; exact H1b.

      (* case where "p" in H1 *)
      + (* in H2, "h --> p" <-> "¬h ∨ p" *)
        destruct H2_state as [H2_notH | H2_P].

        (* case where "¬h" in H2 *)
        --  destruct H2_notH as [H2_notA₀ | H2_notA₂].
            ** left; left; exact H2_notA₀.
            ** left; right; right; exact H2_notA₂.

        (* case where "p" in H2 *)
        --  right.
            split.  (* P₁ ∧ P₂ *)
            ** exact H1_P.
            ** exact H2_P.

    (* case of H2 := G(p) -> False *)
    * (* I know there exists a State where H2_false is True, so I pose
         "j: State" and I remove ∃i from H2b. *)
      destruct H2_false as (j        & H2_false).
      destruct H2_false as (H2_false & H2_else).
      apply state_top in H2_false.
      case H2_false. (* at this State, my hypothesis is False *)

  (* case of H1 := G(p) -> False *)
  - destruct H1_false as (j        & H1_false).
    destruct H1_false as (H1_false & H1_else).
    apply state_top in H1_false.
    case H1_false.
Qed.

(**
 * \brief
 *  Associtivity of the conjunction in a global implication.
 *)
Theorem globally_and_assoc:
  forall π : Path,
    forall A₀ A₁ A₂ P₁ : PathF,
      (π ⊨ G (A₀ ∧ (A₁ ∧ A₂) --> P₁))
    <->
      (π ⊨ G (A₀ ∧ A₁ ∧ A₂ --> P₁)).
Proof.
  intros π.
  intros A₀ A₁ A₂ P₁.
  split.

  (* -> *)
  - intros H.
    simpl.
    left.
    intros i.
    simpl in H.
    destruct H as [H1 | H2].
    + assert (A0:
        (path_up π i ⊨ negP A₀  \/
         path_up π i ⊨ negP A₁  \/
         path_up π i ⊨ negP A₂) \/
         path_up π i ⊨ P₁
      ).
      * apply H1.
      * destruct A0 as [A0 | A1].
        destruct A0 as [A0 | A1].
        -- left; left; left; exact A0.
        -- destruct A1 as [A0 | A1].
          ++ left; left; right; exact A0.
          ++ left; right; exact A1.
        -- right; exact A1.
    + destruct H2 as (s & H2).
      destruct H2 as (H_false & H2).
      apply state_top in H_false.
      case H_false.

  (* <- *)
  - intros H.
    simpl.
    left.
    intros i.
    simpl in H.
    destruct H as [H1 | H2].
    + assert (A0:
        ((path_up π i ⊨ negP A₀  \/
          path_up π i ⊨ negP A₁) \/
          path_up π i ⊨ negP A₂) \/
          path_up π i ⊨ P₁
      ).
      * apply H1.
      * destruct A0 as [A0 | A1].
        destruct A0 as [A0 | A1].
        destruct A0 as [A0 | A1].
        -- left; left; exact A0.
        -- left; right; left; exact A1.
        -- left; right; right; exact A1.
        -- right; exact A1.
    + destruct H2 as (s & H2).
      destruct H2 as (H_false & H2).
      apply state_top in H_false.
      case H_false.
Qed.

Lemma merge_property_1:
  ∀ π  : Path,
  ∀ i₀ i₁ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
      ) ∧ X(
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
      )
    -->
      X(
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 8 15) '== uwconst(i₁, 8))
      )
    ).
Proof.
  intros π.
  intros i₀ i₁.

  apply globally_impP_x2 with
    (π  := π)
    (A₀ :=
          ((('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
              (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
            ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
          )
    )
    (A₁ :=
          (next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8)))
    )
    (A₂ :=
          (next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8)))
    )
    (P₁ :=
          (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 0  7) '== uwconst(i₀, 8)))
    )
    (P₂ :=
          (next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 8 15) '== uwconst(i₁, 8)))
    ).

  split.
  - rewrite <- globally_and_assoc.
    apply data_size_property_0.
  - rewrite <- globally_and_assoc.
    apply data_size_property_1.
Qed.

Lemma merge_property_2:
  ∀ π  : Path,
  ∀ i₀ i₁ i₂ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
      ) ∧ X(
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
      )
    -->
      X(
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
      )
    ).
Proof.
  intros π.
  intros i₀ i₁ i₂.

  apply globally_impP_x2 with
    (π  := π)
    (A₀ :=
          ((('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
              (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
            ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
          )
    )
    (A₁ :=
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
    )
    (A₂ :=
          next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
    )
    (P₁ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
    )
    (P₂ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
    ).

  split.
  - apply merge_property_1.
  - rewrite <- globally_and_assoc.
    apply data_size_property_2.
Qed.

Lemma merge_property_3:
  ∀ π  : Path,
  ∀ i₀ i₁ i₂ i₃ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
      ) ∧ X(
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8))
      )
    -->
      X(
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8))
      )
    ).
Proof.
  intros π.
  intros i₀ i₁ i₂ i₃.

  apply globally_impP_x2 with
    (π  := π)
    (A₀ :=
          ((('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
              (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
            ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
          )
    )
    (A₁ :=
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
    )
    (A₂ :=
          next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8))
    )
    (P₁ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
    )
    (P₂ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8))
    ).

  split.
  - apply merge_property_2.
  - rewrite <- globally_and_assoc.
    apply data_size_property_3.
Qed.

Lemma merge_property_4:
  ∀ π  : Path,
  ∀ i₀ i₁ i₂ i₃ i₄ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
      ) ∧ X(
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8))
      )
    -->
      X(
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8))
      )
    ).
Proof.
  intros π.
  intros i₀ i₁ i₂ i₃ i₄.

  apply globally_impP_x2 with
    (π  := π)
    (A₀ :=
          ((('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
              (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
            ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
          )
    )
    (A₁ :=
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8))
    )
    (A₂ :=
          next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8))
    )
    (P₁ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8))
    )
    (P₂ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8))
    ).

  split.
  - apply merge_property_3.
  - rewrite <- globally_and_assoc.
    apply data_size_property_4.
Qed.

Lemma merge_property_5:
  ∀ π  : Path,
  ∀ i₀ i₁ i₂ i₃ i₄ i₅ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
      ) ∧ X(
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i₅, 8))
      )
    -->
      X(
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i₅, 8))
      )
    ).
Proof.
  intros π.
  intros i₀ i₁ i₂ i₃ i₄ i₅.

  apply globally_impP_x2 with
    (π  := π)
    (A₀ :=
          ((('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
              (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
            ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
          )
    )
    (A₁ :=
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8))
    )
    (A₂ :=
          next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i₅, 8))
    )
    (P₁ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8))
    )
    (P₂ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i₅, 8))
    ).

  split.
  - apply merge_property_4.
  - rewrite <- globally_and_assoc.
    apply data_size_property_5.
Qed.

Lemma merge_property_6:
  ∀ π  : Path,
  ∀ i₀ i₁ i₂ i₃ i₄ i₅ i₆ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
      ) ∧ X(
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i₅, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 7 7 (input '== uwconst(i₆, 8))
      )
    -->
      X(
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i₅, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 48 55) '== uwconst(i₆, 8))
      )
    ).
Proof.
  intros π.
  intros i₀ i₁ i₂ i₃ i₄ i₅ i₆.

  apply globally_impP_x2 with
    (π  := π)
    (A₀ :=
          ((('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
              (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
            ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
          )
    )
    (A₁ :=
          next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i₅, 8))
    )
    (A₂ :=
          next_event_a (('clk) ∧ ('enable)) 7 7 (input '== uwconst(i₆, 8))
    )
    (P₁ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i₅, 8))
    )
    (P₂ :=
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 48 55) '== uwconst(i₆, 8))
    ).

  split.
  - apply merge_property_5.
  - rewrite <- globally_and_assoc.
    apply data_size_property_6.
Qed.

(******************************************************************************)

(**
 * Optionnal! Proof that the specification is implied is complete. Here we show
 * that we can format it to match our manual expression.
 *)

(**
 * Associativity of the conjunction in a globally next on the left side of the
 * implication.
 *)
Lemma and_assoc_globally_next_H:
  forall π : Path,
    forall A₀ A₁ A₂ A₃ P₁ : PathF,
      (π ⊨ G (A₀ ∧ X((A₁ ∧ A₂) ∧ A₃) --> P₁))
    <->
      (π ⊨ G (A₀ ∧ X(A₁ ∧ (A₂ ∧ A₃)) --> P₁)).
Proof.
  intros π.
  intros A₀ A₁ A₂ A₃ P₁.

  split.
  - intros H.
    simpl in H.
    simpl.
    left.
    intros i.
    destruct H as [H1 | H2].
    + assert (A0:
         (path_up π i ⊨ negP A₀ \/
         (path_up (path_up π i) 1 ⊨ negP A₁  \/
          path_up (path_up π i) 1 ⊨ negP A₂) \/
          path_up (path_up π i) 1 ⊨ negP A₃) \/
          path_up π i ⊨ P₁
      ).
      * apply H1.
      * destruct A0 as [A0 | A1].
        destruct A0 as [A0 | A1].
        -- left; left; exact A0.
        -- destruct A1 as [A0 | A1].
           destruct A0 as [A0 | A1].
          ++ left; right; left; exact A0.
          ++ left; right; right; left; exact A1.
          ++ left; right; right; right; exact A1.
        -- right; exact A1.
    + destruct H2 as (s & H2).
      destruct H2 as (H_false & H2).
      apply state_top in H_false.
      case H_false.

  - intros H.
    simpl in H.
    simpl.
    left.
    intros i.
    destruct H as [H1 | H2].
    + assert (A0:
        (path_up π i ⊨ negP A₀ \/
         path_up (path_up π i) 1 ⊨ negP A₁ \/
         path_up (path_up π i) 1 ⊨ negP A₂ \/
         path_up (path_up π i) 1 ⊨ negP A₃) \/
         path_up π i ⊨ P₁
      ).
      * apply H1.
      * destruct A0 as [A0 | A1].
        destruct A0 as [A0 | A1].
        -- left; left; exact A0.
        -- destruct A1 as [A0 | A1].
          ++ left; right; left; left; exact A0.
          ++ destruct A1 as [A0 | A1].
            ** left; right; left; right; exact A0.
            ** left; right; right; exact A1.
        -- right; exact A1.
    + destruct H2 as (s & H2).
      destruct H2 as (H_false & H2).
      apply state_top in H_false.
      case H_false.
Qed.

(**
 * Merge an hypothesis in a globally next on the left side of the implication.
 *)
Lemma merge_next_H:
  forall π : Path,
    forall A₀ A₁ A₂ P₁ : PathF,
      (π ⊨ G (A₀ ∧ X(A₁ ∧ A₂) --> P₁))
    <->
      (π ⊨ G (A₀ ∧ X(A₁) ∧ X(A₂) --> P₁)).
Proof.
  intros π.
  intros A₀ A₁ A₂ P₁.

  split.
  - intros H.
    rewrite <- globally_and_assoc.
    simpl in H.
    simpl.
    exact H.
  - intros H.
    rewrite <- globally_and_assoc in H.
    simpl in H.
    simpl.
    exact H.
Qed.

Theorem specification:
  ∀ π  : Path,
  ∀ i₀ i₁ i₂ i₃ i₄ i₅ i₆ : nat,
    π ⊨ G(
      ( (('clk) ∧ ('reset)) ∨ (('clk) ∧ ('ready)) ) ∧ X(
          (negP (('clk) ∧ ('reset))) U (('clk) ∧ ('ready))
        ∧ (next_event (('clk) ∧ ('ready)) 1 (uwconst(7, 4) '<= data_size))
        ∧ next_event_a (('clk) ∧ ('enable)) 1 1 (input '== uwconst(i₀, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 2 2 (input '== uwconst(i₁, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 3 3 (input '== uwconst(i₂, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 4 4 (input '== uwconst(i₃, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 5 5 (input '== uwconst(i₄, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 6 6 (input '== uwconst(i₅, 8))
        ∧ next_event_a (('clk) ∧ ('enable)) 7 7 (input '== uwconst(i₆, 8))
      )
    -->
      X(
          next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  0  7) '== uwconst(i₀, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data  8 15) '== uwconst(i₁, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 16 23) '== uwconst(i₂, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 24 31) '== uwconst(i₃, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 32 39) '== uwconst(i₄, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 40 47) '== uwconst(i₅, 8))
        ∧ next_event (('clk) ∧ ('ready)) 1 ((range_BitVector data 48 55) '== uwconst(i₆, 8))
      )
    ).
Proof.
  intros π.
  intros i₀ i₁ i₂ i₃ i₄ i₅ i₆.
  do 6 (rewrite and_assoc_globally_next_H).
  rewrite merge_next_H.
  do 5 (rewrite <- and_assoc_globally_next_H).
  apply merge_property_6.
Qed.

End proof.
