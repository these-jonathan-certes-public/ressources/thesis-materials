(*
 * Copyright (C) 2022 Guillaume Dupont
 * guillaume.dupont@toulouse-inp.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import LTL.
Require Import Coq.Strings.String.
Require Import Coq.Bool.Bool.
Require Import Psatz.

(**
 * We define (kinda axiomatically) the notion of "being true for the given path at the given state"
 *)
Definition TrueFor (P : Prop) : Path -> State -> Prop.
Proof.
trivial.
Qed.

(** A handy notation. [[ P ]] pi s means "P is true for path pi at state s" *)
Notation "[[ P ]]" := (TrueFor P) (at level 60).

(*
 * Because TrueFor is *opaque* (it doesn't have a definition) we need to give properties using axioms.
 * We only define 5 axioms, everything else is proven (no guarantee that this is minimal, no guarantee
 * that this is consistent either, but relatively confident)
 *)
(** [[True]] is always True, no matter the path and the state *)
Axiom TrueFor_True : forall pi : Path, forall s : State, [[True]] pi s.
(** [[False]] is never True, no matter the path and the state *)
Axiom TrueFor_False : forall pi : Path, forall s : State, ~([[False]] pi s).
(** [[.]] is *covariant*, meaning in this context that it preserves implication *)
Axiom TrueFor_covariant : forall P Q : Prop, forall pi : Path, forall s : State,
  (P -> Q) -> [[P]] pi s -> [[Q]] pi s.
(** [[.]] has a form of monotony for the path and state (this is due to how the LTL lib works) *)
Axiom TrueFor_mono : forall pi : Path, forall s : State, forall pi' : Path, forall s' : State, forall P : Prop,
  pi s = pi' s' -> [[P]] pi s -> [[P]] pi' s'.

(**
 * This is a variation of the covariance axiom that allows to easily "lift" a theorm, i.e. generate a theorem
 * in the realm of TrueFor from a theorem in the realm of Prop
 *)
Theorem TrueFor_lift_thm {P Q : Prop} (H : P -> Q) : forall pi : Path, forall s : State,
  [[P]] pi s -> [[Q]] pi s.
Proof.
intros pi s H2.
apply TrueFor_covariant with (P := P); assumption.
Qed.

(**
 * If P is always true then [[P]] is always true.
 *)
Theorem TrueFor_universal : forall P : Prop, forall pi : Path, forall s : State,
  P -> [[P]] pi s.
Proof.
intros P pi s H.
apply (TrueFor_covariant True P).
intro; assumption.
apply TrueFor_True.
Qed.

(**
 * [[.]] is "applicative", meaning it allows the application of a theorem (P -> Q) for the given path and state
 * if the premise is true (P) for that path and state, yielding a proposition (Q) that is true for that path
 * and state.
 *)
Axiom TrueFor_applicative : forall P Q : Prop, forall pi : Path, forall s : State,
  [[P -> Q]] pi s -> [[P]] pi s -> [[Q]] pi s.

(**
 * [[.]] is compatible with /\, i.e. you can "cut" [[A /\ B]].
 *)
Theorem TrueFor_and_compat : forall P Q : Prop, forall pi : Path, forall s : State,
  [[P /\ Q]] pi s <-> ([[P]] pi s /\ [[Q]] pi s).
Proof.
intros P Q pi s; split; intro H.
split.
apply (TrueFor_covariant (P /\ Q) P).
tauto.
assumption.
apply (TrueFor_covariant (P /\ Q) Q).
tauto.
assumption.

destruct H as (H1,H2).
assert (P -> Q -> P /\ Q) as H3 by tauto.
apply TrueFor_universal with (pi := pi) (s := s) in H3.
apply (TrueFor_applicative (P) (Q -> P /\ Q) pi s) in H3.
apply (TrueFor_applicative (Q) (P /\ Q) pi s).
all: assumption.
Qed.

(**
 * [[.]] allows to extract a negation. This always work.
 *)
Theorem TrueFor_not_compat : forall P : Prop, forall pi : Path, forall s : State,
  [[~P]] pi s -> ~[[P]] pi s.
Proof.
intros P pi s; unfold not.
intros H1 H2.
apply (TrueFor_False pi s).
apply TrueFor_applicative with (P := P); assumption.
Qed.

(**
 * [[.]] allows to embed a negation (move it inward). This works when the predicate is decidable,
 * I do not know if does work when it isn't (but [[.]] is generally used with decidable predicates because
 * the LTL library works with booleans).
 *)
Theorem TrueFor_not_compat' : forall P : Prop, forall pi : Path, forall s : State,
  { P } + { ~P } -> ~[[P]] pi s -> [[~P]] pi s.
Proof.
unfold not; intros P pi s Heq H.
destruct Heq as [H0|H0].
apply TrueFor_universal with (pi := pi) (s := s) in H0.
apply H in H0.
contradiction.
apply TrueFor_universal with (pi := pi) (s := s) in H0.
assumption.
Qed.

(**
 * Similar to "TrueFor_lift_thm" for theorems with *two* premises.
 *)
Theorem TrueFor_lift_thm2 {P Q R : Prop} (H : P -> Q -> R) : forall pi : Path, forall s : State,
  [[P]] pi s -> [[Q]] pi s -> [[R]] pi s.
Proof.
intros pi s HP HQ.
assert (P /\ Q -> R) by tauto.
apply TrueFor_universal with (pi := pi) (s := s) in H0.
apply TrueFor_applicative with (P := P /\ Q).
assumption.
apply TrueFor_and_compat; split; assumption.
Qed.

(**
 * A rewriting of TrueFor_mono with implicit arguments; this makes it much more easy to use in the proving
 * context, as Coq can (usually) guess the parameters of the theorem.
 *)
Theorem TrueFor_compat {P : Prop} {pi pi' : Path} {s s' : State} : [[P]] pi s -> pi s = pi' s' -> [[P]] pi' s'.
Proof.
intros H1 H2.
apply (TrueFor_mono pi s pi' s' P H2 H1).
Qed.

(**
 * A tactic similar to "exact H" except it allows to match two [[.]] instance (using "TrueFor_mono").
 * When you have "H : [[P]] pi' s' |- [[P]] pi s", it generates the goal "pi' s' = pi s" and tries to
 * solve it by unfolding/reflexivity/lia (this works like 99% of the time).
 *)
Ltac compat H :=
  apply (TrueFor_compat H); unfold path_up; unfold path_sub; try (reflexivity || (f_equal; lia)).

(* The idea now is to juggle with three notions:
 *  - a proposition (P)
 *  - a path-state predicate ([[P]])
 *  - a PathF variable
 * The CVx structures glue together these three notions, allowing a predicate to be used in an LTL formula,
 * and then proof to be performed on said formula, using the theorems defined on that predicate.
 *)

(**
 * CVP (Composite Variable Predicate) is a wrapper for a normal predicate. It is typically used to represent
 * a predicate as a variable PathF.
 *)
Record CVP (P : Prop) : Type := {
  (* Name of the variable in a PathF (no need to defined it in actuality) *)
  strP : string ;
  (* A correspondance axiom: if variable "strP" is true for the given path and state then the corresponding
   * predicate "P" is TrueFor that path and state.
   *)
  corrP : forall pi : Path, forall s : State, pi s strP = true <-> [[P]] pi s
}.

(**
 * Contraposition of corrP (useful in a few places).
 *)
Theorem anti_corrP : forall P : Prop, forall c : CVP P,
  forall pi : Path, forall s : State, pi s (strP P c) = false <-> ~[[P]] pi s.
Proof.
intros P c pi s; split.
intros H1 H2.
rewrite <- (corrP P c pi s) in H2.
apply eq_true_false_abs with (b := pi s (strP P c)); assumption.
intros H1.
apply not_true_is_false.
intro H2.
apply (corrP P c pi s) in H2.
contradiction.
Qed.

(**
 * CV0 is a slight variation of CVP that allows to wrap a *variable*, together with a specific predicate
 * on that variable.
 * For example, this could allow to wrap a natural together with the predicate "is_odd", thus representing
 * "is_odd n" as an LTL PathF.
 * In that case, it is used to wrap *bits*, and the predicate is "equal to 1".
 *)
Record CV0 (T : Type) (P : T -> Prop) : Type := {
  str0 : string ;
  corr0 : forall t : T, forall pi : Path, forall s : State, pi s str0 = true <-> [[P t]] pi s
}.

(** Notation to create the PathF from the CV0 *)
Notation "' b" := (Var (str0 _ _ b)) (at level 100).


