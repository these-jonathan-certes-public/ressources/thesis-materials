(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.transducer.
Require Import Dependencies.lemma_l_enter.
Require Import Dependencies.lemma_l_exit.
Require Import Dependencies.lemma_l_in.
Require Import Dependencies.lemma_l_out.

(******************************************************************************)

Theorem imp_deduced_PC:
  ∀ π : Path, π ⊨ G( ('PC_in_CR) --> ('deduced_PC_in_CR) ).
Proof.
  intros π.
  rewrite simpl_globally.
  intros s.
  simpl.

  induction s.

  (* at state 0, PC is not in CR *)
  - pose proof (reset_to_not_CR π) as H3.
    pose proof (startup_reset   π) as H4.
    rewrite simpl_globally in H3.
    simpl in H3, H4.
    pose proof (H3 0) as H3.
    destruct H3 as [H3 | H3].
    + rewrite Thesis.LTL.path_sub_j_0 in H3.
      rewrite H3 in H4.
      discriminate.
    + left.
      exact H3.

  (* induction on state index *)
  - pose proof (L_enter π) as L_enter.
    pose proof (L_in    π) as L_in.
    rewrite simpl_globally in L_enter, L_in.

    pose proof (L_enter s) as H1.
    pose proof (L_in    s) as H2.
    simpl in H1, H2.

    (* we rewrite all X(a) so that we have a at successor of state s *)
    rewrite Thesis.LTL.path_plus_i_j in H1, H2.
    assert (A0: s + 1 = S s).
    + rewrite Nat.add_comm.
      simpl.
      reflexivity.
    + rewrite A0 in H1, H2.

      (* then we observe all predicates from our hypothesis *)
      destruct H2 as [H2 | H2].
      * destruct H2 as [H2 | H2].
        -- destruct H1 as [H1 | H1].
          ++ destruct H1 as [H1 | H1].

            (* induction on state index helps us here: *)
            ** destruct IHs as [IHs | IHs].
              --- rewrite IHs in H1; discriminate.
              --- rewrite IHs in H2; discriminate.

            ** left; exact H1.
          ++ right; exact H1.
        -- left; exact H2.
      * right; exact H2.
Qed.


Theorem equiv_deduced_PC:
  ∀ π : Path, π ⊨ G( ('deduced_PC_in_CR) --> ('PC_in_CR) ).
Proof.
  intros π.
  rewrite simpl_globally.
  intros s.
  simpl.

  induction s.

  (* at state 0, deduced_PC is not in CR *)
  - pose proof (_P_start_ π) as H3.
    simpl in H3.
    left.
    exact H3.

  (* induction on state index *)
  - pose proof (L_exit π) as L_exit.
    pose proof (L_out  π) as L_out.
    rewrite simpl_globally in L_exit, L_out.

    pose proof (L_exit s) as H1.
    pose proof (L_out  s) as H2.
    simpl in H1, H2.

    (* we rewrite all X(a) so that we have a at successor of state s *)
    rewrite Thesis.LTL.path_plus_i_j in H1, H2.
    assert (A0: s + 1 = S s).
    + rewrite Nat.add_comm.
      simpl.
      reflexivity.
    + rewrite A0 in H1, H2.

      (* then we observe all predicates from our hypothesis *)
      destruct H2 as [H2 | H2].
      * destruct H2 as [H2 | H2].
        -- destruct H1 as [H1 | H1].
          ++ destruct H1 as [H1 | H1].

            (* induction on state index helps us here: *)
            ** destruct IHs as [IHs | IHs].
              --- rewrite IHs in H2; discriminate.
              --- rewrite IHs in H1; discriminate.

            ** right; exact H1.
          ++ left; exact H1.
        -- right; exact H2.
      * left; exact H2.
Qed.

