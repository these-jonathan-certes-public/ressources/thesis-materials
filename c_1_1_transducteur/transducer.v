/*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * \brief
 *  Translates the results of CoreSight trace decoding into the alphabet of
 *  VRASED automata.
 */
module transducer
#(
  parameter CRMIN_ADDR  = 32'h4000_0000, //!< address min of critical region
  parameter CRMAX_ADDR  = 32'h4000_3FFF, //!< address max of critical region
  //
  parameter p_none      = 4'b0000,
  parameter p_isync     = 4'b0001,
  parameter p_atom      = 4'b0010,
  parameter p_branch    = 4'b0011,
  parameter p_waypoint  = 4'b0100,
  parameter p_trigger   = 4'b0101,
  parameter p_contextid = 4'b0110,
  parameter p_vmid      = 4'b0111,
  parameter p_timestamp = 4'b1000,
  parameter p_except    = 4'b1001,
  parameter p_ignore    = 4'b1010
) (
  input nrst, //!< low level active reset
  input clk,  //!< clock

  input [3:0] decompressed_packet, //!< which packet is currently decompressing
  input       packet_ready,        //!< indicates that a packet is decompressed
  //
  input [31:0] decoded_address,    //!< decoded address
  input        decoded_has_except, //!< branch packet has exception information

  output deduced_PC_eq_CRmin, //!< program counter is at CRMIN_ADDR
  output deduced_PC_eq_CRmax, //!< program counter is at CRMAX_ADDR
  output deduced_PC_in_CR,    //!< program counter is in CR
  output deduced_irq_in_CR    //!< an exception occurred while program counter
                              //!  is in CR
);

  reg r_deduced_PC_eq_CRmin;
  reg r_deduced_PC_in_CR;

  initial begin
    r_deduced_PC_eq_CRmin = 1'b0;
    r_deduced_PC_in_CR    = 1'b0;
  end

  always @( posedge(clk) ) begin
    if ( nrst == 1'b0 ) begin
      r_deduced_PC_eq_CRmin <= 1'b0;
      r_deduced_PC_in_CR    <= 1'b0;
    end
    else begin

      if ( packet_ready && (
              (decompressed_packet == p_branch) ||
              (decompressed_packet == p_isync)
            )
      ) begin
        r_deduced_PC_eq_CRmin <= (decoded_address == CRMIN_ADDR);
        r_deduced_PC_in_CR <= (
          (decoded_address >= CRMIN_ADDR && decoded_address < CRMAX_ADDR)
        );
      end
      else begin
        r_deduced_PC_eq_CRmin <= 1'b0;
      end

    end
  end

  /****************************************************************************/

  // we consider that we are at CRmax only when we recieve the packet:
  assign deduced_PC_eq_CRmax = nrst && !r_deduced_PC_eq_CRmin && (
    packet_ready && (
         ((decompressed_packet == p_isync)  && (decoded_address == CRMAX_ADDR))
      || ((decompressed_packet == p_branch) && (decoded_address == CRMAX_ADDR))
    )
  );

  assign deduced_PC_eq_CRmin = r_deduced_PC_eq_CRmin;
  assign deduced_PC_in_CR    = r_deduced_PC_in_CR;

  assign deduced_irq_in_CR = packet_ready                       &&
                              (decompressed_packet == p_branch) &&
                              decoded_has_except;

endmodule

