
/*
 * Copyright (C) 2022 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "msvc-compatible/EverCrypt_HMAC.h"

int main(
  void
) {
  unsigned char *theData = (unsigned char *)("This is my data");
  unsigned char *theKey  = (unsigned char *)("Never gonna give you up");
  unsigned char theDestination[512];

  unsigned long theKeyLen  = strlen((const char *)theKey);
  unsigned long theDataLen = strlen((const char *)theData);

  EverCrypt_HMAC_compute_sha2_256(
    theDestination,
    theKey,
    theKeyLen,
    theData,
    theDataLen
  );

  printf("%s", theDestination);
  while (1) {
    // do nothing
  }

  return 0;
}
