/*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* verilator lint_off UNUSED */
module decide
#(
  parameter END_ADDR = 10'h1ff  //!< address after which we consider that
                                //!  configuration is done
) (
  input clk,                  //!< a clock that is external to the PS7
  input deduced_PC_eq_CRmax,  //!< given by the transducer

  // to monitor:
  input [13:0] sw_araddr,       //!< ARADDR  to AXI slave containing software
  input        sw_arvalid,      //!< ARVALID to AXI slave containing software
  input        trace_ctl,       //!< CoreSight enable bit
  input  [7:0] trace_data,      //!< CoreSight data
  //
  input  [7:0]  decoded_address__0, //!< given by the decoder, Byte 0
  input [15:8]  decoded_address__1, //!< given by the decoder, Byte 1
  input [23:16] decoded_address__2, //!< given by the decoder, Byte 2
  input [31:24] decoded_address__3, //!< given by the decoder, Byte 3

  // to connect to the ROM:
  output [9:0] i,       //!< index of the 64-bit word in the ROM
  input [63:0] rddata,  //!< 64-bit word read from the ROM

  // outputs:
  output i_eq_end_addr, //!< boolean indicates that i = END_ADDR

  output verifying, //!< says that the monitor is currently verifying
  output done,      //!< says that the monitor is done verifying and expecting
                    //!  to reach CR_max (\ref deduced_PC_eq_CRmax)

  output reset_sw_araddr,           //!< a mismatch occurs on sw_araddr
  output reset_sw_arvalid,          //!< a mismatch occurs on sw_arvalid
  output reset_trace_ctl,           //!< a mismatch occurs on trace_ctl
  output reset_trace_data,          //!< a mismatch occurs on trace_data
  output reset_decoded_address__0,  //!< a mismatch occurs on decoded_address__0
  output reset_decoded_address__1,  //!< a mismatch occurs on decoded_address__1
  output reset_decoded_address__2,  //!< a mismatch occurs on decoded_address__2
  output reset_decoded_address__3   //!< a mismatch occurs on decoded_address__3
);
/* verilator lint_on UNUSED */

  // connection to the ROM that contains expected signals:
  reg [9:0] r_addr;
  //
  initial begin
    r_addr = 10'h000;
  end
  //
  always @( posedge(clk) ) begin
    if ( w_verifying ) begin
      r_addr <= r_addr + 10'h001;
    end
    else begin
      r_addr <= 10'h000;
    end
  end
  //
  wire [9:0] w_i;
  assign w_i = (w_verifying) ? (r_addr + 10'h001) : 10'h000;
  assign i_eq_end_addr = (w_i == END_ADDR);
  assign i = w_i;


  // activation of the verification when triggered:
  reg r_verifying;
  //
  initial begin
    r_verifying = 1'b0;
  end
  //
  always @( posedge(clk) ) begin
    if ( (r_addr != END_ADDR) && (sw_arvalid || r_verifying) && !r_done ) begin
      r_verifying <= 1'b1;
    end
    else begin
      r_verifying <= 1'b0;
    end
  end
  //
  wire w_verifying;
  assign w_verifying = (r_addr != END_ADDR) && (sw_arvalid || r_verifying) && !r_done;
  assign   verifying = w_verifying;


  // deactivation of the verification when done:
  reg r_done;
  //
  initial begin
    r_done = 1'b0;
  end
  //
  always @( posedge(clk) ) begin
    if ( (deduced_PC_eq_CRmax != 1'b1) && ((r_addr == END_ADDR) || r_done) ) begin
      r_done <= 1'b1;
    end
    else begin
      r_done <= 1'b0;
    end
  end
  //
  assign done = (r_addr == END_ADDR) || r_done;


  // enforcing security:
  assign reset_sw_araddr          = w_verifying && !(sw_araddr          == rddata[13:0] );
  assign reset_sw_arvalid         = w_verifying && !(sw_arvalid         == rddata[14]   );
  assign reset_trace_ctl          = w_verifying && !(trace_ctl          == rddata[15]   );
  //assign reset_trace_data       = w_verifying && !(trace_data         == rddata[23:16]);
  assign reset_trace_data         = 1'b0;
  assign reset_decoded_address__0 = w_verifying && !(decoded_address__0 == rddata[31:24]);
  assign reset_decoded_address__1 = w_verifying && !(decoded_address__1 == rddata[39:32]);
  assign reset_decoded_address__2 = w_verifying && !(decoded_address__2 == rddata[47:40]);
  assign reset_decoded_address__3 = w_verifying && !(decoded_address__3 == rddata[55:48]);

endmodule

