/*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

module decide_merge_output
(
  input reset_sw_araddr,
  input reset_sw_arvalid,
  input reset_trace_ctl,
  input reset_trace_data,
  input reset_decoded_address__0,
  input reset_decoded_address__1,
  input reset_decoded_address__2,
  input reset_decoded_address__3,
  output reset
);
  assign reset =
    reset_sw_araddr           ||
    reset_sw_arvalid          ||
    reset_trace_ctl           ||
    reset_trace_data          ||
    reset_decoded_address__0  ||
    reset_decoded_address__1  ||
    reset_decoded_address__2  ||
    reset_decoded_address__3;
endmodule

