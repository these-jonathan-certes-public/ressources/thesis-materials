/*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

module decide_merge_input
(
  input   [31:0] decoded_address,
  output  [7:0]  decoded_address__0,
  output [15:8]  decoded_address__1,
  output [23:16] decoded_address__2,
  output [31:24] decoded_address__3
);
  assign decoded_address__0 = decoded_address[7:0];
  assign decoded_address__1 = decoded_address[15:8];
  assign decoded_address__2 = decoded_address[23:16];
  assign decoded_address__3 = decoded_address[31:24];
endmodule

