/*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

`include "decide_merge_input.v"
`include "decide.v"
`include "decide_merge_output.v"

module decide_wrapper
#(
  parameter END_ADDR = 10'h3ff  //!< address after which we consider that
                                //!  configuration is done
) (
  input clk,                  //!< a clock that is external to the PS7
  input deduced_PC_eq_CRmax,  //!< given by the transducer

  // to monitor:
  input [13:0] sw_araddr,       //!< ARADDR  to AXI slave containing software
  input        sw_arvalid,      //!< ARVALID to AXI slave containing software
  input        trace_ctl,       //!< CoreSight enable bit
  input  [7:0] trace_data,      //!< CoreSight data
  input [31:0] decoded_address, //!< given by the decoder

  // to connect to the ROM:
  output [9:0] i,       //!< index of the 64-bit word in the ROM
  input [63:0] rddata,  //!< 64-bit word read from the ROM

  // outputs:
  output i_eq_end_addr, //!< boolean indicates that i = END_ADDR

  output verifying, //!< says that the monitor is currently verifying
  output done,      //!< says that the monitor is done verifying and expecting
                    //!  to reach CR_max (\ref deduced_PC_eq_CRmax)

  output reset      //!< a mismatch occurs on a signal
);

  wire  [7:0]  decoded_address__0;
  wire [15:8]  decoded_address__1;
  wire [23:16] decoded_address__2;
  wire [31:24] decoded_address__3;
  //
  decide_merge_input i_decide_merge_input(
    .decoded_address(decoded_address),
    //
    .decoded_address__0(decoded_address__0),
    .decoded_address__1(decoded_address__1),
    .decoded_address__2(decoded_address__2),
    .decoded_address__3(decoded_address__3)
  );

  wire reset_sw_araddr;
  wire reset_sw_arvalid;
  wire reset_trace_ctl;
  wire reset_trace_data;
  wire reset_decoded_address__0;
  wire reset_decoded_address__1;
  wire reset_decoded_address__2;
  wire reset_decoded_address__3;
  //
  decide #(
    .END_ADDR(END_ADDR)
  ) i_decide (
    .clk                (clk                ),
    .deduced_PC_eq_CRmax(deduced_PC_eq_CRmax),
    .sw_araddr          (sw_araddr          ),
    .sw_arvalid         (sw_arvalid         ),
    .trace_ctl          (trace_ctl          ),
    .trace_data         (trace_data         ),
    .decoded_address__0 (decoded_address__0 ),
    .decoded_address__1 (decoded_address__1 ),
    .decoded_address__2 (decoded_address__2 ),
    .decoded_address__3 (decoded_address__3 ),
    //
    .i     (i     ),
    .rddata(rddata),
    //
    .i_eq_end_addr           (i_eq_end_addr           ),
    .verifying               (verifying               ),
    .done                    (done                    ),
    .reset_sw_araddr         (reset_sw_araddr         ),
    .reset_sw_arvalid        (reset_sw_arvalid        ),
    .reset_trace_ctl         (reset_trace_ctl         ),
    .reset_trace_data        (reset_trace_data        ),
    .reset_decoded_address__0(reset_decoded_address__0),
    .reset_decoded_address__1(reset_decoded_address__1),
    .reset_decoded_address__2(reset_decoded_address__2),
    .reset_decoded_address__3(reset_decoded_address__3)
  );

  decide_merge_output i_decide_merge_output(
    .reset_sw_araddr(         reset_sw_araddr),
    .reset_sw_arvalid(        reset_sw_arvalid),
    .reset_trace_ctl(         reset_trace_ctl),
    .reset_trace_data(        reset_trace_data),
    .reset_decoded_address__0(reset_decoded_address__0),
    .reset_decoded_address__1(reset_decoded_address__1),
    .reset_decoded_address__2(reset_decoded_address__2),
    .reset_decoded_address__3(reset_decoded_address__3),
    //
    .reset(reset)
  );

endmodule

