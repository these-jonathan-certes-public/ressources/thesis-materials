(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.decide_merge_input.
Require Import Dependencies.decide.
Require Import Dependencies.decide_merge_output.

(******************************************************************************)
(******************************************************************************)

(**
 * In our library, we defined how take a range from a bit vector but we did not
 * define how to concatenate vectors. TODO!
 *
 * As a workaround, for now, we consider a particular merge as an axiom.
 *)
Axiom todo_merge_range:
  ∀ π : Path, π ⊨ G(
    (
        ((range_BitVector decoded_address  0  7) '== (range_BitVector rddata 24 31))
      ∧ ((range_BitVector decoded_address  8 15) '== (range_BitVector rddata 32 39))
      ∧ ((range_BitVector decoded_address 16 23) '== (range_BitVector rddata 40 47))
      ∧ ((range_BitVector decoded_address 24 31) '== (range_BitVector rddata 48 55))
    )
  -->
    (decoded_address '== (range_BitVector rddata 24 55))
  ).

(******************************************************************************)

(**
 * \warning
 *  In this theorem, we do not verify the equality of "trace_data" since we
 *  configured CoreSight to output the "Cycle Count" information.
 *
 *  When entering CR for the first time, the "Cycle Count" information is always
 *  different and a reset is triggered. As a consequence, we are secure but not
 *  sound.
 *
 *  To solve this problem, just unset the "Cycle Accurate" option for CoreSight
 *  configuration and the equality of "trace_data" can be verified again.
 *)
Theorem security:
  ∀ π : Path, π ⊨ G(
    ('verifying) ∧ (negP (
        ( sw_araddr                '== (range_BitVector rddata  0 13))
      ∧ ((bcons 0 sw_arvalid bnil) '== (range_BitVector rddata 14 14))
      ∧ ((bcons 0 trace_ctl bnil)  '== (range_BitVector rddata 15 15))
   (* ∧ ( trace_data               '== (range_BitVector rddata 16 23)) *)
      ∧ ( decoded_address          '== (range_BitVector rddata 24 55))
    ))
    --> ('reset)
  ).
Proof.
  intros π.
  rewrite Thesis.toolbox.simpl_globally.
  intros s.

  pose proof (Dependencies.decide_merge_output._decide_merge_reset_ π) as H.
  rewrite Thesis.toolbox.simpl_globally in H.
  pose proof (H s) as H.

  (* either a reset or none of the intermediate resets: *)
  destruct H as [H | H].
  2:{ right; exact H. }
  1:{
    simpl in H.
    destruct H as (H  & H0).
    destruct H as (H  & H1).
    destruct H as (H  & H2).
    destruct H as (H  & H3).
    destruct H as (H  & H4).
    destruct H as (H  & H5).
    destruct H as (H7 & H6).

    (* we add all properties, one by one *)
    pose proof (Dependencies.decide._decide_reset_sw_araddr_ π) as H.
    rewrite Thesis.toolbox.simpl_globally in H.
    pose proof (H s) as H.
    destruct H as [H | H].
    2:{
      (* second case contradicts the no-reset hypothesis *)
      simpl in H.
      rewrite H in H7.
      discriminate.
    }
    1:{
      simpl in H.
      destruct H as [H | H8].
      (* either not verifying: *)
      - left.
        left.
        simpl.
        exact H.

      (* or no intermediate reset ; let's add a new property *)
      -

(* we create a tactic to re-do these steps: *)
Ltac add_prop theProp π s :=
  pose proof (theProp π) as H;
  rewrite Thesis.toolbox.simpl_globally in H;
  pose proof (H s) as H;
  destruct H as [H | H].

  add_prop Dependencies.decide._decide_reset_sw_arvalid_ π s.
  2:{ simpl in H; rewrite H in H6; discriminate. }
  1:{ simpl in H; destruct H as [H | H9].
      - left; left; simpl; exact H.
      -

  add_prop Dependencies.decide._decide_reset_trace_ctl_ π s.
  2:{ simpl in H; rewrite H in H5; discriminate. }
  1:{ simpl in H; destruct H as [H | H10].
      - left; left; simpl; exact H.
      -

(* if "trace_data" is verified: *)
(*
  add_prop Dependencies.decide._decide_reset_trace_data_ π s.
  2:{ simpl in H; rewrite H in H4; discriminate. }
  1:{ simpl in H; destruct H as [H | H11].
      - left; left; simpl; exact H.
      -
*)

  add_prop Dependencies.decide._decide_reset_decoded_address_0_ π s.
  2:{ simpl in H; rewrite H in H3; discriminate. }
  1:{ simpl in H; destruct H as [H | H12].
      - left; left; simpl; exact H.
      -

  add_prop Dependencies.decide._decide_reset_decoded_address_1_ π s.
  2:{ simpl in H; rewrite H in H2; discriminate. }
  1:{ simpl in H; destruct H as [H | H13].
      - left; left; simpl; exact H.
      -

  add_prop Dependencies.decide._decide_reset_decoded_address_2_ π s.
  2:{ simpl in H; rewrite H in H1; discriminate. }
  1:{ simpl in H; destruct H as [H | H14].
      - left; left; simpl; exact H.
      -

  add_prop Dependencies.decide._decide_reset_decoded_address_3_ π s.
  2:{ simpl in H; rewrite H in H0; discriminate. }
  1:{ simpl in H; destruct H as [H | H15].
      - left; left; simpl; exact H.
      -

(* if "trace_data" is verified: *)
(*
  (* now, this is the case where all signals match: *)
  left; right.
  simpl.
  split.
  + split.
    * split.
      -- split.
        ++ exact H8.
        ++ exact H9.
      -- exact H10.
    * exact H11.

  (* for the decoded address, we need to merge the 4 Bytes: *)
  + pose proof (Dependencies.decide_merge_input._decide_merge_decoded_address_0_ π) as H_a0.
    rewrite globally_to_state in H_a0; pose proof (H_a0 s) as H_a0.
    pose proof (Dependencies.decide_merge_input._decide_merge_decoded_address_1_ π) as H_a1.
    rewrite globally_to_state in H_a1; pose proof (H_a1 s) as H_a1.
    pose proof (Dependencies.decide_merge_input._decide_merge_decoded_address_2_ π) as H_a2.
    rewrite globally_to_state in H_a2; pose proof (H_a2 s) as H_a2.
    pose proof (Dependencies.decide_merge_input._decide_merge_decoded_address_3_ π) as H_a3.
    rewrite globally_to_state in H_a3; pose proof (H_a3 s) as H_a3.

    (* for each Byte, we show that is equal to a range of rddata: *)
    assert (A0:
      path_up π s ⊨ ((range_BitVector decoded_address 0 7) '== (range_BitVector rddata 24 31))
    ).
    * apply my_eq_vect_tran with
        (π := π)
        (s := s)
        (size := 8)
        (v1 := (range_BitVector decoded_address 0 7))
        (v2 := decoded_address__0)
        (v3 := (range_BitVector rddata 24 31)).
      split.
      -- exact H_a0.
      -- exact H12.
    *

    assert (A1:
      path_up π s ⊨ ((range_BitVector decoded_address 8 15) '== (range_BitVector rddata 32 39))
    ).
    -- apply my_eq_vect_tran with
        (π := π)
        (s := s)
        (size := 8)
        (v1 := (range_BitVector decoded_address 8 15))
        (v2 := decoded_address__1)
        (v3 := (range_BitVector rddata 32 39)).
      split.
      ++ exact H_a1.
      ++ exact H13.
    --

    assert (A2:
      path_up π s ⊨ ((range_BitVector decoded_address 16 23) '== (range_BitVector rddata 40 47))
    ).
    ++ apply my_eq_vect_tran with
        (π := π)
        (s := s)
        (size := 8)
        (v1 := (range_BitVector decoded_address 16 23))
        (v2 := decoded_address__2)
        (v3 := (range_BitVector rddata 40 47)).
      split.
      ** exact H_a2.
      ** exact H14.
    ++

    assert (A3:
      path_up π s ⊨ ((range_BitVector decoded_address 24 31) '== (range_BitVector rddata 48 55))
    ).
    ** apply my_eq_vect_tran with
        (π := π)
        (s := s)
        (size := 8)
        (v1 := (range_BitVector decoded_address 24 31))
        (v2 := decoded_address__3)
        (v3 := (range_BitVector rddata 48 55)).
      split.
      --- exact H_a3.
      --- exact H15.
    **

  (* then we merge the 4 ranges to a bigger range of rddata: *)
  pose proof (todo_merge_range π) as H.
  rewrite Thesis.toolbox.simpl_globally in H.
  pose proof (H s) as H.
  destruct H as [H | H].
  --- simpl in H.
    destruct H as [H | H].
    +++ destruct H as [H | H].
      *** destruct H as [H | H].
        ---- simpl in A0.
          rewrite A0 in H.
          discriminate.
        ---- simpl in A1; rewrite A1 in H; discriminate.
      *** simpl in A2; rewrite A2 in H; discriminate.
    +++ simpl in A3; rewrite A3 in H; discriminate.
  --- exact H.

  }}}}}}}}}
*)

  (* now, this is the case where all signals match: *)
  left; right.
  simpl.
  split.
  + split.
    * split.
      -- exact H8.
      -- exact H9.
    * exact H10.

  (* for the decoded address, we need to merge the 4 Bytes: *)
  + pose proof (Dependencies.decide_merge_input._decide_merge_decoded_address_0_ π) as H_a0.
    rewrite globally_to_state in H_a0; pose proof (H_a0 s) as H_a0.
    pose proof (Dependencies.decide_merge_input._decide_merge_decoded_address_1_ π) as H_a1.
    rewrite globally_to_state in H_a1; pose proof (H_a1 s) as H_a1.
    pose proof (Dependencies.decide_merge_input._decide_merge_decoded_address_2_ π) as H_a2.
    rewrite globally_to_state in H_a2; pose proof (H_a2 s) as H_a2.
    pose proof (Dependencies.decide_merge_input._decide_merge_decoded_address_3_ π) as H_a3.
    rewrite globally_to_state in H_a3; pose proof (H_a3 s) as H_a3.

    (* for each Byte, we show that is equal to a range of rddata: *)
    assert (A0:
      path_up π s ⊨ ((range_BitVector decoded_address 0 7) '== (range_BitVector rddata 24 31))
    ).
    * apply my_eq_vect_tran with
        (π := π)
        (s := s)
        (size := 8)
        (v1 := (range_BitVector decoded_address 0 7))
        (v2 := decoded_address__0)
        (v3 := (range_BitVector rddata 24 31)).
      split.
      -- exact H_a0.
      -- exact H12.
    *

    assert (A1:
      path_up π s ⊨ ((range_BitVector decoded_address 8 15) '== (range_BitVector rddata 32 39))
    ).
    -- apply my_eq_vect_tran with
        (π := π)
        (s := s)
        (size := 8)
        (v1 := (range_BitVector decoded_address 8 15))
        (v2 := decoded_address__1)
        (v3 := (range_BitVector rddata 32 39)).
      split.
      ++ exact H_a1.
      ++ exact H13.
    --

    assert (A2:
      path_up π s ⊨ ((range_BitVector decoded_address 16 23) '== (range_BitVector rddata 40 47))
    ).
    ++ apply my_eq_vect_tran with
        (π := π)
        (s := s)
        (size := 8)
        (v1 := (range_BitVector decoded_address 16 23))
        (v2 := decoded_address__2)
        (v3 := (range_BitVector rddata 40 47)).
      split.
      ** exact H_a2.
      ** exact H14.
    ++

    assert (A3:
      path_up π s ⊨ ((range_BitVector decoded_address 24 31) '== (range_BitVector rddata 48 55))
    ).
    ** apply my_eq_vect_tran with
        (π := π)
        (s := s)
        (size := 8)
        (v1 := (range_BitVector decoded_address 24 31))
        (v2 := decoded_address__3)
        (v3 := (range_BitVector rddata 48 55)).
      split.
      --- exact H_a3.
      --- exact H15.
    **

  (* then we merge the 4 ranges to a bigger range of rddata: *)
  pose proof (todo_merge_range π) as H.
  rewrite Thesis.toolbox.simpl_globally in H.
  pose proof (H s) as H.
  destruct H as [H | H].
  --- simpl in H.
    destruct H as [H | H].
    +++ destruct H as [H | H].
      *** destruct H as [H | H].
        ---- simpl in A0.
          rewrite A0 in H.
          discriminate.
        ---- simpl in A1; rewrite A1 in H; discriminate.
      *** simpl in A2; rewrite A2 in H; discriminate.
    +++ simpl in A3; rewrite A3 in H; discriminate.
  --- exact H.

  }}}}}}}}
Qed.
