(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

(******************************************************************************)
(******************************************************************************)

(**
 * Definition of all the predicates:
 *)

Axiom clk : CV0 Bit (eq O).
Axiom deduced_PC_eq_CRmax : CV0 Bit (eq O).

Axiom sw_araddr  : (BitVect 14).
Axiom sw_arvalid : Bit.
Axiom trace_ctl  : Bit.
Axiom trace_data : (BitVect 8).
Axiom decoded_address__0 : (BitVect 8).
Axiom decoded_address__1 : (BitVect 8).
Axiom decoded_address__2 : (BitVect 8).
Axiom decoded_address__3 : (BitVect 8).
Axiom decoded_address : (BitVect 32).

Axiom i : (BitVect 10).
Axiom rddata : (BitVect 64).

Axiom i_eq_end_addr : CV0 Bit (eq O).
Axiom verifying     : CV0 Bit (eq O).
Axiom done          : CV0 Bit (eq O).

Axiom reset_sw_araddr          : CV0 Bit (eq O).
Axiom reset_sw_arvalid         : CV0 Bit (eq O).
Axiom reset_trace_ctl          : CV0 Bit (eq O).
Axiom reset_trace_data         : CV0 Bit (eq O).
Axiom reset_decoded_address__0 : CV0 Bit (eq O).
Axiom reset_decoded_address__1 : CV0 Bit (eq O).
Axiom reset_decoded_address__2 : CV0 Bit (eq O).
Axiom reset_decoded_address__3 : CV0 Bit (eq O).

Axiom reset : CV0 Bit (eq O).

Definition END_ADDR := uwconst(0x3ff, 10).

