#
# Copyright (C) 2008 The OpenOCD Project
# Copyright (C) 2021 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

#
# Digilent Cora Z7-07S with Zynq XC7Z007S-1CLG400C / single core ARM CPU
#
# https://reference.digilentinc.com/reference/programmable-logic/cora-z7/reference-manual
#

adapter driver ftdi
ftdi_device_desc "Digilent Adept USB Device"

# The [vendor ID, product ID] of the adapter
# - see the list of plugged devices with `lsusb`
# - see also FT2232HQ datasheet: "Default EEPROM Configuration"
ftdi_vid_pid 0x0403 0x6010

ftdi_layout_init 0x3088 0x1f8b
ftdi_layout_signal nSRST -data 0x3000 -oe 0x3000
ftdi_layout_signal LED   -data 0x0010

reset_config srst_only
adapter srst delay 40

## =============================================================================
# We do not source the openOCD configuration file for zynq_7000 in case of this
# cora Z7: there is only one CPU and we must not create 2 targets.
# Instead, we copy the content of the file and remove the creation and
# configuration of a target for cpu1.
#
# source [find target/zynq_7000.cfg]

set _CHIPNAME zynq
set _TARGETNAME $_CHIPNAME.cpu

jtag newtap zynq_pl     bs -irlen 6 -ircapture 0x1 -irmask 0x03 \
  -expected-id 0x13723093

jtag newtap $_CHIPNAME cpu -irlen 4 -ircapture 0x1 -irmask 0xf \
  -expected-id 0x4ba00477

dap create $_CHIPNAME.dap -chain-position $_CHIPNAME.cpu

target create ${_TARGETNAME}0 cortex_a -dap $_CHIPNAME.dap \
  -coreid 0 -dbgbase 0x80090000
target smp ${_TARGETNAME}0

adapter speed 1000

${_TARGETNAME}0 configure -event reset-assert-post "cortex_a dbginit"

pld device virtex2 zynq_pl.bs 1

set XC7_JSHUTDOWN 0x0d
set XC7_JPROGRAM 0x0b
set XC7_JSTART 0x0c
set XC7_BYPASS 0x3f

proc zynqpl_program {tap} {
  global XC7_JSHUTDOWN XC7_JPROGRAM XC7_JSTART XC7_BYPASS
  irscan $tap $XC7_JSHUTDOWN
  irscan $tap $XC7_JPROGRAM
  runtest 60000
  #JSTART prevents this from working...
  #irscan $tap $XC7_JSTART
  runtest 2000
  irscan $tap $XC7_BYPASS
  runtest 2000
}

##
# end of content for file [find target/zynq_7000.cfg]
# ==============================================================================

# overwrite setting in zynq_7000.cfg
adapter speed 6000
pld device virtex2 zynq_pl.bs 1

init
scan_chain ;# displays the TAPs in the scan chain configuration and their status

