
#
# Copyright (C) 2021 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

set architecture armv7

monitor reset halt
monitor pld load 0 ../2_1_2_ipcore/hw/design_0_wrapper.bit
monitor gdb_sync

# now controlling gdb:
file  ../2_2_1_fsbl/src/main.elf
load  ../2_2_1_fsbl/src/main.elf
break _boot
jump  _boot
#layout regs

# add a breakpoint in each entry of the exception vectors table:
break *0x00
break *0x04
break *0x08
break *0x0c
break *0x10
break *0x14
break *0x18
break *0x1c

# run until we reach Loop (ps7_init terminated properly) or FsblHookFallback (an
# error occurred, case of qemu since all devices cannot be initialized):
break Loop
break FsblHookFallback
continue
continue

# load the application:
file ../2_2_2_app/src/main.elf
load ../2_2_2_app/src/main.elf
break _start
jump  _start

break main
continue
continue

