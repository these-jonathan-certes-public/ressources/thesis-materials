#!/bin/sh
# the next line restarts using tclsh \
exec tclsh "$0" ${1+"$@"}

#
# Copyright (C) 2023 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

#
# converts gdb output to Verilog parameters .INIT_{*}.
#

if { [llength ${argv}] < 1 } {
  puts "Usage:"
  puts "  [info script] FILE"
  puts "  where FILE is a gdb output (results of \"x /8wx ADDR\")"
}

foreach theFile ${argv} {

  if { ![file exists ${theFile}] } {
    puts "** error ** file not found: ${theFile}"
    continue
  }

  set theCode [catch {
    set theBuffer [open ${theFile} "r"]
    set theText   [read ${theBuffer}]
    close ${theBuffer}
  } theReturn]
  if { ${theCode} != 0 } {
    puts "** error ** unable to read file: ${theFile}"
    puts "${theReturn}"
    continue
  }

  set theLines [split ${theText} "\n"]
  for { set i 0 } { ${i} < [llength ${theLines}] } { incr i 2 } {
    set theFirstLine  [lindex ${theLines} ${i}]
    set theSecondLine [lindex ${theLines} [expr {${i} + 1}]]

    # end of a 1024 words BRAM:
    if { [expr {(${i} / 2) % 128}] == 0 } {
      puts ""
    }

    # format to Verilog parameters .INIT_{*}:
    set theOutput ""
    foreach theLine [list ${theFirstLine} ${theSecondLine}] {
      set theList [split ${theLine} "\t"]
      for { set j 1 } { ${j} < [llength ${theList}] } { incr j } {
        set theValue [lindex ${theList} ${j}]
        if { [string first "0x" ${theValue}] == 0 } {
          set theValue [string range ${theValue} 2 end]
        }
        set theOutput "${theValue}${theOutput}"
      }
    }
    if { ![string equal ${theOutput} ""] } {
      set theIndex [format "%02x" [expr {(${i} / 2) % 128}]]
      puts ".INIT_[string toupper ${theIndex}](256'h${theOutput}),"
    }
  }

}
