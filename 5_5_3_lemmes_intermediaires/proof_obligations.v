(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.

(******************************************************************************)
(******************************************************************************)

(**
 * This proof obligation is an atomicity property. It is enforced by an other
 * modified automaton from VRASED.
 *)
Axiom Modified_VRASED_LTL5:
  ∀ π : Path, π ⊨ G(
    (
      ('irq) ∧ ('deduced_PC_in_CR)
    )
  -->
    (
      ('reset)
    )
  ).

(******************************************************************************)
(******************************************************************************)

(**
 * When signal "packet_ready" is set and the decompresser outputs a branch
 * packet with the address in it, then the decoder outputs the decoded address.
 *
 * This is a full address and implicit bits are in the decoded address as well.
 *)
Axiom decoder_in_branch_1:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
      )
    )
  -->
    (
      ('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR)
    )
  ).

(**
 * When signal "packet_ready" is set, if the decompresser outputs a branch
 * packet and the decoder outputs a decoded address, then this address matches
 * the address given by the decompresser.
 *)
Axiom decoder_in_branch_2:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        ('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR)
      )
    )
  -->
    (
      ('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR)
    )
  ).

(******************************************************************************)

(**
 * Same pair with an isync packet.
 *)

Axiom decoder_in_isync_1:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        (('decompressed_packet_eq_isync) ∧ ('decompressed_address_in_CR))
      )
    )
  -->
    (
      ('decompressed_packet_eq_isync) ∧ ('decoded_address_in_CR)
    )
  ).


Axiom decoder_in_isync_2:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        ('decompressed_packet_eq_isync) ∧ ('decoded_address_in_CR)
      )
    )
  -->
    (
      ('decompressed_packet_eq_isync) ∧ ('decompressed_address_in_CR)
    )
  ).

(******************************************************************************)

(**
 * Same pair with a target address outside CR.
 *)

Axiom decoder_out_branch_1:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        (('decompressed_packet_eq_branch) ∧ (negP ('decompressed_address_in_CR)))
      )
    )
  -->
    (
      ('decompressed_packet_eq_branch) ∧ (negP ('decoded_address_in_CR))
    )
  ).

Axiom decoder_out_branch_2:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        ('decompressed_packet_eq_branch) ∧ (negP ('decoded_address_in_CR))
      )
    )
  -->
    (
      ('decompressed_packet_eq_branch) ∧ (negP ('decompressed_address_in_CR))
    )
  ).

(******************************************************************************)

(**
 * Same pair with an isync packet and a target address outside CR.
 *)

Axiom decoder_out_isync_1:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        (('decompressed_packet_eq_isync) ∧ (negP ('decompressed_address_in_CR)))
      )
    )
  -->
    (
      ('decompressed_packet_eq_isync) ∧ (negP ('decoded_address_in_CR))
    )
  ).

Axiom decoder_out_isync_2:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        ('decompressed_packet_eq_isync) ∧ (negP ('decoded_address_in_CR))
      )
    )
  -->
    (
      ('decompressed_packet_eq_isync) ∧ (negP ('decompressed_address_in_CR))
    )
  ).

(******************************************************************************)
(******************************************************************************)

(**
 * When signal "packet_ready" is set and the available packet is a branch
 * packet, then the decompresser outputs the data with the address in it.
 *)
Axiom decompresser_in_branch_1:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
      )
    )
  -->
    (
      ('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR)
    )
  ).

(**
 * When signal "packet_ready" is set, if the decompresser outputs a branch
 * packet and an address, then this address matches the address given by
 * CoreSight.
 *)
Axiom decompresser_in_branch_2:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        ('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR)
      )
    )
  -->
    (
      (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
    )
  ).

(******************************************************************************)

(**
 * Same pair with an isync packet.
 *)

Axiom decompresser_in_isync_1:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        (('available_packet_eq_isync) ∧ ('packet_address_in_CR))
      )
    )
  -->
    (
      ('decompressed_packet_eq_isync) ∧ ('decompressed_address_in_CR)
    )
  ).

Axiom decompresser_in_isync_2:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        ('decompressed_packet_eq_isync) ∧ ('decompressed_address_in_CR)
      )
    )
  -->
    (
      (('available_packet_eq_isync) ∧ ('packet_address_in_CR))
    )
  ).

(******************************************************************************)

(**
 * Same pair with a target address outside CR.
 *)

Axiom decompresser_out_branch_1:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
      )
    )
  -->
    (
      ('decompressed_packet_eq_branch) ∧ (negP ('decompressed_address_in_CR))
    )
  ).

Axiom decompresser_out_branch_2:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        ('decompressed_packet_eq_branch) ∧ (negP ('decompressed_address_in_CR))
      )
    )
  -->
    (
      (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
    )
  ).

(******************************************************************************)

(**
 * Same pair with an isync packet and a target address outside CR.
 *)

Axiom decompresser_out_isync_1:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        (('available_packet_eq_isync) ∧ (negP ('packet_address_in_CR)))
      )
    )
  -->
    (
      ('decompressed_packet_eq_isync) ∧ (negP ('decompressed_address_in_CR))
    )
  ).

Axiom decompresser_out_isync_2:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
        ('decompressed_packet_eq_isync) ∧ (negP ('decompressed_address_in_CR))
      )
    )
  -->
    (
      (('available_packet_eq_isync) ∧ (negP ('packet_address_in_CR)))
    )
  ).

(******************************************************************************)
(******************************************************************************)

