(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

(******************************************************************************)
(******************************************************************************)

(**
 * Definition of all the predicates:
 *)

(* signals in the FPGA *)
Axiom clk nrst                      : CV0 Bit (eq O).

(* to model the inside of the microprocessor *)
Axiom PC_in_CR                      : CV0 Bit (eq O).
Axiom irq                           : CV0 Bit (eq O).
Axiom available_packet_eq_branch    : CV0 Bit (eq O).
Axiom available_packet_eq_isync     : CV0 Bit (eq O).
Axiom packet_address_in_CR          : CV0 Bit (eq O).

(* signals from the decompresser *)
Axiom decompressed_packet_eq_branch : CV0 Bit (eq O).
Axiom decompressed_packet_eq_isync  : CV0 Bit (eq O).
Axiom decompressed_address_in_CR    : CV0 Bit (eq O).
Axiom packet_ready                  : CV0 Bit (eq O).

(* signals from the decoder *)
Axiom decoded_address_in_CR         : CV0 Bit (eq O).

(* signals from the transducer *)
Axiom deduced_PC_in_CR              : CV0 Bit (eq O).

(* signals from the re-used automata *)
Axiom reset                         : CV0 Bit (eq O).

(******************************************************************************)

(**
 * Model of the environment:
 *)

(**
 * In case of a hard reset, the microprocessor moves PC to address 0, which is
 * not in CR.
 *)
Axiom reset_to_not_CR:
  ∀ π : Path, π ⊨ G( ('reset) --> (negP ('PC_in_CR)) ).

(**
 * At startup (i.e. at first state), it is like we have a hard reset.
 *)
Axiom startup_reset:
  ∀ π : Path, π ⊨ ('reset).

(**
 * The clock in the FPGA is always active.
 *)
Axiom clk_is_on:
  ∀ π : Path, π ⊨ G('clk).

(**
 * The manual reset button in the FPGA is never pressed (there is a not gate).
 *)
Axiom nrst_is_on:
  ∀ π : Path, π ⊨ G('nrst).

(**
 * CoreSight is configured to trace the program flow in CR. Either an isync or a
 * branch packet is available when PC is about to enter CR and the destination
 * address is in CR.
 *)
Axiom CoreSight_configuration_model_enter_CR:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
        (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
      ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
    )
  ).

(**
 * Our program does not contain an indirect branch that leads within itself. So,
 * when in CR and with  no exception, there is no packet with the destination in
 * CR.
 *)
Axiom CoreSight_configuration_model_in_CR:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ (X ('PC_in_CR)) ∧ (negP ('irq))
    )
  -->
    (negP (
            (('available_packet_eq_isync)  ∧ (negP ('packet_address_in_CR)))
          ∨ (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
        )
    )
  ).

(**
 * To exit CR normally, our program ends with an indirect branch, so we have a
 * branch packet. If an exception is triggered and leads to outside CR, then we
 * also have a branch packet.
 * In other words, we cannot exit CR whitout having a branch packet.
 *)
Axiom CoreSight_configuration_model_exit_CR:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    )
  -->
    (
      (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
    )
  ).

(**
 * CoreSight is configured to trace the program flow in CR. If PC does not enter
 * CR, then we have no packet with a destination address in CR.
 *)
Axiom CoreSight_configuration_model_out_CR:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X (negP ('PC_in_CR)))
    )
  -->
    (negP (
            (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
          ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
        )
    )
  ).

(******************************************************************************)

(**
 * This is an hypothesis for our model zero: we abstract the delay between
 * having a branch packet available and having this same packet ready.
 *)
Axiom model_0_branch_packet_ready:
  ∀ π : Path, π ⊨ G(
    ('available_packet_eq_branch)
  -->
    ('packet_ready)
  ).

Axiom model_0_isync_packet_ready:
  ∀ π : Path, π ⊨ G(
    ('available_packet_eq_isync)
  -->
    ('packet_ready)
  ).

