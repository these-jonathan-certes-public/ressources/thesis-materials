(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.proof_obligations.
Require Import Dependencies.transducer.

(******************************************************************************)

Require Import Dependencies.lemma_l_exit.

(**
 * Here we remove both clk and nrst from the model-checked property, since both
 * are always on.
 *)
Lemma _P_out__clk_nrst:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('deduced_PC_in_CR)) ∧
      (negP (
        ('packet_ready) ∧ (
            (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
          ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
        )
      ))
    )
  -->
    (X (negP ('deduced_PC_in_CR)))
  ).
Proof.
  intros π.
  pose proof (_P_out_ π) as _P_out_.
  rewrite simpl_globally.
  rewrite simpl_globally in _P_out_.

  intros s.
  pose proof (_P_out_ s) as _P_out_.
  simpl.
  simpl in _P_out_.

  (* we show that cases where clk and nrst are false cannot occur *)
  pose proof (clk_is_on π)  as clk_is_on.
  pose proof (nrst_is_on π) as nrst_is_on.

  (* globally is a disjunction with a state_top in one side *)
  simpl in clk_is_on, nrst_is_on.
  destruct clk_is_on  as [clk_is_on  | H_false_clk],
           nrst_is_on as [nrst_is_on | H_false_nrst].
  2:{
    destruct H_false_nrst as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }
  2:{
    destruct H_false_clk as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }
  2:{
    destruct H_false_clk as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }

  pose proof (clk_is_on  s) as clk_is_on.
  pose proof (nrst_is_on s) as nrst_is_on.
  rewrite clk_is_on  in _P_out_.
  rewrite nrst_is_on in _P_out_.
  simpl in _P_out_.
  destruct _P_out_ as [H | H].
  repeat destruct H as [H | H].
  - discriminate.
  - discriminate.
  - left; left;  exact H.
  - left; right; exact H.
  - right;       exact H.
Qed.


(**
 * Here we abstract decompression and decoding since we consider them correct.
 *)
Lemma simpl__P_out_:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('deduced_PC_in_CR)) ∧
      (negP (
        ('packet_ready) ∧ (
            (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
          ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
        )
      ))
    )
  -->
    (X (negP ('deduced_PC_in_CR)))
  ).
Proof.
  intros π.
  pose proof (_P_out__clk_nrst π) as _P_out__clk_nrst.
  rewrite simpl_globally.
  rewrite simpl_globally in _P_out__clk_nrst.

  intros s.
  pose proof (_P_out__clk_nrst s) as _P_out__clk_nrst.
  simpl.
  simpl in _P_out__clk_nrst.

  destruct _P_out__clk_nrst as [H | H].
  - destruct H as [H | H].
    + left; left; exact H.
    + left; right.
      destruct H as (H1 & H).
      split.
      * exact H1.
      * destruct H as [H | H].

        (* case of an isync packet with address not in CR *)
        -- left.
          destruct H as (H2 & H3).

          (* a decoded address for isync packets is the correct one *)
          pose proof (decoder_in_isync_2 π) as decoder_in_isync_2.
          rewrite simpl_globally in decoder_in_isync_2.
          pose proof (decoder_in_isync_2 s) as decoder_in_isync_2.
          simpl in decoder_in_isync_2.
          destruct decoder_in_isync_2 as [H_decoder_isync_equiv | H_decoder_isync_equiv].
          ++ rewrite H1 in H_decoder_isync_equiv.
            destruct H_decoder_isync_equiv as [H_decoder_isync_equiv | H_decoder_isync_equiv].
            ** discriminate.
            ** destruct H_decoder_isync_equiv as [H_decoder_isync_equiv | H_decoder_isync_equiv].
              --- rewrite H2 in H_decoder_isync_equiv.
                discriminate.
              --- rewrite H3 in H_decoder_isync_equiv.
                discriminate.

          ++ destruct H_decoder_isync_equiv as (H4 & H_decoder_isync_equiv).
            (* a decompressed address for isync packets is the correct one *)
            pose proof (decompresser_in_isync_2 π) as decompresser_in_isync_2.
            rewrite simpl_globally in decompresser_in_isync_2.
            pose proof (decompresser_in_isync_2 s) as decompresser_in_isync_2.
            simpl in decompresser_in_isync_2.
            destruct decompresser_in_isync_2 as [H_decompress_isync_equiv | H_decompress_isync_equiv].
            ** rewrite H1 in H_decompress_isync_equiv.
              destruct H_decompress_isync_equiv as [H_decompress_isync_equiv | H_decompress_isync_equiv].
              --- discriminate.
              --- destruct H_decompress_isync_equiv as [H_decompress_isync_equiv | H_decompress_isync_equiv].
                +++ rewrite H2 in H_decompress_isync_equiv.
                  discriminate.
                +++ rewrite H_decoder_isync_equiv in H_decompress_isync_equiv.
                  discriminate.
            ** exact H_decompress_isync_equiv.

        (* case of a branch packet with address not in CR *)
        -- right.
          destruct H as (H2 & H3).

          (* a decoded address for branch packets is the correct one *)
          pose proof (decoder_in_branch_2 π) as decoder_in_branch_2.
          rewrite simpl_globally in decoder_in_branch_2.
          pose proof (decoder_in_branch_2 s) as decoder_in_branch_2.
          simpl in decoder_in_branch_2.
          destruct decoder_in_branch_2 as [H_decoder_branch_equiv | H_decoder_branch_equiv].
          ++ rewrite H1 in H_decoder_branch_equiv.
            destruct H_decoder_branch_equiv as [H_decoder_branch_equiv | H_decoder_branch_equiv].
            ** discriminate.
            ** destruct H_decoder_branch_equiv as [H_decoder_branch_equiv | H_decoder_branch_equiv].
              --- rewrite H2 in H_decoder_branch_equiv.
                discriminate.
              --- rewrite H3 in H_decoder_branch_equiv.
                discriminate.

          ++ destruct H_decoder_branch_equiv as (H4 & H_decoder_branch_equiv).
            (* a decompressed address for branch packets is the correct one *)
            pose proof (decompresser_in_branch_2 π) as decompresser_in_branch_2.
            rewrite simpl_globally in decompresser_in_branch_2.
            pose proof (decompresser_in_branch_2 s) as decompresser_in_branch_2.
            simpl in decompresser_in_branch_2.
            destruct decompresser_in_branch_2 as [H_decompress_branch_equiv | H_decompress_branch_equiv].
            ** rewrite H1 in H_decompress_branch_equiv.
              destruct H_decompress_branch_equiv as [H_decompress_branch_equiv | H_decompress_branch_equiv].
              --- discriminate.
              --- destruct H_decompress_branch_equiv as [H_decompress_branch_equiv | H_decompress_branch_equiv].
                +++ rewrite H2 in H_decompress_branch_equiv.
                  discriminate.
                +++ rewrite H_decoder_branch_equiv in H_decompress_branch_equiv.
                  discriminate.
            ** exact H_decompress_branch_equiv.

  - right; exact H.
Qed.


Lemma L_out:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('deduced_PC_in_CR)) ∧ (X (negP ('PC_in_CR)))
    )
  -->
    (
      X (negP ('deduced_PC_in_CR))
    )
  ).
Proof.
  intros π.
  pose proof (simpl__P_out_ π) as simpl__P_out_.
  pose proof (CoreSight_configuration_model_out_CR π) as CoreSight_configuration_model_out_CR.
  pose proof (L_exit                               π) as L_exit.
  rewrite simpl_globally.
  rewrite simpl_globally in
    simpl__P_out_,
    CoreSight_configuration_model_out_CR,
    L_exit.

  intros s.
  pose proof (simpl__P_out_ s) as simpl__P_out_.
  pose proof (CoreSight_configuration_model_out_CR s) as CoreSight_configuration_model_out_CR.
  pose proof (L_exit                               s) as L_exit.
  simpl.
  simpl in
    simpl__P_out_,
    CoreSight_configuration_model_out_CR,
    L_exit.

  (* bazooka method, we try all possible cases for all predicates *)
  induction (path_sub (path_up π s) 0 (str0 Bit (eq O) available_packet_eq_branch)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) available_packet_eq_isync)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) packet_address_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) deduced_PC_in_CR)),
            (path_sub (path_up (path_up π s) 1) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up (path_up π s) 1) 0 (str0 Bit (eq O) deduced_PC_in_CR));
  try (right;       reflexivity);
  try (left; right; reflexivity);
  try (left; left;  reflexivity);

  (* we clean all hypothesis and look for contradictions *)
  ( Thesis.toolbox.simpl_all_bool_in simpl__P_out_;
    Thesis.toolbox.simpl_all_bool_in CoreSight_configuration_model_out_CR;
    Thesis.toolbox.simpl_all_bool_in L_exit
  );
  try (case simpl__P_out_);
  try (case CoreSight_configuration_model_out_CR);
  try (case L_exit);

  (* in some cases, the goal looks like: False -> Prop *)
  try (intros H;  case H);
  try (intros H0; case H0).
Qed.

