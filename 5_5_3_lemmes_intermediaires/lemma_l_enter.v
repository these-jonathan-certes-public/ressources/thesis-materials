(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.proof_obligations.
Require Import Dependencies.transducer.

(******************************************************************************)

(**
 * Hypothesis for our model zero: merge both isync and branch.
 *)
Lemma merge_model_0_packet_ready:
  ∀ π : Path, π ⊨ G(
    (
        (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
      ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
    )
  -->
    ('packet_ready)
  ).
Proof.
  intros π.

  pose proof (model_0_branch_packet_ready π) as model_0_branch_packet_ready.
  pose proof (model_0_isync_packet_ready  π) as model_0_isync_packet_ready.
  rewrite Thesis.toolbox.simpl_globally in model_0_branch_packet_ready, model_0_isync_packet_ready.
  apply Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (model_0_branch_packet_ready s) as model_0_branch_packet_ready.
  pose proof (model_0_isync_packet_ready  s) as model_0_isync_packet_ready.
  destruct model_0_branch_packet_ready as [H_branch_a | H_branch_b],
           model_0_isync_packet_ready  as [H_isync_a  | H_isync_b].
  - simpl in H_branch_a, H_isync_a.
    left; simpl.
    split.
    + left; exact H_isync_a.
    + left; exact H_branch_a.
  - right; exact H_isync_b.
  - right; exact H_branch_b.
  - right; exact H_branch_b.
Qed.

(**
 * When signal "packet_ready" is set and the available packet is either an isync
 * or a branch packet, then the decompresser outputs the data with the address
 * in it.
 *)
Lemma merge_decompresser_proof_obligation:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
          (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
        ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
      )
    )
  -->
    (
        (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
      ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
    )
  ).
Proof.
  intros π.
  apply Thesis.toolbox.globally_impP_x2_to_disj with
    (a := ('packet_ready))
    (b := (('available_packet_eq_isync)  ∧ ('packet_address_in_CR)))
    (c := (('available_packet_eq_branch) ∧ ('packet_address_in_CR)))
    (d := (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR)))
    (e := (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))).

  split.
  - apply decompresser_in_isync_1.
  - apply decompresser_in_branch_1.
Qed.


(**
 * When signal "packet_ready" is set and the decompresser outputs the data with
 * the address in it, then the decoder outputs the decoded address.
 *
 * This is a full address and implicit bits are in the decoded address as well.
 *)
Lemma merge_decoder_proof_obligation:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
          (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
        ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
      )
    )
  -->
    (
        (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
      ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
    )
  ).
Proof.
  intros π.
  apply Thesis.toolbox.globally_impP_x2_to_disj with
    (a := ('packet_ready))
    (b := (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR)))
    (c := (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR)))
    (d := (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR)))
    (e := (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))).

  split.
  - apply decoder_in_isync_1.
  - apply decoder_in_branch_1.
Qed.


Lemma merge_step_1:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
      ('packet_ready) ∧ (
          (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
        ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
      )
    )
  ).
Proof.
  intros π.
  apply Thesis.toolbox.globally_impP_conj_commutes.
  apply Thesis.toolbox.globally_impP_x2_to_conj with
    (a := (negP ('PC_in_CR) ∧ X('PC_in_CR)))
    (b := (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
        ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR)))
    (c := ('packet_ready)).

  split.
  - apply CoreSight_configuration_model_enter_CR.
  - apply merge_model_0_packet_ready.
Qed.


Lemma merge_step_2:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
      ('packet_ready) ∧ (
          (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
        ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
      )
    )
  ).
Proof.
  intros π.
  pose proof (merge_step_1 π) as H.

  assert (A0:
  π ⊨ G
    (
      (negP (' PC_in_CR) ∧ X (' PC_in_CR))
    -->
      (
        (
          (' packet_ready) ∧ (
              ((' available_packet_eq_isync)  ∧ (' packet_address_in_CR))
            ∨ ((' available_packet_eq_branch) ∧ (' packet_address_in_CR))
          )
        ) ∧ (
          (
              (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
            ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
          )
        )
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := (negP (' PC_in_CR) ∧ X (' PC_in_CR)) )
      (b := (
              (' packet_ready) ∧ (
                  ((' available_packet_eq_isync)  ∧ (' packet_address_in_CR))
                ∨ ((' available_packet_eq_branch) ∧ (' packet_address_in_CR))
              )
            ) )
      (c := (
                (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
              ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
            ) ).
    split.
    + apply H.
    + apply merge_decompresser_proof_obligation.

  - apply Thesis.toolbox.globally_impP_conj_commutes  in A0.
    apply Thesis.toolbox.globally_and_assoc_P in A0.
    apply Thesis.toolbox.globally_impP_conj_to_single in A0.
    apply Thesis.toolbox.globally_impP_conj_commutes  in A0.
    exact A0.
Qed.


Lemma merge_step_3:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
      ('packet_ready) ∧ (
          (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
        ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
      )
    )
  ).
Proof.
  intros π.
  pose proof (merge_step_2 π) as H.

  assert (A0:
  π ⊨ G
    (
      (negP (' PC_in_CR) ∧ X (' PC_in_CR))
    -->
      (
          (('packet_ready) ∧ (
              (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
            ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
          ))
        ∧ (
            (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
          ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
        )
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := (negP (' PC_in_CR) ∧ X (' PC_in_CR)) )
      (b := (('packet_ready) ∧ (
                (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
              ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
            )) )
      (c := (
                (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
              ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
            ) ).
    split.
    + exact H.
    + apply merge_decoder_proof_obligation.

  - rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    rewrite Thesis.toolbox.globally_and_assoc_P in A0.
    apply   Thesis.toolbox.globally_impP_conj_to_single in A0.
    rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    exact A0.
Qed.


Lemma L_enter:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
      X('deduced_PC_in_CR)
    )
  ).
Proof.
  intros π.
  pose proof (merge_step_3 π) as H.

  assert (A0:
  π ⊨ G
    (
      (negP (' PC_in_CR) ∧ X (' PC_in_CR))
    -->
      (
        (('packet_ready) ∧ (
            (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
          ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
        ))
        ∧ (X('deduced_PC_in_CR))
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := (negP (' PC_in_CR) ∧ X (' PC_in_CR)) )
      (b := (('packet_ready) ∧ (
                (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
              ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
            )) )
      (c := (X('deduced_PC_in_CR))).
    split.
    + exact H.

    (* here we must use the hypothesis that clk and nrst are always on *)
    + pose proof (_P_enter_ π) as H1.
      rewrite Thesis.toolbox.simpl_globally in H1.
      rewrite Thesis.toolbox.simpl_globally.
      intros s.
      pose proof (H1 s) as H2.
      destruct H2 as [H2a | H2b].
      * left.
        simpl in H2a.
        simpl.
        destruct H2a as [H2a | H2].
        -- destruct H2a as [H2a | H2b].

          (* case of clk is off *)
          ++ pose proof (clk_is_on) as clk_is_on.
            simpl in clk_is_on.
            pose proof (clk_is_on π) as clk_is_on.
            destruct clk_is_on as [clk_is_on_H | clk_is_on_False].
            ** pose proof (clk_is_on_H s) as clk_is_on_H.
              rewrite clk_is_on_H in H2a.
              discriminate.
            ** destruct clk_is_on_False as (s_False & H_False).
              destruct H_False as (H_False & H_exists).
              apply LTL.state_top in H_False.
              case H_False.

          (* case of nrst is off *)
          ++ pose proof (nrst_is_on) as nrst_is_on.
            simpl in nrst_is_on.
            pose proof (nrst_is_on π) as nrst_is_on.
            destruct nrst_is_on as [nrst_is_on_H | nrst_is_on_False].
            ** pose proof (nrst_is_on_H s) as clk_is_on_H.
              rewrite nrst_is_on_H in H2b.
              discriminate.
            ** destruct nrst_is_on_False as (s_False & H_False).
              destruct H_False as (H_False & H_exists).
              apply LTL.state_top in H_False.
              case H_False.

        -- exact H2.
      * right; exact H2b.

  - rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    apply   Thesis.toolbox.globally_impP_conj_to_single in A0.
    exact A0.
Qed.

