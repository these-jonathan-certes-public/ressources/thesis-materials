(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.proof_obligations.
Require Import Dependencies.transducer.

(******************************************************************************)

Require Import Dependencies.lemma_l_enter.

(**
 * Here we remove both clk and nrst from the model-checked property, since both
 * are always on.
 *)
Lemma _P_in__clk_nrst:
  ∀ π : Path, π ⊨ G(
    (
      ('deduced_PC_in_CR) ∧
      (negP (
        ('packet_ready) ∧ (
            (('decompressed_packet_eq_isync)  ∧ (negP ('decoded_address_in_CR)))
          ∨ (('decompressed_packet_eq_branch) ∧ (negP ('decoded_address_in_CR)))
        )
      ))
    )
  -->
    (X ('deduced_PC_in_CR))
  ).
Proof.
  intros π.
  pose proof (_P_in_ π) as _P_in_.
  rewrite simpl_globally.
  rewrite simpl_globally in _P_in_.

  intros s.
  pose proof (_P_in_ s) as _P_in_.
  simpl.
  simpl in _P_in_.

  (* we show that cases where clk and nrst are false cannot occur *)
  pose proof (clk_is_on π)  as clk_is_on.
  pose proof (nrst_is_on π) as nrst_is_on.

  (* globally is a disjunction with a state_top in one side *)
  simpl in clk_is_on, nrst_is_on.
  destruct clk_is_on  as [clk_is_on  | H_false_clk],
           nrst_is_on as [nrst_is_on | H_false_nrst].
  2:{
    destruct H_false_nrst as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }
  2:{
    destruct H_false_clk as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }
  2:{
    destruct H_false_clk as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }

  pose proof (clk_is_on  s) as clk_is_on.
  pose proof (nrst_is_on s) as nrst_is_on.
  rewrite clk_is_on  in _P_in_.
  rewrite nrst_is_on in _P_in_.
  simpl in _P_in_.
  destruct _P_in_ as [H | H].
  repeat destruct H as [H | H].
  - discriminate.
  - discriminate.
  - left; left;  exact H.
  - left; right; exact H.
  - right;       exact H.
Qed.


(**
 * Here we abstract decompression and decoding since we consider them correct.
 *)
Lemma simpl__P_in_:
  ∀ π : Path, π ⊨ G(
    (
      ('deduced_PC_in_CR) ∧
      (negP (
        ('packet_ready) ∧ (
            (('available_packet_eq_isync)  ∧ (negP ('packet_address_in_CR)))
          ∨ (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
        )
      ))
    )
  -->
    (X ('deduced_PC_in_CR))
  ).
Proof.
  intros π.
  pose proof (_P_in__clk_nrst π) as _P_in__clk_nrst.
  rewrite simpl_globally.
  rewrite simpl_globally in _P_in__clk_nrst.

  intros s.
  pose proof (_P_in__clk_nrst s) as _P_in__clk_nrst.
  simpl.
  simpl in _P_in__clk_nrst.

  destruct _P_in__clk_nrst as [H | H].
  - destruct H as [H | H].
    + left; left; exact H.
    + left; right.
      destruct H as (H1 & H).
      split.
      * exact H1.
      * destruct H as [H | H].

        (* case of an isync packet with address not in CR *)
        -- left.
          destruct H as (H2 & H3).

          (* a decoded address for isync packets is the correct one *)
          pose proof (decoder_out_isync_2 π) as decoder_out_isync_2.
          rewrite simpl_globally in decoder_out_isync_2.
          pose proof (decoder_out_isync_2 s) as decoder_out_isync_2.
          simpl in decoder_out_isync_2.
          destruct decoder_out_isync_2 as [H_decoder_isync_equiv | H_decoder_isync_equiv].
          ++ rewrite H1 in H_decoder_isync_equiv.
            destruct H_decoder_isync_equiv as [H_decoder_isync_equiv | H_decoder_isync_equiv].
            ** discriminate.
            ** destruct H_decoder_isync_equiv as [H_decoder_isync_equiv | H_decoder_isync_equiv].
              --- rewrite H2 in H_decoder_isync_equiv.
                discriminate.
              --- rewrite H3 in H_decoder_isync_equiv.
                discriminate.

          ++ destruct H_decoder_isync_equiv as (H4 & H_decoder_isync_equiv).
            (* a decompressed address for isync packets is the correct one *)
            pose proof (decompresser_out_isync_2 π) as decompresser_out_isync_2.
            rewrite simpl_globally in decompresser_out_isync_2.
            pose proof (decompresser_out_isync_2 s) as decompresser_out_isync_2.
            simpl in decompresser_out_isync_2.
            destruct decompresser_out_isync_2 as [H_decompress_isync_equiv | H_decompress_isync_equiv].
            ** rewrite H1 in H_decompress_isync_equiv.
              destruct H_decompress_isync_equiv as [H_decompress_isync_equiv | H_decompress_isync_equiv].
              --- discriminate.
              --- destruct H_decompress_isync_equiv as [H_decompress_isync_equiv | H_decompress_isync_equiv].
                +++ rewrite H2 in H_decompress_isync_equiv.
                  discriminate.
                +++ rewrite H_decoder_isync_equiv in H_decompress_isync_equiv.
                  discriminate.
            ** exact H_decompress_isync_equiv.

        (* case of a branch packet with address not in CR *)
        -- right.
          destruct H as (H2 & H3).

          (* a decoded address for branch packets is the correct one *)
          pose proof (decoder_out_branch_2 π) as decoder_out_branch_2.
          rewrite simpl_globally in decoder_out_branch_2.
          pose proof (decoder_out_branch_2 s) as decoder_out_branch_2.
          simpl in decoder_out_branch_2.
          destruct decoder_out_branch_2 as [H_decoder_branch_equiv | H_decoder_branch_equiv].
          ++ rewrite H1 in H_decoder_branch_equiv.
            destruct H_decoder_branch_equiv as [H_decoder_branch_equiv | H_decoder_branch_equiv].
            ** discriminate.
            ** destruct H_decoder_branch_equiv as [H_decoder_branch_equiv | H_decoder_branch_equiv].
              --- rewrite H2 in H_decoder_branch_equiv.
                discriminate.
              --- rewrite H3 in H_decoder_branch_equiv.
                discriminate.

          ++ destruct H_decoder_branch_equiv as (H4 & H_decoder_branch_equiv).
            (* a decompressed address for branch packets is the correct one *)
            pose proof (decompresser_out_branch_2 π) as decompresser_out_branch_2.
            rewrite simpl_globally in decompresser_out_branch_2.
            pose proof (decompresser_out_branch_2 s) as decompresser_out_branch_2.
            simpl in decompresser_out_branch_2.
            destruct decompresser_out_branch_2 as [H_decompress_branch_equiv | H_decompress_branch_equiv].
            ** rewrite H1 in H_decompress_branch_equiv.
              destruct H_decompress_branch_equiv as [H_decompress_branch_equiv | H_decompress_branch_equiv].
              --- discriminate.
              --- destruct H_decompress_branch_equiv as [H_decompress_branch_equiv | H_decompress_branch_equiv].
                +++ rewrite H2 in H_decompress_branch_equiv.
                  discriminate.
                +++ rewrite H_decoder_branch_equiv in H_decompress_branch_equiv.
                  discriminate.
            ** exact H_decompress_branch_equiv.

  - right; exact H.
Qed.


Lemma L_in:
  ∀ π : Path, π ⊨ G(
    (
      ('deduced_PC_in_CR) ∧ (X ('PC_in_CR))
    )
  -->
    (
      X ('deduced_PC_in_CR)
    )
  ).
Proof.
  intros π.
  pose proof (simpl__P_in_ π) as simpl__P_in_.
  pose proof (CoreSight_configuration_model_in_CR π) as CoreSight_configuration_model_in_CR.
  pose proof (L_enter                             π) as L_enter.
  pose proof (Modified_VRASED_LTL5                π) as Modified_VRASED_LTL5.
  pose proof (reset_to_not_CR                     π) as reset_to_not_CR.
  rewrite simpl_globally.
  rewrite simpl_globally in
    simpl__P_in_,
    CoreSight_configuration_model_in_CR,
    L_enter,
    Modified_VRASED_LTL5,
    reset_to_not_CR.

  intros s.
  pose proof (simpl__P_in_ s) as simpl__P_in_.
  pose proof (CoreSight_configuration_model_in_CR s) as CoreSight_configuration_model_in_CR.
  pose proof (L_enter                             s) as L_enter.
  pose proof (Modified_VRASED_LTL5                s) as Modified_VRASED_LTL5.
  pose proof (reset_to_not_CR                     s) as reset_to_not_CR.
  simpl.
  simpl in
    simpl__P_in_,
    CoreSight_configuration_model_in_CR,
    L_enter,
    Modified_VRASED_LTL5,
    reset_to_not_CR.

  (* bazooka method, we try all possible cases for all predicates *)
  induction (path_sub (path_up π s) 0 (str0 Bit (eq O) irq)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) reset)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) available_packet_eq_branch)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) available_packet_eq_isync)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) packet_address_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) deduced_PC_in_CR)),
            (path_sub (path_up (path_up π s) 1) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up (path_up π s) 1) 0 (str0 Bit (eq O) deduced_PC_in_CR));
  try (right;       reflexivity);
  try (left; right; reflexivity);
  try (left; left;  reflexivity);

  (* we clean all hypothesis and look for contradictions *)
  ( Thesis.toolbox.simpl_all_bool_in simpl__P_in_;
    Thesis.toolbox.simpl_all_bool_in CoreSight_configuration_model_in_CR;
    Thesis.toolbox.simpl_all_bool_in L_enter;
    Thesis.toolbox.simpl_all_bool_in Modified_VRASED_LTL5;
    Thesis.toolbox.simpl_all_bool_in reset_to_not_CR
  );
  try (case simpl__P_in_);
  try (case CoreSight_configuration_model_in_CR);
  try (case L_enter);
  try (case Modified_VRASED_LTL5);
  try (case reset_to_not_CR);

  (* in some cases, the goal looks like: False -> Prop *)
  try (intros H;  case H);
  try (intros H0; case H0).
Qed.
