(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import LTL.
Require Import Nat.

(**
 * Definition of PSL expression "next_event_a(b)[i:i](f)".
 *)
Fixpoint next_event (b: PathF) (i: nat) (f: PathF) :=
  match i with
  | 0   => bot
  | 1   => ((negP b) U (b ∧ f)) ∨ G(negP b)
  | S n => ((negP b) U (b ∧ (X (next_event b n f)))) ∨ G(negP b)
  end.

(**
 * Definition of PSL expression "next_event_a(b)[i:j](f)".
 *)
Fixpoint next_event_a (b: PathF) (i j: nat) (f: PathF) :=
  match j with
  | 0   => (next_event b i f)
  | S n =>
    if ( j <? i ) then
      bot
    else
      if ( j =? i ) then
        (next_event b i f)
      else
        (next_event_a b i n f) ∧ (next_event b (n+1) f)
  end.


Section examples.
  Variable (ф ψ: At).

  Compute (next_event_a (Var ф) 1 1 (Var ψ)).
  Compute (next_event   (Var ф)   1 (Var ψ)).
  Compute (next_event_a (Var ф) 2 2 (Var ψ)).
  Compute (next_event_a (Var ф) 3 3 (Var ψ)).

  Compute (next_event_a (Var ф) 1 2 (Var ψ)).
  Compute (next_event_a (Var ф) 2 3 (Var ψ)).
  Compute (next_event_a (Var ф) 1 3 (Var ψ)).
End examples.
