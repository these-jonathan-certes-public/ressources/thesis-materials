--
-- Copyright (C) 2022 Jonathan Certes
-- jonathan.certes@online.fr
--
-- This program is free software: you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your option) any later
-- version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU General Public License along with
-- this program. If not, see <http://www.gnu.org/licenses/>.
--

DEFINE "END_ADDR" := 0uh10_1ff;


-- to help model-checking:

INVARSPEC
  NAME "decide i_eq_end_addr_imp" :=
  (
    "i_eq_end_addr" -> ("i" = "END_ADDR")
  );

INVARSPEC
  NAME "decide i_eq_end_addr_equiv" :=
  (
    ("i" = "END_ADDR") -> "i_eq_end_addr"
  );


--------------------------------------------------------------------------------
-- security:
--------------------------------------------------------------------------------

INVARSPEC
  NAME "decide reset sw_araddr"          :=
  (("verifying" & !("sw_araddr"          = "rddata"[13:0] )) -> "reset_sw_araddr"         );

INVARSPEC
  NAME "decide reset sw_arvalid"         :=
  (("verifying" & !(word1("sw_arvalid")  = "rddata"[14:14])) -> "reset_sw_arvalid"        );

INVARSPEC
  NAME "decide reset trace_ctl"          :=
  (("verifying" & !(word1("trace_ctl" )  = "rddata"[15:15])) -> "reset_trace_ctl"         );

-- -- no verification of trace_data: causes a problem of soundness if cycle
-- -- count is activated in the configuration of Coresight:
-- INVARSPEC
--   NAME "decide reset trace_data"         :=
--   (("verifying" & !("trace_data"         = "rddata"[23:16])) -> "reset_trace_data"        );

INVARSPEC
  NAME "decide reset decoded_address__0" :=
  (("verifying" & !("decoded_address__0" = "rddata"[31:24])) -> "reset_decoded_address__0");

INVARSPEC
  NAME "decide reset decoded_address__1" :=
  (("verifying" & !("decoded_address__1" = "rddata"[39:32])) -> "reset_decoded_address__1");

INVARSPEC
  NAME "decide reset decoded_address__2" :=
  (("verifying" & !("decoded_address__2" = "rddata"[47:40])) -> "reset_decoded_address__2");

INVARSPEC
  NAME "decide reset decoded_address__3" :=
  (("verifying" & !("decoded_address__3" = "rddata"[55:48])) -> "reset_decoded_address__3");


--------------------------------------------------------------------------------
-- proof obligations:
--------------------------------------------------------------------------------

-----------------
-- wait sequence:
-----------------

INVARSPEC
  NAME "decide wait sound" :=
  (
    (
      "clk" & !"verifying" & !"done" & next(!"sw_arvalid")
    )
  ->
    (
      next(!"verifying" & !"done")
    )
  );

PSLSPEC
  NAME "decide wait until" :=
  (always("clk")) -> (
    always(
      (!"verifying" & !"done") -> ((!"verifying" & !"done") until "sw_arvalid")
    )
  );


-------------------------
-- verification sequence:
-------------------------

INVARSPEC
  NAME "decide verify secure" :=
  (
    (
      "clk" & !"verifying" & !"done" & next( "sw_arvalid")
    )
  ->
    (
      next( "verifying")
    )
  );

PSLSPEC
  NAME "decide verify until" :=
  (always("clk")) -> (
    always(
      ("verifying") -> ("verifying" until "i_eq_end_addr")
    )
  );

INVARSPEC
  NAME "decide done secure" :=
  (
    (
      "clk" & !"verifying" & !"done" & next("i_eq_end_addr")
    )
  ->
    (
      next(!"done")
    )
  );


-----------------
-- done sequence:
-----------------

INVARSPEC
  NAME "decide done sound" :=
  (
    (
      "clk" & !"done" & "i_eq_end_addr"
    )
  ->
    (
      next("done")
    )
  );

PSLSPEC
  NAME "decide done until" :=
  (always("clk")) -> (
    always(
      ("done") -> ("done" until "deduced_PC_eq_CRmax")
    )
  );

INVARSPEC
  NAME "decide wait secure" :=
  (
    (
      "clk" & "done" & "deduced_PC_eq_CRmax" & next(!"i_eq_end_addr")
    )
  ->
    (
      next(!"done")
    )
  );
-- note: by design, we never have next("i_eq_end_addr") if we have
-- "deduced_PC_eq_CRmax". The trusted algorithm finishes when we reach CR_min.
-- So we can see this property as:
-- ("clk" & "done" & "deduced_PC_eq_CRmax") -> next(!"done")


----------------------------------
-- always in an expected sequence:
----------------------------------

INVARSPEC
  NAME "decide not verifying and done" :=
  !("verifying" & "done");


-----------------------
-- requests to the ROM:
-----------------------

INVARSPEC
  NAME "decide rom i init" :=
  (
    (
      "clk" & !"verifying" & !"done" & next("verifying")
    )
  ->
    (
      ("i" = 0uh10_000)
    )
  );

-- WARNING:
-- expression (next("i")) = ("i" + 0uh10_001) cannont be converted to coq.
INVARSPEC
  NAME "TO_CONVERT_MANUALLY rom i induction" :=
  (
    (
      "clk" & next("verifying")
    )
    ->
    (
      (next("i")) = ("i" + 0uh10_001)
    )
  );

LTLSPEC
  NAME "decide rom verifying init" :=
    (!"sw_arvalid") -> (!"verifying");

