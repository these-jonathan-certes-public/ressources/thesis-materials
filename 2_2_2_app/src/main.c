
/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "gpio.h"

/**
 * \brief
 *  Integer that refers to the input pin index: <tt> EMIO[1] </tt>. See details
 *  of \ref gpio_read().
 */
#define GPIO_INPUT_INDEX 55

/**
 * \brief
 *  Integer that refers to the output pin index: <tt> EMIO[0] </tt>. See details
 *  of \ref gpio_read().
 */
#define GPIO_OUTPUT_INDEX 54

/******************************************************************************/

/**
 * \brief
 *  GPIO output is set to the opposite value as the GPIO input.
 */
int main (
  void
) {
  uint32_t theInputValue;   // what is read on GPIO input

  gpio_setDirection(GPIO_OUTPUT_INDEX, 1);
  gpio_setOuputEnable(GPIO_OUTPUT_INDEX, 1);
  //
  gpio_setDirection(GPIO_INPUT_INDEX, 0);

  printf("Hello World.\n");

  while ( 1 ) {
    // sets the output to the opposite value as the input:
    theInputValue = gpio_read(GPIO_INPUT_INDEX);
    gpio_write(GPIO_OUTPUT_INDEX, !theInputValue);
  }

  return 0;
}

