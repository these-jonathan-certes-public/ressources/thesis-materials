(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

(******************************************************************************)
(******************************************************************************)

(**
 * Definition of all the predicates:
 *)

Axiom reset         : CV0 Bit (eq O).
Axiom PC_in_CR      : CV0 Bit (eq O).
Axiom PC_eq_CRmin   : CV0 Bit (eq O).
Axiom PC_eq_CRmax   : CV0 Bit (eq O).
Axiom irq           : CV0 Bit (eq O).
Axiom R_en          : CV0 Bit (eq O).
Axiom W_en          : CV0 Bit (eq O).
Axiom Daddr_in_KR   : CV0 Bit (eq O).
Axiom Daddr_in_XS   : CV0 Bit (eq O).
Axiom Daddr_in_MR   : CV0 Bit (eq O).
Axiom DMA_en        : CV0 Bit (eq O).
Axiom DMAaddr_in_KR : CV0 Bit (eq O).
Axiom DMAaddr_in_XS : CV0 Bit (eq O).

