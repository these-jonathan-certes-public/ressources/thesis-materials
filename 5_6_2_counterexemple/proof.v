(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.

(******************************************************************************)
(******************************************************************************)

(**
 * This is a property verified by a model that is unsound.
 * The purpose is just to provide a counterexample that using local properties
 * to prove the soundness is not sufficient.
 *)
Axiom unsound_model_prop:
  ∀ π : Path, π ⊨ G('reset).

(******************************************************************************)

(**
 * Since we always have reset, we can prove almost all local properties with
 * the same tactic.
 *)
Ltac same_tactic := (
  intros π;
  pose proof (unsound_model_prop π) as H;
  rewrite Thesis.toolbox.globally_to_state;
  rewrite Thesis.toolbox.globally_to_state in H;
  intros s;
  pose proof (H s) as H;
  right
).

Ltac same_tactic_next := (
  intros π;
  pose proof (unsound_model_prop π) as H;
  rewrite Thesis.toolbox.globally_to_state;
  rewrite Thesis.toolbox.globally_to_state in H;
  intros s;
  pose proof (H (s+1)) as H;
  right
).

(******************************************************************************)

Theorem LTL_2:
  ∀ π : Path, π ⊨ G(
    (negP ('PC_in_CR)) ∧ ('R_en) ∧ ('Daddr_in_KR)
  -->
    ('reset)
  ).
Proof.
  same_tactic; exact H.
Qed.

Theorem LTL_3:
  ∀ π : Path, π ⊨ G(
    (negP ('reset)) ∧ ('PC_in_CR) ∧ X(negP ('PC_in_CR))
  -->
    (
      ('PC_eq_CRmax) ∨ X('reset)
    )
  ).
Proof.
  same_tactic_next.
  right.
  simpl; simpl in H.
  rewrite LTL.path_plus_i_j.
  exact H.
Qed.

Theorem LTL_4:
  ∀ π : Path, π ⊨ G(
    (negP ('reset)) ∧ (negP ('PC_in_CR)) ∧ X('PC_in_CR)
  -->
    (
      X('PC_eq_CRmin) ∨ X('reset)
    )
  ).
Proof.
  same_tactic_next.
  right.
  simpl; simpl in H.
  rewrite LTL.path_plus_i_j.
  exact H.
Qed.

Theorem LTL_5:
  ∀ π : Path, π ⊨ G(
    ('irq) ∧ ('PC_in_CR)
  -->
    ('reset)
  ).
Proof.
  same_tactic; exact H.
Qed.

Theorem LTL_6:
  ∀ π : Path, π ⊨ G(
    (negP ('PC_in_CR)) ∧ (('R_en) ∨ ('W_en)) ∧ ('Daddr_in_XS)
  -->
    ('reset)
  ).
Proof.
  same_tactic; exact H.
Qed.

Theorem LTL_7:
  ∀ π : Path, π ⊨ G(
    ('PC_in_CR) ∧ ('W_en) ∧ (negP ('Daddr_in_XS)) ∧ (negP ('Daddr_in_MR))
  -->
    ('reset)
  ).
Proof.
  same_tactic; exact H.
Qed.

Theorem LTL_8:
  ∀ π : Path, π ⊨ G(
    ('DMA_en) ∧ ('DMAaddr_in_KR)
  -->
    ('reset)
  ).
Proof.
  same_tactic; exact H.
Qed.

Theorem LTL_9:
  ∀ π : Path, π ⊨ G(
    ('DMA_en) ∧ ('DMAaddr_in_XS)
  -->
    ('reset)
  ).
Proof.
  same_tactic; exact H.
Qed.

Theorem LTL_10:
  ∀ π : Path, π ⊨ G(
    ('PC_in_CR) ∧ ('DMA_en)
  -->
    ('reset)
  ).
Proof.
  same_tactic; exact H.
Qed.
