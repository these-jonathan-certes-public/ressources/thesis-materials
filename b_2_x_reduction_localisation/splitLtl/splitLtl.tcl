#!/bin/sh
# the next line restarts using tclsh \
exec tclsh "$0" ${1+"$@"}

#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

#===============================================================================

##
# \brief
#   Stores the variables that describe the file to split.
#
namespace eval splitltl {

  set theInputFile "stdin"
  set theText      ""
  set theDestination [file join "ltl_tmp" "ltlspec"]

  # List of SMV keywords. Comes from NnuSMV user manual.
  set theSmvKeywordsList {
    MODULE     DEFINE    MDEFINE   CONSTANTS VAR        IVAR       FROZENVAR
    INIT       TRANS     INVAR     SPEC      CTLSPEC    LTLSPEC    PSLSPEC
    COMPUTE    INVARSPEC FAIRNESS  JUSTICE   COMPASSION ISA        ASSIGN
    CONSTRAINT SIMPWFF   CTLWFF    LTLWFF    PSLWFF     COMPWFF    IN
    MIN        MAX       MIRROR    PRED      PREDICATES
  }

  ##
  # activates the expansion of PSL forall operator:
  #
  set doExpand 0
  #
  # this is the regular expression, it must match 3 substrings, the iteration
  # variable and two digits, respectively the lower and the upper bounds.
  #
  set theExpandRegexp \
    {^\s*forall\s+(\S+)\s+in\s+\{\s*([[:digit:]]+)\s*:\s*([[:digit:]]+)\s*\}}

}

#===============================================================================

##
# \brief
#   Prints help to the standard output and then exits.
#
proc splitltl::printHelpAndExit {
  {theCode 0}
} {
  puts "Split LTLSPEC into seprated files:

SYNOPSIS:
  ./splitLtl.tcl \[FILE\]

DESCRIPTION:
  Reads a SMV file containing LTLSPEC keywords. For each LTL specification,
  an output file is created, starting with \"MODULE main\" and containing the
  LTL spectification.

  Output files destinations are :
    ${splitltl::theDestination}\${i}
  where \${i} is an integer that is incremented for each file.

  This script expects one argument, the path to the SMV file. If no argument
  is given, it reads standard input.

OPTIONS:
  -e OR --expand : activates expansion of PSL forall operator. When this option
                   is activated, an output file is generated foreach possibily
                   described by the interval. In each output, the iterval is
                   replaced by a single integer.

  -c OR --clean  : removes the folder where output files are created and exits

  -h OR --help   : prints this help and exits

EXAMPLES:
  ./splitLtl.tcl --expand inputfile.smv

  cat inputfile.smv | ./splitLtl.tcl
"

  exit $theCode
}

#===============================================================================

##
# \brief
#   Parses the vector of arguments and updates the variables in namespace \ref
#   splitltl.
#
# \details
#   More informations about the syntax are given in the text printed by \ref
#   splitltl::printHelpAndExit.
#
# \param  argv  Vector of arguments.
#
proc splitltl::parseArgv {
  argv
} {
  foreach theArg [string trim $argv] {
    if { [string first "-" $theArg] == 0 } {
      switch $theArg {
        "-e" - "--expand" { set splitltl::doExpand 1 }
        "-c" - "--clean"  {
          set theDir [file dirname ${splitltl::theDestination}]
          if { [file isdirectory $theDir] } {
            puts "Removing folder: $theDir"
            file delete -force     $theDir
          }
          exit 0
        }
        "-h" - "--help"   { splitltl::printHelpAndExit }
        default  {
          puts "** Error ** unable to parse argument: $theArg"
          splitltl::printHelpAndExit 1
        }
      }
    } else {
      set splitltl::theInputFile $theArg
    }
  }

  # reading the input file:
  if { ${splitltl::theInputFile} == "stdin" } {
    set splitltl::theText [read "stdin"]
  } else {
    if { ![file exists ${splitltl::theInputFile}] } {
      puts "** Error ** input file does not exist: ${splitltl::theInputFile}"
      exit 1
    }
    #
    set theCode [catch {
      set    theBuffer         [open ${splitltl::theInputFile} "r"]
      set    splitltl::theText [read $theBuffer]
      close $theBuffer
    } theReturn]
    #
    if { $theCode != 0 } {
      puts "** Error ** $theReturn"
      exit $theCode
    } else {
      puts "Reading file: ${splitltl::theInputFile}"
    }
  }

  return 0
}

#===============================================================================

##
# \brief
#   Creates a non-existing output file and prints some text in it.
#
# \details
#   Suffixes the file name with an integer that is incremented each time the
#   file exists.
#
# \param  theFile   Path to access the file to create.
#
# \param  theText   Text to be printed in the file.
#
proc splitltl::createOutputFile {
  theFile
  theText
} {
  set theParentFolder [file dirname [file normalize $theFile]]
  if { ![file isdirectory $theParentFolder] } {
    set theCode [catch { file mkdir $theParentFolder } theReturn]
    if { $theCode != 0 } {
      puts "** Error ** $theReturn"
      return $theCode
    }
  }

  set theFileIndex 0
  while { [file isfile "${theFile}${theFileIndex}"] } {
    incr theFileIndex
  }

  set theCode [catch {
    set    theBuffer [open "${theFile}${theFileIndex}" "w"]
    puts  $theBuffer ${theText}
    close $theBuffer
  } theReturn]
  #
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
  } else {
    puts "Creation of file: ${theFile}${theFileIndex}"
  }
  return $theCode
}

#===============================================================================

##
# \brief
#   Parses the input text and, for each LTLSPEC, PSLSPEC and CTLSPEC content,
#   concatenates it with the rest of the text (without other specifications) and
#   calls function \ref splitltl::createOutputFile.
#
proc splitltl::parseAndSplit {
} {
  set theCode [catch {
    file delete -force [file dirname ${splitltl::theDestination}]
  } theReturn]
  #
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
  }

  #
  set theSpecList [list]
  #
  set theText "" ;# to store the content of the text without the specifications
  set theSpec "" ;# to store the content of one specification
  set isSpec  0  ;# boolean, true if the line must be added to the specification
  #
  append theText "MODULE main\n"
  #
  foreach theLine [split ${splitltl::theText} "\n"] {
    foreach theSmvKeyword ${splitltl::theSmvKeywordsList} {
      if { [lindex [split $theLine] 0] == $theSmvKeyword } {
        # new keyord
        if { [string trim $theSpec] != "" } {
          # end of one specification
          if { $splitltl::doExpand != 0 &&
  [regexp -lineanchor $splitltl::theExpandRegexp $theSpec match var start stop]
             } {
            for { set i $start } { $i <= $stop } { incr i } {
              set theNewSpec \
                [string map [list "$match" "forall $var in \{$i\}"] $theSpec]
              lappend theSpecList $theNewSpec
            }
          } else {
            lappend theSpecList $theSpec
          }
          set theSpec ""
        }
        if { $theSmvKeyword == "LTLSPEC"   ||
             $theSmvKeyword == "PSLSPEC"   ||
             $theSmvKeyword == "CTLSPEC"   ||
             $theSmvKeyword == "INVARSPEC" } {
          set isSpec 1
        } else {
          set isSpec 0
        }
        break
      }
    }
    #
    if { $isSpec == 1 } {
      append theSpec "\n" $theLine
    } else {
      append theText "\n" $theLine
    }
  }

  # end of input file:
  if { [string trim $theSpec] != "" } {
    if { $splitltl::doExpand != 0 &&
  [regexp -lineanchor $splitltl::theExpandRegexp $theSpec match var start stop]
       } {
      for { set i $start } { $i <= $stop } { incr i } {
        set theNewSpec \
          [string map [list "$match" "forall $var in \{$i\}"] $theSpec]
        lappend theSpecList $theNewSpec
      }
    } else {
      lappend theSpecList $theSpec
    }
  }

  foreach theSpec $theSpecList {
    splitltl::createOutputFile ${splitltl::theDestination} \
                              "${theText}${theSpec}"
  }

  return 0
}

#===============================================================================
#===============================================================================

splitltl::parseArgv $::argv
splitltl::parseAndSplit

