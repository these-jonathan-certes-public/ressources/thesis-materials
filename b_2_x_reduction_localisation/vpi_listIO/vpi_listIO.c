/**
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <vpi_user.h>
#include <stdlib.h>

/**
 * \brief
 *  Here is a VPI module that uses the simulator to get the list of inputs and
 *  outputs for top modules in verilog.
 *
 * \details
 *  When loaded, it prints to standard output the list of ports of every top
 *  modules. This output is formated as accepeted as arguments for script
 *  createVerilogWrapper.tcl. Simulation does not run as this module exits when
 *  all data is printed.
 */

/******************************************************************************/

/**
 * \brief
 *  Prints to standard output the list of ports of every top modules.
 */
static int32_t CFunc_listPorts(
  p_cb_data theCallbackDataPtr //!< pointer to the callback data
) {
  vpiHandle theModuleIterator, theModuleHandle;
  vpiHandle theNetIterator, thePortHandle;
  PLI_INT32 thePortDirection, thePortSize;

  // make sure this function is not called when not supposed to:
  if ( theCallbackDataPtr->reason != cbEndOfCompile ) {
    return 1;
  }

  // iterate on all the top modules:
  theModuleIterator = vpi_iterate( vpiModule, NULL );
  if ( theModuleIterator != NULL ) {
    for ( theModuleHandle  = vpi_scan(theModuleIterator);
          theModuleHandle != NULL;
          theModuleHandle  = vpi_scan(theModuleIterator)
        ) {
      vpi_printf( "--module %s ", vpi_get_str(vpiName, theModuleHandle) );

      // iterate on all the ports:
      theNetIterator = vpi_iterate(vpiPort, theModuleHandle);
      if ( theNetIterator != NULL ) {
        for ( thePortHandle  = vpi_scan(theNetIterator);
              thePortHandle != NULL;
              thePortHandle  = vpi_scan(theNetIterator)
            ) {
          thePortDirection = vpi_get(vpiDirection, thePortHandle);
          switch ( thePortDirection ) {
            case vpiInput:
              vpi_printf( "--input ");
              break;
            default:
              vpi_printf( "--output ");
              break;
          }
          vpi_printf( "\"%s", vpi_get_str(vpiName, thePortHandle) );
          //
          thePortSize = vpi_get(vpiSize, thePortHandle);
          if ( thePortSize > 1 ) {
            vpi_printf( "[%d:0]", thePortSize-1 );
          }
          vpi_printf("\" ");
        }
      }
      vpi_printf("\n");

    }
  }

  /**
   * No need for a cleanup:
   * The iterator object shall automatically be freed when vpi_scan() returns
   * NULL.
   */

  // do not run the simulation:
  exit(0);
}

/******************************************************************************/

/**
 * \brief
 *  Adds callback routines.
 */
void register_callbacks(
  void
) {
  s_cb_data theCallbackData;
  theCallbackData.reason    = cbEndOfCompile;
  theCallbackData.cb_rtn    = CFunc_listPorts;
  theCallbackData.user_data = 0;
  vpi_register_cb( &theCallbackData );
}

/******************************************************************************/

/**
 * \brief
 *  Places the register function in the externally visible
 *  vlog_startup_routines[] array.
 *
 * \details
 *  Contains a zero-terminated list of functions that have to be called at
 *  startup.
 */
void (*vlog_startup_routines[])( void ) = {
  register_callbacks,
  0
};

/******************************************************************************/

