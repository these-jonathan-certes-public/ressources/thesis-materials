/**
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

module hardware(
  input        clk,   //!< clock, active at rising edge
  input  [7:0] in_1,  //!< input data
  output [7:0] out,   //!< latch of input data
  output [7:0] copy   //!< copy  of input data
);
  reg [7:0] register;

  always @( posedge(clk) ) begin
    register <= in_1;
  end

  assign out  = register;
  assign copy = in_1;
endmodule

