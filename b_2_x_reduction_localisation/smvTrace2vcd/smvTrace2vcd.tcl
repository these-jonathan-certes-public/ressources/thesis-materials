#!/bin/sh
# the next line restarts using tclsh \
exec tclsh "$0" ${1+"$@"}

#
# Copyright (C) 2022 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

#===============================================================================

##
# \brief
#   Stores the program enviromnement variables.
#
namespace eval tracevcd {

  ##
  # \brief
  #   Path to the input file or "stdin". This variable is set by function \ref
  #   tracevcd::parseArgv.
  #
  set theInputFile "stdin"
  ##
  # \brief
  #   Content of the input file. This variable is set by function \ref
  #   tracevcd::parseArgv.
  #
  set theInputText ""

  ##
  # \brief
  #   Path to the output file or "stdout". This variable is set by function \ref
  #   tracevcd::parseArgv.
  #
  set theOutputFile "stdout"
  ##
  # \brief
  #   Content of the output text.
  #
  set theOutputText ""

  ##
  # \brief
  #   Remembers the number of vcd variables to pick the next identifier in \ref
  #   vcd_identifierList.
  #
  set vcd_index 0

  ##
  # \brief
  #   List of ASCII identifiers to assign vcd variables to.
  #
  # \details
  #   Each variable is assigned an arbitrary, compact ASCII identifier for use
  #   in the value change section. The identifier is composed of printable ASCII
  #   characters from ! to ~ (decimal 33 to 126).
  #   This variable is set by function \ref tracevcd::init.
  #
  set vcd_identifierList [list]

  ##
  # \brief
  #   Names of input, output and loop signals that are found in the trace.
  #
  # \details
  #   These variables are set by function \ref tracevcd::parseInput.
  #
  set inputSignals  [list]
  set outputSignals [list]
  set loopSignals   [list]

  ##
  # \brief
  #   One variable is added to the vcd to keep track of the state. This is its
  #   ASCII identifier.
  #
  # \details
  #   This variable is set by function \ref tracevcd::parseInput.
  #
  set theStateIdentifier "~"

}

#===============================================================================

##
# \brief
#   Initializes namespace \ref tracevcd.
#
proc tracevcd::init {
} {
  # creation of vcd identifiers:
  for { set i 33 } { $i <= 126 } { incr i } {
    set theChar [format "%c" $i]
    lappend tracevcd::vcd_identifierList $theChar
  }
}

#===============================================================================

##
# \brief
#   Prints help to the standard output and then exits.
#
proc tracevcd::printHelpAndExit {
  {theCode 0}
} {
  puts "Converts SMV counterexample traces to VCD

SYNOPSIS:
  ./smvTrace2vcd.tcl \[--from FILE1\] \[--to FILE2\]

DESCRIPTION:
  Parses the ouptut of NuSMV to convert its counterexample traces into Value
  Change Dump (VCD).
  This script expects arguments to define both its input and its output. If none
  is given, input is read from stdin an output is written to stdout.

OPTIONS:
  -f OR --from : the argument following this option gives a path to the file to
                 read SMV counterexample traces from. If this argument is
                 ommited, the SMV counterexample traces are read from standard
                 input.

  -t OR --to   : the argument following this option gives a path to the file to
                 print VCD syntax to. If this argument is omitted, the VCD is
                 printed to the standard output.

  -h OR --help : prints this help and exits

EXAMPLE:
  NuSMV model.smv | ./smvTrace2vcd.tcl --to output.vcd
"
  exit $theCode
}

#===============================================================================

##
# \brief
#   Parses the vector of arguments and updates the variables in namespace \ref
#   tracevcd.
#
# \details
#   More informations about the syntax are given in the text printed by \ref
#   tracevcd::printHelpAndExit.
#
# \param  argv  Vector of arguments.
#
proc tracevcd::parseArgv {
  argv
} {
  set previousArg ""
  foreach theArg [string trim $argv] {
    if { [string first "-" $theArg] == 0 } {
      switch $theArg {
        "-f" - "--from"   { set previousArg "from" }
        "-t" - "--to"     { set previousArg "to"   }
        "-h" - "--help"   { tracevcd::printHelpAndExit }
        default  {
          puts "** Error ** unable to parse argument: $theArg"
          tracevcd::printHelpAndExit 1
        }
      }
    } else {
      switch $previousArg {
        "from"    { set tracevcd::theInputFile $theArg
                    set previousArg ""
                  }
        "to"      { set tracevcd::theOutputFile $theArg
                    set previousArg ""
                  }
        default  {
          puts "** Error ** unable to parse argument: $previousArg $theArg"
          tracevcd::printHelpAndExit 2
        }
      }
    }
  }

  # reading the input file:
  if { ${tracevcd::theInputFile} == "stdin" } {
    set tracevcd::theInputText [read "stdin"]
  } else {
    if { ![file exists ${tracevcd::theInputFile}] } {
      puts "** Error ** input file does not exist: ${tracevcd::theInputFile}"
      exit 1
    }
    #
    set theCode [catch {
      set    theBuffer         [open ${tracevcd::theInputFile} "r"]
      set    tracevcd::theInputText [read $theBuffer]
      close $theBuffer
    } theReturn]
    #
    if { $theCode != 0 } {
      puts "** Error ** $theReturn"
      exit $theCode
    } else {
      puts "Reading file: ${tracevcd::theInputFile}"
    }
  }

  return 0
}

#===============================================================================

##
# \brief
#   From one line of SMV output trace, gets the signal name, size and value.
#
# \details
#   Also adds the identifier, either:
#    - if the signal is one from global variable lists \ref
#      tracevcd::inputSignals and \ref tracevcd::outputSignals
#    - else, by looking into global variable \ref tracevcd::vcd_identifierList
#      at index from global variable \ref tracevcd::vcd_index.
#
# \returns
#   a list containing the size, the identifier, the name and the value.
#
# \param
#   theLine, one line from SMV output trace
#
proc tracevcd::parseSignal {
  theLine
} {
  set theLine [string trim $theLine]
  set theName [lindex [split $theLine] 0]
  set theName [string map { "\"" "" } $theName]
  #
  set theSize  1
  set theValue [lindex $theLine 2]
  switch -glob $theValue {
    "TRUE"    { set theValue 1 }
    "FALSE"   { set theValue 0 }
    "0ud*_*"  {
                set theSize  [string range $theValue 3 end]
                set theValue [lindex [split $theSize "_"] 1]
                set theSize  [lindex [split $theSize "_"] 0]
              }
    default   { set theValue "x" ; set theSize 1 }
  }
  #
  set theIdentifier [lindex $tracevcd::vcd_identifierList $tracevcd::vcd_index]
  foreach theSignal [concat $tracevcd::inputSignals $tracevcd::outputSignals] {
    if { [lindex $theSignal 2] == $theName } {
      set theIdentifier [lindex $theSignal 1]
      break
    }
  }

  return [list $theSize $theIdentifier $theName $theValue]
}

#===============================================================================

##
# \brief
#   Parses input text from \ref \ref tracevcd::theInputText and updates
#   variables \ref tracevcd::inputSignals, \ref tracevcd::outputSignals and \ref
#   tracevcd::loopSignals.
#
proc tracevcd::parseInput {
} {
  set theState "0.0"  ;# index in the SMV trace
  set theInput "0.0"  ;# index in the SMV trace
  set theLoopIndex 0
  #
  set isLineIgnored 1 ;# ignore the beginning of the input file
  foreach theLine [split $tracevcd::theInputText "\n"] {
    set theLine [string trim $theLine]

    if { $isLineIgnored } {
      if { $theLine == "Trace Type: Counterexample" } {
        set isLineIgnored 0
      }
      continue
    }

    if { $theLine == "" ||
         [string first "-- specification"   $theLine] == 0 ||
         [string first "-- as demonstrated" $theLine] == 0 } {
      continue
    }

    if { [string match "-> State: *" $theLine] } {
      set theState [lindex $theLine 2]

    } elseif { [string match "-> Input: *" $theLine] } {
      set theInput [lindex $theLine 2]

    } elseif { [string match "-- Loop starts here" $theLine] } {
      set theLoopIdentifier \
        [lindex $tracevcd::vcd_identifierList $tracevcd::vcd_index]
      lappend tracevcd::loopSignals \
        [list 1 $theLoopIdentifier "loop${theLoopIndex}" "x"]
      incr tracevcd::vcd_index
      incr theLoopIndex

    } elseif { [string first "=" $theLine] > 0 } {
      set theSignal [tracevcd::parseSignal $theLine]
      set theName   [lindex $theSignal 2]
      # define if we are parsing a state or an input, make sure we compare
      # integers since 1.9 > 1.10 but 10 > 9.
      set theState [string map { . "" } $theState]
      set theInput [string map { . "" } $theInput]
      if { $theState >= $theInput } {
        # parsing the outputs:
        if { [lsearch -exact -index 2 $tracevcd::outputSignals $theName] == -1
           } {
          lappend tracevcd::outputSignals $theSignal
          incr tracevcd::vcd_index
        }
      } else {
        # parsing the intputs:
        if { [lsearch -index 2 $tracevcd::inputSignals $theName] == -1 } {
          lappend tracevcd::inputSignals $theSignal
          incr tracevcd::vcd_index
        }
      }

    }
  }
  #
  set tracevcd::theStateIdentifier \
    [lindex $tracevcd::vcd_identifierList $tracevcd::vcd_index]
  incr tracevcd::vcd_index
}

#===============================================================================

##
# \brief
#   Appends to \ref tracevcd::theOutputText a line containing a signal
#   declaration following *.vcd file syntax.
#
# \param
#   theSignal, list containing the size, the identifier, the name and the value;
#   i.e. what is returned by function \ref tracevcd::parseSignal.
#
# \param
#   theVarType, the *.vcd variable type ; i.e integer, reg, wire...
#
proc tracevcd::appendSignalDeclaration {
  theSignal
  theVarType
} {
  set theSize       [lindex $theSignal 0]
  set theIdentifier [lindex $theSignal 1]
  set theName       [lindex $theSignal 2]
  append tracevcd::theOutputText "\$var $theVarType"
  if { $theSize       != "" } {
    append tracevcd::theOutputText " $theSize"
  }
  if { $theIdentifier != "" } {
    append tracevcd::theOutputText " $theIdentifier"
  }
  if { $theName       != "" } {
    append tracevcd::theOutputText " $theName"
  }
  if { $theSize > 1 } {
    append tracevcd::theOutputText " \[[expr $theSize-1]:0\]"
  }
  append tracevcd::theOutputText " \$end\n"
}

#===============================================================================

##
# \brief
#   Appends to \ref tracevcd::theOutputText a VCD header.
#
# \details
#   Iterates on all signals from lists \ref tracevcd::inputSignals,
#   tracevcd::outputSignals and tracevcd::loopSignals, calls function \ref
#   tracevcd::appendSignalDeclaration for each of them.
#
proc tracevcd::appendHeader {
} {
  set theStateName "SMV_State"
  set timescale    "1ns"

  append tracevcd::theOutputText "\$date
  \t[clock format [clock seconds] -format {%A %b %d %H:%M:%S %Y}]
  \$end
  \$version
  \tSMV traces to VCD
  \$end
  \$timescale
  \t$timescale
  \$end
  \$scope module inputs \$end
  "

  tracevcd::appendSignalDeclaration \
    [list 32 $tracevcd::theStateIdentifier $theStateName "x"] "integer"
  foreach theSignal $tracevcd::inputSignals {
    tracevcd::appendSignalDeclaration $theSignal "reg"
  }

  append tracevcd::theOutputText "\$upscope \$end\n"
  append tracevcd::theOutputText "\$scope module outputs \$end\n"
  foreach theSignal $tracevcd::outputSignals {
    tracevcd::appendSignalDeclaration $theSignal "wire"
  }

  append tracevcd::theOutputText "\$upscope \$end\n"
  append tracevcd::theOutputText "\$scope module loops \$end\n"
  foreach theSignal $tracevcd::loopSignals {
    tracevcd::appendSignalDeclaration $theSignal "reg"
  }

  append tracevcd::theOutputText "\$upscope \$end\n"
  append tracevcd::theOutputText "\$enddefinitions \$end\n"
}

#===============================================================================

##
# \brief
#   Reads \ref \ref tracevcd::theinputText and appends accordingly to \ref
#   tracevcd::theOutputText the value changes.
#
proc tracevcd::appendValueChanges {
} {
  set theTime 1
  append tracevcd::theOutputText "#${theTime}0\n"

  set isLineIgnored  1 ;# ignore the beginning of the input file
  set theLoopIndex   0
  set loopStartsHere 0 ;# boolean to make a loop start at the next state change

  foreach theLine [split $tracevcd::theInputText "\n"] {
    set theLine [string trim $theLine]
    if { $isLineIgnored } {
      if { $theLine == "Trace Type: Counterexample" } {
        set isLineIgnored 0
      }
      continue
    }

    if { $theLine == "" ||
         [string first "-- specification"   $theLine] == 0 ||
         [string first "-- as demonstrated" $theLine] == 0 } {
      continue
    }

    if { [string match "-- Loop starts here" $theLine] } {
      set theSignal     [lindex $tracevcd::loopSignals $theLoopIndex]
      set theIdentifier [lindex $theSignal 1]
      incr theLoopIndex
      set loopStartsHere 1

    } elseif { [string match "-> State: *" $theLine] } {
      set theTime [lindex $theLine 2]
      set theTime [lindex [split $theTime "."] 1]
      append tracevcd::theOutputText "#${theTime}0\n"
      # print the state changes:
      append tracevcd::theOutputText \
        "b[format "%b" ${theTime}] $tracevcd::theStateIdentifier\n"
      if { $loopStartsHere } {
        append tracevcd::theOutputText "1${theIdentifier}\n"
        set loopStartsHere 0
      }

    } elseif { [string match "-> Input: *" $theLine] } {
      append tracevcd::theOutputText "#${theTime}9\n"

    } elseif { [string first "=" $theLine] > 0 } {
      set theSignal [tracevcd::parseSignal $theLine]
      set theSize       [lindex $theSignal 0]
      set theIdentifier [lindex $theSignal 1]
      set theValue      [lindex $theSignal 3]
      if { $theSize > 1 } {
        append tracevcd::theOutputText \
          "b[format "%b" ${theValue}] $theIdentifier\n"
      } else {
        append tracevcd::theOutputText "${theValue}${theIdentifier}\n"
      }
    }
  }
  append tracevcd::theOutputText "#[expr {${theTime} + 1}]0\n"
}

#===============================================================================
#===============================================================================

tracevcd::init
if { [info exists ::argv] } {
  tracevcd::parseArgv $::argv
}
tracevcd::parseInput
tracevcd::appendHeader
tracevcd::appendValueChanges

# prints the output:
if { $tracevcd::theOutputFile == "stdout" } {
  puts $tracevcd::theOutputText
} else {
  set theParentFolder [file dirname [file normalize ${tracevcd::theOutputFile}]]
  if { ![file isdirectory $theParentFolder] } {
    set theCode [catch { file mkdir $theParentFolder } theReturn]
    if { $theCode != 0 } {
      puts "** Error ** $theReturn"
      exit $theCode
    }
  }

  set theCode [catch {
    set    theBuffer [open ${tracevcd::theOutputFile} "w"]
    puts  $theBuffer $tracevcd::theOutputText
    close $theBuffer
  } theReturn]
  #
  if { $theCode != 0 } {
    puts "** Error ** $theReturn"
  } else {
    puts "Creation of file: ${tracevcd::theOutputFile}"
  }
}

