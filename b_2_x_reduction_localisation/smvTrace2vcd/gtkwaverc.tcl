#
# Copyright (C) 2021 Jonathan Certes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# this is a gtkwave init script

# identify the signals from the name of their parent in the hierarchy:
set theInputsList  [list]
set theLoopsList   [list]
set theOutputsList [list]
#
for { set i 0 } { $i < [gtkwave::getNumFacs] } { incr i } {
  set theFacName [gtkwave::getFacName $i]
  if {       [string first "inputs." ${theFacName}] == 0 } {
    lappend theInputsList  ${theFacName}
  } elseif { [string first "loops."  ${theFacName}] == 0 } {
    lappend theLoopsList   ${theFacName}
  } else {
    lappend theOutputsList ${theFacName}
  }
}

# plot the signals:
gtkwave::/Edit/Insert_Comment "States:"
foreach signal ${theInputsList} {
  gtkwave::addSignalsFromList [list ${signal}]
  gtkwave::/Edit/Color_Format/Blue
  gtkwave::/Edit/Data_Format/Decimal
}
#
gtkwave::/Edit/Insert_Blank
gtkwave::/Edit/Insert_Comment "Loops:"
foreach signal ${theLoopsList} {
  gtkwave::addSignalsFromList [list ${signal}]
  gtkwave::/Edit/Color_Format/Red
}
#
gtkwave::/Edit/Insert_Blank
gtkwave::/Edit/Insert_Comment "State variables:"
foreach signal ${theOutputsList} {
  gtkwave::addSignalsFromList [list ${signal}]
  gtkwave::/Edit/Color_Format/Green
  if { [string first "data_size" ${signal}] < 0 } {
    gtkwave::/Edit/Data_Format/Binary
  } else {
    gtkwave::/Edit/Data_Format/Decimal
  }
}

# zoom to fit:
gtkwave::/Time/Zoom/Zoom_Full
