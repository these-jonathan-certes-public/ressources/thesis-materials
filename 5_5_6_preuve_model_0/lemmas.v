(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.proof_obligations.
Require Import Dependencies.transducer.

(******************************************************************************)

(**
 * Hypothesis for our model zero: merge both isync and branch.
 *)
Lemma merge_model_0_packet_ready:
  ∀ π : Path, π ⊨ G(
    (
        (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
      ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
    )
  -->
    ('packet_ready)
  ).
Proof.
  intros π.

  pose proof (model_0_branch_packet_ready π) as model_0_branch_packet_ready.
  pose proof (model_0_isync_packet_ready  π) as model_0_isync_packet_ready.
  rewrite Thesis.toolbox.simpl_globally in model_0_branch_packet_ready, model_0_isync_packet_ready.
  apply Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (model_0_branch_packet_ready s) as model_0_branch_packet_ready.
  pose proof (model_0_isync_packet_ready  s) as model_0_isync_packet_ready.
  destruct model_0_branch_packet_ready as [H_branch_a | H_branch_b],
           model_0_isync_packet_ready  as [H_isync_a  | H_isync_b].
  - simpl in H_branch_a, H_isync_a.
    left; simpl.
    split.
    + left; exact H_isync_a.
    + left; exact H_branch_a.
  - right; exact H_isync_b.
  - right; exact H_branch_b.
  - right; exact H_branch_b.
Qed.


(**
 * When signal "packet_ready" is set and the available packet is either an isync
 * or a branch packet, then the decompresser outputs the data with the address
 * in it.
 *)
Lemma merge_decompresser_proof_obligation:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
          (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
        ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
      )
    )
  -->
    (
        (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
      ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
    )
  ).
Proof.
  intros π.
  apply Thesis.toolbox.globally_impP_x2_to_disj with
    (a := ('packet_ready))
    (b := (('available_packet_eq_isync)  ∧ ('packet_address_in_CR)))
    (c := (('available_packet_eq_branch) ∧ ('packet_address_in_CR)))
    (d := (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR)))
    (e := (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))).

  split.
  - apply decompresser_in_isync_1.
  - apply decompresser_in_branch_1.
Qed.


(**
 * When signal "packet_ready" is set and the decompresser outputs the data with
 * the address in it, then the decoder outputs the decoded address.
 *
 * This is a full address and implicit bits are in the decoded address as well.
 *)
Lemma merge_decoder_proof_obligation:
  ∀ π : Path, π ⊨ G(
    (
      ('packet_ready) ∧ (
          (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
        ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
      )
    )
  -->
    (
        (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
      ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
    )
  ).
Proof.
  intros π.
  apply Thesis.toolbox.globally_impP_x2_to_disj with
    (a := ('packet_ready))
    (b := (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR)))
    (c := (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR)))
    (d := (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR)))
    (e := (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))).

  split.
  - apply decoder_in_isync_1.
  - apply decoder_in_branch_1.
Qed.

(******************************************************************************)

Lemma L_enter_merge_step_1:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
      ('packet_ready) ∧ (
          (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
        ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
      )
    )
  ).
Proof.
  intros π.
  apply Thesis.toolbox.globally_impP_conj_commutes.
  apply Thesis.toolbox.globally_impP_x2_to_conj with
    (a := (negP ('PC_in_CR) ∧ X('PC_in_CR)))
    (b := (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
        ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR)))
    (c := ('packet_ready)).

  split.
  - apply CoreSight_configuration_model_enter_CR.
  - apply merge_model_0_packet_ready.
Qed.

Lemma L_enter_merge_step_2:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
      ('packet_ready) ∧ (
          (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
        ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
      )
    )
  ).
Proof.
  intros π.
  pose proof (L_enter_merge_step_1 π) as H.

  assert (A0:
  π ⊨ G
    (
      (negP (' PC_in_CR) ∧ X (' PC_in_CR))
    -->
      (
        (
          (' packet_ready) ∧ (
              ((' available_packet_eq_isync)  ∧ (' packet_address_in_CR))
            ∨ ((' available_packet_eq_branch) ∧ (' packet_address_in_CR))
          )
        ) ∧ (
          (
              (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
            ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
          )
        )
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := (negP (' PC_in_CR) ∧ X (' PC_in_CR)) )
      (b := (
              (' packet_ready) ∧ (
                  ((' available_packet_eq_isync)  ∧ (' packet_address_in_CR))
                ∨ ((' available_packet_eq_branch) ∧ (' packet_address_in_CR))
              )
            ) )
      (c := (
                (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
              ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
            ) ).
    split.
    + apply H.
    + apply merge_decompresser_proof_obligation.

  - apply Thesis.toolbox.globally_impP_conj_commutes  in A0.
    apply Thesis.toolbox.globally_and_assoc_P in A0.
    apply Thesis.toolbox.globally_impP_conj_to_single in A0.
    apply Thesis.toolbox.globally_impP_conj_commutes  in A0.
    exact A0.
Qed.

Lemma L_enter_merge_step_3:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
      ('packet_ready) ∧ (
          (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
        ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
      )
    )
  ).
Proof.
  intros π.
  pose proof (L_enter_merge_step_2 π) as H.

  assert (A0:
  π ⊨ G
    (
      (negP (' PC_in_CR) ∧ X (' PC_in_CR))
    -->
      (
          (('packet_ready) ∧ (
              (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
            ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
          ))
        ∧ (
            (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
          ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
        )
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := (negP (' PC_in_CR) ∧ X (' PC_in_CR)) )
      (b := (('packet_ready) ∧ (
                (('decompressed_packet_eq_isync)  ∧ ('decompressed_address_in_CR))
              ∨ (('decompressed_packet_eq_branch) ∧ ('decompressed_address_in_CR))
            )) )
      (c := (
                (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
              ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
            ) ).
    split.
    + exact H.
    + apply merge_decoder_proof_obligation.

  - rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    rewrite Thesis.toolbox.globally_and_assoc_P in A0.
    apply   Thesis.toolbox.globally_impP_conj_to_single in A0.
    rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    exact A0.
Qed.

Lemma L_enter:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
      X('deduced_PC_in_CR)
    )
  ).
Proof.
  intros π.
  pose proof (L_enter_merge_step_3 π) as H.

  assert (A0:
  π ⊨ G
    (
      (negP (' PC_in_CR) ∧ X (' PC_in_CR))
    -->
      (
        (('packet_ready) ∧ (
            (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
          ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
        ))
        ∧ (X('deduced_PC_in_CR))
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := (negP (' PC_in_CR) ∧ X (' PC_in_CR)) )
      (b := (('packet_ready) ∧ (
                (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
              ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
            )) )
      (c := (X('deduced_PC_in_CR))).
    split.
    + exact H.

    (* here we must use the hypothesis that clk and nrst are always on *)
    + pose proof (_P_enter_ π) as H1.
      rewrite Thesis.toolbox.simpl_globally in H1.
      rewrite Thesis.toolbox.simpl_globally.
      intros s.
      pose proof (H1 s) as H2.
      destruct H2 as [H2a | H2b].
      * left.
        simpl in H2a.
        simpl.
        destruct H2a as [H2a | H2].
        -- destruct H2a as [H2a | H2b].

          (* case of clk is off *)
          ++ pose proof (clk_is_on) as clk_is_on.
            simpl in clk_is_on.
            pose proof (clk_is_on π) as clk_is_on.
            destruct clk_is_on as [clk_is_on_H | clk_is_on_False].
            ** pose proof (clk_is_on_H s) as clk_is_on_H.
              rewrite clk_is_on_H in H2a.
              discriminate.
            ** destruct clk_is_on_False as (s_False & H_False).
              destruct H_False as (H_False & H_exists).
              apply LTL.state_top in H_False.
              case H_False.

          (* case of nrst is off *)
          ++ pose proof (nrst_is_on) as nrst_is_on.
            simpl in nrst_is_on.
            pose proof (nrst_is_on π) as nrst_is_on.
            destruct nrst_is_on as [nrst_is_on_H | nrst_is_on_False].
            ** pose proof (nrst_is_on_H s) as clk_is_on_H.
              rewrite nrst_is_on_H in H2b.
              discriminate.
            ** destruct nrst_is_on_False as (s_False & H_False).
              destruct H_False as (H_False & H_exists).
              apply LTL.state_top in H_False.
              case H_False.

        -- exact H2.
      * right; exact H2b.

  - rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    apply   Thesis.toolbox.globally_impP_conj_to_single in A0.
    exact A0.
Qed.

(******************************************************************************)

Lemma L_exit_merge_step_1:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    )
  -->
    (
      ('packet_ready) ∧ (
        (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
      )
    )
  ).
Proof.
  intros π.
  apply Thesis.toolbox.globally_impP_conj_commutes.
  apply Thesis.toolbox.globally_impP_x2_to_conj with
    (a := ('PC_in_CR) ∧ (X(negP ('PC_in_CR))))
    (b := (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR))))
    (c := ('packet_ready)).

  split.
  - apply CoreSight_configuration_model_exit_CR.
  - pose proof (model_0_branch_packet_ready π) as H.
    rewrite Thesis.toolbox.simpl_globally in H.
    rewrite Thesis.toolbox.simpl_globally.
    intros s.
    pose proof (H s) as H.
    simpl in H.
    simpl.
    destruct H as [H | H].
    + left; left; exact H.
    + right; exact H.
Qed.


Lemma L_exit_merge_step_2:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    )
  -->
    (
      ('packet_ready) ∧ (
        (('decompressed_packet_eq_branch) ∧ (negP ('decompressed_address_in_CR)))
      )
    )
  ).
Proof.
  intros π.
  pose proof (L_exit_merge_step_1 π) as H.

  assert (A0:
  π ⊨ G
    (
      ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    -->
      (
        (
          (' packet_ready) ∧ (
            ((' available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
          )
        ) ∧ (
          (
            (('decompressed_packet_eq_branch) ∧ (negP ('decompressed_address_in_CR)))
          )
        )
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := ('PC_in_CR) ∧ (X(negP ('PC_in_CR))) )
      (b := (
              ('packet_ready) ∧ (
                (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
              )
            ) )
      (c := (
              (('decompressed_packet_eq_branch) ∧ (negP ('decompressed_address_in_CR)))
            ) ).
    split.
    + apply H.
    + apply decompresser_out_branch_1.

  - apply Thesis.toolbox.globally_and_assoc_P in A0.
    apply Thesis.toolbox.globally_impP_conj_commutes  in A0.
    apply Thesis.toolbox.globally_and_assoc_P in A0.
    apply Thesis.toolbox.globally_impP_conj_commutes  in A0.
    apply Thesis.toolbox.globally_impP_conj_to_single in A0.
    apply Thesis.toolbox.globally_impP_conj_commutes  in A0.
    exact A0.
Qed.


Lemma L_exit_merge_step_3:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    )
  -->
    (
      ('packet_ready) ∧ (
        (('decompressed_packet_eq_branch) ∧ (negP ('decoded_address_in_CR)))
      )
    )
  ).
Proof.
  intros π.
  pose proof (L_exit_merge_step_2 π) as H.

  assert (A0:
  π ⊨ G
    (
      ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    -->
      (
          (('packet_ready) ∧ (
            (('decompressed_packet_eq_branch) ∧ (negP ('decompressed_address_in_CR)))
          ))
        ∧ (
          (('decompressed_packet_eq_branch) ∧ (negP ('decoded_address_in_CR)))
        )
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := ('PC_in_CR) ∧ (X(negP ('PC_in_CR))) )
      (b := (('packet_ready) ∧ (
              (('decompressed_packet_eq_branch) ∧ (negP ('decompressed_address_in_CR)))
            )) )
      (c := (
              (('decompressed_packet_eq_branch) ∧ (negP ('decoded_address_in_CR)))
            ) ).
    split.
    + exact H.
    + apply decoder_out_branch_1.

  - rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    rewrite Thesis.toolbox.globally_and_assoc_P in A0.
    apply   Thesis.toolbox.globally_impP_conj_to_single in A0.
    rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    exact A0.
Qed.


Lemma L_exit:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    )
  -->
    (
      X(negP ('deduced_PC_in_CR))
    )
  ).
Proof.
  intros π.
  pose proof (L_exit_merge_step_3 π) as H.

  assert (A0:
  π ⊨ G
    (
      ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    -->
      (
        (('packet_ready) ∧ (
          (('decompressed_packet_eq_branch) ∧ (negP ('decoded_address_in_CR)))
        ))
        ∧ (X(negP ('deduced_PC_in_CR)))
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := ('PC_in_CR) ∧ (X(negP ('PC_in_CR))) )
      (b := (('packet_ready) ∧ (
              (('decompressed_packet_eq_branch) ∧ (negP ('decoded_address_in_CR)))
            )) )
      (c := (X(negP ('deduced_PC_in_CR)))).
    split.
    + exact H.

    (* here we must use the hypothesis that clk and nrst are always on *)
    + pose proof (_P_exit_ π) as H1.
      rewrite simpl_globally in H1.
      rewrite simpl_globally.
      intros s.
      pose proof (H1 s) as H2.
      destruct H2 as [H2a | H2b].
      * left.
        simpl in H2a.
        simpl.
        destruct H2a as [H2a | H2].
        -- destruct H2a as [H2a | H2b].

          (* case of clk is off *)
          ++ pose proof (clk_is_on) as clk_is_on.
            simpl in clk_is_on.
            pose proof (clk_is_on π) as clk_is_on.
            destruct clk_is_on as [clk_is_on_H | clk_is_on_False].
            ** pose proof (clk_is_on_H s) as clk_is_on_H.
              rewrite clk_is_on_H in H2a.
              discriminate.
            ** destruct clk_is_on_False as (s_False & H_False).
              destruct H_False as (H_False & H_exists).
              apply LTL.state_top in H_False.
              case H_False.

          (* case of nrst is off *)
          ++ pose proof (nrst_is_on) as nrst_is_on.
            simpl in nrst_is_on.
            pose proof (nrst_is_on π) as nrst_is_on.
            destruct nrst_is_on as [nrst_is_on_H | nrst_is_on_False].
            ** pose proof (nrst_is_on_H s) as clk_is_on_H.
              rewrite nrst_is_on_H in H2b.
              discriminate.
            ** destruct nrst_is_on_False as (s_False & H_False).
              destruct H_False as (H_False & H_exists).
              apply LTL.state_top in H_False.
              case H_False.

        -- exact H2.
      * right; exact H2b.

  - apply Thesis.toolbox.globally_and_assoc_P in A0.
    apply Thesis.toolbox.globally_impP_conj_commutes  in A0.
    apply Thesis.toolbox.globally_and_assoc_P in A0.
    apply Thesis.toolbox.globally_impP_conj_commutes  in A0.
    apply Thesis.toolbox.globally_impP_conj_to_single in A0.
    apply Thesis.toolbox.globally_impP_conj_to_single in A0.
    exact A0.
Qed.

(******************************************************************************)

(**
 * Here we remove both clk and nrst from the model-checked property, since both
 * are always on.
 *)
Lemma _P_in__clk_nrst:
  ∀ π : Path, π ⊨ G(
    (
      ('deduced_PC_in_CR) ∧
      (negP (
        ('packet_ready) ∧ (
            (('decompressed_packet_eq_isync)  ∧ (negP ('decoded_address_in_CR)))
          ∨ (('decompressed_packet_eq_branch) ∧ (negP ('decoded_address_in_CR)))
        )
      ))
    )
  -->
    (X ('deduced_PC_in_CR))
  ).
Proof.
  intros π.
  pose proof (_P_in_ π) as _P_in_.
  rewrite simpl_globally.
  rewrite simpl_globally in _P_in_.

  intros s.
  pose proof (_P_in_ s) as _P_in_.
  simpl.
  simpl in _P_in_.

  (* we show that cases where clk and nrst are false cannot occur *)
  pose proof (clk_is_on π)  as clk_is_on.
  pose proof (nrst_is_on π) as nrst_is_on.

  (* globally is a disjunction with a state_top in one side *)
  simpl in clk_is_on, nrst_is_on.
  destruct clk_is_on  as [clk_is_on  | H_false_clk],
           nrst_is_on as [nrst_is_on | H_false_nrst].
  2:{
    destruct H_false_nrst as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }
  2:{
    destruct H_false_clk as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }
  2:{
    destruct H_false_clk as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }

  pose proof (clk_is_on  s) as clk_is_on.
  pose proof (nrst_is_on s) as nrst_is_on.
  rewrite clk_is_on  in _P_in_.
  rewrite nrst_is_on in _P_in_.
  simpl in _P_in_.
  destruct _P_in_ as [H | H].
  repeat destruct H as [H | H].
  - discriminate.
  - discriminate.
  - left; left;  exact H.
  - left; right; exact H.
  - right;       exact H.
Qed.

(**
 * Here we abstract decompression and decoding since we consider them correct.
 *)
Lemma simpl__P_in_:
  ∀ π : Path, π ⊨ G(
    (
      ('deduced_PC_in_CR) ∧
      (negP (
        ('packet_ready) ∧ (
            (('available_packet_eq_isync)  ∧ (negP ('packet_address_in_CR)))
          ∨ (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
        )
      ))
    )
  -->
    (X ('deduced_PC_in_CR))
  ).
Proof.
  intros π.
  pose proof (_P_in__clk_nrst π) as _P_in__clk_nrst.
  rewrite simpl_globally.
  rewrite simpl_globally in _P_in__clk_nrst.

  intros s.
  pose proof (_P_in__clk_nrst s) as _P_in__clk_nrst.
  simpl.
  simpl in _P_in__clk_nrst.

  destruct _P_in__clk_nrst as [H | H].
  - destruct H as [H | H].
    + left; left; exact H.
    + left; right.
      destruct H as (H1 & H).
      split.
      * exact H1.
      * destruct H as [H | H].

        (* case of an isync packet with address not in CR *)
        -- left.
          destruct H as (H2 & H3).

          (* a decoded address for isync packets is the correct one *)
          pose proof (decoder_out_isync_2 π) as decoder_out_isync_2.
          rewrite simpl_globally in decoder_out_isync_2.
          pose proof (decoder_out_isync_2 s) as decoder_out_isync_2.
          simpl in decoder_out_isync_2.
          destruct decoder_out_isync_2 as [H_decoder_isync_equiv | H_decoder_isync_equiv].
          ++ rewrite H1 in H_decoder_isync_equiv.
            destruct H_decoder_isync_equiv as [H_decoder_isync_equiv | H_decoder_isync_equiv].
            ** discriminate.
            ** destruct H_decoder_isync_equiv as [H_decoder_isync_equiv | H_decoder_isync_equiv].
              --- rewrite H2 in H_decoder_isync_equiv.
                discriminate.
              --- rewrite H3 in H_decoder_isync_equiv.
                discriminate.

          ++ destruct H_decoder_isync_equiv as (H4 & H_decoder_isync_equiv).
            (* a decompressed address for isync packets is the correct one *)
            pose proof (decompresser_out_isync_2 π) as decompresser_out_isync_2.
            rewrite simpl_globally in decompresser_out_isync_2.
            pose proof (decompresser_out_isync_2 s) as decompresser_out_isync_2.
            simpl in decompresser_out_isync_2.
            destruct decompresser_out_isync_2 as [H_decompress_isync_equiv | H_decompress_isync_equiv].
            ** rewrite H1 in H_decompress_isync_equiv.
              destruct H_decompress_isync_equiv as [H_decompress_isync_equiv | H_decompress_isync_equiv].
              --- discriminate.
              --- destruct H_decompress_isync_equiv as [H_decompress_isync_equiv | H_decompress_isync_equiv].
                +++ rewrite H2 in H_decompress_isync_equiv.
                  discriminate.
                +++ rewrite H_decoder_isync_equiv in H_decompress_isync_equiv.
                  discriminate.
            ** exact H_decompress_isync_equiv.

        (* case of a branch packet with address not in CR *)
        -- right.
          destruct H as (H2 & H3).

          (* a decoded address for branch packets is the correct one *)
          pose proof (decoder_out_branch_2 π) as decoder_out_branch_2.
          rewrite simpl_globally in decoder_out_branch_2.
          pose proof (decoder_out_branch_2 s) as decoder_out_branch_2.
          simpl in decoder_out_branch_2.
          destruct decoder_out_branch_2 as [H_decoder_branch_equiv | H_decoder_branch_equiv].
          ++ rewrite H1 in H_decoder_branch_equiv.
            destruct H_decoder_branch_equiv as [H_decoder_branch_equiv | H_decoder_branch_equiv].
            ** discriminate.
            ** destruct H_decoder_branch_equiv as [H_decoder_branch_equiv | H_decoder_branch_equiv].
              --- rewrite H2 in H_decoder_branch_equiv.
                discriminate.
              --- rewrite H3 in H_decoder_branch_equiv.
                discriminate.

          ++ destruct H_decoder_branch_equiv as (H4 & H_decoder_branch_equiv).
            (* a decompressed address for branch packets is the correct one *)
            pose proof (decompresser_out_branch_2 π) as decompresser_out_branch_2.
            rewrite simpl_globally in decompresser_out_branch_2.
            pose proof (decompresser_out_branch_2 s) as decompresser_out_branch_2.
            simpl in decompresser_out_branch_2.
            destruct decompresser_out_branch_2 as [H_decompress_branch_equiv | H_decompress_branch_equiv].
            ** rewrite H1 in H_decompress_branch_equiv.
              destruct H_decompress_branch_equiv as [H_decompress_branch_equiv | H_decompress_branch_equiv].
              --- discriminate.
              --- destruct H_decompress_branch_equiv as [H_decompress_branch_equiv | H_decompress_branch_equiv].
                +++ rewrite H2 in H_decompress_branch_equiv.
                  discriminate.
                +++ rewrite H_decoder_branch_equiv in H_decompress_branch_equiv.
                  discriminate.
            ** exact H_decompress_branch_equiv.

  - right; exact H.
Qed.

Lemma L_in:
  ∀ π : Path, π ⊨ G(
    (
      ('deduced_PC_in_CR) ∧ (X ('PC_in_CR))
    )
  -->
    (
      X ('deduced_PC_in_CR)
    )
  ).
Proof.
  intros π.
  pose proof (simpl__P_in_ π) as simpl__P_in_.
  pose proof (CoreSight_configuration_model_in_CR π) as CoreSight_configuration_model_in_CR.
  pose proof (L_enter                             π) as L_enter.
  pose proof (Modified_VRASED_LTL5                π) as Modified_VRASED_LTL5.
  pose proof (reset_to_not_CR                     π) as reset_to_not_CR.
  rewrite simpl_globally.
  rewrite simpl_globally in
    simpl__P_in_,
    CoreSight_configuration_model_in_CR,
    L_enter,
    Modified_VRASED_LTL5,
    reset_to_not_CR.

  intros s.
  pose proof (simpl__P_in_ s) as simpl__P_in_.
  pose proof (CoreSight_configuration_model_in_CR s) as CoreSight_configuration_model_in_CR.
  pose proof (L_enter                             s) as L_enter.
  pose proof (Modified_VRASED_LTL5                s) as Modified_VRASED_LTL5.
  pose proof (reset_to_not_CR                     s) as reset_to_not_CR.
  simpl.
  simpl in
    simpl__P_in_,
    CoreSight_configuration_model_in_CR,
    L_enter,
    Modified_VRASED_LTL5,
    reset_to_not_CR.

  (* bazooka method, we try all possible cases for all predicates *)
  induction (path_sub (path_up π s) 0 (str0 Bit (eq O) irq)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) reset)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) available_packet_eq_branch)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) available_packet_eq_isync)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) packet_address_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) deduced_PC_in_CR)),
            (path_sub (path_up (path_up π s) 1) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up (path_up π s) 1) 0 (str0 Bit (eq O) deduced_PC_in_CR));
  try (right;       reflexivity);
  try (left; right; reflexivity);
  try (left; left;  reflexivity);

  (* we clean all hypothesis and look for contradictions *)
  ( Thesis.toolbox.simpl_all_bool_in simpl__P_in_;
    Thesis.toolbox.simpl_all_bool_in CoreSight_configuration_model_in_CR;
    Thesis.toolbox.simpl_all_bool_in L_enter;
    Thesis.toolbox.simpl_all_bool_in Modified_VRASED_LTL5;
    Thesis.toolbox.simpl_all_bool_in reset_to_not_CR
  );
  try (case simpl__P_in_);
  try (case CoreSight_configuration_model_in_CR);
  try (case L_enter);
  try (case Modified_VRASED_LTL5);
  try (case reset_to_not_CR);

  (* in some cases, the goal looks like: False -> Prop *)
  try (intros H;  case H);
  try (intros H0; case H0).
Qed.

(******************************************************************************)

(**
 * Here we remove both clk and nrst from the model-checked property, since both
 * are always on.
 *)
Lemma _P_out__clk_nrst:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('deduced_PC_in_CR)) ∧
      (negP (
        ('packet_ready) ∧ (
            (('decompressed_packet_eq_isync)  ∧ ('decoded_address_in_CR))
          ∨ (('decompressed_packet_eq_branch) ∧ ('decoded_address_in_CR))
        )
      ))
    )
  -->
    (X (negP ('deduced_PC_in_CR)))
  ).
Proof.
  intros π.
  pose proof (_P_out_ π) as _P_out_.
  rewrite simpl_globally.
  rewrite simpl_globally in _P_out_.

  intros s.
  pose proof (_P_out_ s) as _P_out_.
  simpl.
  simpl in _P_out_.

  (* we show that cases where clk and nrst are false cannot occur *)
  pose proof (clk_is_on π)  as clk_is_on.
  pose proof (nrst_is_on π) as nrst_is_on.

  (* globally is a disjunction with a state_top in one side *)
  simpl in clk_is_on, nrst_is_on.
  destruct clk_is_on  as [clk_is_on  | H_false_clk],
           nrst_is_on as [nrst_is_on | H_false_nrst].
  2:{
    destruct H_false_nrst as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }
  2:{
    destruct H_false_clk as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }
  2:{
    destruct H_false_clk as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
  }

  pose proof (clk_is_on  s) as clk_is_on.
  pose proof (nrst_is_on s) as nrst_is_on.
  rewrite clk_is_on  in _P_out_.
  rewrite nrst_is_on in _P_out_.
  simpl in _P_out_.
  destruct _P_out_ as [H | H].
  repeat destruct H as [H | H].
  - discriminate.
  - discriminate.
  - left; left;  exact H.
  - left; right; exact H.
  - right;       exact H.
Qed.


(**
 * Here we abstract decompression and decoding since we consider them correct.
 *)
Lemma simpl__P_out_:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('deduced_PC_in_CR)) ∧
      (negP (
        ('packet_ready) ∧ (
            (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
          ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
        )
      ))
    )
  -->
    (X (negP ('deduced_PC_in_CR)))
  ).
Proof.
  intros π.
  pose proof (_P_out__clk_nrst π) as _P_out__clk_nrst.
  rewrite simpl_globally.
  rewrite simpl_globally in _P_out__clk_nrst.

  intros s.
  pose proof (_P_out__clk_nrst s) as _P_out__clk_nrst.
  simpl.
  simpl in _P_out__clk_nrst.

  destruct _P_out__clk_nrst as [H | H].
  - destruct H as [H | H].
    + left; left; exact H.
    + left; right.
      destruct H as (H1 & H).
      split.
      * exact H1.
      * destruct H as [H | H].

        (* case of an isync packet with address not in CR *)
        -- left.
          destruct H as (H2 & H3).

          (* a decoded address for isync packets is the correct one *)
          pose proof (decoder_in_isync_2 π) as decoder_in_isync_2.
          rewrite simpl_globally in decoder_in_isync_2.
          pose proof (decoder_in_isync_2 s) as decoder_in_isync_2.
          simpl in decoder_in_isync_2.
          destruct decoder_in_isync_2 as [H_decoder_isync_equiv | H_decoder_isync_equiv].
          ++ rewrite H1 in H_decoder_isync_equiv.
            destruct H_decoder_isync_equiv as [H_decoder_isync_equiv | H_decoder_isync_equiv].
            ** discriminate.
            ** destruct H_decoder_isync_equiv as [H_decoder_isync_equiv | H_decoder_isync_equiv].
              --- rewrite H2 in H_decoder_isync_equiv.
                discriminate.
              --- rewrite H3 in H_decoder_isync_equiv.
                discriminate.

          ++ destruct H_decoder_isync_equiv as (H4 & H_decoder_isync_equiv).
            (* a decompressed address for isync packets is the correct one *)
            pose proof (decompresser_in_isync_2 π) as decompresser_in_isync_2.
            rewrite simpl_globally in decompresser_in_isync_2.
            pose proof (decompresser_in_isync_2 s) as decompresser_in_isync_2.
            simpl in decompresser_in_isync_2.
            destruct decompresser_in_isync_2 as [H_decompress_isync_equiv | H_decompress_isync_equiv].
            ** rewrite H1 in H_decompress_isync_equiv.
              destruct H_decompress_isync_equiv as [H_decompress_isync_equiv | H_decompress_isync_equiv].
              --- discriminate.
              --- destruct H_decompress_isync_equiv as [H_decompress_isync_equiv | H_decompress_isync_equiv].
                +++ rewrite H2 in H_decompress_isync_equiv.
                  discriminate.
                +++ rewrite H_decoder_isync_equiv in H_decompress_isync_equiv.
                  discriminate.
            ** exact H_decompress_isync_equiv.

        (* case of a branch packet with address not in CR *)
        -- right.
          destruct H as (H2 & H3).

          (* a decoded address for branch packets is the correct one *)
          pose proof (decoder_in_branch_2 π) as decoder_in_branch_2.
          rewrite simpl_globally in decoder_in_branch_2.
          pose proof (decoder_in_branch_2 s) as decoder_in_branch_2.
          simpl in decoder_in_branch_2.
          destruct decoder_in_branch_2 as [H_decoder_branch_equiv | H_decoder_branch_equiv].
          ++ rewrite H1 in H_decoder_branch_equiv.
            destruct H_decoder_branch_equiv as [H_decoder_branch_equiv | H_decoder_branch_equiv].
            ** discriminate.
            ** destruct H_decoder_branch_equiv as [H_decoder_branch_equiv | H_decoder_branch_equiv].
              --- rewrite H2 in H_decoder_branch_equiv.
                discriminate.
              --- rewrite H3 in H_decoder_branch_equiv.
                discriminate.

          ++ destruct H_decoder_branch_equiv as (H4 & H_decoder_branch_equiv).
            (* a decompressed address for branch packets is the correct one *)
            pose proof (decompresser_in_branch_2 π) as decompresser_in_branch_2.
            rewrite simpl_globally in decompresser_in_branch_2.
            pose proof (decompresser_in_branch_2 s) as decompresser_in_branch_2.
            simpl in decompresser_in_branch_2.
            destruct decompresser_in_branch_2 as [H_decompress_branch_equiv | H_decompress_branch_equiv].
            ** rewrite H1 in H_decompress_branch_equiv.
              destruct H_decompress_branch_equiv as [H_decompress_branch_equiv | H_decompress_branch_equiv].
              --- discriminate.
              --- destruct H_decompress_branch_equiv as [H_decompress_branch_equiv | H_decompress_branch_equiv].
                +++ rewrite H2 in H_decompress_branch_equiv.
                  discriminate.
                +++ rewrite H_decoder_branch_equiv in H_decompress_branch_equiv.
                  discriminate.
            ** exact H_decompress_branch_equiv.

  - right; exact H.
Qed.


Lemma L_out:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('deduced_PC_in_CR)) ∧ (X (negP ('PC_in_CR)))
    )
  -->
    (
      X (negP ('deduced_PC_in_CR))
    )
  ).
Proof.
  intros π.
  pose proof (simpl__P_out_ π) as simpl__P_out_.
  pose proof (CoreSight_configuration_model_out_CR π) as CoreSight_configuration_model_out_CR.
  pose proof (L_exit                               π) as L_exit.
  rewrite simpl_globally.
  rewrite simpl_globally in
    simpl__P_out_,
    CoreSight_configuration_model_out_CR,
    L_exit.

  intros s.
  pose proof (simpl__P_out_ s) as simpl__P_out_.
  pose proof (CoreSight_configuration_model_out_CR s) as CoreSight_configuration_model_out_CR.
  pose proof (L_exit                               s) as L_exit.
  simpl.
  simpl in
    simpl__P_out_,
    CoreSight_configuration_model_out_CR,
    L_exit.

  (* bazooka method, we try all possible cases for all predicates *)
  induction (path_sub (path_up π s) 0 (str0 Bit (eq O) available_packet_eq_branch)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) available_packet_eq_isync)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) packet_address_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) deduced_PC_in_CR)),
            (path_sub (path_up (path_up π s) 1) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up (path_up π s) 1) 0 (str0 Bit (eq O) deduced_PC_in_CR));
  try (right;       reflexivity);
  try (left; right; reflexivity);
  try (left; left;  reflexivity);

  (* we clean all hypothesis and look for contradictions *)
  ( Thesis.toolbox.simpl_all_bool_in simpl__P_out_;
    Thesis.toolbox.simpl_all_bool_in CoreSight_configuration_model_out_CR;
    Thesis.toolbox.simpl_all_bool_in L_exit
  );
  try (case simpl__P_out_);
  try (case CoreSight_configuration_model_out_CR);
  try (case L_exit);

  (* in some cases, the goal looks like: False -> Prop *)
  try (intros H;  case H);
  try (intros H0; case H0).
Qed.

(******************************************************************************)

Lemma imp_deduced_PC:
  ∀ π : Path, π ⊨ G( ('PC_in_CR) --> ('deduced_PC_in_CR) ).
Proof.
  intros π.
  rewrite simpl_globally.
  intros s.
  simpl.

  induction s.

  (* at state 0, PC is not in CR *)
  - pose proof (reset_to_not_CR π) as H3.
    pose proof (startup_reset   π) as H4.
    rewrite simpl_globally in H3.
    simpl in H3, H4.
    pose proof (H3 0) as H3.
    destruct H3 as [H3 | H3].
    + rewrite Thesis.LTL.path_sub_j_0 in H3.
      rewrite H3 in H4.
      discriminate.
    + left.
      exact H3.

  (* induction on state index *)
  - pose proof (L_enter π) as L_enter.
    pose proof (L_in    π) as L_in.
    rewrite simpl_globally in L_enter, L_in.

    pose proof (L_enter s) as H1.
    pose proof (L_in    s) as H2.
    simpl in H1, H2.

    (* we rewrite all X(a) so that we have a at successor of state s *)
    rewrite Thesis.LTL.path_plus_i_j in H1, H2.
    assert (A0: s + 1 = S s).
    + rewrite Nat.add_comm.
      simpl.
      reflexivity.
    + rewrite A0 in H1, H2.

      (* then we observe all predicates from our hypothesis *)
      destruct H2 as [H2 | H2].
      * destruct H2 as [H2 | H2].
        -- destruct H1 as [H1 | H1].
          ++ destruct H1 as [H1 | H1].

            (* induction on state index helps us here: *)
            ** destruct IHs as [IHs | IHs].
              --- rewrite IHs in H1; discriminate.
              --- rewrite IHs in H2; discriminate.

            ** left; exact H1.
          ++ right; exact H1.
        -- left; exact H2.
      * right; exact H2.
Qed.

Lemma equiv_deduced_PC:
  ∀ π : Path, π ⊨ G( ('deduced_PC_in_CR) --> ('PC_in_CR) ).
Proof.
  intros π.
  rewrite simpl_globally.
  intros s.
  simpl.

  induction s.

  (* at state 0, deduced_PC is not in CR *)
  - pose proof (_P_start_ π) as H3.
    simpl in H3.
    left.
    exact H3.

  (* induction on state index *)
  - pose proof (L_exit π) as L_exit.
    pose proof (L_out  π) as L_out.
    rewrite simpl_globally in L_exit, L_out.

    pose proof (L_exit s) as H1.
    pose proof (L_out  s) as H2.
    simpl in H1, H2.

    (* we rewrite all X(a) so that we have a at successor of state s *)
    rewrite Thesis.LTL.path_plus_i_j in H1, H2.
    assert (A0: s + 1 = S s).
    + rewrite Nat.add_comm.
      simpl.
      reflexivity.
    + rewrite A0 in H1, H2.

      (* then we observe all predicates from our hypothesis *)
      destruct H2 as [H2 | H2].
      * destruct H2 as [H2 | H2].
        -- destruct H1 as [H1 | H1].
          ++ destruct H1 as [H1 | H1].

            (* induction on state index helps us here: *)
            ** destruct IHs as [IHs | IHs].
              --- rewrite IHs in H2; discriminate.
              --- rewrite IHs in H1; discriminate.

            ** right; exact H1.
          ++ left; exact H1.
        -- right; exact H2.
      * left; exact H2.
Qed.

(******************************************************************************)

