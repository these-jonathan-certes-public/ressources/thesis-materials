(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.proof_obligations.
Require Import Dependencies.transducer.

(******************************************************************************)
(* L_except_next *)
(******************************************************************************)

(**
 * Hypothesis for our model zero: merge both isync and branch.
 *)
Lemma merge_model_0_packet_ready:
  ∀ π : Path, π ⊨ G(
    (
        (('available_packet_eq_branch) ∧ ('packet_has_except))
    )
  -->
    ('packet_ready)
  ).
Proof.
  intros π.

  pose proof (model_0_branch_packet_ready π) as model_0_branch_packet_ready.
  rewrite Thesis.toolbox.simpl_globally in model_0_branch_packet_ready.
  apply Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (model_0_branch_packet_ready s) as model_0_branch_packet_ready.
  destruct model_0_branch_packet_ready as [H_branch_a | H_branch_b].
  - simpl in H_branch_a.
    left; simpl.
    left; exact H_branch_a.
  - right; exact H_branch_b.
Qed.

Lemma l_except_merge_step_1:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ ('irq)
    )
  -->
    (
      ('packet_ready) ∧ (
          (('available_packet_eq_branch) ∧ ('packet_has_except))
      )
    )
  ).
Proof.
  intros π.
  apply Thesis.toolbox.globally_impP_conj_commutes.
  apply Thesis.toolbox.globally_impP_x2_to_conj with
    (a := (('PC_in_CR) ∧ ('irq)))
    (b := (('available_packet_eq_branch) ∧ ('packet_has_except)))
    (c := ('packet_ready)).

  split.
  - apply CoreSight_configuration_model_when_except.
  - apply merge_model_0_packet_ready.
Qed.


Lemma l_except_merge_step_2:
  ∀ π : Path, π ⊨ G(
    (
     ('PC_in_CR) ∧ ('irq)
    )
  -->
    (
      ('packet_ready) ∧ (
          (('decompressed_packet_eq_branch) ∧ ('decompressed_has_except))
      )
    )
  ).
Proof.
  intros π.
  pose proof (l_except_merge_step_1 π) as H.

  assert (A0:
  π ⊨ G
    (
      ('PC_in_CR) ∧ ('irq)
    -->
      (
        (
          (' packet_ready) ∧ (
              ((' available_packet_eq_branch) ∧ (' packet_has_except))
          )
        ) ∧ (
          (
              (('decompressed_packet_eq_branch) ∧ ('decompressed_has_except))
          )
        )
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := ('PC_in_CR) ∧ ('irq) )
      (b := (
              (' packet_ready) ∧ (
                  ((' available_packet_eq_branch) ∧ (' packet_has_except))
              )
            ) )
      (c := (
                (('decompressed_packet_eq_branch) ∧ ('decompressed_has_except))
            ) ).
    split.
    + apply H.
    + apply decompresser_except_branch_1.

  - apply <- Thesis.toolbox.globally_and_assoc_P in A0.
    apply    Thesis.toolbox.globally_impP_conj_commutes  in A0.
    apply    Thesis.toolbox.globally_and_assoc_P in A0.
    apply    Thesis.toolbox.globally_impP_conj_commutes  in A0.
    apply    Thesis.toolbox.globally_impP_conj_to_single in A0.
    apply    Thesis.toolbox.globally_impP_conj_commutes  in A0.
    exact A0.
Qed.


Lemma l_except_merge_step_3:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ ('irq)
    )
  -->
    (
      ('packet_ready) ∧ (
          (('decompressed_packet_eq_branch) ∧ ('decoded_has_except))
      )
    )
  ).
Proof.
  intros π.
  pose proof (l_except_merge_step_2 π) as H.

  assert (A0:
  π ⊨ G
    (
      ('PC_in_CR) ∧ ('irq)
    -->
      (
          (('packet_ready) ∧ (
              (('decompressed_packet_eq_branch) ∧ ('decompressed_has_except))
          ))
        ∧ (
            (('decompressed_packet_eq_branch) ∧ ('decoded_has_except))
        )
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := ('PC_in_CR) ∧ ('irq) )
      (b := (('packet_ready) ∧ (
                (('decompressed_packet_eq_branch) ∧ ('decompressed_has_except))
            )) )
      (c := (
                (('decompressed_packet_eq_branch) ∧ ('decoded_has_except))
            ) ).
    split.
    + exact H.
    + apply decoder_except_branch_1.

  - rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    rewrite Thesis.toolbox.globally_and_assoc_P in A0.
    apply   Thesis.toolbox.globally_impP_conj_to_single in A0.
    rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    exact A0.
Qed.

(******************************************************************************)

Theorem imp_deduced_irq_in_CR:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ ('irq)
    )
  -->
    (
      ('deduced_irq_in_CR)
    )
  ).
Proof.
  intros π.
  pose proof (l_except_merge_step_3 π) as H.

  assert (A0:
  π ⊨ G
    (
      ('PC_in_CR) ∧ ('irq)
    -->
      (
        (('packet_ready) ∧ (
            (('decompressed_packet_eq_branch) ∧ ('decoded_has_except))
        ))
        ∧ ('deduced_irq_in_CR)
      )
    )
  ).
  - apply Thesis.toolbox.globally_impP_x2_to_conj with
      (a := ('PC_in_CR) ∧ ('irq) )
      (b := (('packet_ready) ∧ (
                (('decompressed_packet_eq_branch) ∧ ('decoded_has_except))
            )) )
      (c := ('deduced_irq_in_CR)).
    split.
    + exact H.

    (* here we must use the hypothesis that clk and nrst are always on *)
    + pose proof (_P_except_ π) as H1.
      rewrite Thesis.toolbox.simpl_globally in H1.
      rewrite Thesis.toolbox.simpl_globally.
      intros s.
      pose proof (H1 s) as H2.
      destruct H2 as [H2a | H2b].
      * left.
        simpl in H2a.
        simpl.
        destruct H2a as [H2a | H2].
        -- destruct H2a as [H2a | H2b].

          (* case of clk is off *)
          ++ pose proof (clk_is_on) as clk_is_on.
            simpl in clk_is_on.
            pose proof (clk_is_on π) as clk_is_on.
            destruct clk_is_on as [clk_is_on_H | clk_is_on_False].
            ** pose proof (clk_is_on_H s) as clk_is_on_H.
              rewrite clk_is_on_H in H2a.
              discriminate.
            ** destruct clk_is_on_False as (s_False & H_False).
              destruct H_False as (H_False & H_exists).
              apply LTL.state_top in H_False.
              case H_False.

          (* case of nrst is off *)
          ++ pose proof (nrst_is_on) as nrst_is_on.
            simpl in nrst_is_on.
            pose proof (nrst_is_on π) as nrst_is_on.
            destruct nrst_is_on as [nrst_is_on_H | nrst_is_on_False].
            ** pose proof (nrst_is_on_H s) as clk_is_on_H.
              rewrite nrst_is_on_H in H2b.
              discriminate.
            ** destruct nrst_is_on_False as (s_False & H_False).
              destruct H_False as (H_False & H_exists).
              apply LTL.state_top in H_False.
              case H_False.

        -- exact H2.
      * right; exact H2b.

  - rewrite Thesis.toolbox.globally_impP_conj_commutes in A0.
    apply   Thesis.toolbox.globally_impP_conj_to_single in A0.
    exact A0.
Qed.
