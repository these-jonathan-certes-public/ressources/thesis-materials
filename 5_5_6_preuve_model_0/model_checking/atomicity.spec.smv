--
-- Copyright (C) 2022 Jonathan Certes
-- jonathan.certes@online.fr
--
-- This program is free software: you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your option) any later
-- version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU General Public License along with
-- this program. If not, see <http://www.gnu.org/licenses/>.
--

INVARSPEC
  NAME "P_atomicity_except" :=
  (
    "clk" & "deduced_irq_in_CR"
  ->
    "reset"
  );

INVARSPEC
  NAME "P_atomicity_CRmax" :=
  (
    (
      "clk" & next("clk") &
      !"reset" & "deduced_PC_in_CR" & (next(!"deduced_PC_in_CR"))
    )
  ->
    (
      "deduced_PC_eq_CRmax" | (next("reset"))
    )
  );

INVARSPEC
  NAME "P_atomicity_CRmin" :=
  (
    (
      "clk" &
      !"reset" & !"deduced_PC_in_CR" & (next("deduced_PC_in_CR"))
    )
  ->
    (
      next("deduced_PC_eq_CRmin") | (next("reset"))
    )
  );

