(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.atomicity.
Require Import Dependencies.has_except.

(******************************************************************************)

Theorem LTL_5:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ ('irq)
    )
  -->
    (
      ('reset)
    )
  ).
Proof.
  intros π.
  pose proof (imp_deduced_irq_in_CR π) as imp_deduced_irq_in_CR.
  pose proof (_P_atomicity_except_  π) as _P_atomicity_except_.
  pose proof (clk_is_on             π) as clk_is_on.

  rewrite Thesis.toolbox.simpl_globally in imp_deduced_irq_in_CR.
  rewrite Thesis.toolbox.simpl_globally in _P_atomicity_except_.
  apply   Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (imp_deduced_irq_in_CR s) as imp_deduced_irq_in_CR.
  pose proof (_P_atomicity_except_  s) as _P_atomicity_except_.

  simpl in clk_is_on.
  destruct clk_is_on  as [clk_is_on  | H_false_clk].
  - simpl in _P_atomicity_except_, imp_deduced_irq_in_CR.
    simpl.
    pose proof (clk_is_on s) as clk_is_on.
    rewrite clk_is_on in _P_atomicity_except_.
    destruct _P_atomicity_except_ as [_P_atomicity_except_ | _P_atomicity_except_].
    + destruct _P_atomicity_except_ as [H_false | H1].
      * discriminate.
      * destruct imp_deduced_irq_in_CR as [H2 | H3].
        -- left; exact H2.
        -- rewrite H1 in H3; discriminate.
    + right; exact _P_atomicity_except_.

  - destruct H_false_clk as (s_False & H_False).
    destruct H_False as (H_False & H_exists).
    apply LTL.state_top in H_False.
    case H_False.
Qed.

(******************************************************************************)
