(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

Require Import Dependencies.axioms.
Require Import Dependencies.proof_obligations.
Require Import Dependencies.transducer.
Require Import Dependencies.pc_in_cr.
Require Import Dependencies.pc_eq_crmin.
Require Import Dependencies.pc_eq_crmax.
Require Import Dependencies.atomicity.
Require Import Dependencies.key_access_control.
Require Import Dependencies.exclusive_stack.
Require Import Dependencies.ltl_5.

(******************************************************************************)

Theorem LTL2:
  ∀ π : Path, π ⊨ G(
    (negP ('PC_in_CR)) ∧ ('R_en) ∧ ('D_addr_in_KR)
  -->
    ('reset)
  ).
Proof.
  intros π.
  pose proof (equiv_deduced_PC       π) as equiv_deduced_PC.
  pose proof (AXI_slave_read_KR      π) as AXI_slave_read_KR.
  pose proof (_P_key_access_control_ π) as _P_key_access_control_.

  rewrite Thesis.toolbox.simpl_globally in
    equiv_deduced_PC, AXI_slave_read_KR, _P_key_access_control_.
  rewrite Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (equiv_deduced_PC       s) as equiv_deduced_PC.
  pose proof (AXI_slave_read_KR      s) as AXI_slave_read_KR.
  pose proof (_P_key_access_control_ s) as _P_key_access_control_.

  simpl.
  simpl in equiv_deduced_PC, AXI_slave_read_KR, _P_key_access_control_.

  (* bazooka method, we try all possible cases for all predicates *)
  induction (path_sub (path_up π s) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) deduced_PC_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) R_en)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) D_addr_in_KR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) KR_axi_arvalid)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) reset));
  try (right;             reflexivity);
  try (left; right;       reflexivity);
  try (left; left; right; reflexivity);
  try (left; left; left;  reflexivity);

  (* we clean all hypothesis and look for contradictions *)
  ( Thesis.toolbox.simpl_all_bool_in equiv_deduced_PC;
    Thesis.toolbox.simpl_all_bool_in AXI_slave_read_KR;
    Thesis.toolbox.simpl_all_bool_in _P_key_access_control_
  );
  try (case equiv_deduced_PC);
  try (case AXI_slave_read_KR);
  try (case _P_key_access_control_).
Qed.

(******************************************************************************)

Theorem LTL3:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('reset)) ∧ ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    )
  -->
    (
      ('PC_eq_CRmax) ∨ (X('reset))
    )
  ).
Proof.
  intros π.
  pose proof (imp_deduced_PC            π) as imp_deduced_PC.
  pose proof (equiv_deduced_PC          π) as equiv_deduced_PC.
  pose proof (equiv_deduced_PC_eq_CRmax π) as equiv_deduced_PC_eq_CRmax.
  pose proof (_P_atomicity_CRmax_       π) as _P_atomicity_CRmax_.
  pose proof (clk_is_on                 π) as clk_is_on.

  rewrite Thesis.toolbox.simpl_globally in
    imp_deduced_PC, equiv_deduced_PC, equiv_deduced_PC_eq_CRmax,
    _P_atomicity_CRmax_.
  rewrite Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (_P_atomicity_CRmax_ s) as _P_atomicity_CRmax_.
  simpl in _P_atomicity_CRmax_.
  destruct _P_atomicity_CRmax_ as [_P_atomicity_CRmax_ | _P_atomicity_CRmax_].
  - destruct _P_atomicity_CRmax_ as [_P_atomicity_CRmax_ | _P_atomicity_CRmax_].
    + destruct _P_atomicity_CRmax_ as [_P_atomicity_CRmax_ | _P_atomicity_CRmax_].
      * destruct _P_atomicity_CRmax_ as [_P_atomicity_CRmax_ | _P_atomicity_CRmax_].

        (* case of clk is off *)
        -- simpl in clk_is_on.
          destruct clk_is_on  as [clk_is_on  | H_false_clk].
          ++ destruct _P_atomicity_CRmax_ as [_P_atomicity_CRmax_ | _P_atomicity_CRmax_].
            ** pose proof (clk_is_on s) as clk_is_on.
              rewrite clk_is_on in _P_atomicity_CRmax_.
              discriminate.
            ** pose proof (clk_is_on (s + 1)) as clk_is_on.
              rewrite <- Thesis.LTL.path_plus_i_j in clk_is_on.
              rewrite clk_is_on in _P_atomicity_CRmax_.
              discriminate.
          ++ destruct H_false_clk as (s_False & H_False).
            destruct H_False as (H_False & H_exists).
            apply LTL.state_top in H_False.
            case H_False.

        (* case of reset *)
        -- left; left; left; simpl; exact _P_atomicity_CRmax_.

      (* case of not deduced_PC_in_CR *)
      * pose proof (imp_deduced_PC s) as imp_deduced_PC.
        destruct imp_deduced_PC as [imp_deduced_PC | imp_deduced_PC].
        -- simpl in imp_deduced_PC.
          left; left; right; simpl.
          exact imp_deduced_PC.
        -- simpl in imp_deduced_PC.
          rewrite imp_deduced_PC in _P_atomicity_CRmax_.
          discriminate.

    (* case of X(deduced_PC_in_CR) *)
    + pose proof (equiv_deduced_PC (s + 1)) as equiv_deduced_PC.
      destruct equiv_deduced_PC as [equiv_deduced_PC | equiv_deduced_PC].
      * simpl in equiv_deduced_PC.
        rewrite <- Thesis.LTL.path_plus_i_j in equiv_deduced_PC.
        rewrite equiv_deduced_PC in _P_atomicity_CRmax_.
        discriminate.
      * rewrite <- Thesis.LTL.path_plus_i_j in equiv_deduced_PC.
        left; right; simpl.
        exact equiv_deduced_PC.

  - destruct _P_atomicity_CRmax_ as [_P_atomicity_CRmax_ | _P_atomicity_CRmax_].

    (* case of not deduced_PC_eq_CRmax *)
    + pose proof (equiv_deduced_PC_eq_CRmax s) as equiv_deduced_PC_eq_CRmax.
      destruct equiv_deduced_PC_eq_CRmax as [equiv_deduced_PC_eq_CRmax | equiv_deduced_PC_eq_CRmax].
      * simpl in equiv_deduced_PC_eq_CRmax.
        rewrite equiv_deduced_PC_eq_CRmax in _P_atomicity_CRmax_.
        discriminate.
      * right; left; exact equiv_deduced_PC_eq_CRmax.

    (* case of X(reset) *)
    + right; right; exact _P_atomicity_CRmax_.
Qed.

(******************************************************************************)

Theorem LTL4:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('reset)) ∧ (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
      (X('PC_eq_CRmin)) ∨ (X('reset))
    )
  ).
Proof.
  intros π.
  pose proof (imp_deduced_PC            π) as imp_deduced_PC.
  pose proof (equiv_deduced_PC          π) as equiv_deduced_PC.
  pose proof (equiv_deduced_PC_eq_CRmin π) as equiv_deduced_PC_eq_CRmin.
  pose proof (_P_atomicity_CRmin_       π) as _P_atomicity_CRmin_.
  pose proof (clk_is_on                 π) as clk_is_on.

  rewrite Thesis.toolbox.simpl_globally in
    imp_deduced_PC, equiv_deduced_PC, equiv_deduced_PC_eq_CRmin,
    _P_atomicity_CRmin_.
  rewrite Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (_P_atomicity_CRmin_ s) as _P_atomicity_CRmin_.
  simpl in _P_atomicity_CRmin_.
  destruct _P_atomicity_CRmin_ as [_P_atomicity_CRmin_ | _P_atomicity_CRmin_].
  - destruct _P_atomicity_CRmin_ as [_P_atomicity_CRmin_ | _P_atomicity_CRmin_].
    + destruct _P_atomicity_CRmin_ as [_P_atomicity_CRmin_ | _P_atomicity_CRmin_].
      * destruct _P_atomicity_CRmin_ as [_P_atomicity_CRmin_ | _P_atomicity_CRmin_].

        (* case of clk is off *)
        -- simpl in clk_is_on.
          destruct clk_is_on  as [clk_is_on  | H_false_clk].
          ++ pose proof (clk_is_on s) as clk_is_on.
            rewrite clk_is_on in _P_atomicity_CRmin_.
            discriminate.
          ++ destruct H_false_clk as (s_False & H_False).
            destruct H_False as (H_False & H_exists).
            apply LTL.state_top in H_False.
            case H_False.

        (* case of reset *)
        -- left; left; left; simpl; exact _P_atomicity_CRmin_.

      (* case of not deduced_PC_in_CR *)
      * pose proof (equiv_deduced_PC s) as equiv_deduced_PC.
        destruct equiv_deduced_PC as [equiv_deduced_PC | equiv_deduced_PC].
        -- simpl in equiv_deduced_PC.
          rewrite equiv_deduced_PC in _P_atomicity_CRmin_.
          discriminate.
        -- simpl in equiv_deduced_PC.
          left; left; right; simpl.
          exact equiv_deduced_PC.

    (* case of X(deduced_PC_in_CR) *)
    + pose proof (imp_deduced_PC (s + 1)) as imp_deduced_PC.
      destruct imp_deduced_PC as [imp_deduced_PC | imp_deduced_PC].
      * rewrite <- Thesis.LTL.path_plus_i_j in imp_deduced_PC.
        left; right; simpl.
        exact imp_deduced_PC.
      * simpl in imp_deduced_PC.
        rewrite <- Thesis.LTL.path_plus_i_j in imp_deduced_PC.
        rewrite imp_deduced_PC in _P_atomicity_CRmin_.
        discriminate.

  - destruct _P_atomicity_CRmin_ as [_P_atomicity_CRmin_ | _P_atomicity_CRmin_].

    (* case of not X(deduced_PC_eq_CRmin) *)
    + pose proof (equiv_deduced_PC_eq_CRmin (s + 1)) as equiv_deduced_PC_eq_CRmin.
      destruct equiv_deduced_PC_eq_CRmin as [equiv_deduced_PC_eq_CRmin | equiv_deduced_PC_eq_CRmin].

      * simpl in equiv_deduced_PC_eq_CRmin.
        rewrite <- Thesis.LTL.path_plus_i_j in equiv_deduced_PC_eq_CRmin.
        rewrite equiv_deduced_PC_eq_CRmin in _P_atomicity_CRmin_.
        discriminate.
      * right; left.
        simpl.
        rewrite <- Thesis.LTL.path_plus_i_j in equiv_deduced_PC_eq_CRmin.
        exact equiv_deduced_PC_eq_CRmin.

    (* case of X(reset) *)
    + right; right; exact _P_atomicity_CRmin_.
Qed.

(******************************************************************************)

Theorem LTL_5:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ ('irq)
    )
  -->
    (
      ('reset)
    )
  ).
Proof.
  apply Dependencies.ltl_5.LTL_5. (* already proven *)
Qed.

(******************************************************************************)

Theorem LTL_6:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (('R_en) ∨ ('W_en)) ∧ ('D_addr_in_XS)
    )
  -->
    (
      ('reset)
    )
  ).
Proof.
  intros π.
  pose proof (equiv_deduced_PC        π) as equiv_deduced_PC.
  pose proof (AXI_slave_read_write_XS π) as AXI_slave_read_write_XS.
  pose proof (_P_exclusive_stack_     π) as _P_exclusive_stack_.
  pose proof (clk_is_on               π) as clk_is_on.

  rewrite Thesis.toolbox.simpl_globally in equiv_deduced_PC,
    AXI_slave_read_write_XS, _P_exclusive_stack_.
  rewrite Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (_P_exclusive_stack_ s) as _P_exclusive_stack_.
  simpl in _P_exclusive_stack_.
  destruct _P_exclusive_stack_ as [_P_exclusive_stack_ | _P_exclusive_stack_].
  - destruct _P_exclusive_stack_ as [_P_exclusive_stack_ | _P_exclusive_stack_].
    + destruct _P_exclusive_stack_ as [_P_exclusive_stack_ | _P_exclusive_stack_].

      (* case of clk is off *)
      * simpl in clk_is_on.
        destruct clk_is_on  as [clk_is_on  | H_false_clk].
        -- pose proof (clk_is_on s) as clk_is_on.
          rewrite clk_is_on in _P_exclusive_stack_.
          discriminate.
        -- destruct H_false_clk as (s_False & H_False).
          destruct H_False as (H_False & H_exists).
          apply LTL.state_top in H_False.
          case H_False.

      (* case of not deduced_PC_in_CR *)
      * pose proof (equiv_deduced_PC s) as equiv_deduced_PC.
        destruct equiv_deduced_PC as [equiv_deduced_PC | equiv_deduced_PC].
        -- simpl in equiv_deduced_PC.
          rewrite equiv_deduced_PC in _P_exclusive_stack_.
          discriminate.
        -- simpl in equiv_deduced_PC.
          left; left; left; simpl.
          exact equiv_deduced_PC.

    (* no read or write in the exclusve stack *)
    + pose proof (AXI_slave_read_write_XS s) as  AXI_slave_read_write_XS.
      simpl in AXI_slave_read_write_XS.
      destruct AXI_slave_read_write_XS as [AXI_slave_read_write_XS | AXI_slave_read_write_XS].
      * left; simpl.
        destruct AXI_slave_read_write_XS as [AXI_slave_read_write_XS | AXI_slave_read_write_XS].
        -- left; right; exact AXI_slave_read_write_XS.
        -- right; exact AXI_slave_read_write_XS.
      * destruct _P_exclusive_stack_ as (_P_exclusive_stack_ & H1).
        destruct _P_exclusive_stack_ as (H2 & H3).
        destruct AXI_slave_read_write_XS as [AXI_slave_read_write_XS | AXI_slave_read_write_XS].
        -- destruct AXI_slave_read_write_XS as [AXI_slave_read_write_XS | AXI_slave_read_write_XS].
          ++ rewrite H2 in AXI_slave_read_write_XS; discriminate.
          ++ rewrite H3 in AXI_slave_read_write_XS; discriminate.
        -- rewrite H1 in AXI_slave_read_write_XS; discriminate.

    (* case of X(reset) *)
    - right; simpl; exact _P_exclusive_stack_.
Qed.

(******************************************************************************)

Theorem LTL_7:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ ('W_en) ∧ (negP ('D_addr_in_XS)) ∧ (negP ('D_addr_in_MR))
    )
  -->
    (
      ('reset)
    )
  ).
Proof.
  intros π.
  pose proof (MMU_write_restrict π) as MMU_write_restrict.

  rewrite Thesis.toolbox.simpl_globally in MMU_write_restrict.
  rewrite Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (MMU_write_restrict s) as MMU_write_restrict.
  simpl in MMU_write_restrict.
  simpl.
  destruct MMU_write_restrict as [MMU_write_restrict | MMU_write_restrict].
  - left; left; left; exact MMU_write_restrict.
  - destruct MMU_write_restrict as [MMU_write_restrict | MMU_write_restrict].
    + left; left; right; exact MMU_write_restrict.
    + left; right; exact MMU_write_restrict.
Qed.

(******************************************************************************)

Theorem LTL_8:
  ∀ π : Path, π ⊨ G(
    (
      ('DMA_en) ∧ ('D_addr_in_KR)
    )
  -->
    (
      ('reset)
    )
  ).
Proof.
  intros π.
  pose proof (DMA_KR_not_connected π) as DMA_KR_not_connected.

  rewrite Thesis.toolbox.simpl_globally.
  simpl.
  simpl in DMA_KR_not_connected.
  intros s.

  destruct DMA_KR_not_connected as [DMA_KR_not_connected | DMA_KR_not_connected].
  - pose proof (DMA_KR_not_connected s) as DMA_KR_not_connected.
    left; exact DMA_KR_not_connected.
  - destruct DMA_KR_not_connected as (s2 & DMA_KR_not_connected).
    destruct DMA_KR_not_connected as (H_False & H).
    apply LTL.state_top in H_False.
    case H_False.
Qed.

(******************************************************************************)

Theorem LTL_9:
  ∀ π : Path, π ⊨ G(
    (
      ('DMA_en) ∧ ('D_addr_in_XS)
    )
  -->
    (
      ('reset)
    )
  ).
Proof.
  intros π.
  pose proof (DMA_XS_not_connected π) as DMA_XS_not_connected.

  rewrite Thesis.toolbox.simpl_globally.
  simpl.
  simpl in DMA_XS_not_connected.
  intros s.

  destruct DMA_XS_not_connected as [DMA_XS_not_connected | DMA_XS_not_connected].
  - pose proof (DMA_XS_not_connected s) as DMA_XS_not_connected.
    left; exact DMA_XS_not_connected.
  - destruct DMA_XS_not_connected as (s2 & DMA_XS_not_connected).
    destruct DMA_XS_not_connected as (H_False & H).
    apply LTL.state_top in H_False.
    case H_False.
Qed.

(******************************************************************************)

Theorem LTL_10:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ ('DMA_en)
    )
  -->
    (
      ('reset)
    )
  ).
Proof.
  intros π.
  pose proof (DMA_disable π) as DMA_disable.

  rewrite Thesis.toolbox.simpl_globally in DMA_disable.
  rewrite Thesis.toolbox.simpl_globally.

  intros s.
  pose proof (DMA_disable s) as DMA_disable.
  simpl in DMA_disable.
  simpl.

  left.
  exact DMA_disable.
Qed.

(******************************************************************************)
