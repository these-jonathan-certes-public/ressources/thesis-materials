(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Arith.
Require Import Coq.Logic.Classical.
Require Import Psatz.

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

(******************************************************************************)
(******************************************************************************)

(**
 * Definition of all the predicates:
 *)

(* signals in the FPGA *)
Axiom clk nrst                      : CV0 Bit (eq O).

(* to model the inside of the microprocessor *)
Axiom PC_in_CR                      : CV0 Bit (eq O).
Axiom PC_eq_CRmin                   : CV0 Bit (eq O).
Axiom PC_eq_CRmax                   : CV0 Bit (eq O).
Axiom irq                           : CV0 Bit (eq O).
Axiom R_en                          : CV0 Bit (eq O).
Axiom W_en                          : CV0 Bit (eq O).
Axiom D_addr_in_KR                  : CV0 Bit (eq O).
Axiom D_addr_in_XS                  : CV0 Bit (eq O).
Axiom D_addr_in_MR                  : CV0 Bit (eq O).
Axiom DMA_en                        : CV0 Bit (eq O).
Axiom DMA_addr_in_KR                : CV0 Bit (eq O).
Axiom DMA_addr_in_XS                : CV0 Bit (eq O).
Axiom available_packet_eq_branch    : CV0 Bit (eq O).
Axiom available_packet_eq_isync     : CV0 Bit (eq O).
Axiom packet_address_in_CR          : CV0 Bit (eq O).
Axiom packet_address_eq_CRmin       : CV0 Bit (eq O).
Axiom packet_address_eq_CRmax       : CV0 Bit (eq O).
Axiom packet_has_except             : CV0 Bit (eq O).

(* signals from the decompresser *)
Axiom decompressed_packet_eq_branch : CV0 Bit (eq O).
Axiom decompressed_packet_eq_isync  : CV0 Bit (eq O).
Axiom decompressed_address_in_CR    : CV0 Bit (eq O).
Axiom decompressed_address_eq_CRmin : CV0 Bit (eq O).
Axiom decompressed_address_eq_CRmax : CV0 Bit (eq O).
Axiom decompressed_has_except       : CV0 Bit (eq O).
Axiom packet_ready                  : CV0 Bit (eq O).

(* signals from the decoder *)
Axiom decoded_address_in_CR         : CV0 Bit (eq O).
Axiom decoded_address_eq_CRmin      : CV0 Bit (eq O).
Axiom decoded_address_eq_CRmax      : CV0 Bit (eq O).
Axiom decoded_has_except            : CV0 Bit (eq O).

(* signals from the transducer *)
Axiom deduced_PC_in_CR              : CV0 Bit (eq O).
Axiom deduced_PC_eq_CRmin           : CV0 Bit (eq O).
Axiom deduced_PC_eq_CRmax           : CV0 Bit (eq O).
Axiom deduced_irq_in_CR             : CV0 Bit (eq O).

(* signals from the AXI slaves *)
Axiom KR_axi_arvalid                : CV0 Bit (eq O).
Axiom XS_axi_arvalid                : CV0 Bit (eq O).
Axiom XS_axi_awvalid                : CV0 Bit (eq O).
Axiom XS_axi_wvalid                 : CV0 Bit (eq O).

(* signals from the re-used automata *)
Axiom reset                         : CV0 Bit (eq O).

(******************************************************************************)

(**
 * Model of the environment:
 *)

(**
 * In case of a hard reset, the microprocessor moves PC to address 0, which is
 * not in CR.
 *)
Axiom reset_to_not_CR:
  ∀ π : Path, π ⊨ G( ('reset) --> (negP ('PC_in_CR)) ).

(**
 * In case of a hard reset, the microprocessor moves PC to address 0, which is
 * not at CRmin.
 *)
Axiom reset_to_not_CRmin:
  ∀ π : Path, π ⊨ G( ('reset) --> (negP ('PC_eq_CRmin)) ).

(**
 * In case of a hard reset, the microprocessor moves PC to address 0, which is
 * not at CRmax.
 *)
Axiom reset_to_not_CRmax:
  ∀ π : Path, π ⊨ G( ('reset) --> (negP ('PC_eq_CRmax)) ).

(**
 * At startup (i.e. at first state), it is like we have a hard reset.
 *)
Axiom startup_reset:
  ∀ π : Path, π ⊨ ('reset).

(**
 * The clock in the FPGA is always active.
 *)
Axiom clk_is_on:
  ∀ π : Path, π ⊨ G('clk).

(**
 * The manual reset button in the FPGA is never pressed (there is a not gate).
 *)
Axiom nrst_is_on:
  ∀ π : Path, π ⊨ G('nrst).

(******************************************************************************)

(**
 * There is no connexion between AXI slaves and other peripherals.
 *)
Axiom DMA_KR_not_connected:
  ∀ π : Path,
    π ⊨ G(negP (('DMA_en) ∧ ('D_addr_in_KR))).

Axiom DMA_XS_not_connected:
  ∀ π : Path,
    π ⊨ G(negP (('DMA_en) ∧ ('D_addr_in_XS))).

(******************************************************************************)

(**
 * Access to KR is through an AXI slave and cache memories are deactivated when
 * PC is in CR. As a consequence, every read access to KR produces an access on
 * the AXI bus.
 *)
Axiom AXI_slave_read_KR:
  ∀ π : Path, π ⊨ G(
    (('R_en) ∧ ('D_addr_in_KR))
  -->
    ('KR_axi_arvalid)
  ).

(**
 * Access to XS is through an AXI slave and cache memories are deactivated when
 * PC is in CR. As a consequence, every read access to XS produces an access on
 * the AXI bus.
 *)
Axiom AXI_slave_read_write_XS:
  ∀ π : Path, π ⊨ G(
    (
      (('R_en) ∨ ('W_en)) ∧ ('D_addr_in_XS)
    )
  -->
    (
      ('XS_axi_arvalid) ∨ ('XS_axi_awvalid) ∨ ('XS_axi_wvalid)
    )
  ).

(******************************************************************************)

(**
 * Hypothesis H6.
 * Before the execution of the attesting function, MMU restricts write accesses
 * to XS and MR.
 *)
Axiom MMU_write_restrict:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ ('W_en)
    )
  -->
    (
      ('D_addr_in_XS) ∨ ('D_addr_in_MR)
    )
  ).

(******************************************************************************)

(**
 * Hypothesis H6.
 * Before the execution of the attesting function, DMA accesses are disabled.
 *)
Axiom DMA_disable:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR)
    )
  -->
    (negP ('DMA_en))
  ).

(******************************************************************************)

(**
 * CoreSight is configured to trace the program flow in CR. Either an isync or a
 * branch packet is available when PC is about to enter CR and the destination
 * address is in CR.
 *)
Axiom CoreSight_configuration_model_enter_CR:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X('PC_in_CR))
    )
  -->
    (
        (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
      ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
    )
  ).

(**
 * Our program does not contain an indirect branch that leads within itself. So,
 * when in CR and with  no exception, there is no packet with the destination in
 * CR.
 *)
Axiom CoreSight_configuration_model_in_CR:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ (X ('PC_in_CR)) ∧ (negP ('irq))
    )
  -->
    (negP (
            (('available_packet_eq_isync)  ∧ (negP ('packet_address_in_CR)))
          ∨ (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
        )
    )
  ).

(**
 * To exit CR normally, our program ends with an indirect branch, so we have a
 * branch packet. If an exception is triggered and leads to outside CR, then we
 * also have a branch packet.
 * In other words, we cannot exit CR whitout having a branch packet.
 *)
Axiom CoreSight_configuration_model_exit_CR:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ (X(negP ('PC_in_CR)))
    )
  -->
    (
      (('available_packet_eq_branch) ∧ (negP ('packet_address_in_CR)))
    )
  ).

(**
 * CoreSight is configured to trace the program flow in CR. If PC does not enter
 * CR, then we have no packet with a destination address in CR.
 *)
Axiom CoreSight_configuration_model_out_CR:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_in_CR)) ∧ (X (negP ('PC_in_CR)))
    )
  -->
    (negP (
            (('available_packet_eq_isync)  ∧ ('packet_address_in_CR))
          ∨ (('available_packet_eq_branch) ∧ ('packet_address_in_CR))
        )
    )
  ).

(**
 * CoreSight is configured to trace the program flow in CR. CRmin is in CR, it
 * can only be reached with an indirect branch or when branching from outside
 * CR.
 *)
Axiom CoreSight_configuration_model_at_CRmin:
  ∀ π : Path, π ⊨ G(
    (
      (X('PC_eq_CRmin))
    )
  -->
    (
        (('available_packet_eq_isync)  ∧ ('packet_address_eq_CRmin))
      ∨ (('available_packet_eq_branch) ∧ ('packet_address_eq_CRmin))
    )
  ).

Axiom CoreSight_configuration_model_not_CRmin:
  ∀ π : Path, π ⊨ G(
    (
      (X(negP ('PC_eq_CRmin)))
    )
  -->
    (negP (
        (('available_packet_eq_isync)  ∧ ('packet_address_eq_CRmin))
      ∨ (('available_packet_eq_branch) ∧ ('packet_address_eq_CRmin))
    ))
  ).

(**
 * CoreSight is configured to trace the program flow in CR. CRmax is the last
 * instruction in CR, it is an indirect branch to outside CR.
 *)
Axiom CoreSight_configuration_model_at_CRmax:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_eq_CRmax)
    )
  -->
    (
        (('available_packet_eq_isync)  ∧ ('packet_address_eq_CRmax))
      ∨ (('available_packet_eq_branch) ∧ ('packet_address_eq_CRmax))
    )
  ).

Axiom CoreSight_configuration_model_not_CRmax:
  ∀ π : Path, π ⊨ G(
    (
      (negP ('PC_eq_CRmax))
    )
  -->
    (negP (
        (('available_packet_eq_isync)  ∧ ('packet_address_eq_CRmax))
      ∨ (('available_packet_eq_branch) ∧ ('packet_address_eq_CRmax))
    ))
  ).

(**
 * CoreSight is configured to trace the program flow in CR. An exception
 * generates a trace with a branch packet.
 *)
Axiom CoreSight_configuration_model_when_except:
  ∀ π : Path, π ⊨ G(
    (
      ('PC_in_CR) ∧ ('irq)
    )
  -->
    (
      ('available_packet_eq_branch) ∧ ('packet_has_except)
    )
  ).

(******************************************************************************)

(**
 * This is an hypothesis for our model zero: we abstract the delay between
 * having a branch packet available and having this same packet ready.
 *)
Axiom model_0_branch_packet_ready:
  ∀ π : Path, π ⊨ G(
    ('available_packet_eq_branch)
  -->
    ('packet_ready)
  ).

Axiom model_0_isync_packet_ready:
  ∀ π : Path, π ⊨ G(
    ('available_packet_eq_isync)
  -->
    ('packet_ready)
  ).

