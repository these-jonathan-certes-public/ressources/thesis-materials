(*
 * Copyright (C) 2022 Jonathan Certes
 * jonathan.certes@online.fr
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *)

Require Import Thesis.LTL.
Require Import Thesis.bit.
Require Import Thesis.bitvect.
Require Import Thesis.nat2bitvect.
Require Import Thesis.truefor.
Require Import Thesis.bitvectltl.
Require Import Thesis.psl.
Require Import Thesis.toolbox.

Import Thesis.bit.Bit.
Import Thesis.bitvect.BitVect.

(******************************************************************************)
(******************************************************************************)

Section ltl2.

Variable PC_in_CR   R_en   Daddr_in_KR   KR_axi_arvalid   : CV0 Bit (eq O).
Variable PC_in_CR_d R_en_d Daddr_in_KR_d KR_axi_arvalid_d : CV0 Bit (eq O).
Variable reset : CV0 Bit (eq O).

Axiom A0:
  ∀ π : Path, π ⊨ G(
    ('PC_in_CR_d)
  -->
    ('PC_in_CR)
  )
/\
  ∀ π : Path, π ⊨ G(
    (('R_en)   ∧ ('Daddr_in_KR))
  -->
    (('R_en_d) ∧ ('Daddr_in_KR_d))
  ).

Hypothesis P0:
  ∀ π : Path, π ⊨ G(
    (negP ('PC_in_CR_d)) ∧ ('R_en_d) ∧ ('Daddr_in_KR_d)
  -->
    ('reset)
  ).

Theorem LTL2:
  ∀ π : Path, π ⊨ G(
    (negP ('PC_in_CR)) ∧ ('R_en) ∧ ('Daddr_in_KR)
  -->
    ('reset)
  ).
Proof.
  intros π.
  pose proof (A0 π) as H1.
  destruct H1 as (H1 & H2).
  pose proof (H2 π) as H2.
  pose proof (P0 π) as P0.

  rewrite Thesis.toolbox.simpl_globally.
  rewrite Thesis.toolbox.simpl_globally in H1, H2, P0.

  intros s.
  pose proof (H1 s) as H1.
  pose proof (H2 s) as H2.
  pose proof (P0 s) as P0.

  simpl.
  simpl in H1, H2, P0.

  (* bazooka method, we try all possible cases for all predicates *)
  induction (path_sub (path_up π s) 0 (str0 Bit (eq O) PC_in_CR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) PC_in_CR_d)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) R_en)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) R_en_d)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) Daddr_in_KR)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) Daddr_in_KR_d)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) KR_axi_arvalid)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) KR_axi_arvalid_d)),
            (path_sub (path_up π s) 0 (str0 Bit (eq O) reset));
  try (right;             reflexivity);
  try (left; right;       reflexivity);
  try (left; left; right; reflexivity);
  try (left; left; left;  reflexivity);

  (* we clean all hypothesis and look for contradictions *)
  ( Thesis.toolbox.simpl_all_bool_in H1;
    Thesis.toolbox.simpl_all_bool_in H2;
    Thesis.toolbox.simpl_all_bool_in P0
  );
  try (case H1);
  try (case H2);
  try (case P0).
Qed.

End ltl2.
